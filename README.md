<!-- horrible... but effective apparently -->
Access to: <button name="button" onclick="doc/documentation.md">[DOCUMENTATION](doc/documentation.md)</button> .. <button name="button" onclick="https://sake.re">[LOGBOOKS](https://sake.re)</button>

# _Lazy lavender_

_Lazy Lavender_ is a simulation environment for "cultural knowledge evolution", i.e., experimenting with several agents evolving their own knowledge through interacting.


# Quick command line instructions

## Getting the soft

~~~
$ git clone https://gitlab.inria.fr/moex/lazylav.git
~~~

or

~~~
$ git clone git@gitlab.inria.fr:moex/lazylav.git
~~~

## Compiling

The git repo does not contain a compiled version. However, it contains all libraries needed to compile the code. This can be achieved by:

~~~
$ ant jar
~~~

or

~~~
$ ant compileall
~~~

(yes you need `ant`).

## Running

Can be achieved by a simple:

~~~
$ java -cp lib/lazylav/ll.jar fr.inria.exmo.lazylavender.engine.Monitor
~~~

For more information see the parameters and sample experiments.

## Logging

You usually have the message:

~~~
SLF4J: Failed to load class "org.slf4j.impl.StaticLoggerBinder".
SLF4J: Defaulting to no-operation (NOP) logger implementation
SLF4J: See http://www.slf4j.org/codes.html#StaticLoggerBinder for further details.
~~~

We advise to use `logback` as a logger (but you may have other preferences) and to run:

~~~
$ java -Dlog.level=DEBUG -cp lib/lazylav/ll.jar:lib/slf4j/logback-classic-1.2.10.jar:lib/slf4j/logback-core-1.2.10.jar:. fr.inria.exmo.lazylavender.engine.Monitor
~~~

It can be controlled by a `logback.xml` file, and parameters:

~~~
$ java -Dlog.level=DEBUG -cp lib/lazylav/ll.jar:lib/slf4j/logback-classic-1.2.10.jar:lib/slf4j/logback-core-1.2.10.jar:. fr.inria.exmo.lazylavender.engine.Monitor -DrevisionModality=add -DverboseExpeLoggers=1
~~~

---
<small>
[https://gitlab.inria.fr/moex/lazylav](https://gitlab.inria.fr/moex/lazylav)
</small>

<small>
© INRIA, 2014—2024
</small>
