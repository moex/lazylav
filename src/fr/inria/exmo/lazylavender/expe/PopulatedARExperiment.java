/*
 * Copyright (C) INRIA, 2019-2021, 2024
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 */

package fr.inria.exmo.lazylavender.expe;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.io.PrintWriter;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.File;
import java.net.URI;
import java.lang.Integer;
import java.util.BitSet;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.Random;
import java.util.Vector;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.semanticweb.owl.align.AlignmentException;

import org.semanticweb.owl.align.AlignmentRepairer;

import org.semanticweb.owlapi.model.IRI;

import fr.inria.exmo.lazylavender.agent.PopulationAlignmentAdjustingAgent;
import fr.inria.exmo.lazylavender.agent.LLAgent;
import fr.inria.exmo.lazylavender.env.LLEnvironment;
import fr.inria.exmo.lazylavender.pop.Population;
import fr.inria.exmo.lazylavender.env.NOOEnvironment;
import fr.inria.exmo.lazylavender.logger.ActionLogger;
import fr.inria.exmo.lazylavender.model.LLException;

public class PopulatedARExperiment implements LLExperiment { 
    final static Logger logger = LoggerFactory.getLogger( PopulatedARExperiment.class );
    
    public static FileWriter dataFile;
    public static PrintWriter pdata;

    //protected AlignmentAdjustingAgent[] agents;
    protected Population[] populations;
    
    /**
     * Number of agent populations to play (-DnbPopulations)
     */
    private int numberOfPopulations = 4;
    private int numberOfAgents = 4; // Should be the same as in populations

    /**
     * Number of games to be played (-DnbIterations)
     */
    private int numberOfIterations = 200;

    /**
     * Subdirectories for saving data
     */
    File refDir = null;
    File initDir = null;
    File finalDir = null;

    /**
     * Where (and if) to save games to
     */
    File saveGames = null;

    /**
     * Where (and if) to load games from
     */
    String loadGames = null;

    /*
    // At what interval to synchronise?
    */
    private int synchronisationRate = 0;

    /*
    // At what interval to change generation?
    */
    private int generationRate = 0;

    // Measure parameters
    private boolean reportInc = false;
    private boolean precRec = false;

    protected LLEnvironment env = null;

    public PopulatedARExperiment() {}

    protected ActionLogger alog = null;

    public ActionLogger init( Properties params ) throws LLException {
	if ( params.getProperty( "nbPopulations" ) != null ) {
	    numberOfPopulations = Integer.parseInt( params.getProperty( "nbPopulations" ) );
	} else { // Set the default for sub processes
	    params.setProperty( "nbPopulations", Integer.toString( numberOfPopulations ) );
	}
	// More should be done in the env
	params.setProperty( "nbOntologies", ""+numberOfPopulations );
	if ( params.getProperty( "nbAgents" ) != null ) {
	    numberOfAgents = Integer.parseInt( params.getProperty( "nbAgents" ) );
	}
	if ( params.getProperty( "nbIterations" ) != null ) {
	    numberOfIterations = Integer.parseInt( params.getProperty( "nbIterations" ) );
	}
	String rate = params.getProperty( "synchronisationRate" );
	if ( rate == null ) rate = params.getProperty( "synchronizationRate" );
	if ( rate != null ) {
	    synchronisationRate = Integer.parseInt( rate );
	}
	if ( params.getProperty( "replayGames" ) != null ) {
	    loadGames = params.getProperty( "loadRunDir" );
	}
	reportInc = ( params.getProperty( "reportIncoherence" ) != null );
	precRec = ( params.getProperty( "reportPrecRec" ) != null );
	// Create an environment corresponding to parameters
	env = new NOOEnvironment( true, true, false, false );
	// 2018: why not creating it before? where
	String runDir = params.getProperty( "runDir" );
	if ( runDir != null ) {
	    File runDirD = new File( runDir );
	    runDirD.mkdir();
	    if ( params.getProperty( "saveInit" ) != null ) {
		initDir = new File( runDir, "init" );
		params.setProperty( "initDir", initDir.toString() );
	    }
	    if ( params.getProperty( "saveFinal" ) != null ) {
		finalDir = new File( runDir, "final" );
		params.setProperty( "finalDir", finalDir.toString() );
	    }
	    if ( params.getProperty( "saveRef" ) != null ) { // Is there another one?
		refDir = new File( runDir, "reference" );
		params.setProperty( "refDir", refDir.toString() );
	    }
	    if ( params.getProperty( "saveGames" ) != null ) {
		saveGames = new File( runDir, "games.tsv" );
	    }
	}
	// Tell the environment to load the ontologies from the populations
	if ( params.getProperty( "loadAgents" ) != null || params.getProperty( "loadEnv" ) != null ) {
	    params.setProperty( "loadOnto", "population" ); // name of the directory containing ontologies
	}
	env.init( params );
	// Create reference network
	((NOOEnvironment)env).createReferenceNetwork( true );
	// Setup loggers
	boolean verboseLoggers = false;
	if ( params.getProperty( "verboseExpeLoggers" ) != null ) verboseLoggers = true;
	String[] measureNames;
	if ( reportInc ) {
	    String[] xx = { "srate", "size", "fm", "inc" };
	    measureNames = xx;
	} else if ( precRec ) {
	    String[] xx = { "srate", "size", "prec", "rec" };
	    measureNames = xx;
	} else {
	    String[] xx = { "srate", "size", "fm" };
	    measureNames = xx;
	}
	alog = new ActionLogger( verboseLoggers, measureNames );

	// Create populations
	populations = new Population[numberOfPopulations];
	Iterator<URI> ontologies = ((NOOEnvironment)env).getOntologies().iterator();
	while ( ontologies.hasNext() ) logger.debug( "     Ontology : {}", ontologies.next() );
	ontologies = ((NOOEnvironment)env).getOntologies().iterator();
	for ( int p = 0; p < numberOfPopulations; p++ ) {
	    populations[p] = new Population( ((NOOEnvironment)env), alog, p, ontologies.next() );
	    populations[p].init( params );
	}
	initStore(); // initialising measure cache
	return alog;
    }

    public void process() throws LLException {
	initSavePopulations();
	env.logReference();
	logInit();
	((NOOEnvironment)env).printNetwork( "INITIAL NETWORK" );
	Random agentFinder = new Random(); // could use setSeed( long seed );
	int remaining = numberOfIterations;
	int batch = 0;
	PrintWriter prGames = null;
	if ( saveGames != null ) {
	    try {
		prGames = new PrintWriter( saveGames );
	    } catch (FileNotFoundException fnfex) {
		logger.debug( "IGNORED: file not found", fnfex );
	    }
	}
	// load games
	// Could also use [what is more efficient?]
	// BufferedReader buf = new BufferedReader(new FileReader("/home/little/Downloads/test"));
	// spec = buf.readLine();
	Iterator<String> gameIt = null;
	if ( loadGames != null ) {
	    try {
		gameIt = Files.lines( Paths.get( loadGames, "games.tsv") ).iterator();
		logger.debug( "Found games.tsv in {}", loadGames );
	    } catch (IOException ioex) {
		logger.debug( "IGNORED: exception", ioex );
	    }
	}
	// Loop
	while( true ) {
	    if (remaining == numberOfIterations){ 
		double count = 0, prec = 0, rec = 0, fm = 0;
		Set<URI> setOfURI = ((NOOEnvironment)this.env).getOntologies();
		for ( Population p: populations ){
		    count += p.getSizeAlignments(); prec += p.getPrecision(); rec+= p.getRecall(); fm+=p.getFMeasure();
		}
		count=count/4; prec=prec/4; rec=rec/4; fm=fm/4; //to make the average - and not sum of all populations
		double[] val={count,0,0,prec,rec,fm};
	    }
	    // Synchronise
	    if ( synchronisationRate > 0 && remaining != numberOfIterations
		 && (numberOfIterations-remaining)%synchronisationRate == 0 ) {
	    	int Rank = (numberOfIterations-remaining); 
		logger.info( "Synchronising after {} games", numberOfIterations-remaining );
		for ( Population pop: populations ) {
		    try{
		    	pop.synchronizeAlignments();
		    } catch ( LLException llex ) {
			logger.error( "Error synchronizing population {} alignments {}", pop, llex );
		    }
		}
	    }
	    // New generation
	    if ( generationRate > 0 && remaining != numberOfIterations
		 && (numberOfIterations-remaining)%generationRate == 0 ) {
		logger.info( "New generation after {} games", numberOfIterations-remaining );
		for ( Population pop: populations ) {
		    pop.newGeneration();
		}
	    }
	    // we will do at least one round
	    if ( remaining <= 0 ) break;
	    logger.debug( "============================================= Remaining iterations : {}/{}", remaining, numberOfIterations );
	    remaining--;
	    // Get two agents
	    LLAgent firstAgent;
	    LLAgent secondAgent;
	    BitSet instance;
	    if ( gameIt != null && gameIt.hasNext() ) {
		logger.debug( "Replaying line "+(numberOfIterations-remaining) );
		// parse game
		String[] spec = gameIt.next().split( "\t" );
		int firstAgentId = Integer.parseInt( spec[0] );
		int secondAgentId = Integer.parseInt( spec[1] );
		instance = env.parseInstance( spec[2] );
		if ( instance == null )
		    logger.error( "Erroneous game file ({}/games.tsv) on line {}", loadGames, (numberOfIterations-remaining) );
		firstAgent = agentPopulation( firstAgentId ).getAgent( firstAgentId );
		secondAgent = agentPopulation( secondAgentId ).getAgent( secondAgentId );
		// JE2018: TO SEE... Or may be during parameter merge:
		// Cannot have more run
		// Can have more games but debug
		// Difficult to have more agents...
		//} else {
		//    logger.debug( "End of loaded games, new ones will be generated" );
		//    gameIt = null;
		//}
	    } else {
		// Agents are guaranteed to be from different populations
		int firstPopulationId = agentFinder.nextInt( numberOfPopulations );
		int secondPopulationId = firstPopulationId;
		while ( secondPopulationId == firstPopulationId ) secondPopulationId = agentFinder.nextInt( numberOfPopulations );
		firstAgent = populations[ firstPopulationId ].getRandomAgent();
		secondAgent = populations[ secondPopulationId ].getRandomAgent();
		// Get one instance
		instance = env.generateInstance();
	    }
	    // Cannot get them that easily...
	    InstanceIdByAlignmentGame game = new InstanceIdByAlignmentGame( firstAgent, secondAgent, instance );
	    alog.logGame( firstAgent.getId(), secondAgent.getId(), ""+instance );
	    if ( prGames != null ) game.save( prGames );
	    firstAgent.playGame( remaining, game );
	    // 'in principle' useless if success... but not really...
	    // Note: this is the second agent that has been updated
	    updateStore( ((PopulationAlignmentAdjustingAgent)secondAgent).getPopulation(), ((PopulationAlignmentAdjustingAgent)firstAgent).getOntologyURI() );
	    alog.logMeasures( getMeasures( reportInc ) );
	}
	if ( prGames != null ) prGames.close();
	logFinal();
	((NOOEnvironment)env).printNetwork( "FINAL NETWORK" );
	finalSavePopulations();
    }

    protected Population agentPopulation( int agentId ) {
	return populations[ agentId / numberOfAgents ];
    }
    
    /**
     * Creates the directory necessary for saving sessions
     */
    public void initSavePopulations() throws LLException {
	if ( initDir != null ) {
	    initDir.mkdir();
	    savePopulations( initDir );
	}
	if ( finalDir != null ) finalDir.mkdir();
    }

    public void finalSavePopulations() throws LLException {
	if ( finalDir != null ) {
	    //finalDir.mkdir();
	    savePopulations( finalDir );
	}
    }

    // Should be set in Populations...
    // And why isn't it done by agents by the way?
    public void savePopulations( File dir ) throws LLException {
	File populationdir = new File( dir, "population" );
	populationdir.mkdir();
	for ( int i=0; i < numberOfPopulations; i++ ) {
	    File popdir = new File( populationdir, ""+i );
	    popdir.mkdir();
	    populations[i].save( popdir );
	}
    }

    // The network loggers of NOOEnvironment are not accurate
    public void logInit() throws LLException {
	logNetworkStatistics( "Initial" );
    }

    public void logFinal() throws LLException {
	logNetworkStatistics( "Final" );
    }
    
    public void logNetworkStatistics( String name ) {
	double[] mvect = getMeasures( true );
	if ( !precRec ) {
	    logger.info( "{} Size = {}; F-measure = {}; incoherence = {}", name, mvect[1], mvect[2], mvect[3] );
	} else {
	    double glinc = 0;
	    for ( int p=0; p < numberOfPopulations; p++ ) glinc += populations[p].computeIncoherenceRate();
	    logger.info( "{} Size = {}; Precision = {}; Recall = {}; incoherence = {}", name, mvect[1], mvect[2], mvect[3], glinc/numberOfPopulations );
	}
    }
    
    // Not sure that all are needed
    protected double[] size;
    protected double[] inc;
    protected double[] fmeas;
    protected double[] prec;
    protected double[] rec;

    protected void initStore() {
	// Create values
	size = new double[numberOfPopulations];
	inc = new double[numberOfPopulations];
	fmeas = new double[numberOfPopulations];
	prec = new double[numberOfPopulations];
	rec = new double[numberOfPopulations];
	for ( int p=0; p < numberOfPopulations; p++ ) {
	    Population pop = populations[p];
	    size[p] = pop.getSizeAlignments();
	    inc[p] = pop.getIncoherenceRate();
	    fmeas[p] = pop.getFMeasure();
	    prec[p] = pop.getPrecision();
	    rec[p] = pop.getRecall();
	}
    }

    // To do for the correct dependency
    protected void updateStore( Population pop, URI u ) {
	pop.updateStore( u );
	int p = pop.getId();
	size[p] = pop.getSizeAlignments();
	inc[p] = pop.getIncoherenceRate();
	fmeas[p] = pop.getFMeasure();
	prec[p] = pop.getPrecision();
	rec[p] = pop.getRecall();
    }
    
    /**
     * We do not use the measures of the network of ontology
     */
    protected double[] getMeasures( final boolean procinc ) {
	// Ask the concerned population to recompute its values and store them
	// - retrieve alignments (maybe retrieve the reference... i do not know)
	// - divided by number of agent
	// Reaggregate
	double glsize = 0;
	double glinc = 0;
	double glprec = 0.;
	double glrec = 0.;
	for ( int p=0; p < numberOfPopulations; p++ ) {
	    glsize += size[p];
	    if ( procinc ) glinc += inc[p];
	    glprec += prec[p];
	    glrec += rec[p];
	}
	glprec = glprec / numberOfPopulations;
	glrec = glrec / numberOfPopulations;
	double glfmeas = (2*glprec*glrec)/(glprec+glrec);
	if ( precRec ) {
	    double[] mvect = { 0, glsize, glprec, glrec };
	    return mvect;
	} else if ( procinc ) {
	    glinc = glinc / numberOfPopulations;
	    double[] mvect = { 0, glsize, glfmeas, glinc };
	    return mvect;
	} else {
	    // compute Fmeasure
	    double[] mvect = { 0, glsize, glfmeas };
	    return mvect;
	}
    }

    // Not used?
    public void report() {
	alog.fullReport( new PrintWriter(System.out, true) );
    }
    
    class sPoint{
    	int itr;
    	double[] values;
    	
    	public sPoint(int a, double[] v){
    		this.itr = a;
    		this.values = v;  	}
    }

}
