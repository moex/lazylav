/*
 * Copyright (C) INRIA, 2019
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 */

package fr.inria.exmo.lazylavender.expe;

import java.util.BitSet;

import org.semanticweb.owlapi.model.OWLClass;

import org.semanticweb.owl.align.Alignment;
import org.semanticweb.owl.align.Cell;

import fr.inria.exmo.lazylavender.model.LLException;
import fr.inria.exmo.lazylavender.agent.LLAgent;

public class InstanceIdByAlignmentGame extends InstanceIdGame {

    Alignment alignment = null;
    Cell cell = null;
    OWLClass sourceClass = null;
    boolean finished = false;
    boolean status;
    
    public InstanceIdByAlignmentGame( final LLAgent firstAgent, final LLAgent secondAgent, final BitSet instance ) {
	super( firstAgent, secondAgent, instance);
    }
    
    public Alignment getAlignment() {
	return alignment;
    }
    
    public void setAlignment( Alignment al ) {
	alignment = al;
    }
    
    public Cell getCell() {
	return cell;
    }
    
    public void setCell( Cell c ) {
	cell = c;
    }
    
    public OWLClass getSourceClass() {
	return sourceClass;
    }
    
    public void setSourceClass( OWLClass cl ) {
	sourceClass = cl;
    }
    
    public boolean isFinished() {
	return finished;
    }

    public boolean isSuccess() {
	return status;
    }

    public void setSuccess() {
	status = true;
	finished = true;
    }

    public void setFailure() {
	status = false;
	finished = true;
    }
    
}
