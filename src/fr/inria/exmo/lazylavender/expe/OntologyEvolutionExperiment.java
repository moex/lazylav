/*
 * Copyright (C) INRIA, 2016-2019
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 */

package fr.inria.exmo.lazylavender.expe;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;
import java.util.Random;
import java.util.Iterator;
import java.io.File;
import java.io.PrintWriter;
import java.io.Serializable;
import java.io.FileNotFoundException;
import java.io.ObjectOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLEquivalentClassesAxiom;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.inria.exmo.lazylavender.agent.LLAgent;
import fr.inria.exmo.lazylavender.expe.LLGame;
import fr.inria.exmo.lazylavender.agent.OntologyEvolvingAgent;
import fr.inria.exmo.lazylavender.env.features.FeatureEnvironment;
import fr.inria.exmo.lazylavender.env.features.OWLOntologyHelper;
import fr.inria.exmo.lazylavender.logger.ActionLogger;
import fr.inria.exmo.lazylavender.model.LLException;

public class OntologyEvolutionExperiment implements LLExperiment {
    private final static Logger logger = LoggerFactory.getLogger(OntologyEvolutionExperiment.class);

    private FeatureEnvironment environment;

    // Honestly an array would do it...
    private List<OntologyEvolvingAgent> agents;

    private int numberOfAgents = 2;
    private int numberOfIterations = 100;

    /**
     * Where (and if) to save data to
     */
    File refDir = null;
    File initDir = null;
    File finalDir = null;
    File saveGames = null;

    /**
     * Where (and if) to load data from
     */
    String loadDir = null;
    boolean loadAgents = false;
    boolean loadEnv = false;
    String loadGames = null;

    private HashMap<ClassificationTableKey, List<ClassificationTableValue>> classificationTable = new HashMap<>();

    private ArrayList<LLGame> gameRounds = new ArrayList<LLGame>();

    private Random random;

    private ActionLogger actionLogger;

    @Override
    public ActionLogger init( Properties param ) throws LLException {
	boolean verboseLoggers = initParams(param);
	
	environment = new FeatureEnvironment();
	environment.init(param);
	
	agents = new ArrayList<>(numberOfAgents);

	if ( loadAgents && loadDir != null ) { // ex- "ontologyFilesInputLocation"
	    String ontologyFilesInputLocation = loadDir+"/init";
	    for (int i = 0; i < numberOfAgents; i++) {
		// LOAD2018: Is this used in NOO? (discrepancy as the owl is needed here...)
		OWLOntology agentOntology = OWLOntologyHelper.loadOntologyFromFile( loadDir+"/agent/"+i+"/onto.owl");
		OntologyEvolvingAgent ontologyEvolvingAgent = new OntologyEvolvingAgent( agentOntology, i );
		agents.add(i, ontologyEvolvingAgent);
	    }
	} else {
	    List<OWLOntology> agentOntologies = environment.createAgentConjunctionOntologies( numberOfAgents );
	    for ( int i=0; i < numberOfAgents; i++ ) {
		OntologyEvolvingAgent ontologyEvolvingAgent = new OntologyEvolvingAgent( agentOntologies.get(i), i );
		agents.add(i, ontologyEvolvingAgent);
	    }
	}
	if ( initDir != null ) { // Here is would make sense that agents can save themselves and read themselves
	    String initDirRoot = initDir.toString()+"/agent/";
	    logger.debug("Ontologies saved at location {}", initDir );
	    for ( OntologyEvolvingAgent agent : agents ) {
		OWLOntologyHelper.saveOntologyToFile( agent.getOntology(), initDirRoot+agent.getId()+"/", "onto" );
	    }
	}
	
	random = new Random();
	
	String[] measureNames = { "success_rate", "initial_categ", "total_categ", "learned_intersection", "adopted", "%-intersection", "%-adopted" };
	actionLogger = new ActionLogger( verboseLoggers, measureNames );
	return actionLogger;
    }

    private boolean initParams(Properties param) throws LLException {
	if (param.getProperty("nbAgents") != null) {
	    numberOfAgents = Integer.parseInt(param.getProperty("nbAgents"));
	}
	if (numberOfAgents < 2) {
	    throw new LLException("There must be at least two agents for the experiment!");
	}
	if (param.getProperty("nbIterations") != null) {
	    numberOfIterations = Integer.parseInt(param.getProperty("nbIterations"));
	}

	String runDir = param.getProperty( "runDir" );
	if ( runDir != null ) {
	    File runDirD = new File( runDir );
	    runDirD.mkdir();
	    if ( param.getProperty( "saveInit" ) != null ) {
		initDir = new File( runDir, "init" );
		param.setProperty( "initDir", initDir.toString() );
	    }
	    if ( param.getProperty( "saveFinal" ) != null ) {
		finalDir = new File( runDir, "final" );
		param.setProperty( "finalDir", finalDir.toString() );
	    }
	    if ( param.getProperty( "saveRef" ) != null ) { // Is there another one?
		refDir = new File( runDir, "reference" );
		param.setProperty( "refDir", refDir.toString() );
	    }
	    if ( param.getProperty( "saveGames" ) != null ) {
		saveGames = new File( runDir, "games.tsv" );
	    }
	}

	if ( param.getProperty( "loadRunDir" ) != null ) {
	    loadDir = param.getProperty( "loadRunDir" ) + "/init/";
	    if ( param.getProperty( "loadAgents" ) != null ) loadAgents = true;
	    if ( param.getProperty( "loadEnv" ) != null ) loadEnv = true;
	}
	if ( param.getProperty( "replayGames" ) != null ) {
	    loadGames = param.getProperty( "loadRunDir" );
	}

	// verbose loggers (JE: is that standard?)
	return (param.getProperty("verboseExpeLoggers") != null);
    }

    @Override
    public void process() throws LLException {
	// Could also use [what is more efficient?]
	// BufferedReader buf = new BufferedReader(new FileReader("/home/little/Downloads/test"));
	// spec = buf.readLine();
	Iterator<String> gameIt = null;
	if ( loadGames != null ) {
	    try {
		logger.debug( "Found games.tsv in {}", loadGames );
		gameIt = Files.lines( Paths.get( loadGames, "games.tsv") ).iterator();
	    } catch (IOException ioex) {
		logger.debug( "IGNORED: exception", ioex );
	    }
	}
	for (int i = 0; i < numberOfIterations; i++) {
	    logger.debug("================== Round " + i + " ====================");
	    int agent1;
	    int agent2;
	    HashMap<String, String> instance;
	    if ( gameIt != null && gameIt.hasNext() ) {
		// Parse game
		String[] spec = gameIt.next().split( "\t" );
		agent1 = Integer.parseInt( spec[0] );
		agent2 = Integer.parseInt( spec[1] );
		// Read spec now
		instance = new HashMap<String, String>();
		String input = spec[2];
		for ( String pair : input.substring(input.indexOf("{")+1,input.lastIndexOf("}")).split(",[ \t]*") ) {
		    String[] val = pair.split("[ \t]*=[ \t]*");
		    instance.put( val[0], val[1] );
		}
	    } else {
		agent1 = random.nextInt(numberOfAgents);
		agent2 = random.nextInt(numberOfAgents);
		while (agent2 == agent1) agent2 = random.nextInt(numberOfAgents);
		instance = environment.generateObject();
	    }
	    playRound( i, agent1, agent2, instance );
	    logger.trace("CLASSIFICATION TABLE:" + classificationTable);
	}
	// SAVE: all at the end, may not be the best
	if ( saveGames != null ) {
	    PrintWriter prGames = null;
	    try {
		prGames = new PrintWriter( saveGames );
		for ( LLGame gameRound : gameRounds ) {
		    gameRound.save( prGames );
		}
	    } catch ( FileNotFoundException fnfex ) {
		logger.debug( "IGNORED: file not found", fnfex );
	    } finally {
		if ( prGames != null ) prGames.close();
	    }
	}
	
	if ( finalDir != null ) { // Here is would make sense that agents can save themselves and read themselves
	    String finalDirRoot = finalDir.toString()+"/agent/";
	    logger.debug("Ontologies saved at location {}", finalDirRoot );
	    for ( OntologyEvolvingAgent agent : agents ) {
		OWLOntologyHelper.saveOntologyToFile( agent.getOntology(), finalDirRoot+agent.getId()+"/", "onto" );
	    }
	}
    }

    // JE2018: This should be to the agents to playgame
    private void playRound( final int i, final int agent1, final int agent2, final HashMap<String, String> object ) throws LLException {
	OntologyEvolvingAgent firstAgent = agents.get( agent1 );
	OntologyEvolvingAgent secondAgent = agents.get( agent2 );
	ObjectIdGame game = new ObjectIdGame( firstAgent, secondAgent, object );
	// Unfortunatelly, this does nothing... This should have been in the agent implementation
	actionLogger.logGame( agent1, agent2, object.toString());
	firstAgent.playGame( i, game );
	gameRounds.add( game );
	logger.debug("First_Agent_id: " + agent1 + " Second_Agent_id: " + agent2 + " Object: " + object );
	// Real stuff is below (This is the key here)
	OWLClass classInFirstAgent = firstAgent.classify(object);
	logger.debug("classification result First Agent: " + classInFirstAgent);
	OWLClass classInSecondAgent = secondAgent.classify(object);
	logger.debug("classification result Second Agent: " + classInSecondAgent);
	
	String firstAgentClassName = firstAgent.getEntityName(classInFirstAgent);
	String secondAgentClassName = secondAgent.getEntityName(classInSecondAgent);
	String secondAgentDifferentCategory = addClassificationEntry(agent1, agent2, firstAgentClassName, secondAgentClassName, object);

	boolean failureToDistinguish = false;
	if ( secondAgentDifferentCategory != null ) {
	    // Second agent has a finer grain classification, first agent cannot distinguish between some objects, while second agent can
	    logger.debug("AGENT " + agent1 + " ADOPTS THE FOLLOWING CATEGORIES FROM AGENT " + agent2 + " :" + secondAgentDifferentCategory + ", " + secondAgentClassName);
	    // firstAgent adopts the two categories from secondAgentinto his ontology

	    // secondAgent.determineRelationship(secondAgentClassName, secondAgentDifferentCategory);
	    
	    boolean newClass1 = addSecondAgentCategoryToFirstAgentOntology(firstAgent, secondAgent, secondAgentDifferentCategory);
	    boolean newClass2 = addSecondAgentCategoryToFirstAgentOntology(firstAgent, secondAgent, secondAgentClassName);
	    failureToDistinguish = newClass1 || newClass2;
	}
	actionLogger.logResult( failureToDistinguish?"FAILURE":"SUCCESS", !failureToDistinguish, null );
	actionLogger.logMeasures( getMeasures() );
    }

    private double[] getMeasures() throws LLException {
	double totalNbCategoriesCreatedByIntersection = 0;
	double procentageCategoriesCreatedByIntersection = 0;
	
	double totalNbCategoriesLearnedFromOtherAgents = 0;
	double procentageCategoriesLearnedFromOtherAgents = 0;
	
	double totalInitialNbCategories = 0;
	double totalNbCategories = 0;
	
	String agentLog="";
	for (OntologyEvolvingAgent ontologyEvolvingAgent : agents) {
	    int envid = ontologyEvolvingAgent.getId();
	    double initialNbCategories = ontologyEvolvingAgent.getInitNbCategories();
	    double nbCategoriesCreatedByIntersection = ontologyEvolvingAgent.getNbCategoriesCreatedByIntersection();
	    double nbCategoriesLearnedFromOtherAgents = ontologyEvolvingAgent.getNbCategoriesLearnedFromOtherAgents();
	    
	    double nbOfCategories = initialNbCategories + nbCategoriesCreatedByIntersection + nbCategoriesLearnedFromOtherAgents;
	    agentLog+=" Agent" + envid + "_categories: " + nbOfCategories + " [initial= " + initialNbCategories + " ,created_by_intersection= " + nbCategoriesCreatedByIntersection + " ,learned_from_other_agents= " + nbCategoriesLearnedFromOtherAgents + " ]";

	    totalNbCategories += nbOfCategories;
	    totalInitialNbCategories += initialNbCategories;
	    totalNbCategoriesCreatedByIntersection += nbCategoriesCreatedByIntersection;
	    totalNbCategoriesLearnedFromOtherAgents += nbCategoriesLearnedFromOtherAgents;
	    
	    procentageCategoriesCreatedByIntersection = totalNbCategoriesCreatedByIntersection / totalNbCategories;
	    procentageCategoriesLearnedFromOtherAgents = totalNbCategoriesLearnedFromOtherAgents / totalNbCategories;
	    
	}
	logger.trace("{}, Average_categories: {} [initial= {},created_by_intersection= {},learned_from_other_agents= {}]",
		     agentLog, totalNbCategories / numberOfAgents, totalInitialNbCategories / numberOfAgents,
		     totalNbCategoriesCreatedByIntersection / numberOfAgents, totalNbCategoriesLearnedFromOtherAgents / numberOfAgents );

	logger.debug("Environment total number of categories:" + totalNbCategories + "[ initial=" + totalInitialNbCategories + ", created by intersection=" + totalNbCategoriesCreatedByIntersection + ", learned from other agents=" + totalNbCategoriesLearnedFromOtherAgents + "]");
	double[] mvect = { 0, totalInitialNbCategories, totalNbCategories, totalNbCategoriesCreatedByIntersection, totalNbCategoriesLearnedFromOtherAgents, procentageCategoriesCreatedByIntersection, procentageCategoriesLearnedFromOtherAgents };
	
	return mvect;
    }

    // FIXME see if this is useful
    private double[] getMeasurementsForAllAgents() throws LLException {
	int totalNbCategoriesCreatedByIntersection = 0;
	int totalNbCategoriesLearnedFromOtherAgents = 0;
	int totalInitialNbCategories = 0;
	int totalNbCategories = 0;
	
	List<Double> vect = new ArrayList<>();
	vect.add(Double.valueOf(0));
	vect.add(Double.valueOf(0));
	vect.add(Double.valueOf(0));
	vect.add(Double.valueOf(0));
	
	for (OntologyEvolvingAgent ontologyEvolvingAgent : agents) {
	    int envid = ontologyEvolvingAgent.getId();
	    int initialNbCategories = ontologyEvolvingAgent.getInitNbCategories();
	    // TODO if all behaves well, it is faster to use addition
	    int nbOfCategories = ontologyEvolvingAgent.getNbOfCategories();
	    int nbCategoriesCreatedByIntersection = ontologyEvolvingAgent.getNbCategoriesCreatedByIntersection();
	    int nbCategoriesLearnedFromOtherAgents = ontologyEvolvingAgent.getNbCategoriesLearnedFromOtherAgents();
	    
	    vect.add(Double.valueOf(nbOfCategories));
	    vect.add(Double.valueOf(nbCategoriesCreatedByIntersection));
	    vect.add(Double.valueOf(nbCategoriesLearnedFromOtherAgents));
	    
	    logger.debug("Agent " + envid + " number of categories:" + nbOfCategories + "[ initial=" + initialNbCategories + ", created by intersection=" + nbCategoriesCreatedByIntersection + ", learned from other agents=" + nbCategoriesLearnedFromOtherAgents + "]");
	    if (nbOfCategories != initialNbCategories + nbCategoriesCreatedByIntersection + nbCategoriesLearnedFromOtherAgents) {
		logger.error("Invalid nb of categories measurements!");
	    }
	    totalNbCategories += nbOfCategories;
	    totalInitialNbCategories += initialNbCategories;
	    totalNbCategoriesCreatedByIntersection += nbCategoriesCreatedByIntersection;
	    totalNbCategoriesLearnedFromOtherAgents += nbCategoriesLearnedFromOtherAgents;
	}
	
	logger.debug("Environment total number of categories:" + totalNbCategories + "[ initial=" + totalInitialNbCategories + ", created by intersection=" + totalNbCategoriesCreatedByIntersection + ", learned from other agents=" + totalNbCategoriesLearnedFromOtherAgents + "]");
	// double[] mvect = { 0, totalNbCategories, totalNbCategoriesCreatedByIntersection, totalNbCategoriesLearnedFromOtherAgents };
	
	vect.set(1, Double.valueOf(totalNbCategories));
	vect.add(2, Double.valueOf(totalNbCategoriesCreatedByIntersection));
	vect.add(3, Double.valueOf(totalNbCategoriesLearnedFromOtherAgents));
	
	double[] array = new double[vect.size()];
	for (int i = 0; i < vect.size(); i++) {
	    array[i] = vect.get(i);
	}
	
	return array;
    }

    private boolean addSecondAgentCategoryToFirstAgentOntology(OntologyEvolvingAgent firstAgent, OntologyEvolvingAgent secondAgent, String secondAgentCategory) throws LLException {
	boolean didAdoptClass = false;
	if (!secondAgentCategory.equals(FeatureEnvironment.all_objects_in_environment)) {
	    
	    OWLOntology secondAgentOntology = secondAgent.getOntology();
	    OWLClass secondAgentCategoryToAdopt = FeatureEnvironment.getCategory(secondAgentOntology, secondAgent.getId(), secondAgentCategory);
	    OWLClassExpression categoryDefinition = FeatureEnvironment.getCategoryDefinition(secondAgentOntology, secondAgentCategoryToAdopt);
	    
	    // Check that an equivalent class does not already exist
	    if (!firstAgent.equivalentCategoryAlreadyExists(categoryDefinition)) {
		didAdoptClass = true;
		String categoryNameInSecondAgentOntology = secondAgent.getEntityName(secondAgentCategoryToAdopt);
		String adoptedCategoryNameInFirstAgentOntology = "(" + categoryNameInSecondAgentOntology + ")" + secondAgent.getId();
		
		OWLOntology firstAgentOntology = firstAgent.getOntology();
		OWLOntologyManager firstAgentOntologyManager = firstAgentOntology.getOWLOntologyManager();
		OWLDataFactory firstAgentDataFactory = firstAgentOntologyManager.getOWLDataFactory();
		
		// Adopt classes from second agent
		OWLClass adoptedCategory = firstAgentDataFactory.getOWLClass(firstAgent.getEntityIRI(adoptedCategoryNameInFirstAgentOntology));
		OWLEquivalentClassesAxiom owlEquivalentClassesAxiom = firstAgentDataFactory.getOWLEquivalentClassesAxiom(adoptedCategory, categoryDefinition);
		firstAgentOntologyManager.addAxiom(firstAgentOntology, owlEquivalentClassesAxiom);
		
		firstAgent.setNbCategoriesLearnedFromOtherAgents(firstAgent.getNbCategoriesLearnedFromOtherAgents() + 1);
	    }
	    // else {
	    //// System.out.println("Category already exists: " + secondAgentCategoryToAdopt);
	    // }
	}
	return didAdoptClass;
    }
    
    private String addClassificationEntry(int firstAgentIndex, int secondAgentIndex, String firstAgentClass, String secondAgentClass, HashMap<String, String> object) {
	ClassificationTableValue value = new ClassificationTableValue(secondAgentClass, object);
	ClassificationTableKey key = new ClassificationTableKey(firstAgentIndex, secondAgentIndex, firstAgentClass);
	
	String secondAgentDifferentClassification = secondAgentHasFinerClassification(key, value);

	List<ClassificationTableValue> list = classificationTable.get(key);
	if (list == null) {
	    list = new ArrayList<>();
	    list.add(value);
	    classificationTable.put(key, list);
	} else {
	    list.add(value);
	}
	
	return secondAgentDifferentClassification;
    }
    
    private String secondAgentHasFinerClassification(ClassificationTableKey key, ClassificationTableValue value) {
	List<ClassificationTableValue> list = classificationTable.get(key);
	if (list == null || list.isEmpty()) {
	    // the two agents never communicated, or they have never seen an object classified the same by the first agent
	    return null;
	}
	// Get the most recent entries
	for (int i = list.size() - 1; i >= 0; i--) {
	    ClassificationTableValue classificationTableValue = list.get(i);
	    
	    String secondAgentClassPastEntry = classificationTableValue.getSecondAgentClass();
	    
	    boolean classifiedDifferentlyBySecondAgent = !secondAgentClassPastEntry.equals(value.getSecondAgentClass());
	    
	    if (classifiedDifferentlyBySecondAgent) {
		return secondAgentClassPastEntry;
	    }
	}
	
	return null;
    }
    
    class ClassificationTableKey {
	private int firstAgentIndex;
	private int secondAgentIndex;
	private String firstAgentClass;
	
	public ClassificationTableKey(int firstAgentIndex, int secondAgentIndex, String firstAgentClass) {
	    this.firstAgentIndex = firstAgentIndex;
	    this.secondAgentIndex = secondAgentIndex;
	    this.firstAgentClass = firstAgentClass;
	}
	
	public String toString() {
	    return "firstAgent: " + firstAgentIndex + ", secondAgent: " + secondAgentIndex + ", firstAgentClass:" + firstAgentClass;
	}
	
	@Override
	public boolean equals(Object obj) {
	    if (obj == null) {
		return false;
	    }
	    if (obj == this) {
		return true;
	    }
	    if (!(obj instanceof ClassificationTableKey)) {
		return false;
	    }
	    
	    ClassificationTableKey classificationTableKey = (ClassificationTableKey) obj;
	    return firstAgentIndex == classificationTableKey.firstAgentIndex && secondAgentIndex == classificationTableKey.secondAgentIndex && firstAgentClass.equals(classificationTableKey.firstAgentClass);
	}
	
	@Override
	public int hashCode() {
	    final int prime = 31;
	    int result = 1;
	    result = prime * result + firstAgentIndex;
	    result = prime * result + secondAgentIndex;
	    result = prime * result + firstAgentClass.hashCode();
	    return result;
	}
    }

    class ClassificationTableValue {
	private String secondAgentClass;
	private HashMap<String, String> object;
	
	public ClassificationTableValue(String secondAgentClass, HashMap<String, String> object) {
	    this.setSecondAgentClass(secondAgentClass);
	    this.setObject(object);
	}
	
	public String toString() {
	    return "secondAgentClass: " + secondAgentClass + ", object: " + object;
	}
	
	public String getSecondAgentClass() {
	    return secondAgentClass;
	}
	
	public void setSecondAgentClass(String secondAgentClass) {
	    this.secondAgentClass = secondAgentClass;
	}
	
	public HashMap<String, String> getObject() {
	    return object;
	}
	
	public void setObject(HashMap<String, String> object) {
	    this.object = object;
	}
	
    }
    
}
