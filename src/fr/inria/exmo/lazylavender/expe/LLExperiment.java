/*
 *
 * Copyright (C) INRIA, 2016-2017
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 */

package fr.inria.exmo.lazylavender.expe;

import java.util.Properties;

import fr.inria.exmo.lazylavender.logger.ActionLogger;
import fr.inria.exmo.lazylavender.model.LLException;

/**
 * Interface for experiments on ontologies trough agent interaction
 * @author idragost
 *
 */
public interface LLExperiment {
	
	ActionLogger init( Properties param ) throws LLException;
	
	void process() throws LLException;

}
