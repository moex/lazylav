/*
 * Copyright (C) INRIA, 2014-2019, 2022, 2024
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 */

package fr.inria.exmo.lazylavender.expe;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.io.PrintWriter;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.io.File;
import java.net.URI;
import java.lang.Integer;
import java.util.BitSet;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.Random;
import java.util.Vector;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.semanticweb.owl.align.AlignmentRepairer;

import org.semanticweb.owlapi.model.IRI;

import br.uniriotec.ppgi.alignmentRevision.revision.RevisorInterface;

import fr.inria.exmo.lazylavender.agent.LLAgent;
import fr.inria.exmo.lazylavender.agent.AlignmentAdjustingAgent;
import fr.inria.exmo.lazylavender.env.LLEnvironment;
import fr.inria.exmo.lazylavender.env.NOOEnvironment;
import fr.inria.exmo.lazylavender.logger.ActionLogger;
import fr.inria.exmo.lazylavender.model.LLException;

// Would be nicer that these guys would be threads!
public class AlignmentRevisionExperiment implements LLExperiment { 
    final static Logger logger = LoggerFactory.getLogger( AlignmentRevisionExperiment.class );

    protected AlignmentAdjustingAgent[] agents;
    
    /**
     * Number of agents to play (-DnbAgents)
     */
    private int numberOfAgents = 3;

    /**
     * Number of games to be played (-DnbIterations)
     */
    private int numberOfIterations = 200;

    /**
     * Subdirectories for saving data
     */
    File refDir = null;
    File initDir = null;
    File finalDir = null;

    /**
     * Where (and if) to save games to
     */
    File saveGames = null;

    /**
     * Where (and if) to load games from
     */
    String loadGames = null;

    // Measure parameters
    private boolean reportInc = false;
    private boolean precRec = false;

    // At what interval to apply repair?
    private int repeatedRepair = 0;

    private RevisorInterface revisor = null;

    protected int theoryRevisionSteps = -1; // do we proceed to theory revision

    private boolean inplaceLogMap = false;
    private boolean inplaceAlcomo = false;
    private Class<? extends AlignmentRepairer> inplaceRepairer = null;

    // List of repairers
    // could be Class<? extends AlignmentRepairer>
    private List<Class<? extends AlignmentRepairer>> repairers = null;

    protected LLEnvironment env = null;

    public AlignmentRevisionExperiment() {}

    protected ActionLogger alog = null;

    public ActionLogger init( Properties param ) throws LLException {
		if ( param.getProperty( "nbAgents" ) != null ) {
			numberOfAgents = Integer.parseInt( param.getProperty( "nbAgents" ) );
		} else { // Set the default for sub processes
			param.setProperty( "nbAgents", Integer.toString( numberOfAgents ) );
		}
		if ( param.getProperty( "nbOntologies" ) == null ) { // Set the default for sub processes
			param.setProperty( "nbOntologies", Integer.toString( numberOfAgents ) );
		}
		if ( param.getProperty( "nbIterations" ) != null ) {
			numberOfIterations = Integer.parseInt( param.getProperty( "nbIterations" ) );
		}
		// the repairers that will be applied (at least applied at the begining)
		repairers = new Vector<Class<? extends AlignmentRepairer>>();
		if ( param.getProperty( "repairers" ) != null ) {
			for ( String rep : param.getProperty( "repairers" ).split(",") ) {
				if ( "logmap".equals( rep ) ) repairers.add( fr.inria.exmo.lazylavender.env.repair.LogMapRepairer.class );
				else if ( "alcomo".equals( rep ) ) repairers.add( fr.inria.exmo.lazylavender.env.repair.AlcomoRepairer.class );
				else {
					try {
						Class<? extends AlignmentRepairer> cl = Class.forName( rep ).asSubclass(AlignmentRepairer.class);
						repairers.add( cl );
					} catch (Exception ex) {
						logger.warn( "Cannot understand repairer = {} (IGNORED)", rep );
					}
				}
			}
		}
		if ( param.getProperty( "replayGames" ) != null ) {
			loadGames = param.getProperty( "loadRunDir" );
		}
		// when to repeat repairing
		if ( param.getProperty( "repeatRepairs" ) != null ) {
			repeatedRepair = Integer.parseInt( param.getProperty( "repeatRepairs" ) );
		}
		if ( param.getProperty( "tRevision" ) != null ) {
			logger.debug("Initialising theory revision");
			try {
				theoryRevisionSteps = Integer.parseInt( param.getProperty( "tRevision" ) );
			} catch (Exception ex) { theoryRevisionSteps = 100; };
		}
		// Repair classes used to regularly repair the network
		if ( param.getProperty( "rawRepair" ) != null ) {
			String rep = param.getProperty( "rawRepair" );
			if ( "logmap".equals( rep ) ) {
				inplaceLogMap = true;
				inplaceRepairer = fr.inria.exmo.lazylavender.env.repair.LogMapRepairer.class;
			} else if ( "alcomo".equals( rep ) ) {
				inplaceAlcomo = true;
				inplaceRepairer = fr.inria.exmo.lazylavender.env.repair.AlcomoRepairer.class;
			} else {
				try {
					inplaceRepairer = Class.forName( rep ).asSubclass(AlignmentRepairer.class);
				} catch (Exception ex) {
					logger.warn( "Cannot understand rawRepair = {} (IGNORED)", rep );
				}
			}
		}
		reportInc = ( param.getProperty( "reportIncoherence" ) != null );
		precRec = ( param.getProperty( "reportPrecRec" ) != null );
		// Create an environment corresponding to parameters
		// 2015: I must put ontology public to change agent access to their ontologies!
		// This should be changed back after generalisation
		env = new NOOEnvironment( true, true, false, false );
		// 2018: why not creating it before? where
		String runDir = param.getProperty( "runDir" );
		if ( runDir != null ) {
			File runDirD = new File( runDir );
			runDirD.mkdir();
			if ( param.getProperty( "saveInit" ) != null ) {
				initDir = new File( runDir, "init" );
				param.setProperty( "initDir", initDir.toString() );
			}
			if ( param.getProperty( "saveFinal" ) != null ) {
				finalDir = new File( runDir, "final" );
				param.setProperty( "finalDir", finalDir.toString() );
			}
			if ( param.getProperty( "saveRef" ) != null ) { // Is there another one?
				refDir = new File( runDir, "reference" );
				param.setProperty( "refDir", refDir.toString() );
			}
			if ( param.getProperty( "saveGames" ) != null ) {
				saveGames = new File( runDir, "games.tsv" );
			}
		}
		if ( param.getProperty( "loadEnv" ) != null || param.getProperty( "loadAgents" ) != null ) {
			param.setProperty( "loadOnto", "agent" ); // name of the directory containing ontologies
		}
		env.init( param );
		// Create reference and initial networks
		((NOOEnvironment)env).createReferenceNetwork( false );
		((NOOEnvironment)env).initCompleteDisjunctiveNetwork();
		if ( theoryRevisionSteps > -1 ) {
			((NOOEnvironment)env).initTheoryRevision ();
			revisor = ((NOOEnvironment)env).getRevisor();
		}

		// Setup loggers
		boolean verboseLoggers = false;
		if ( param.getProperty( "verboseExpeLoggers" ) != null ) verboseLoggers = true;
		// JE2017: This is in connection with the agents' measure()
		String[] measureNames;
		if ( reportInc ) {
			String[] xx = { "srate", "size", "fm", "inc" };
			measureNames = xx;
		} else if ( precRec ) {
			String[] xx = { "srate", "size", "prec", "rec" };
			measureNames = xx;
		} else {
			String[] xx = { "srate", "size", "fm" };
			measureNames = xx;
		}
		alog = new ActionLogger( verboseLoggers, measureNames );

		// Create agents
		agents = new AlignmentAdjustingAgent[numberOfAgents];
		Iterator<URI> ontologies = ((NOOEnvironment)env).getOntologies().iterator();
		for ( int ag = 0; ag < numberOfAgents; ag++ ) {
			agents[ag] = new AlignmentAdjustingAgent( ((NOOEnvironment)env), alog, ag, ontologies.next() );
			agents[ag].init( param );
		}
		// Initializing agent alignments
		for ( int ag = 0; ag < numberOfAgents; ag++ ) {
			agents[ag].precomputeOntologies();
		}
		return alog;
    }

    public void process() throws LLException {
		initSaveAgents();
		env.logReference();
		env.logInit();
		logicalNetworkRepair(); // if repair is necessary
		Random agentFinder = new Random(); // could use setSeed( long seed );
		int remaining = numberOfIterations;
		int batch = 0;
		PrintWriter prGames = null;
		if ( saveGames != null ) {
			try {
				prGames = new PrintWriter( saveGames );
			} catch (FileNotFoundException fnfex) {
				logger.debug( "IGNORED: file not found", fnfex );
			}
		}
		// load games
		// Could also use [what is more efficient?]
		// BufferedReader buf = new BufferedReader(new FileReader("/home/little/Downloads/test"));
		// spec = buf.readLine();
		Iterator<String> gameIt = null;
		if ( loadGames != null ) {
			try {
				gameIt = Files.lines( Paths.get( loadGames, "games.tsv") ).iterator();
				logger.debug( "Found games.tsv in {}", loadGames );
			} catch (IOException ioex) {
				logger.debug( "IGNORED: exception", ioex );
			}
		}
		// Loop
		while( true ) {
			// Is there anything to produce at that stage?
			if ( repeatedRepair > 0 && remaining != numberOfIterations
				 && (numberOfIterations-remaining)%repeatedRepair == 0 ) {
				logger.info( "Repeated repair after {} games", numberOfIterations-remaining );
				((NOOEnvironment)env).logNetworkStatistics( "Current", ((NOOEnvironment)env).getCurrentNetwork() );
				logicalNetworkRepair();
			}
			// In principle here print the batch
			if ( ( theoryRevisionSteps != -1 ) && ( remaining != numberOfIterations )
				 && ( remaining%theoryRevisionSteps == 0 ) ) {
				batch++;
				logger.debug( "*** Batch #{} of {} examples", batch, theoryRevisionSteps );
				try {
					((NOOEnvironment)env).applyModifications( revisor.reviseTheory() );
					if (remaining == 0) revisor.reset();
				} catch (Exception ex) {
					throw new LLException( "Theory revision failed", ex );
				}
			}
			// we will do at least one round
			if ( remaining <= 0 ) break;
			logger.debug( "============================================= Remaining iterations : {}/{}", remaining, numberOfIterations );
			remaining--;
			// Get two agents
			LLAgent firstAgent;
			LLAgent secondAgent;
			BitSet instance;
			if ( gameIt != null && gameIt.hasNext() ) {
				logger.debug( "Replaying line "+(numberOfIterations-remaining) );
				// parse game
				String[] spec = gameIt.next().split( "\t" );
				firstAgent = agents[ Integer.parseInt( spec[0] ) ];
				secondAgent = agents[ Integer.parseInt( spec[1] ) ];
				instance = env.parseInstance( spec[2] );
				if ( instance == null )
					logger.error( "Erroneous game file ({}/games.tsv) on line {}", loadGames, (numberOfIterations-remaining) );
				// JE2018: TO SEE... Or may be during parameter merge:
				// Cannot have more run
				// Can have more games but debug
				// Difficult to have more agents...
				//} else {
				//    logger.debug( "End of loaded games, new ones will be generated" );
				//    gameIt = null;
				//}
			} else {
				int firstAgentId = agentFinder.nextInt( numberOfAgents );
				int secondAgentId = firstAgentId;
				while ( firstAgentId == secondAgentId ) secondAgentId = agentFinder.nextInt( numberOfAgents );
				firstAgent = agents[ firstAgentId ];
				secondAgent = agents[ secondAgentId ];
				// Get one instance
				instance = env.generateInstance();
			}
			InstanceIdGame game = new InstanceIdByAlignmentGame( firstAgent, secondAgent, instance );
			// For self-motivated agents, they may choose the object
			if ( gameIt == null ) game = ((AlignmentAdjustingAgent)firstAgent).chooseGame( game );
			alog.logGame( firstAgent.getId(), secondAgent.getId(), ""+((InstanceIdByAlignmentGame)game).getInstance() );
			if ( prGames != null ) game.save( prGames );
			firstAgent.playGame( remaining, game );
			alog.logMeasures( getMeasures() );
		}
		if ( prGames != null ) prGames.close();
		env.logFinal();
		finalSaveAgents();
    }

    /**
     * Creates the directory necessary for saving sessions
     */
    public void initSaveAgents() throws LLException {
		if ( initDir != null ) {
			initDir.mkdir();
			saveAgents( initDir );
		}
		if ( finalDir != null ) finalDir.mkdir();
    }

    public void finalSaveAgents() throws LLException {
		if ( finalDir != null ) {
			//finalDir.mkdir();
			saveAgents( finalDir );
		}
    }

    public void saveAgents( File dir ) throws LLException {
		File agentsdir = new File( dir, "agent" );
		agentsdir.mkdir();
		for ( int ag = 0; ag < numberOfAgents; ag++ ) {
			File agdir = new File( agentsdir, ""+ag );
			agdir.mkdir();
			agents[ag].save( agdir );
		}
    }
    
    public void logicalNetworkRepair() {
		for ( Class<? extends AlignmentRepairer> rep : repairers ) {
			((NOOEnvironment)env).repairNetworkAndReport( rep, (rep==inplaceRepairer) );
		}
    }

    protected double[] getMeasures() {
		if ( reportInc ) {
			double[] mvect = { 0, (double)((NOOEnvironment)env).currentSizeAlign(), ((NOOEnvironment)env).currentFMeasure(), ((NOOEnvironment)env).currentIncoherence() };
			return mvect;
		} else if ( precRec ) {
			double[] pr = ((NOOEnvironment)env).currentPrecRec();
			double precision = pr[0];
			double recall = pr[1];
			double[] mvect = { 0, (double)((NOOEnvironment)env).currentSizeAlign(), precision, recall };
			return mvect;
		} else {
			double[] mvect = { 0, (double)((NOOEnvironment)env).currentSizeAlign(), ((NOOEnvironment)env).currentFMeasure() };
			return mvect;
		}
    }

    // Not used?
    public void report() {
		alog.fullReport( new PrintWriter(System.out, true) );
    }

}
