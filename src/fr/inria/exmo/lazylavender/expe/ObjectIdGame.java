/*
 * Copyright (C) INRIA, 2019
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 */

package fr.inria.exmo.lazylavender.expe;

import java.util.BitSet;
import java.util.HashMap;
import java.lang.String;
import java.io.PrintWriter;

import fr.inria.exmo.lazylavender.model.LLException;
import fr.inria.exmo.lazylavender.agent.LLAgent;

public class ObjectIdGame implements LLGame {

    LLAgent agent1 = null;
    LLAgent agent2 = null;
    HashMap<String, String> indiv = null;
    
    public ObjectIdGame( final LLAgent firstAgent, final LLAgent secondAgent, final HashMap<String, String> instance ) {
	agent1 = firstAgent;
	agent2 = secondAgent;
	indiv = instance;
    }
    
    public LLAgent getFirstAgent() {
	return agent1;
    }
    
    public LLAgent getSecondAgent() {
	return agent2;
    }
    
    public HashMap<String, String> getInstance() {
	return indiv;
    }

    @Override
    public String toString() {
	return agent1.getId() + "\t" + agent2.getId() + "\t" + indiv;
    }
    
    public void save( PrintWriter file ) throws LLException {
	//file.println( this );
	file.printf( "%d\t%d\t%s\n", agent1.getId(), agent2.getId(), indiv );
    }
}
