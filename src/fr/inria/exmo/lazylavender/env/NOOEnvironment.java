/*
 * Copyright (C) INRIA, 2014-2019, 2021
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 */

package fr.inria.exmo.lazylavender.env;

import java.io.File;
import java.io.OutputStreamWriter;
import java.io.BufferedWriter;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Properties;
import java.util.Random;
import java.util.List;
import java.util.Set;
import java.util.BitSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import org.semanticweb.owl.align.Alignment;
import org.semanticweb.owl.align.AlignmentException;
import org.semanticweb.owl.align.AlignmentRepairer;
import org.semanticweb.owl.align.Cell;
import org.semanticweb.owl.align.Relation;
import org.semanticweb.owl.align.OntologyNetwork;
import org.semanticweb.owl.align.Evaluator;

import fr.inrialpes.exmo.align.parser.AlignmentParser;
import fr.inrialpes.exmo.align.impl.ObjectAlignment;
import fr.inrialpes.exmo.align.impl.URIAlignment;

import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.AddAxiom;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException; //??
import org.semanticweb.owlapi.model.OWLOntologyManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.unima.alcomox.mapping.Mapping;
import de.unima.alcomox.metric.IncoherenceMetric;
import de.unima.alcomox.ontology.IOntology;
// Alcomo for incoherence test
import de.unima.alcomox.util.AlcomoLogger;

import fr.inria.exmo.lazylavender.env.repair.AlcomoRepairer;
import fr.inria.exmo.lazylavender.model.LLException;

import br.uniriotec.ppgi.alignmentRevision.revision.Revisor;
import br.uniriotec.ppgi.alignmentRevision.revision.RevisorInterface;
import br.uniriotec.ppgi.alignmentRevision.revision.modification.Modification;

import fr.inrialpes.exmo.align.impl.BasicOntologyNetwork;
import fr.inrialpes.exmo.align.impl.ObjectAlignment;
import fr.inrialpes.exmo.align.impl.Namespace;
import fr.inrialpes.exmo.align.impl.Annotations;
import fr.inrialpes.exmo.align.impl.rel.EquivRelation;
import fr.inrialpes.exmo.align.impl.rel.SubsumeRelation;
import fr.inrialpes.exmo.align.impl.rel.SubsumedRelation;
import fr.inrialpes.exmo.align.impl.eval.SemPRecEvaluator;
import fr.inrialpes.exmo.align.impl.eval.PRecEvaluator;
import fr.inrialpes.exmo.align.impl.renderer.RDFRendererVisitor;
import fr.inrialpes.exmo.ontowrap.HeavyLoadedOntology;
import fr.inrialpes.exmo.ontowrap.OntologyFactory;
import fr.inrialpes.exmo.ontowrap.OntowrapException;

/**
 * This class defines an environment made of a network of ontologies.
 * A set of ontologies and a set of alignments between them.
 * Ontologies, alignments, entities, links... may or not be public.
 *
 */

public class NOOEnvironment extends BasicOntologyNetwork implements LLEnvironment {
    final static Logger logger = LoggerFactory.getLogger( NOOEnvironment.class );

    private boolean publicOntologies = false;
    private boolean publicAlignments = false;
    private boolean publicDatasets = false;
    private boolean publicLinks = false;
    private static String uriprefix = "http://lazylav.exmo.inria.fr/";
    private static String ontoprefix = uriprefix+"ontology/";
    private static int ontoPrefLen = ontoprefix.length();
    private Random rand = null; 
    private boolean precRec = false;

    public boolean ontologyPermutation = true;
    // Ontologies indexed by their URIs
    protected Hashtable<URI,HeavyLoadedOntology<OWLOntology>> ontologyTable;
    // Ontology signature
    protected Hashtable<URI,Object> ontologySignature;

    private OWLOntologyManager manager = null;
    private OWLDataFactory factory = null;

    public int numberOfOntologies = 0;

    /**
     * Number of features in the environment
     */
    private int numberOfFeatures = 4;

    public int numberOfCorrespondences = 0;

    public int disjointRatio = 100;

    protected boolean theoryRevision = false; // do we proceed to theory revision
    protected RevisorInterface revisor = null;
    
    protected boolean startempty = false;

    public boolean outputNetworks = false;
    public boolean syntacticEvaluation = false;
    public boolean realistic = false;

    /**
     * Where (and if) to save data to
     */
    File refDir = null;
    File initDir = null;
    File finalDir = null;

    /**
     * Where (and if) to load data from
     */
    String loadDir = null;
    String loadOnto = null;
    boolean loadEnv = false;

    public NOOEnvironment() {
		super();
		ontologyTable = new Hashtable<URI,HeavyLoadedOntology<OWLOntology>>();
		ontologySignature = new Hashtable<URI,Object>();
    }

    public NOOEnvironment( boolean rightOnto, boolean rightAlign, boolean rightData, boolean rightLinks ) {
		this();
		publicOntologies = rightOnto;
		publicAlignments = rightAlign;
		publicDatasets = rightData;
		publicLinks = rightLinks;
    }

    public NOOEnvironment init() throws LLException { // Default
		return init( new Properties() );
    }

    public NOOEnvironment init( Properties param ) throws LLException { // to be particularised by the type of alignments
		rand = new Random(); // could use setSeed( long seed );

		if ( param.getProperty( "outputNetworks" ) != null ) outputNetworks = true;
		// SAVE: new stuff
		if ( param.getProperty( "refDir" ) != null ) {
			refDir = new File( param.getProperty( "refDir" ) );
			refDir.mkdir();
			refDir = new File( refDir, "NOOEnvironment" );
		}
		if ( param.getProperty( "initDir" ) != null ) {
			initDir = new File( param.getProperty( "initDir" ), "NOOEnvironment" );
		}
		if ( param.getProperty( "finalDir" ) != null ) {
			finalDir = new File( param.getProperty( "finalDir" ), "NOOEnvironment" );
		}
		// LOAD
		if ( param.getProperty( "loadRunDir" ) != null ) {
			loadDir = param.getProperty( "loadRunDir" ) + "/init/";
			loadOnto = param.getProperty( "loadOnto" );
			if ( param.getProperty( "loadEnv" ) != null ) loadEnv = true;
		}
	
		manager = OWLManager.createOWLOntologyManager();
		factory = manager.getOWLDataFactory();

		if ( param.getProperty( "nbOntologies" ) != null && !param.getProperty( "nbOntologies" ).equals("") ) {
			numberOfOntologies = Integer.parseInt( param.getProperty( "nbOntologies" ) );
		} else {
			logger.error( "Cannot parse int number of ontologies. Continuing with defaults." ); 
		}
		if ( param.getProperty( "nbFeatures" ) != null && !param.getProperty( "nbFeatures" ).equals("") ) {
			numberOfFeatures = Integer.parseInt( param.getProperty( "nbFeatures" ) );
		} else {
			numberOfFeatures = numberOfOntologies;
		}
		if ( param.getProperty( "nbCorresp" ) != null && !param.getProperty( "nbCorresp" ).equals("") ) {
			numberOfCorrespondences = Integer.parseInt( param.getProperty( "nbCorresp" ) );
		} else {
			// JE: Beware of those power of 2, which can quickly go to MAXINT
			// (int MULT will even give 0)
			// int k = 1;
			// for ( int zz = n; zz > 1; zz-- ) k *= 2;
			// This is faster on large numbers
			numberOfCorrespondences = (int)java.lang.Math.pow( 2, numberOfOntologies-1 );
		}
		if ( param.getProperty( "disjointRatio" ) != null && !param.getProperty( "disjointRatio" ).equals("") ) {
			disjointRatio = Integer.parseInt( param.getProperty( "disjointRatio" ) );
		}
		if ( param.getProperty( "noo" ) != null && param.getProperty( "noo" ).equals( "random" ) ) ontologyPermutation = false;
		precRec = ( param.getProperty( "reportPrecRec" ) != null );
		syntacticEvaluation = ( param.getProperty( "syntactic" ) != null );
		realistic = ( param.getProperty( "realistic" ) != null );
		if ( param.getProperty( "tRevision" ) != null ) {
			try {
				theoryRevision = true;
				revisor = new Revisor();
				revisor.init( param );
			} catch (Exception ex) {
				throw new LLException( "Cannot create theory revisor", ex );
			}
		}
		// Unfortunate naming
		if ( param.getProperty( "startempty" ) != null || param.getProperty( "startEmpty" ) != null ) startempty = true;
		// Set up the skeleton of network of ontologies (only ontologies)
		initCoreNetwork();
		return this;
    }

    // TREV
    public RevisorInterface getRevisor() {
		return revisor;
    }

    public void applyModifications( List<Modification> modifs ) throws LLException {
		for ( Modification mod : modifs ) {
			applyModification( mod );
		}
		// and report:
		logNetworkStatistics( revisor.getClass().getSimpleName(), getCurrentNetwork() );
    }

    /**
     * This only deals with addition/deletion of subsumption correspondences
     */
    public void applyModification( Modification mod ) throws LLException {
		try {
			// Decode prolog names
			String c1 = mod.source;
			String c2 = mod.target;
			int c1sharp = c1.indexOf( 'c' );
			int c2sharp = c2.indexOf( 'c' );
			// Find the ontologies
			URI o1uri = new URI( ontoprefix+c1.substring( 1, c1sharp ) );
			URI o2uri = new URI( ontoprefix+c2.substring( 1, c2sharp ) );
			HeavyLoadedOntology<OWLOntology> o1 = ontologyTable.get( IRI.create( o1uri ) );
			HeavyLoadedOntology<OWLOntology> o2 = ontologyTable.get( IRI.create( o2uri ) );
			// Find the classes
			OWLClass class1 = factory.getOWLClass(IRI.create( o1uri+"#"+c1.substring( c1sharp+1 ) ));
			OWLClass class2 = factory.getOWLClass(IRI.create( o2uri+"#"+c2.substring( c2sharp+1 ) ));
			// Find the alignment
			Set<Alignment> alignments = getAlignments( o1uri, o2uri );
			boolean itoj = true;
			if ( alignments.isEmpty() ) {
				alignments = getAlignments( o2uri, o1uri );
				itoj = false;
			}
			Alignment al = alignments.iterator().next(); //assumed normalised
			// Apply (nothing is done for non subsumption correspondences)
			if ( mod.add == true ) {
				logger.debug( "+ {} {} {}", class1, (mod.subsumption?">":"%"), class2 );
				if ( itoj ) {
					al.addAlignCell( class1, class2, ">", 1. );
				} else {
					al.addAlignCell( class2, class1, "<", 1. );
				}
			} else {
				logger.debug( "- {} {} {}", class1, (mod.subsumption?">":"%"), class2 );
				// Find the cell
				Cell foundcell = null;
				// If it is = suppress only one part
				if ( itoj ) {
					for ( Cell c: al.getAlignCells1( class1 ) ) {
						if ( c.getObject2() == class2 ) {
							al.remCell( c );
							if ( c.getRelation() instanceof EquivRelation ) {
								al.addAlignCell( class1, class2, "<", 1.0 );
							}
							break;
						}
					}
				} else {
					for ( Cell c: al.getAlignCells1( class2 ) ) {
						if ( c.getObject2() == class1 ) {
							al.remCell( c );
							if ( c.getRelation() instanceof EquivRelation ) {
								al.addAlignCell( class2, class1, ">", 1.0 );
							}
							break;
						}
					}
				}
			}
		} catch (Exception ex) {
			throw new LLException( "Cannot revise network", ex );
		}
    }

    // SAVE
    public void saveNOO( File dir, BasicOntologyNetwork noo ) {
		//lenToStrip = prefLen+10;
		int lenToStrip = 5; // "file:"
		if ( dir != null ) {
			dir.mkdir();
			PrintWriter pr = null;
			try {
				pr = new PrintWriter( new File( dir, "noo.rdf" ) );
				noo.write( pr );
				pr.close();
				for ( Alignment al: noo.getAlignments() ) {
					String uri = al.getExtension( Namespace.ALIGNMENT.uri, Annotations.ID );
					pr = new PrintWriter (
										  new BufferedWriter(
															 new OutputStreamWriter( new FileOutputStream( dir+"/"+uri.substring( lenToStrip ) ), StandardCharsets.UTF_8.name() )), true );
					RDFRendererVisitor renderer = new RDFRendererVisitor( pr );
					renderer.init( new Properties() );
					al.render( renderer );
					pr.close();
				}
			} catch ( UnsupportedEncodingException ueex ) {
				logger.debug( "IGNORED Exception", ueex );
			} catch (FileNotFoundException fnfex) {
				logger.debug( "IGNORED Exception", fnfex );
			} catch (AlignmentException alex) {
				logger.warn( "IGNORED Exception", alex );
			} finally {
				pr.close();
			}
		}
    }

    /**
     * 2018: I do not think that this is true of non permutative networks!
     * 2015 Reimplementation:
     * ontologies are indexed by their URI in the OntologyNetwork
     * ontologies : URI -> onto
     * signature : URI -> levels [i][j]...
     * Everything is uniform here
     * ontologies[i] is the ontology starting at level i
     * it thus has levels i..n O..i-2 [n of course]
     *
     * So, if I want to know to which class belong individual
     * [ T, T, F, F, T, F, T ] 0..6
     * in ontology 3
     * it will belong to class: TFTTTF, i.e., 101110
     *
     * For generating reference alignments
     * i[top] == j[top]
     * assuming that i < j, then
     * Each j-class is subsumed by the i-class with its j-(i+1) [n] digits as prefix
     * Each i-class is subsumed by the j-class with its i-(j+1) [n] digits as prefix
     *
     * The size of this alignment is (consider super class is half of a ==)
     * 1+2^{n-1}
     */
    // LOAD (loadEnv... load the network and its alignments)
    public void initCompleteDisjunctiveNetwork() throws LLException {
		// For each pair of ontology considered in the reference
		// JE2018 LOAD: It may be a problem that this is relying on the reference...
		// It could also be better to directly load a network of ontologies
		//if ( loadEnv ) { // If I load it and it does not have the same refs, this will not work!
	    // Load the alignment
	    // ingest it
		//} else {
	    // Add a corresponding alignment
		AlignmentParser aparser = new AlignmentParser();
		for ( Alignment al : references.getAlignments() ) {
			URI uI = null, uJ = null;
			try {
				uI = al.getOntology1URI();
				uJ = al.getOntology2URI();
				// SAVE: for saving alignments must have names
				Alignment newal = null;
				if ( loadEnv && !startempty ) {
					String oI = uI.toString().substring( ontoPrefLen );
					String oJ = uJ.toString().substring( ontoPrefLen );
					//System.err.println( " >> "+loadDir+"/NOOEnvironment/"+oI+"___"+oJ+".rdf" );
					Alignment ual = aparser.parse( "file:"+loadDir+"/NOOEnvironment/"+oI+"___"+oJ+".rdf" );
					newal = new ObjectAlignment( ((URIAlignment)ual) );
				} else {
					newal = createInitialAlignment( uI, uJ );
				}
				// This may also be achieved in the reference alignment...
				newal.setExtension( Namespace.ALIGNMENT.uri, Annotations.ID, al.getExtension( Namespace.ALIGNMENT.uri, Annotations.ID ) );
				addAlignment( newal );
			} catch ( AlignmentException alex ) {
				throw new LLException( "Cannot create alignment between "+uI+" "+uJ, alex );
			}
		}
		//}
		printNetwork( "INITIAL NETWORK" );
    }

    public Alignment createInitialAlignment( URI uI, URI uJ ) throws LLException {
		HeavyLoadedOntology<OWLOntology> ontoI = ontologyTable.get( uI );
		HeavyLoadedOntology<OWLOntology> ontoJ = ontologyTable.get( uJ );
		Alignment al = initAlignment( ontoI, ontoJ, false );
		if ( !startempty ) {
			if ( realistic ) {
				String prefixI = uI.toString();
				String prefixJ = uJ.toString();
				// from i to j
				generateCorrespondences( true, al, ontoI, ontoJ, prefixI, prefixJ, computeNumberOfCorrespondences( uI, uJ ) );
				// from j to 
				generateCorrespondences( false, al, ontoI, ontoJ, prefixI, prefixJ, computeNumberOfCorrespondences( uJ, uI ) );
			} else {
				fillAlignment( al, ontoI, ontoJ, uI, uJ, numberOfCorrespondences, "=" );
			}
		}
		return al;
    }

    // Only an alignment from uI to uJ with uI classes subsumed by uJ classes
    public Alignment createInitialOneWayAlignment( URI uI, URI uJ ) throws LLException {
		HeavyLoadedOntology<OWLOntology> ontoI = ontologyTable.get( uI );
		HeavyLoadedOntology<OWLOntology> ontoJ = ontologyTable.get( uJ );
		Alignment al = initAlignment( ontoI, ontoJ, true );
		if ( !startempty ) {
			if ( realistic ) {
				// from i to j
				generateCorrespondences( true, al, ontoI, ontoJ, uI.toString(), uJ.toString(), computeNumberOfCorrespondences( uI, uJ ) );
			} else {
				fillAlignment( al, ontoI, ontoJ, uI, uJ, numberOfCorrespondences/2, "<" );
			}
		}
		return al;
    }

    // This function is here to load alignments which are not one-way (because of top)
    // and make them one-way
    public ObjectAlignment makeOneWay( Alignment ual ) throws AlignmentException {
        ObjectAlignment result = new ObjectAlignment( (URIAlignment)ual );
        for ( Cell c : result ) {
            if ( c.getRelation() instanceof EquivRelation ) {
                logger.trace("Rectifying corresp: {} {} {}", c.getObject1(), c.getRelation().getRelation(), c.getObject2());
                c.setRelation( new SubsumedRelation() );
            }
        }
        return result;
    }

    /**
     * Creates the ontologies manipulated by agents
     */
    // LOAD (loadOnto/loadEnv... load the agents)
    // See Irina's initWithOntologiesFromFiles in Onto...
	//@SuppressWarnings("unchecked")
    public void initCoreNetwork() throws LLException {
		OntologyFactory ontowrapFactory = OntologyFactory.getFactory();
		// Create N ontologies
		for ( int i=0; i<numberOfOntologies; i++ ) {
			String prefix = uriprefix+"ontology/";
			Object sig = null;
			String ouri = null;
			if ( loadOnto != null ) { // This assumes an ontology per agent
				try ( BufferedReader br = new BufferedReader(new FileReader(loadDir+loadOnto+"/"+i+"/sig.txt"))) {
					ouri = br.readLine();
					sig = br.readLine();
				} catch ( IOException ioex ) {
					throw new LLException( "Cannot read agent specification "+loadDir+loadOnto+"/"+i+"/sig.txt", ioex );
				}
			}
			if ( ontologyPermutation ) {
				if ( sig != null ) {
					sig = Integer.valueOf( (String)sig );
				} else {
					sig = Integer.valueOf( i ); // for ( int k=0; k < n-1; k++ ) sig[k] = i+k; modulo n
				}
				prefix += sig;
			} else {
				// LOAD JE2018TODO: If this is to be loaded, then load the sig.txt
				// create the signature and the string prefix
				if ( sig != null ) {
					// prefix should be set!
					prefix += sig;
					// read an array of int from sig
					int[] sigvect = new int[numberOfOntologies-1];
					// on split ssig par '-' et on converti en entier et on mets dans sig...
					int j=0;
					for ( String level : ((String)sig).split("-") ) {
						sigvect[j] = Integer.parseInt( level );
						j++;
					}
					sig = sigvect;
				} else {
					sig = new int[numberOfOntologies-1];
					int val = rand.nextInt( numberOfOntologies );
					for ( int k=0; k < numberOfOntologies-1; k++ ) ((int[])sig)[k] = val;
					// The way to ensure that there are different individual ontologies is to prefix them
					// by the agent number... Agent can have the same ontology but do not know...
					prefix += i+"+";
					prefix += val;
					for ( int k=1; k < numberOfOntologies-1; k++ ) {
						boolean found = false;
						while ( !found ) {
							val = rand.nextInt( numberOfOntologies );
							found = true;
							for ( int j=0; j<k; j++ ) if ( ((int[])sig)[j] == val ) { found = false; break; }
						}
						((int[])sig)[k] = val;
						prefix += "-"+val;
					}
				}
			}
			try {
				// LOAD + There is the load of the signature...
				HeavyLoadedOntology<OWLOntology> onto = null;
				if ( loadOnto != null ) {
					File file = new File( loadDir+loadOnto+"/"+i+"/onto.owl" );
					try {
						OWLOntology owlonto = manager.loadOntologyFromOntologyDocument( file );
						onto = (HeavyLoadedOntology<OWLOntology>)ontowrapFactory.newOntology( owlonto );
					} catch ( OWLOntologyCreationException oocex ) {
						throw new LLException( "Error while loading ontology from file "+file, oocex );
					}
				} else {
					IRI iri = IRI.create( prefix );
					//System.err.println( prefix );
					onto = (HeavyLoadedOntology<OWLOntology>)ontowrapFactory.newOntology( manager.createOntology( iri ) );
					OWLClass top = factory.getOWLClass( IRI.create( prefix+"#top") );
					// 2015: this should be passed the signature
					recursiveFillClasses( onto.getOntology(), top, prefix+"#", numberOfOntologies-1 );
				}
				//URI u = iri.toURI();
				URI u = onto.getURI();
				addOntology( u );
				ontologyTable.put( u, onto );
				ontologySignature.put( u, sig );
			} catch ( OWLOntologyCreationException ocex ) {
				// I must raise something
				logger.debug( "IGNORED: Cannot create ontology "+prefix );
				ocex.printStackTrace();
			} catch ( OntowrapException owex ) {
				throw new LLException( "Cannot create ontology "+prefix, owex );
			}
		}
    }

    /**
     * Fills the classes in the ontologies layer per layer
     */
    public void recursiveFillClasses( OWLOntology onto, OWLClass cl, String pref, int level ) {
		logger.trace( "Expanding class {}", cl );
		if ( level == 0 ) return;
		OWLClass sub0 = factory.getOWLClass(IRI.create(pref+"0"));
		addSubClass( onto, sub0, cl );
		OWLClass sub1 = factory.getOWLClass(IRI.create(pref+"1"));
		addSubClass( onto, sub1, cl );
		// There should be a random ratio (disjointRatio)
		if ( rand.nextInt( 100 ) <= disjointRatio ) {
			addDisjointClasses( onto, sub0, sub1 );
		}
		recursiveFillClasses( onto, sub0, pref+"0", level-1 );
		recursiveFillClasses( onto, sub1, pref+"1", level-1 );
    }

    public void addSubClass( OWLOntology onto, OWLClass sub, OWLClass sup ) {
		OWLAxiom axiom = factory.getOWLSubClassOfAxiom( sub, sup );
		manager.applyChange( new AddAxiom( onto, axiom ) );
    }

    public void addDisjointClasses( OWLOntology onto, OWLClass cl1, OWLClass cl2 ) {
		OWLAxiom axiom = factory.getOWLDisjointClassesAxiom( cl1, cl2 );
		manager.applyChange( new AddAxiom( onto, axiom ) );
    }

    BasicOntologyNetwork references;
    public OntologyNetwork getReferenceNetwork() {
		return references;
    }
    public OntologyNetwork getCurrentNetwork() {
		return this;
    }

    public void logNetworkStatistics( String name, OntologyNetwork noo ) {
		if ( !precRec ) {
			logger.info( "{} Size = {}; F-measure = {}; incoherence = {}", name, computeSizeAlign( noo ), computeFMeasure( noo ), computeIncoherence( noo ) );
		} else {
			double[] pr = computePrecRec( noo );
			logger.info( "{} Size = {}; Precision = {}; Recall = {}; incoherence = {}", name, computeSizeAlign( noo ), pr[0], pr[1], computeIncoherence( noo ) );
		}
    }

    /**
     * Returns the number of correspondences between two ontologies
     */
    public int computeNumberOfCorrespondences( final URI uI, final URI uJ ) {
		int nbcorresp = 0;
		if ( ontologyPermutation ) {
			int i = ((Integer)ontologySignature.get( uI )).intValue();
			int j = ((Integer)ontologySignature.get( uJ )).intValue();
			int d = i-j;
			if ( d<0 ) d += numberOfOntologies;
			for ( int l = numberOfOntologies-d-1; l >= 1; l-- ) {
				nbcorresp += (int)java.lang.Math.pow( 2, l+d );
			}
		} else { // get it from the reference
			Set<Alignment> refset = references.getAlignments( uI, uJ );
			boolean itoj = !refset.isEmpty();
			Alignment ref = null;
			if ( itoj ) {
				ref = refset.iterator().next();
			} else { // should work in anycase (otherwise, raise NPE)
				ref = references.getAlignments( uJ, uI ).iterator().next();
			} 
			for ( Cell c : ref ) {
				if ( c.getRelation() instanceof EquivRelation ||
					 ( itoj && c.getRelation() instanceof SubsumeRelation ) ||
					 ( !itoj && c.getRelation() instanceof SubsumedRelation ) ) nbcorresp++;
			}
			nbcorresp--; // non counting the top
		}
		return nbcorresp;
    }

    public Alignment initAlignment( final URI uI, final URI uJ, boolean oneway ) {
		return initAlignment( ontologyTable.get( uI ), ontologyTable.get( uJ ), oneway );
    }

    protected Alignment initAlignment( final HeavyLoadedOntology<OWLOntology> ontoI, final HeavyLoadedOntology<OWLOntology> ontoJ, boolean oneway ) {
		Alignment al = new ObjectAlignment();
		String prefixI = ontoI.getURI().toString();
		String prefixJ = ontoJ.getURI().toString();
		try {
			al.init( ontoI, ontoJ );
			if ( oneway ) {
				al.addAlignCell( ontoI.getEntity( new URI( prefixI+"#top" ) ), ontoJ.getEntity( new URI( prefixJ+"#top" ) ), "<", 1.0 );
			} else if ( realistic || startempty ) {
				al.addAlignCell( ontoI.getEntity( new URI( prefixI+"#top" ) ), ontoJ.getEntity( new URI( prefixJ+"#top" ) ), ">", 1.0 );
				al.addAlignCell( ontoI.getEntity( new URI( prefixI+"#top" ) ), ontoJ.getEntity( new URI( prefixJ+"#top" ) ), "<", 1.0 );
			} else { // This is the initial old school way...
				al.addAlignCell( ontoI.getEntity( new URI( prefixI+"#top" ) ), ontoJ.getEntity( new URI( prefixJ+"#top" ) ) );
			}
		} catch (AlignmentException alex) {
			logger.debug( "IGNORED Alignment exception: {}", alex );
		} catch (OntowrapException owex) {
			logger.debug( "IGNORED Ontowrap exception: {}", owex );
		} catch (URISyntaxException usex) {
			logger.debug( "IGNORED URISyntax exception: {}", usex );
		}
		return al;
    }
    
    protected void generateCorrespondences( final boolean itoj, final Alignment al, final HeavyLoadedOntology<OWLOntology> ontoI, final HeavyLoadedOntology<OWLOntology> ontoJ, final String prefixI, final String prefixJ, int k ) {
		// Only one correspondence per class
		HashSet<String> visited = new HashSet<String>();
		while ( k > 0 ) {
			k--;
			try {
				String u1 = generateClassName();
				String u2 = generateClassName();
				while ( visited.contains(u2) ) { u2 = generateClassName(); }
				visited.add( u2 );
				if ( itoj ) {
					logger.trace( " Alignment: {} > {}", prefixI+"#"+u1, prefixJ+"#"+u2 );
					al.addAlignCell( ontoI.getEntity( new URI( prefixI+"#"+u1 ) ), ontoJ.getEntity( new URI( prefixJ+"#"+u2 ) ), ">", 1. );
				} else {
					logger.trace( " Alignment: {} < {}", prefixI+"#"+u2, prefixJ+"#"+u1 );
					al.addAlignCell( ontoI.getEntity( new URI( prefixI+"#"+u2 ) ), ontoJ.getEntity( new URI( prefixJ+"#"+u1 ) ), "<", 1. );
				}
			} catch (OntowrapException owex) {
				logger.debug( "IGNORED Ontowrap exception: {}", owex );
			} catch (URISyntaxException usex) {
				logger.debug( "IGNORED URISyntax exception: {}", usex );
			} catch (AlignmentException alex) {
				logger.debug( "IGNORED Alignment exception: {}", alex );
			}
		}
    }

    /**
     * fill an alignment with k random correspondences
     */
    public void fillAlignment( final Alignment al, final HeavyLoadedOntology<OWLOntology> ontoI, final HeavyLoadedOntology<OWLOntology> ontoJ, final URI uI, final URI uJ, int k, final String rel ) {
		String prefixI = uI.toString();
		String prefixJ = uJ.toString();
		// Only one correspondence per class
		HashSet<String> s1 = new HashSet<String>();
		HashSet<String> s2 = new HashSet<String>();
		while ( k > 0 ) {
			k--;
			try {// Could use listClasses()
				String u1 = generateClassName();
				while ( s1.contains(u1) ) { u1 = generateClassName(); }
				s1.add( u1 );
				String u2 = generateClassName();
				while ( s2.contains(u2) ) { u2 = generateClassName(); }
				s2.add( u2 );
				logger.trace( " Alignment: {} {} {}", prefixI+"#"+u1, rel, prefixJ+"#"+u2 );
				al.addAlignCell( ontoI.getEntity( new URI( prefixI+"#"+u1 ) ), ontoJ.getEntity( new URI( prefixJ+"#"+u2 ) ), rel, 1.0 );
			} catch (OntowrapException owex) {
				logger.debug( "IGNORED Ontowrap exception: {}", owex );
			} catch (URISyntaxException usex) {
				logger.debug( "IGNORED URISyntax exception: {}", usex );
			} catch (AlignmentException alex) {
				logger.debug( "IGNORED Alignment exception: {}", alex );
			}
		}
    }

    // LOAD (loadRef/loadEnv...): not necessarily, it can be recreated
    // symmetric alignments in each direction
    public void createReferenceNetwork( final boolean symmetric ) throws LLException {
		references = this.networkClone();
		// For each pair of ontology: add the corresponding alignment
		for ( URI u1 : getOntologies() ) {
			for ( URI u2 : getOntologies() ) {
				if ( !u1.equals(u2) && references.getAlignments( u2, u1 ).isEmpty() ) {
					try {
						if ( symmetric ) {
							references.addAlignment( oneWayRefAlignment( u1, u2, ontologySignature.get( u1 ), ontologySignature.get( u2 ), numberOfOntologies ) );
							references.addAlignment( oneWayRefAlignment( u2, u1, ontologySignature.get( u2 ), ontologySignature.get( u1 ), numberOfOntologies ) );
						} else {
							references.addAlignment( refAlignment( u1, u2, ontologySignature.get( u1 ), ontologySignature.get( u2 ), numberOfOntologies ) );
						}
					} catch ( AlignmentException alex ) {
						throw new LLException( "Cannot create reference alignment between "+u1+" "+u2, alex );
						//System.err.print( alex );
					}
				}
			}
		}
		// SAVE: obsolete?
		printNetwork( "REFERENCE NETWORK", references );
    }


    /**
     * Generates the reference alignment between two ontologies
     * i[top] == j[top]
     */
    public Alignment refAlignment( final URI uI, final URI uJ, final Object sigI, final Object sigJ, int n ) {
		Alignment al = new ObjectAlignment();
		HeavyLoadedOntology<OWLOntology> ontoI = ontologyTable.get( uI );
		HeavyLoadedOntology<OWLOntology> ontoJ = ontologyTable.get( uJ );
		try {
			al.init( ontoI, ontoJ );
			al.addAlignCell( ontoI.getEntity( new URI( uI+"#top" ) ), ontoJ.getEntity( new URI( uJ+"#top" ) ) );
		} catch (OntowrapException owex) {
			logger.debug( "IGNORED Ontowrap exception: {}", owex );
		} catch (AlignmentException alex) {
			logger.debug( "IGNORED Alignment exception: {}", alex );
		} catch (URISyntaxException usex) {
			logger.debug( "IGNORED URISyntax exception: {}", usex );
		}
		//al.setExtension( Namespace.ALIGNMENT.uri, Annotations.ID, uriprefix+"alignment/"+signatureToString( uI )+"___"+signatureToString( uJ ) );
		//al.setExtension( Namespace.ALIGNMENT.uri, Annotations.ID, "file:"+signatureToString( uI )+"___"+signatureToString( uJ )+".rdf" );
		al.setExtension( Namespace.ALIGNMENT.uri, Annotations.ID, "file:"+uI.toString().substring( ontoPrefLen )+"___"+uJ.toString().substring( ontoPrefLen )+".rdf" );
		orientedRefAlignment( al, true, uI, uJ, sigI, sigJ, n );
		orientedRefAlignment( al, false, uJ, uI, sigJ, sigI, n );
		return al;
    }

    /**
     * Generates the reference alignment between two ontologies
     * i[top] &lt; j[top]
     */
    public Alignment oneWayRefAlignment( final URI uI, final URI uJ, final Object sigI, final Object sigJ, int n ) {
		Alignment al = new ObjectAlignment();
		HeavyLoadedOntology<OWLOntology> ontoI = ontologyTable.get( uI );
		HeavyLoadedOntology<OWLOntology> ontoJ = ontologyTable.get( uJ );
		try {
			al.init( ontoI, ontoJ );
			al.addAlignCell( ontoI.getEntity( new URI( uI+"#top" ) ), ontoJ.getEntity( new URI( uJ+"#top" ) ), "<", 1.0 );
		} catch (OntowrapException owex) {
			logger.debug( "IGNORED Ontowrap exception: {}", owex );
		} catch (AlignmentException alex) {
			logger.debug( "IGNORED Alignment exception: {}", alex );
		} catch (URISyntaxException usex) {
			logger.debug( "IGNORED URISyntax exception: {}", usex );
		}
		//al.setExtension( Namespace.ALIGNMENT.uri, Annotations.ID, uriprefix+"alignment/"+signatureToString( uI )+"___"+signatureToString( uJ ) );
		//al.setExtension( Namespace.ALIGNMENT.uri, Annotations.ID, "file:"+signatureToString( uI )+"___"+signatureToString( uJ )+".rdf" );
		al.setExtension( Namespace.ALIGNMENT.uri, Annotations.ID, "file:"+uI.toString().substring( ontoPrefLen )+"___"+uJ.toString().substring( ontoPrefLen )+".rdf" );
		orientedRefAlignment( al, false, uJ, uI, sigJ, sigI, n );
		return al;
    }

    /**
     * Fills the alignment al with the reference correspondence given by the signature of the ontologies
     */
    public Alignment orientedRefAlignment( final Alignment al, final boolean itoj, final URI uI, final URI uJ, final Object sigI, final Object sigJ, final int height ) {
		if ( ontologyPermutation ) {
			int depth = 1;
			int i = ((Integer)sigI).intValue();
			int j = ((Integer)sigJ).intValue();
			int diff = (i>j)?i-j:height-(j-i);
			while ( diff+depth < height ) {
				// beware the switch i/j because the methods below wants i and j at their correct place
				addCorrectCorrespondencesSuffix( al, itoj?uI:uJ, itoj?uJ:uI, itoj, "", depth, diff );
				depth++;
			}
		} else { // random level order
			//logger.debug( "Reference alignment for {} and {}", uI, uJ );
			boolean excluded = false;
			int[] corresp = new int[numberOfOntologies-1]; // correspondences from J to I
			// direction i -> j
			int j = 0;
			for ( int k=0; k < numberOfOntologies-1 ; k++ ) corresp[k] = -1;
			for ( int i=0; !excluded && i < numberOfOntologies-1 ; i++ ) {
				int k=0;
				//System.err.println( "-It :"+i+"/"+j+"/"+k+"   "+showVect(corresp) );
				while ( k <= j && ((int[])sigI)[i] != ((int[])sigJ)[k] ) { k++; }
				// if k != j, then the feature is in the prefix, do nothing, it will be taken care of later
				if ( k <= j ) {
					corresp[k] = i; // record the corresponding item
					if ( i == numberOfOntologies-2 ) addReferenceCorrespondencesIEnum( al, uI, uJ, itoj, corresp, i, j, 0, new int[i+1] );
				} else { // k == j+1
					// The correspondences generated by 0..i-1 are minimal -> generate them
					if ( i != 0 ) addReferenceCorrespondencesIEnum( al, uI, uJ, itoj, corresp, i-1, j, 0, new int[i+1] );
					// Look for other possible suffixes
					while ( k < numberOfOntologies-1 && ((int[])sigI)[i] != ((int[])sigJ)[k] ) { k++; }
					if ( k < numberOfOntologies-1 ) { // found in the suffix  ((int[])sigI)[i] == ((int[])sigJ)[k]
						corresp[k] = i; // record the corresponding item
						j = k;
					} else excluded = true; // that's over
				}
				if ( !excluded && i == numberOfOntologies-2 ) addReferenceCorrespondencesIEnum( al, uI, uJ, itoj, corresp, i, j, 0, new int[i+1] );
			}
		}
		return al;
    }

    // Accumulators should be bitsets...
    protected void addReferenceCorrespondencesIEnum( final Alignment al, final URI uI, final URI uJ, final boolean IFirst, final int[] corresp, final int iend, final int jend, final int i, final int[] iaccu ) {
		//logger.trace( "   ({}) {} {}/{}/{} {}", IFirst, showVect(corresp), iend, jend, i, showVect(iaccu) );
		if ( i <= iend ) {
			iaccu[i] = 0;
			addReferenceCorrespondencesIEnum( al, uI, uJ, IFirst, corresp, iend, jend, i+1, iaccu );
			iaccu[i] = 1;
			addReferenceCorrespondencesIEnum( al, uI, uJ, IFirst, corresp, iend, jend, i+1, iaccu );
		} else { // could reuse the accu as well...
			addReferenceCorrespondencesJEnum( al, uI, uJ, IFirst, corresp, iend, jend, 0, iaccu, new int[jend+1] ); //???? +1
		}
    }

    protected void addReferenceCorrespondencesJEnum( final Alignment al, final URI uI, final URI uJ, final boolean IFirst, final int[] corresp, final int iend, final int jend, final int j, final int[] iaccu, final int[] jaccu ) {
		//logger.trace( "     ({}) {} {} / {}", IFirst, j, showVect(iaccu), showVect(jaccu) );
		if ( j <= jend ) {
			if ( corresp[j] == -1 ) {
				jaccu[j] = 0;
				addReferenceCorrespondencesJEnum( al, uI, uJ, IFirst, corresp, iend, jend, j+1, iaccu, jaccu );
				jaccu[j] = 1;
				addReferenceCorrespondencesJEnum( al, uI, uJ, IFirst, corresp, iend, jend, j+1, iaccu, jaccu );
			} else {
				jaccu[j] = iaccu[corresp[j]];
				addReferenceCorrespondencesJEnum( al, uI, uJ, IFirst, corresp, iend, jend, j+1, iaccu, jaccu );
			}
		} else {
			simplyAddReferenceCorrespondence( al, uI, uJ, IFirst, ontologyTable.get( uI ), ontologyTable.get( uJ ), iend, jend, iaccu, jaccu );
		}
    }

    protected void simplyAddReferenceCorrespondence( final Alignment al, final URI uI, final URI uJ, final boolean IFirst, final HeavyLoadedOntology<OWLOntology> ontoI, final HeavyLoadedOntology<OWLOntology> ontoJ, final int iend, final int jend, final int[] iaccu, final int[] jaccu ) {
		String iURI = uI+"#";
		for ( int i = 0; i <= iend; i++ ) iURI += iaccu[i];
		String jURI = uJ+"#";
		for ( int i = 0; i <= jend; i++ ) jURI += jaccu[i];
		//logger.trace( "      ==> {} {} {}", iURI, (IFirst?">":"<"), jURI );
		try {
			if ( IFirst ) {
				logger.trace( iURI+"  >  "+jURI );
				al.addAlignCell( ontoI.getEntity( new URI( iURI ) ), ontoJ.getEntity( new URI( jURI ) ), ">", 1.0 );
			} else {
				logger.trace( jURI+"  <  "+iURI );
				al.addAlignCell( ontoJ.getEntity( new URI( jURI ) ), ontoI.getEntity( new URI( iURI ) ), "<", 1.0 );
			}
		} catch (OntowrapException owex) {
			logger.debug( "IGNORED Ontowrap exception: {}", owex );
		} catch (URISyntaxException usex) {
			logger.debug( "IGNORED URISyntax exception: {}", usex );
		} catch (AlignmentException alex) {
			logger.debug( "IGNORED Alignment exception: {}", alex );
		}
    }

    private String showVect( int[] vect ) {
		String v = "";
		for ( int i : vect ) v += ":"+i;
		return v;
    }
    
    // Enumerate all suffixes;
    // -> a recursive function based on depth (suffix length)
    protected void addCorrectCorrespondencesSuffix( final Alignment al, final URI uI, final URI uJ, final boolean IFirst, final String suffix, final int level, final int diff ) {
		if ( level > 0 ) { 
			addCorrectCorrespondencesSuffix( al, uI, uJ, IFirst, suffix+"0", level-1, diff );
			addCorrectCorrespondencesSuffix( al, uI, uJ, IFirst, suffix+"1", level-1, diff );
		} else {
			addCorrectCorrespondencesPrefix( al, uI, uJ, IFirst, suffix, "", diff );
		}
    }

    // Enumerate all prefixes
    // -> a recursive function based on diff (prefix length)
    // Add the alignments
    protected void addCorrectCorrespondencesPrefix( Alignment al, URI uI, URI uJ, final boolean IFirst, final String suffix, final String prefix, final int diff ) {
		if ( diff > 0 ) { 
			addCorrectCorrespondencesPrefix( al, uI, uJ, IFirst, suffix, prefix+"0", diff-1 );
			addCorrectCorrespondencesPrefix( al, uI, uJ, IFirst, suffix, prefix+"1", diff-1 );
		} else {
			simplyAddCorrespondence( al, ontologyTable.get( uI ), ontologyTable.get( uJ ), uI, uJ, IFirst, prefix, suffix );
		}
    }

    /**
     * Adds a correspondence in the alignment from one ontology to the other (depending on IFirst)
     * In which a suffix is mapped into a prefix
     */
    protected void simplyAddCorrespondence( final Alignment al, final HeavyLoadedOntology<OWLOntology> ontoI, final HeavyLoadedOntology<OWLOntology> ontoJ, final URI uI, final URI uJ, final boolean IFirst, final String prefix, final String suffix ) {
		String u = prefix+suffix;
		try {
			if ( IFirst ) {
				logger.trace( uI+"#"+suffix+"  >  "+uJ+"#"+u );
				al.addAlignCell( ontoI.getEntity( new URI( uI+"#"+suffix ) ), ontoJ.getEntity( new URI( uJ+"#"+u ) ), ">", 1.0 );
			} else {
				logger.trace( uI+"#"+u+"  <  "+uJ+"#"+suffix );
				al.addAlignCell( ontoI.getEntity( new URI( uI+"#"+u ) ), ontoJ.getEntity( new URI( uJ+"#"+suffix ) ), "<", 1.0 );
			}
		} catch (OntowrapException owex) {
			logger.debug( "IGNORED Ontowrap exception: {}", owex );
		} catch (URISyntaxException usex) {
			logger.debug( "IGNORED URISyntax exception: {}", usex );
		} catch (AlignmentException alex) {
			logger.debug( "IGNORED Alignment exception: {}", alex );
		}
    }

    public int numberOfFeatures() {
		return numberOfFeatures;
    }

    /**
     * Draws a class name at random 
     * Top is excluded of the generated class names...
     */
    public String generateClassName() {
		return generateClassName( numberOfFeatures );
    }

    public String generateClassName( int n ) {
		String name = "";
		int size = rand.nextInt( n-1 ); // can it be 0?
		for ( int i=size; i >= 0; i-- ) {
			name += rand.nextBoolean()?"1":"0";
		}
		return name;
    }

    /**
     * Generate instance
     */
    public BitSet generateInstance() {
		BitSet instance = new BitSet( numberOfFeatures );
		for ( int i = 0; i<numberOfFeatures ; i++ ) instance.set( i, rand.nextBoolean() );
		return instance;
    }

    /**
     * Parse saved instance
     */
    private Pattern pattern = Pattern.compile("\\{([0-9,\\s]*)\\}");
    
    public BitSet parseInstance( String instanceString ) {
		// I must parse the bitset!
		Matcher matcher = pattern.matcher( instanceString );
		if ( matcher.find() ) {
			BitSet instance = new BitSet( numberOfFeatures );
			for ( String item : matcher.group(1).split("\\s*,\\s*") ) {
				if ( !("".equals(item)) )
					instance.set( Integer.parseInt( item ) );
			}
			return instance;
		} else return null;
    }

    /**
     * Computes FMeasure of a matrix of alignments w.r.t. the reference alignment
     * Thats a remanence of GroupEval
     */
    public double computeFMeasure( OntologyNetwork noo ) {
		double[] pr = computePrecRec( noo );
		double precision = pr[0];
		double recall = pr[1];
		return (2 * precision * recall) / (precision + recall);
    }

    public double[] computePrecRec( OntologyNetwork noo ) {
		int nbexpected = 0; // expected so far
		int nbfound = 0; // found so far
		int nbfoundentailed = 0;
		int nbexpectedentailed = 0;
		int nbcorrect = 0; // correct so far
		// Iterate on the full set of cells
		for ( URI u1 : noo.getOntologies() ) {
			for ( URI u2 : noo.getOntologies() ) {
				Set<Alignment> refset = references.getAlignments( u1, u2 );
				Set<Alignment> actual = noo.getAlignments( u1, u2 );
				if ( !refset.isEmpty() ) {  // is normalised
					Alignment ref = refset.iterator().next();
					if ( actual.isEmpty() ) { // should never happen
						nbexpected += ref.nbCells();
					} else {
						try {
							Alignment al = actual.iterator().next(); // This should also be normalised
							Evaluator eval = null;
							if ( syntacticEvaluation ) {
								eval = new PRecEvaluator( ref, al ).init();
							} else {
								eval = new SemPRecEvaluator( ref, al ).init((Properties)null);
							}
							eval.eval( (Properties)null ) ;
							nbexpected += ((PRecEvaluator)eval).getExpected();
							if ( syntacticEvaluation ) {
								nbfound += ((PRecEvaluator)eval).getFound();
								nbcorrect += ((PRecEvaluator)eval).getCorrect();
								logger.trace("Prec/rec {} <=> {} == {} / {}", u1, u2, ((PRecEvaluator)eval).getPrecision(), ((PRecEvaluator)eval).getRecall() );
							} else {
								nbfound += ((SemPRecEvaluator)eval).getFound();
								nbfoundentailed += ((SemPRecEvaluator)eval).getFoundEntailed();
								nbexpectedentailed += ((SemPRecEvaluator)eval).getExpectedEntailed();
							}
						} catch ( AlignmentException alex ) {
							logger.warn( "IGNORED: AlignmentException", alex );
						}
					}
				} else { // should never happen
					for ( Alignment al : actual ) { // should be one
						nbfound += al.nbCells();
					}
				}
			}
		}
		double precision = 0.;
		double recall = 0.;
		if ( syntacticEvaluation ) {
			precision = (double)nbcorrect/nbfound;
			recall = (double)nbcorrect/nbexpected;
		} else {
			precision = (double)nbfoundentailed / (double)nbfound;
			recall = (double)nbexpectedentailed / (double)nbexpected;
		}
		double[] res = { precision, recall };
		return res;
    }

    /**
     * Computes the number of correspondences in a network
     * JE 2017: to replace by noo.nbCells() in AlignAPI 4.9
     */
    public int computeSizeAlign ( OntologyNetwork noo ) {
		int nbcells = 0; 
		for ( Alignment al : noo.getAlignments() ) {
			nbcells += al.nbCells();
		}
		return nbcells;
    }

    /**
     * Computes the number of correspondences in the network
     */
    public int currentSizeAlign () {
		return computeSizeAlign( this );
    }

    public void logInit() throws LLException {
		logNetworkStatistics( "Initial", getCurrentNetwork() );
		saveNOO( initDir, this );
    }

    public void logReference() throws LLException {
		logNetworkStatistics( "Reference", getReferenceNetwork() );
		saveNOO( refDir, this );
    }

    public void logFinal() throws LLException {
		printNetwork( "CURRENT NETWORK" ); // SAVE: should be obsoleted
		logNetworkStatistics( "Final", getCurrentNetwork() );
		saveNOO( finalDir, this );
    }
    
    public void printNetwork( String announce, BasicOntologyNetwork network ) {
		if ( outputNetworks ) {
			System.err.println( announce );
			printOutNetwork( network );
		}
    }

    public void printNetwork( String announce ) {
		printNetwork( announce, this );
    }

    public void printOutNetwork ( final OntologyNetwork noo ) {
		for ( Alignment al : noo.getAlignments() ) {
			try {
				System.err.println( "  *** Alignment between "+al.getOntology1URI()+" <=> "+al.getOntology2URI() );
				for ( Cell c : al ) {
					System.err.println( "    "+shortClassName2( c.getObject1AsURI( al ) )
										+"  "+c.getRelation().getRelation()+"  "+
										shortClassName2( c.getObject2AsURI( al ) ) );
				}
			} catch ( AlignmentException alex ) {
				logger.debug( "IGNORED Exception", alex );
			};
		}
    }
    
    public void initTheoryRevision () throws LLException {
		// Generate Horn clauses for theory revision
		addRevisorOntologies( this );
		addRevisorAlignments( this );
    }

    protected void addRevisorOntologies ( final OntologyNetwork noo ) throws LLException {
		for ( URI ontouri : noo.getOntologies() ) {
			OWLOntology onto = ontologyTable.get( ontouri ).getOntology();
			OWLDataFactory factory = onto.getOWLOntologyManager().getOWLDataFactory();
			String ontoname = stripOntoPrefix( ontouri.toString() ); //not a classname but same result
			int ontolength = ontouri.toString().length() + 1; // necessary we do not know the length of ontology prefix
			logger.debug( "--------------- [disjunction axioms]" );
			for ( OWLClass cl: onto.getClassesInSignature() ) {
				// get name... (there is a getIRIString in newer version)
				String name = cl.getIRI().toString();
				String shortname = stripOntoPrefix( name );
				if ( shortname.endsWith( "top" ) ) {
					name = name.substring( 0, ontolength );
					shortname = shortname.substring( 0, shortname.length()-3 );
				} else { // get superclass
					String supname = null;
					int sharpind = shortname.lastIndexOf( '#' );
					if ( sharpind+1 == shortname.length()-1 ) supname = shortname.substring( 0, sharpind )+"#top";
					else supname = shortname.substring( 0, shortname.length()-1 );
				}
				try{
					if ( onto.containsClassInSignature( IRI.create( name+"1" ) ) ) {
						// getdirect sibblings
						logger.debug( "disjunction_axiom( {}, {} ).", format(shortname+"0"), format(shortname+"1") );
						revisor.addAxiom( "disjunction_axiom(o"+ontoname+","+format(shortname+"0")+","+format(shortname+"1")+").");
					}
				} catch (Exception ex) {
					logger.debug( "IGNORED Exception: invalid disjunction axiom", ex );
				}
			}
			logger.debug( "--------------- [subsumption axioms]" );
			for ( OWLClass cl: onto.getClassesInSignature() ) {
				// get name... (there is a getIRIString in newer version
				String name = cl.getIRI().toString();
				String shortname = stripOntoPrefix( name );
				//System.err.println( "> "+name );
				if ( shortname.endsWith( "top" ) ) {
					//name = name.substring( 0, prefLen+2 );
					name = name.substring( 0, ontolength );
					shortname = shortname.substring( 0, shortname.length()-3 );
				} else { // get superclass
					String supname = null;
					int sharpind = shortname.lastIndexOf( '#' );
					if ( sharpind+1 == shortname.length()-1 ) supname = shortname.substring( 0, sharpind )+"#top";
					else supname = shortname.substring( 0, shortname.length()-1 );
					try{
						logger.debug( "subsumption_axiom( {}, {} ).", format(shortname), format(supname));
						revisor.addAxiom( "subsumption_axiom(o"+ontoname+","+format(supname)+","+format(shortname)+").");
					} catch (Exception ex) {
						logger.debug( "IGNORED Exception: invalid subsumption axiom", ex );
					}
				}
			}
			logger.debug( "--------------- [classes]" );
			for ( OWLClass cl: onto.getClassesInSignature() ) {
				// get name... (there is a getIRIString in newer version
				String name = cl.getIRI().toString();
				String shortname = stripOntoPrefix( name );
				try{
					logger.debug( "( o{}, {} )", ontoname, format(shortname) );
					revisor.addConcept("(o"+ontoname+","+format(shortname)+")");
				} catch (Exception ex) {
					logger.debug( "IGNORED Exception: invalid class", ex );
				}
			}
			/*
			// Useless for the moment
			System.err.println( "// Instances" );
			for ( OWLClass cl: onto.getClassesInSignature() ) {
			// get name... (there is a getIRIString in newer version
			String name = cl.getIRI().toString();
			String shortname = stripOntoPrefix( name );
			//System.err.println( "> "+name );
			if ( cl.getSubClasses( onto ).isEmpty() ) { // creates a set...
		    System.err.println( "belongsTo( "+format(shortname)+"instance, "+format(shortname)+" ).");
			}
			}
			*/
		}
    }
    
    protected void addRevisorAlignments( final OntologyNetwork noo ) throws LLException {
		//System.err.println( "\n[subsumption correspondences]" );
		for ( Alignment al : noo.getAlignments() ) {
			for ( Cell c : al ) {
				Relation rel = c.getRelation();
				assert rel != null;
				try{
					String conceptOntologyIdentifier1 = "o"+ stripOntoPrefix(c.getObject1AsURI( al ).toString()).substring(0, stripOntoPrefix(c.getObject1AsURI( al ).toString()).indexOf("#"));
					String conceptOntologyIdentifier2 = "o" + stripOntoPrefix(c.getObject2AsURI( al ).toString()).substring(0, stripOntoPrefix(c.getObject2AsURI( al ).toString()).indexOf("#"));
					if ( rel instanceof EquivRelation || rel instanceof SubsumeRelation ) {
						logger.debug( "subsumption_coresp( {}, {} ).", shortClassName( c.getObject1AsURI( al ) ), shortClassName( c.getObject2AsURI( al ) ) );
						revisor.addCorrespondence( "subsumption_corresp("+conceptOntologyIdentifier1+","+shortClassName( c.getObject1AsURI( al ) )+","+conceptOntologyIdentifier2+","+shortClassName( c.getObject2AsURI( al ) )+").");
					}
					if ( rel instanceof EquivRelation || rel instanceof SubsumedRelation ) {
						logger.debug( "subsumption_coresp( {}, {} ).", shortClassName( c.getObject2AsURI( al ) ), shortClassName( c.getObject1AsURI( al ) ) );
						revisor.addCorrespondence( "subsumption_corresp("+conceptOntologyIdentifier2+","+shortClassName( c.getObject2AsURI( al ) )+","+conceptOntologyIdentifier1+","+shortClassName( c.getObject1AsURI( al ) )+").");
					}
				} catch (Exception ex) {
					logger.debug( "IGNORED Exception: invalid correspondence", ex );
				}
			}
		}
    }

    //http://lazylav.exmo.inria.fr/ontology/ substring( 38 );
    public String shortClassName2( final URI c ) {
		return "o"+stripOntoPrefix( c.toString() );
    }

    public String shortClassName( final URI c ) {
		return format( stripOntoPrefix( c.toString() ) );
    }
    
    public String shortClassName( final IRI c ) {
		return format( stripOntoPrefix( c.toString() ) );
    }

    public String stripOntoPrefix( final String uri ){
		return uri.substring( ontoPrefLen );
    }
    
    public String format( final String c ) {
		return "o"+c.replace( '#', 'c' );
    }
    
    //********************************************************************************

    /**
     * Computes precision and recall of the current state of the network w.r.t. the reference alignment
     */
    public double currentFMeasure () {
		return computeFMeasure( this );
    }

    public double[] currentPrecRec () {
		return computePrecRec( this );
    }

    /**
     * Computes a repaired networks of alignments (e.g., with LogMap or Alcomo)
     * JE 2015: try reentrance! (same repairer)
     * inplace: (use with moderation) used to repair the actual network...
     */
    public BasicOntologyNetwork repairNetworkAndReport( Class<? extends AlignmentRepairer> repairerClass, boolean inplace ) {
		BasicOntologyNetwork repairedAlignments;
		String name = repairerClass.getSimpleName();
		if ( inplace ) repairedAlignments = repairNetwork( this, repairerClass );
		else repairedAlignments = repairNetwork( this.networkClone(), repairerClass );
		printNetwork( name+" NETWORK", repairedAlignments );
		logNetworkStatistics( name, repairedAlignments );
		return repairedAlignments;
    }

	//@SuppressWarnings("rawtypes")
    BasicOntologyNetwork repairNetwork( BasicOntologyNetwork noo, Class<? extends AlignmentRepairer> repairerClass ) {
		Set<Alignment> toadd = new HashSet<Alignment>();
		for ( Alignment al : noo.getAlignments() ) {
			// This is filled in that order because we can get the subset of alignments
			// which bring to j with alignments[j]
			AlignmentRepairer repairer = null;
			try {
				Constructor repairerConstructor = repairerClass.getConstructor();
				repairer = (AlignmentRepairer)repairerConstructor.newInstance();
				//repairer = repairerClass.newInstance();
			} catch ( NoSuchMethodException nsmex ) {
				logger.debug( "IGNORED Exception: no repair instance", nsmex );
			} catch ( InvocationTargetException itex ) {
				logger.debug( "IGNORED Exception: no repair instance", itex );
			} catch ( InstantiationException iex ) {
				logger.debug( "IGNORED Exception: no repair instance", iex );
			} catch ( IllegalAccessException iaex ) {
				logger.debug( "IGNORED Exception: no repair instance", iaex );
			}
			if ( repairer != null ) {
				try {
					toadd.add( repairer.repair( al, (Properties)null ) );
				} catch ( AlignmentException alex ) {
					logger.debug( "IGNORED Exception: no repair performed", alex );
					toadd.add( al );
				}
			}
		}
		noo.cleanUpAlignments();
		for ( Alignment al : toadd ) {
			try { noo.addAlignment( al ); }
			catch (AlignmentException alex) { logger.warn( "IGNORED Alignment exception: {}", alex ); }
		}
		return noo;
    }

    // Beware: no check
    public void setAlignment( Alignment al ) throws LLException {
		try {
			URI o1 = al.getOntology1URI();
			URI o2 = al.getOntology2URI();
			for ( Alignment old : getAlignments( o1, o2 ) ) {
				remAlignment( old );
			}
			addAlignment( al );
		} catch (AlignmentException alex) {
			throw new LLException( "Cannot replace alignment : "+al, alex );
		}
    }

    // Only usable by agent id in principle
    /*public HeavyLoadedOntology<OWLOntology> getOntology( int id ) throws LLException {
	  if ( ontologies==null ) throw new LLException( "Ontologies are not initialized" );
	  return ontologies[id];
	  }*/
    /*public String getOntology( URI u ) {
	  if ( !publicOntologies ) {
	  logger.warn( "Invalid access to private ontology {}", u );
	  return (String)null;
	  }
	  return (String)null;
	  }*/

    public HeavyLoadedOntology<OWLOntology> getOntology( URI uri ) throws LLException {
		if ( !publicOntologies ) {
			logger.warn( "Invalid access to private ontology {}", uri );
			return null;
		}
		return ontologyTable.get( uri );
    }

    /**
     * Returns the way the ontology has been generated from its URI
     */
    public Object getSignature( URI uri ) {
		return ontologySignature.get( uri );
    }

    public String signatureToString( URI uri ) {
		if (  ontologyPermutation ) {
			return ontologySignature.get( uri ).toString();
		} else { // can also directly be extracted from the URI
			int[] sig = (int[])ontologySignature.get( uri );
			String prefix = ""+sig[0];
			for ( int j=1; j<sig.length; j++ ) { prefix += "-"+sig[j]; }
			//for ( int k : sig ) { prefix += "-"+k; }
			return prefix;
		}
    }

    // Only usable by agent agid in principle
    public Alignment getAlignment( URI u1, URI u2 ) {
		Set<Alignment> alignments = getAlignments( u1, u2 );
		//if ( alignments.isEmpty() ) alignments = getAlignments( u2, u1 );
		if ( !alignments.isEmpty() ) return alignments.iterator().next(); //assumed normalised
		else return null;
    }

    public String getUriPrefix() {
		return uriprefix;
    }
    
    /**
     * Computes the incoherence degree of the current state of the network
     */
    public double currentIncoherence () {
		return computeIncoherence( this );
    }

    // Adapted from code provided by Christian Meilicke
    /**
     * Computes Incoherence degree of a matrix of alignments
     * Since this is a ratio of coherence, it could be computed in several way
     * - average
     * - full ratio = numberofincogerencecorrespondences / allcorrespondences
     * - ration of incoherent alignments
     */
    public double computeIncoherence ( OntologyNetwork noo ) {
		double degree = 0.;
		int n = 0;
		//AlcomoLogger.resetTimer(); // Useless, but harmless
		for( Alignment al : noo.getAlignments() ) {
			try {
				ObjectAlignment bal = (ObjectAlignment)al;
				double deg = incoherenceDegree( (HeavyLoadedOntology<Object>)bal.getOntologyObject1(), (HeavyLoadedOntology<Object>)bal.getOntologyObject2(), al );
				n++;
				degree += deg; // average degree
			} catch (Exception ex) {
				logger.debug( "IGNORED Exception", ex );
			};
		}
		if ( n == 0 ) return 0.;
		else return degree/((double)n); // average degree
		//return degree/cells; // full incoherence
		//return (double) inc/(n*2); // incoherent alignment ratio
    }

    public BasicOntologyNetwork networkClone() {
		//return ((BasicOntologyNetwork)this).clone();
		return new BasicOntologyNetwork( (BasicOntologyNetwork)this );
    }

    public double incoherenceDegree( HeavyLoadedOntology<Object> onto1, HeavyLoadedOntology<Object> onto2, Alignment al ) throws AlignmentException, LLException {
		IncoherenceMetric im = new IncoherenceMetric(5000);
		IOntology ont1 = new IOntology( (OWLOntology)onto1.getOntology() );
		IOntology ont2 = new IOntology( (OWLOntology)onto2.getOntology() );
		Mapping mapping = AlcomoRepairer.convertAlignment( al );
		im.eval( ont1, ont2, mapping );
		if ( im.evaluationSuccessful() ) {
			if ( !im.evaluationTerminated() ) {
				logger.warn( "The generated value is only a lower bound, the precise degree is above that value" );
				logger.warn( "{} {} {}", onto1, al, onto2 );
			}
			return im.getDegreeOfIncoherence();
		} else {
			throw new LLException( "Unable to compute incoherence degree for "+al );
		}
    }

}
