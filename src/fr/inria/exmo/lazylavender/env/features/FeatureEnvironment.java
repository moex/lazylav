/*
 *
 * Copyright (C) INRIA, 2016-2017, 2024
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 */

package fr.inria.exmo.lazylavender.env.features;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.Random;
import java.util.Set;
import java.util.TreeMap;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;

import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.AddAxiom;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLClassAssertionAxiom;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLDifferentIndividualsAxiom;
import org.semanticweb.owlapi.model.OWLEntity;
import org.semanticweb.owlapi.model.OWLFunctionalObjectPropertyAxiom;
import org.semanticweb.owlapi.model.OWLNamedIndividual;
import org.semanticweb.owlapi.model.OWLObjectProperty;
import org.semanticweb.owlapi.model.OWLObjectPropertyRangeAxiom;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.inrialpes.exmo.ontowrap.HeavyLoadedOntology;
import fr.inrialpes.exmo.ontowrap.OntologyFactory;
import fr.inrialpes.exmo.ontowrap.OntowrapException;

import fr.inria.exmo.lazylavender.model.LLException;

public class FeatureEnvironment {
       	private final static Logger logger = LoggerFactory.getLogger(FeatureEnvironment.class);

	private static final String FEATURES_PROPERTY = "features";

	public static final String uriprefix = "http://lazylav.exmo.inria.fr/ontology/features";
	public static final String all_objects_in_environment = "all";
	public static final String agent_prefix = "/agent";

	private List<ObservableFeature> observableFeatures;

	private OWLOntologyManager manager;
	private OWLDataFactory factory;

	private OWLOntology commonKnowledgeFeaturesOntology;
	private OWLClass allObjectsInThisEnvironment;

	/**
	 * Generates an object from the environment completely described by all its feature values. key=feature name, value=feature value
	 * 
	 * @return
	 */
	public HashMap<String, String> generateObject() {
		HashMap<String, String> object = new HashMap<>();

		for (ObservableFeature observableFeature : getObservableFeatures()) {
			Random randomGenerator = new Random();
			List<String> values = observableFeature.getValues();
			int index = randomGenerator.nextInt(observableFeature.getValues().size());
			String value = values.get(index);
			object.put(observableFeature.getName(), value);
		}
		return object;
	}

	/**
	 * Initializes the environment with the set of observable features and their possible values. Creates an ontology to store knowledge about these features. <br>
	 * the ontology URI is "http://lazylav.exmo.inria.fr/ontology/features"
	 * 
	 * @param param
	 * @return
	 * @throws LLException
	 */
	public FeatureEnvironment init(Properties param) throws LLException {
		initializeParameters(param);
		
		manager = OWLManager.createOWLOntologyManager();
		factory = manager.getOWLDataFactory();
		commonKnowledgeFeaturesOntology = createKnowledgeAboutFeaturesOntology();
		
		return this;
	}

	public List<OWLOntology> createAgentConjunctionOntologies(int numberOfOntologies) throws LLException {
		List<OWLOntology> agentOntologies = initializeAgentOntologiesWithCommonKnowledge(numberOfOntologies);
		addConjunctionCategories(agentOntologies);
		return agentOntologies;
	}

	private List<OWLOntology> initializeAgentOntologiesWithCommonKnowledge(int numberOfOntologies) throws LLException {
		List<OWLOntology> ontologies = new ArrayList<>(numberOfOntologies);
		for (int i = 0; i < numberOfOntologies; i++) {
			OWLOntology agentOntology = OWLOntologyHelper.mergeOntologies(uriprefix + agent_prefix + i, manager);
			ontologies.add(agentOntology);
		}
		return ontologies;
	}

	private void addConjunctionCategories(List<OWLOntology> agentOntologies) throws LLException {
		for (int i = 0; i < agentOntologies.size(); i++) {
			OWLOntology agentOntology = agentOntologies.get(i);
			addConjunctionCategoriesToAgentOntology(agentOntology, i);
		}
	}

	private void initializeParameters(Properties param) throws LLException {
		if (param.getProperty(FEATURES_PROPERTY) != null && !param.getProperty(FEATURES_PROPERTY).equals("")) {
			obtainFeatures(param.getProperty(FEATURES_PROPERTY));
		} else {
			throw new LLException("Environment without features: [" + param.getProperty(FEATURES_PROPERTY) + "]");
		}
	}

	/**
	 * Creates an ontology with knowledge about features as properties, and their values as class instances <br>
	 * Ontology prefix: http://lazylav.exmo.inria.fr/ontology/features
	 * 
	 * @return
	 * @throws LLException
	 */
	//@SuppressWarnings("unchecked")
	private OWLOntology createKnowledgeAboutFeaturesOntology() throws LLException {
	    HeavyLoadedOntology<OWLOntology> onto = null;
	    String initialUri = uriprefix;
	    IRI iri = IRI.create(initialUri);
	    OntologyFactory ontowrapFactory = OntologyFactory.getFactory();
	    try {
			onto = (HeavyLoadedOntology<OWLOntology>) ontowrapFactory.newOntology(manager.createOntology(iri));
	    } catch (OWLOntologyCreationException | OntowrapException e) {
			throw new LLException("Cannot create ontology " + initialUri, e);
	    }
	    
	    OWLOntology featureOntology = onto.getOntology();
	    createCommonKnowledgeAboutFeatures(initialUri, featureOntology);
	    
	    return featureOntology;
	}

	/**
	 * Adds categorization classes to the agent's ontology <code>agentOntology</code>. The categorization classes are created by a conjunction of atomic categories chosen at random
	 * 
	 * @param agentOntology
	 *            agent's ontology
	 * @param agentId
	 * @throws LLException
	 */
	private void addConjunctionCategoriesToAgentOntology(OWLOntology agentOntology, int agentId) throws LLException {
		Random random = new Random();
		int maxFeatureSpace = 1;

		for (ObservableFeature observableFeature : getObservableFeatures()) {
			int size = observableFeature.getValues().size();
			maxFeatureSpace *= size;
		}
		int maxNbCategorizationClasses = random.nextInt(maxFeatureSpace + 1);
		// TODO see how agents with just "all" class behave. Can they learn something?
		// TODO see interaction with agents that have "all" classes
		for (int i = 0; i <= maxNbCategorizationClasses; i++) {
			/* Tree Map returns ordered entries, so there cannot be two equivalent categories with different names */
			TreeMap<String, OWLClassExpression> atomicCategories = new TreeMap<>();
			for (ObservableFeature observableFeature : getObservableFeatures()) {
				boolean considerFeature = random.nextBoolean();
				if (considerFeature) {
					OWLObjectProperty featureProperty = getFeatureObjectProperty(agentOntology, observableFeature.getName());

					int featureValueIndex = random.nextInt(observableFeature.getValues().size());
					String featureValue = observableFeature.getValues().get(featureValueIndex);

					OWLNamedIndividual featureValueInstance = getFeatureValueIndividual(agentOntology, featureValue);

					OWLClassExpression hasFeatureThisValue = factory.getOWLObjectHasValue(featureProperty, featureValueInstance);
					atomicCategories.put(observableFeature.getName() + "_" + featureValue, hasFeatureThisValue);
				}
			}
			String categoryName = atomicCategories.keySet().toString().replace(" ", "");

			if (!atomicCategories.isEmpty()) {
				// duplicate classes are ignored
				OWLClass categoryClass = factory.getOWLClass(IRI.create(uriprefix + agent_prefix + agentId + "#" + categoryName));

				Set<OWLClassExpression> classExpressions = new HashSet<>();
				classExpressions.addAll(atomicCategories.values());
				classExpressions.add(allObjectsInThisEnvironment);

				OWLOntologyHelper.addConjunctionClass(agentOntology, categoryClass, classExpressions);
			}

		}
	}

	public static OWLNamedIndividual getFeatureValueIndividual(OWLOntology agentOntology, String featureValue) throws LLException {
		OWLEntity entityValue = OWLOntologyHelper.getEntityByURI(agentOntology, uriprefix + "#" + featureValue);
		if (entityValue == null) {
			throw new LLException("No entity with name [" + "#" + featureValue + "] found!");
		}
		if (!(entityValue instanceof OWLNamedIndividual)) {
			throw new LLException("entity with name [" + "#" + featureValue + "] has type [" + entityValue.getClass() + "] instead of OWLNamedIndividual!");
		}
		OWLNamedIndividual featureValueInstance = (OWLNamedIndividual) entityValue;
		return featureValueInstance;
	}

	public static OWLObjectProperty getFeatureObjectProperty(OWLOntology agentOntology, String featureName) throws LLException {
		OWLEntity entityFeature = OWLOntologyHelper.getEntityByURI(agentOntology, uriprefix + "#has" + featureName);
		if (entityFeature == null) {
			throw new LLException("No entity with name [" + "#has" + featureName + "] found!");
		}
		if (!(entityFeature instanceof OWLObjectProperty)) {
			throw new LLException("entity with name [" + "#has" + featureName + "] has type [" + entityFeature.getClass() + "] instead of OWLObjectProperty!");
		}
		OWLObjectProperty featureProperty = (OWLObjectProperty) entityFeature;
		return featureProperty;
	}

	public static OWLClass getCategory(OWLOntology agentOntology, int agentId, String categoryName) throws LLException {
		if(all_objects_in_environment.equals(categoryName)){
			return getAllObjectsFromTheEnvironmentCategory(agentOntology);
		} else {
		
		OWLEntity entityCategory = OWLOntologyHelper.getEntityByURI(agentOntology, uriprefix + agent_prefix + agentId + "#" + categoryName);
		if (entityCategory == null) {
			throw new LLException("No entity with name [" + "#" + categoryName + "] found!");
		}
		if (!(entityCategory instanceof OWLClass)) {
			throw new LLException("entity with name [" + "#" + categoryName + "] has type [" + entityCategory.getClass() + "] instead of OWLClass!");
		}
		OWLClass category = (OWLClass) entityCategory;

		return category;
		}
	}

	public static OWLClass getAllObjectsFromTheEnvironmentCategory(OWLOntology agentOntology) throws LLException {
		String uri = uriprefix + "#" + all_objects_in_environment;
		OWLEntity allCategory = OWLOntologyHelper.getEntityByURI(agentOntology, uri);
		if (allCategory == null) {
			throw new LLException("No entity with name [" + "#" + uri + "] found!");
		}
		if (!(allCategory instanceof OWLClass)) {
			throw new LLException("entity with name [" + "#" + uri + "] has type [" + allCategory.getClass() + "] instead of OWLClass!");
		}
		OWLClass category = (OWLClass) allCategory;

		return category;
	}

	public static OWLClassExpression getCategoryDefinition(OWLOntology ontology, OWLClass category) throws LLException {
		Set<OWLClassExpression> categoryDefinitions = category.getEquivalentClasses(ontology);
		if (categoryDefinitions == null || categoryDefinitions.isEmpty()) {
			throw new LLException("Inexistent category definition[" + categoryDefinitions + "] for class [" + category + "] !");
		}
		if(categoryDefinitions.size()!=1){
			logger.warn("multiple categoryDefinitions ["+categoryDefinitions+"] for category ["+category+"]");
		}
		OWLClassExpression categoryDefinition = categoryDefinitions.iterator().next();
		return categoryDefinition;
	}

	private void createCommonKnowledgeAboutFeatures(String initialUri, OWLOntology oWLOntology) {
		allObjectsInThisEnvironment = factory.getOWLClass(IRI.create(initialUri + "#" + all_objects_in_environment));
		for (ObservableFeature observableFeature : getObservableFeatures()) {
			// create feature range
			OWLClass featureRangeClass = factory.getOWLClass(IRI.create(initialUri + "#" + observableFeature.getName()));

			Set<OWLNamedIndividual> featureValuesNamedIndividuals = new HashSet<>();
			for (String featureValueName : observableFeature.getValues()) {
				OWLNamedIndividual featureValueIndividual = factory.getOWLNamedIndividual(IRI.create(initialUri + "#" + featureValueName));
				OWLClassAssertionAxiom classAssertion = factory.getOWLClassAssertionAxiom(featureRangeClass, featureValueIndividual);
				manager.addAxiom(oWLOntology, classAssertion);

				featureValuesNamedIndividuals.add(featureValueIndividual);
			}
			OWLDifferentIndividualsAxiom owlDifferentIndividualsAxiom = factory.getOWLDifferentIndividualsAxiom(featureValuesNamedIndividuals);
			manager.applyChange(new AddAxiom(oWLOntology, owlDifferentIndividualsAxiom));
			
			// FIXME axiom that class exhausts the set of these individuals

			// create hasFeature property
			OWLObjectProperty featureProperty = factory.getOWLObjectProperty(IRI.create(initialUri + "#has" + observableFeature.getName()));
			OWLFunctionalObjectPropertyAxiom owlFunctionalObjectPropertyAxiom = factory.getOWLFunctionalObjectPropertyAxiom(featureProperty);
			manager.applyChange(new AddAxiom(oWLOntology, owlFunctionalObjectPropertyAxiom));

			// /* all objects in the environment have that feature */
			// // FIXME is domain axiom equivalent to this?
			// // OWLObjectPropertyDomainAxiom owlObjectPropertyDomainAxiom = factory.getOWLObjectPropertyDomainAxiom(featureProperty, top);
			// // manager.applyChange(new AddAxiom(oWLOntology,
			// // owlObjectPropertyDomainAxiom));
			
			OWLClassExpression hasFeature = factory.getOWLObjectSomeValuesFrom(featureProperty, factory.getOWLThing());
			//featureRangeClass
			OWLOntologyHelper.addSubClass(oWLOntology, allObjectsInThisEnvironment, hasFeature);

			// FIXME do I need this range axion? Do I need restriction instead?
			OWLObjectPropertyRangeAxiom owlObjectPropertyRangeAxiom = factory.getOWLObjectPropertyRangeAxiom(featureProperty, featureRangeClass);
			manager.applyChange(new AddAxiom(oWLOntology, owlObjectPropertyRangeAxiom));

			// TODO maybe assert that properties are disjoint
		}
	}

	//@SuppressWarnings("unchecked")
	private void obtainFeatures(String serializedFeatures) throws LLException {
		ObjectMapper mapper = new ObjectMapper();
		try {
			setObservableFeatures(mapper.readValue(serializedFeatures, new TypeReference<List<ObservableFeature>>() {
			}));
		} catch (IOException e) {
			throw new LLException("Error reading features: [" + serializedFeatures + "]", e);
		}

		/*validate features*/
		for (ObservableFeature observableFeature : getObservableFeatures()) {
			List<String> values = observableFeature.getValues();
			if (values.size() < 2) {
				throw new LLException("Invalid features! Each feature must have at least two possible values!");
			}
			for (String value : values) {
				if(values.indexOf(value)!=values.lastIndexOf(value)){
					throw new LLException("Invalid feature values! They must be unique!");
				}
			}
			
			for (int i= getObservableFeatures().indexOf(observableFeature)+1; i < getObservableFeatures().size(); i++) {
				if(observableFeature.getName().equals(getObservableFeatures().get(i).getName())){
					throw new LLException("Invalid features! They must have unique names!");
				}
			}
		}
		if(observableFeatures.isEmpty()){
			throw new LLException("Environment must contain at least one feature!");
		}
	}
	
	 

	public OWLOntology getCommonKnowledgeFeaturesOntology() {
		return commonKnowledgeFeaturesOntology;
	}

	public List<ObservableFeature> getObservableFeatures() {
		return observableFeatures;
	}

	public void setObservableFeatures(List<ObservableFeature> observableFeatures) {
		this.observableFeatures = observableFeatures;
	}


}
