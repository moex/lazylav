/*
 * Copyright (C) INRIA, 2016-2017
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 */

package fr.inria.exmo.lazylavender.env.features;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ObservableFeature implements Serializable {

	private static final long serialVersionUID = -5951665481336254268L;

	private String name;

	private List<String> values = new ArrayList<>();

	public ObservableFeature(String name, List<String> values) {
		this.setName(name);
		this.setValues(values);
	}
	
	public ObservableFeature() {
	}

	public List<String> getValues() {
		return values;
	}

	public void setValues(List<String> values) {
		this.values = values;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
