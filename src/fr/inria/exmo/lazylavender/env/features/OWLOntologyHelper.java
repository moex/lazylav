/*
 * Copyright (C) INRIA, 2016-2017
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 */

package fr.inria.exmo.lazylavender.env.features;

import fr.inrialpes.exmo.align.impl.renderer.RDFRendererVisitor;

import org.semanticweb.owl.align.Alignment;
import org.semanticweb.owl.align.AlignmentException;

import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.io.StringDocumentTarget;
import org.semanticweb.owlapi.model.AddAxiom;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLEntity;
import org.semanticweb.owlapi.model.OWLEquivalentClassesAxiom;
import org.semanticweb.owlapi.model.OWLObjectSomeValuesFrom;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.model.OWLOntologyStorageException;
import org.semanticweb.owlapi.util.OWLOntologyMerger;
import org.semanticweb.owlapi.util.OWLOntologyWalker;
import org.semanticweb.owlapi.util.OWLOntologyWalkerVisitor;

import java.util.Collections;
import java.util.Set;
import java.util.Properties;

import java.io.File;
import java.io.PrintWriter;
import java.io.OutputStream;
import java.io.FileOutputStream;
import java.io.BufferedWriter;
import java.io.OutputStreamWriter;
import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.inria.exmo.lazylavender.model.LLException;

public class OWLOntologyHelper {

    private final static Logger logger = LoggerFactory.getLogger(OWLOntologyHelper.class);

    public static OWLEntity getEntityByURI(OWLOntology ontology, String uri) {
	OWLEntity next = null;
	Set<OWLEntity> entitiesInSignature = ontology.getEntitiesInSignature(IRI.create(uri));
	if (entitiesInSignature.size() != 1) {
	    logger.warn("entities found for URI [{}] : [{}]!", uri, entitiesInSignature );
	} else {
	    next = entitiesInSignature.iterator().next();
	}
	return next;
    }

    public static void addConjunctionClass(OWLOntology onto, OWLClass categoryClass, Set<OWLClassExpression> classExpressions) {
	OWLOntologyManager manager = onto.getOWLOntologyManager();
	OWLDataFactory dataFactory = manager.getOWLDataFactory();
	OWLClassExpression conjunctionClass = dataFactory.getOWLObjectIntersectionOf(classExpressions);
	OWLEquivalentClassesAxiom owlEquivalentClassesAxiom = dataFactory.getOWLEquivalentClassesAxiom(categoryClass, conjunctionClass);
	manager.addAxiom(onto, owlEquivalentClassesAxiom);
    }

    public static void addSubClass(OWLOntology onto, OWLClass sub, OWLClassExpression sup) {
	OWLOntologyManager manager = onto.getOWLOntologyManager();
	OWLDataFactory dataFactory = manager.getOWLDataFactory();
	OWLAxiom axiom = dataFactory.getOWLSubClassOfAxiom(sub, sup);
	manager.applyChange(new AddAxiom(onto, axiom));
    }

    public static OWLOntology mergeOntologies(String mergedOntologyIri, OWLOntologyManager manager) throws LLException {
	OWLOntologyMerger merger = new OWLOntologyMerger(manager);
	IRI mergedOntologyIRI = IRI.create(mergedOntologyIri);
	OWLOntology merged;
	try {
	    merged = merger.createMergedOntology(manager, mergedOntologyIRI);
	} catch (OWLOntologyCreationException e) {
	    throw new LLException("Error during ontology merging", e);
	}
	return merged;
    }

    public static void printOntology(OWLOntology o) throws LLException {
	OWLOntologyManager manager = o.getOWLOntologyManager();
	StringDocumentTarget target = new StringDocumentTarget();
	try {
	    manager.saveOntology(o, target);
	    System.out.println(target);
	} catch (OWLOntologyStorageException e) {
	    throw new LLException("Error while printing ontology", e);
	}
    }

    public static String getOntologyToString(OWLOntology o) throws LLException {
	OWLOntologyManager manager = o.getOWLOntologyManager();
	StringDocumentTarget target = new StringDocumentTarget();
	try {
	    manager.saveOntology(o, target);
	} catch (OWLOntologyStorageException e) {
	    throw new LLException("Error while printing ontology", e);
	}
	return target.toString();
    }

    public static void saveOntologyToFile(OWLOntology o, String location, String docname) throws LLException {
	// FIXME param file path
	File file = new File(location  + docname + ".owl");
	saveOntologyToFile( o, file );
    }

    public static void saveOntologyToFile(OWLOntology o, File file) throws LLException {
	OWLOntologyManager manager = o.getOWLOntologyManager();
	logger.trace("Ontology saved at location {}", file );
	try {
	    manager.saveOntology(o, IRI.create(file.toURI()));
	} catch (OWLOntologyStorageException e) {
	    throw new LLException("Error while printing ontology [" + o + "] to file [" + file + "] !", e);
	}
    }

    public static void saveAlignmentToFile( Alignment al, String location, String docname ) throws LLException {
	// Create renderer
	PrintWriter writer = null;
	try {
	    OutputStream stream = new FileOutputStream( "/tmp/align.rdf" );
	    writer = new PrintWriter (
				      new BufferedWriter(
							 new OutputStreamWriter( stream, "UTF-8" )), true);
	    RDFRendererVisitor renderer = new RDFRendererVisitor( writer );
	    renderer.init( new Properties() );
	    al.render( renderer );
	} catch (FileNotFoundException fnfex) {
	    throw new LLException( "Cannot open file", fnfex );
	} catch (UnsupportedEncodingException ueex) {
	    throw new LLException( "Bad encoding", ueex );
	} catch (AlignmentException alex) {
	    throw new LLException( "Error rendering", alex );
	} finally {
	    writer.flush();
	    writer.close();
	}
    }

    public static void printAxioms(OWLOntology owlOntology) {
	// Create the walker
	OWLOntologyWalker walker = new OWLOntologyWalker(Collections.singleton(owlOntology));
	// Now ask our walker to walk over the ontology
	OWLOntologyWalkerVisitor<Object> visitor = new OWLOntologyWalkerVisitor<Object>(walker) {
	    @Override
	    public Object visit(OWLObjectSomeValuesFrom desc) {
		System.out.println(desc);
		System.out.println(" " + getCurrentAxiom());
		return null;
	    }
	};
	walker.walkStructure(visitor);
    }
	
    public static OWLOntology loadOntologyFromFile(String filePath) throws LLException{
	OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
	File file = new File(filePath);
	OWLOntology ontology;
	try {
	    ontology = manager.loadOntologyFromOntologyDocument(file);
	} catch (OWLOntologyCreationException e) {
	    throw new LLException("Error while loading ontology from file "+filePath);
	}
	//IRI documentIRI = manager.getOntologyDocumentIRI(localPizza);
	return ontology;
    }
}
