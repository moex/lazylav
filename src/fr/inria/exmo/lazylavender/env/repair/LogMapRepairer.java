/*
 * Software released in the public domain with the intent that it is integrated in LogMap
 * J. Euzenat
 * 2014-2015, 2017
 */

// Please change this package name to one of yours
package fr.inria.exmo.lazylavender.env.repair;

import java.io.PrintStream;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;

import org.semanticweb.owl.align.Alignment;
import org.semanticweb.owl.align.AlignmentException;
import org.semanticweb.owl.align.Cell;
import org.semanticweb.owl.align.Relation;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLOntology;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.inrialpes.exmo.align.impl.AbstractRepairer;
import fr.inrialpes.exmo.align.impl.ObjectAlignment;
import fr.inrialpes.exmo.align.impl.rel.EquivRelation;
import fr.inrialpes.exmo.align.impl.rel.SubsumeRelation;
import fr.inrialpes.exmo.align.impl.rel.SubsumedRelation;
import uk.ac.ox.krr.logmap2.LogMap2_RepairFacility;
import uk.ac.ox.krr.logmap2.mappings.objects.MappingObjectStr;

public class LogMapRepairer extends AbstractRepairer {
    final static Logger logger = LoggerFactory.getLogger( LogMapRepairer.class );

    public Alignment repair( Alignment alinit, Properties param ) throws AlignmentException {
	boolean extractModules = false;  //If ontology modules are extracted before the repair
	boolean satCheck = false; //If the repair process is performed in two steps
	// Parse parameters
	if ( param != null ) {
	    if ( param.getProperty( "postSATCheck" ) != null && !"false".equals(param.getProperty( "postSATCheck" )) )
		satCheck = true;
	    if ( param.getProperty( "extractModules" ) != null && !"false".equals(param.getProperty( "extractModules" )) )
		extractModules = true;
	}
	// load the two ontologies that have been matched
        // ** Other arangements may be taked, e.g., the type may be URI
        if ( !(alinit.getOntology1() instanceof OWLOntology && alinit.getOntology2() instanceof OWLOntology ) ) {
	    throw new AlignmentException( "Can only repair OWLOntology" );
	}
	OWLOntology onto1 = (OWLOntology)alinit.getOntology1();
	OWLOntology onto2 = (OWLOntology)alinit.getOntology2();
	Set<MappingObjectStr> input_mappings = convertAlignment( alinit );
        // perform repair
	// ...silently...
	PrintStream sysout = System.out;
	try {
	    System.setOut(new PrintStream(System.out){
		    public void print(String s){ logger.trace(s); }
		    public void println(String s){ logger.trace(s); }
		});
	    LogMap2_RepairFacility logmap2_repair = 
		new LogMap2_RepairFacility( onto1, onto2, input_mappings, extractModules, satCheck);
	    // extract repaired alignment
	    return extractAlignment( alinit, logmap2_repair.getCleanMappings() );
	} catch (Exception ex ) {
	    throw new AlignmentException( "Error during LogMap repair", ex );
	} finally {
	    System.setOut( sysout );
	}
    }

    /**
     * Statically convert alignments to LogMap format
     */
    public static Set<MappingObjectStr> convertAlignment( Alignment alinit ) throws AlignmentException {
	Set<MappingObjectStr> mappings = new HashSet<MappingObjectStr>();
	for ( Cell c : alinit ) {
	    try {
		// Convert correspondences into MappingObjectStr
		mappings.add(new MappingObjectStr(c.getObject1AsURI(alinit).toString(),
						  c.getObject2AsURI(alinit).toString(),
						  c.getStrength(),
						  translateRelation( c.getRelation() ),
						  MappingObjectStr.CLASSES
						  )
			     );
	    } catch (AlignmentException alex) {
		logger.debug( "IGNORED Alignment exception: {}", alex );
	    }
	}
	return mappings;
    }

    protected static int translateRelation( Relation rel ) throws AlignmentException {
	if ( rel instanceof EquivRelation ) return MappingObjectStr.EQ;
	//else if ( rel instanceof IncompatRelation ) return ;
	else if ( rel instanceof SubsumeRelation ) return MappingObjectStr.SUP;
	else if ( rel instanceof SubsumedRelation ) return MappingObjectStr.SUB;
	else throw new AlignmentException( "Cannot translate relation "+rel );
    }

    /**
     * Statically extract alignments from LogMap format
     */
    public static Alignment extractAlignment( Alignment alinit, Set<MappingObjectStr> mappings ) throws AlignmentException {
	ObjectAlignment al = new ObjectAlignment();
	OWLOntology onto1 = (OWLOntology)alinit.getOntology1();
	OWLOntology onto2 = (OWLOntology)alinit.getOntology2();
	al.init( alinit.getOntology1URI(), alinit.getOntology2URI() );
	for ( MappingObjectStr map : mappings ) {
	    //getTypeOfMapping() (useful for decoding the IRI into a OWL object...
	    String rel = "=";
	    switch ( map.getMappingDirection() ) {
	    case MappingObjectStr.EQ: rel = "="; break;
	    case MappingObjectStr.SUB: rel = "<"; break;
	    case MappingObjectStr.SUP: rel = ">"; break;
	    }
	    try {
		al.addAlignCell( getEntity( onto1, map.getIRIStrEnt1() ),
				 getEntity( onto2, map.getIRIStrEnt2() ),
				 rel,
				 map.getConfidence() );
	    } catch ( Exception ex) {
		logger.debug( "IGNORED exception: {} cannot deal with cell", ex );
	    }
	}
	return al;
    }

    private static Object getEntity( OWLOntology onto, String uri ) throws Exception { //NoSuchElementException
	return onto.getEntitiesInSignature( IRI.create( uri ) ).iterator().next();
    }

    //public OntologyNetwork repair( OntologyNetwork network, Properties param ) throws AlignmentException {
    //	throw new AlignmentException( "Not implemented yet" );
    //}
}
