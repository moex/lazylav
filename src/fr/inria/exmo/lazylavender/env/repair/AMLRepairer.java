/*
 * Software released in the public domain with  the intent that it is integrated in AML
 * J. Euzenat
 * 2015, 2017
 */


// Please change this package name to one of yours
package fr.inria.exmo.lazylavender.env.repair;

import java.util.Properties;

import org.semanticweb.owl.align.Alignment;
import org.semanticweb.owl.align.AlignmentException;
import org.semanticweb.owl.align.Cell;
import org.semanticweb.owl.align.OntologyNetwork;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLOntology;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import aml.AML;
//import aml.match.Alignment;
import aml.match.Mapping;
import aml.ontology.Ontology;
import aml.settings.MappingRelation;
import fr.inrialpes.exmo.align.impl.AbstractRepairer;
import fr.inrialpes.exmo.align.impl.ObjectAlignment;

public class AMLRepairer extends AbstractRepairer {
    final static Logger logger = LoggerFactory.getLogger( AMLRepairer.class );

    public Alignment repair( Alignment alinit, Properties param ) throws AlignmentException {
	// Parse parameters
	// load the two ontologies that have been matched
	OWLOntology onto1 = null;
	OWLOntology onto2 = null;
        // ** Other arangements may be taked, e.g., the type may be URI
	if ( alinit.getOntology1() instanceof OWLOntology && alinit.getOntology2() instanceof OWLOntology ) {
	    onto1 = (OWLOntology)alinit.getOntology1();
	    onto2 = (OWLOntology)alinit.getOntology2();
	} else {
	    throw new AlignmentException( "Can only repair OWLOntology" );
	}
	logger.debug( "#1: Onto1: |{}| Onto2: |{}|", onto1, onto2 );
	AML aml = AML.getInstance();
	// ** Here taking the arguments from the parameters if they exists would be best
	Ontology s = new Ontology( onto1, true);
	Ontology t = new Ontology( onto2, true);
	aml.match.Alignment a = new aml.match.Alignment();
	logger.debug( "#2: Onto1: |{}| Onto2: |{}|", onto1, onto2 );
	
	for( Cell c : alinit ) {
	    try {
		a.add( c.getObject1AsURI(alinit).toString(),
		       c.getObject2AsURI(alinit).toString(), 
		       c.getStrength(),
		       MappingRelation.parseRelation( c.getRelation().getRelation() ) );
	    } catch (AlignmentException alex) {
		logger.debug( "IGNORED Alignment exception: {}", alex );
	    }
	}
	aml.setOntologies( s, t );
	aml.setAlignment( a );
	logger.debug( "#3: Onto1: |{}| Onto2: |{}|", onto1, onto2 );
	// Repair
        // ** no arguments
	aml.repair();
	logger.debug( "#4: Onto1: |{}| Onto2: |{}|", onto1, onto2 );
	// Output it
	try {
	    ObjectAlignment al = new ObjectAlignment();
	    al.init( alinit.getOntology1URI(), alinit.getOntology2URI() );
	    for( Mapping m : aml.getAlignment() ) {
		try { 
		    logger.debug( "URI1: |{}| URI2: |{}|", m.getSourceURI(), m.getTargetURI() );
		    al.addAlignCell( getEntity( onto1, m.getSourceURI().toString() ),
				     getEntity( onto2, m.getTargetURI().toString() ),
				     m.getRelationship().toString(),
				     m.getSimilarity() );
		} catch ( Exception ex) {
		    logger.debug( "IGNORED exception: {} cannot deal with cell", ex );
		}
	    }
	    return al;
	} catch (AlignmentException alex) {
	    logger.debug( "IGNORED Alignment exception: {}", alex );
	    return (Alignment)null;
	}
    }

    public OntologyNetwork repair( OntologyNetwork network, Properties param ) throws AlignmentException {
	throw new AlignmentException( "Not implemented yet" );
    }

    private Object getEntity( OWLOntology onto, String uri ) throws Exception { //NoSuchElementException
	logger.debug( "Onto: |{}| URI: |{}|", onto, uri );
	return onto.getEntitiesInSignature( IRI.create( uri ) ).iterator().next();
    }

}
