/*
 * Software released in the public domain with the intent that it is integrated in Alcomo
 * J. Euzenat
 * 2014-2015, 2017
 */

// Please change this package name to one of yours
package fr.inria.exmo.lazylavender.env.repair;

import java.util.HashSet;
import java.util.Properties;
import java.util.Set;

import org.semanticweb.owl.align.Alignment;
import org.semanticweb.owl.align.AlignmentException;
import org.semanticweb.owl.align.Cell;
import org.semanticweb.owl.align.Relation;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLOntology;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.unima.alcomox.ExtractionProblem;
import de.unima.alcomox.Settings;
import de.unima.alcomox.exceptions.AlcomoException;
import de.unima.alcomox.exceptions.CorrespondenceException;
import de.unima.alcomox.exceptions.PCFException;
import de.unima.alcomox.mapping.Correspondence;
import de.unima.alcomox.mapping.Mapping;
import de.unima.alcomox.mapping.SemanticRelation;
import de.unima.alcomox.ontology.IOntology;
import fr.inrialpes.exmo.align.impl.AbstractRepairer;
import fr.inrialpes.exmo.align.impl.ObjectAlignment;
import fr.inrialpes.exmo.align.impl.rel.EquivRelation;
import fr.inrialpes.exmo.align.impl.rel.IncompatRelation;
import fr.inrialpes.exmo.align.impl.rel.SubsumeRelation;
import fr.inrialpes.exmo.align.impl.rel.SubsumedRelation;

public class AlcomoRepairer extends AbstractRepairer {
    final static Logger logger = LoggerFactory.getLogger( AlcomoRepairer.class );

    public void init ( Properties param ) {
	if ( param == null ) return;
	if ( param.getProperty( "reasonner" ) != null ) {
	    String val = param.getProperty( "reasonner" );
	    if ( val.equals( "hermit" ) ) {
		Settings.BLACKBOX_REASONER = Settings.BlackBoxReasoner.HERMIT;
	    } else if ( val.equals( "pellet" ) ) {
		Settings.BLACKBOX_REASONER = Settings.BlackBoxReasoner.PELLET;
	    } else {
		logger.debug( "IGNORED reasoner parameter {} (using {})", val, Settings.BLACKBOX_REASONER );
	    }
	}
    }

    public Alignment repair( Alignment alinit, Properties param ) throws AlignmentException {
	// This is the optimal solution (from the Usage page of Alcomo)
	int entities = ExtractionProblem.ENTITIES_CONCEPTSPROPERTIES;
	int method = ExtractionProblem.METHOD_OPTIMAL;
	int completeness = ExtractionProblem.REASONING_COMPLETE;
	// Parse parameters
	if ( param != null ) {
	    if ( param.getProperty( "entities" ) != null ) {
		String val = param.getProperty( "entities" );
		if ( val.equals( "concepts" ) ) {
		    entities = ExtractionProblem.ENTITIES_ONLYCONCEPTS;
		} else if ( val.equals( "conceptproperties" ) ) {
		    entities = ExtractionProblem.ENTITIES_CONCEPTSPROPERTIES;
		} else {
		    logger.debug( "IGNORED entities parameter {} (using default: {})", val, entities );
		}
	    }
	    if ( param.getProperty( "optimality" ) != null ) {
		String val = param.getProperty( "optimality" );
		if ( val.equals( "local" ) ) {
		    method = ExtractionProblem.METHOD_GREEDY;
		} else if ( val.equals( "global" ) ) {
		    method = ExtractionProblem.METHOD_OPTIMAL;
		} else if ( val.equals( "greedy" ) ) {
		    method = ExtractionProblem.METHOD_GREEDY;
		} else if ( val.equals( "greedymin" ) ) {
		    method = ExtractionProblem.METHOD_GREEDY_MINIMIZE;
		} else if ( val.equals( "globaliso" ) ) {
		    method = ExtractionProblem.METHOD_OPTIMAL_HUN;
		} else {
		    logger.debug( "IGNORED optimality parameter {} (using default: {})", val, method );
		}
	    }
	    if ( param.getProperty( "completeness" ) != null ) {
		String val = param.getProperty( "completeness" );
		if ( val.equals( "complete" ) ) {
		    completeness = ExtractionProblem.REASONING_COMPLETE;
		} else if ( val.equals( "efficient" ) ) {
		    completeness = ExtractionProblem.REASONING_EFFICIENT;
		} else if ( val.equals( "bruteforce" ) ) {
		    completeness = ExtractionProblem.REASONING_BRUTEFORCE;
		} else {
		    logger.debug( "IGNORED completeness parameter {} (using default: {})", val, completeness );
		}
	    }
	}
	try {
	    // load the two ontologies that have been matched
	    ExtractionProblem ep = new ExtractionProblem( entities, method, completeness );
	    if ( alinit.getOntology1() instanceof OWLOntology && alinit.getOntology2() instanceof OWLOntology ) {
		ep.bindSourceOntology( new IOntology( (OWLOntology)alinit.getOntology1() ) );
		ep.bindTargetOntology( new IOntology( (OWLOntology)alinit.getOntology2() ) );
	    } else {
		throw new AlignmentException( "Can only repair OWLOntology" );
	    }
	    // load the alignment
	    ep.bindMapping( convertAlignment( alinit ) );
	    // proceed to repair
	    ep.init();
	    ep.solve();
	    // Extract the resulting alignment
	    return extractAlignment( alinit, ep );
	} catch (PCFException pcfeex) {
	    //logger.debug( "IGNORED Alcomo PCFE exception: {}", pcfeex );
	    throw new AlignmentException( "Alcomo repairing exception", pcfeex );
	} catch (AlcomoException cmex) {
	    //logger.debug( "IGNORED Alcomo exception: {}", cmex );
	    throw new AlignmentException( "Alcomo repairing exception", cmex );
	}
    }

    /**
     * Statically convert alignments to Alcomo format
     */
    public static Mapping convertAlignment( Alignment alinit ) throws AlignmentException {
	Set<Correspondence> corresps = new HashSet<Correspondence>();
	for ( Cell c : alinit ) {
	    try {
		corresps.add( new Correspondence( c.getObject1AsURI(alinit).toString(),
						  c.getObject2AsURI(alinit).toString(),
						  translateRelation( c.getRelation() ),
						  c.getStrength() ) );
	    } catch (CorrespondenceException corex) {
		logger.debug( "IGNORED Correspondence exception: {}", corex );
	    }
	}
	return new Mapping( corresps );
    }

    /**
     * Statically extract alignments from Alcomo format
     */
    public static Alignment extractAlignment( Alignment alinit, ExtractionProblem ep ) throws AlignmentException {
	ObjectAlignment al = new ObjectAlignment();
	OWLOntology onto1 = (OWLOntology)alinit.getOntology1();
	OWLOntology onto2 = (OWLOntology)alinit.getOntology2();
	al.init( alinit.getOntology1URI(), alinit.getOntology2URI() );
	try {
	    for ( Correspondence map : ep.getExtractedMapping() ) {
		String rel = "=";
		switch ( map.getRelation().getType() ) {
		case SemanticRelation.EQUIV : rel = "="; break;
		case SemanticRelation.SUB : rel = "<"; break;
		case SemanticRelation.SUPER : rel = ">"; break;
		case SemanticRelation.DIS : rel = "%"; break;
		}
		try {
		    al.addAlignCell( getEntity( onto1, map.getSourceEntityUri() ),
				     getEntity( onto2, map.getTargetEntityUri() ),
				     rel,
				     map.getConfidence() );
		} catch ( Exception ex) {
		    logger.debug( "IGNORED exception: {} cannot deal with cell", ex );
		}
	    }
	}  catch (PCFException pcfeex) { // will return an empty alignment
	    logger.debug( "IGNORED Alcomo PCFE exception: {}", pcfeex );
	}
	return al;
    }

    protected static SemanticRelation translateRelation( Relation rel ) throws AlignmentException, CorrespondenceException {
	if ( rel instanceof EquivRelation ) return new SemanticRelation( SemanticRelation.EQUIV );
	else if ( rel instanceof IncompatRelation ) return new SemanticRelation( SemanticRelation.DIS );
	else if ( rel instanceof SubsumeRelation ) return new SemanticRelation( SemanticRelation.SUPER );
	else if ( rel instanceof SubsumedRelation ) return new SemanticRelation( SemanticRelation.SUB );
	else throw new AlignmentException( "Cannot translate relation "+rel );
    }

    private static Object getEntity( OWLOntology onto, String uri ) throws Exception { //NoSuchElementException
	return onto.getEntitiesInSignature( IRI.create( uri ) ).iterator().next();
    }

    //public OntologyNetwork repair( OntologyNetwork network, Properties param ) throws AlignmentException {
    //	throw new AlignmentException( "Not implemented yet" );
    //}

}
