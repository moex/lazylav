/*
 * Copyright (C) INRIA, 2019
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 */

package fr.inria.exmo.lazylavender.env;

import java.util.Properties;
import java.util.BitSet;

import fr.inria.exmo.lazylavender.model.LLException;

/**
 * This class defines agents environment
 * It can be initialised and logged.
 *
 */

public interface LLEnvironment {

    public LLEnvironment init() throws LLException;

    public LLEnvironment init( Properties param ) throws LLException;

    // JE 2019: In principle, onely one single logging function should be enough
    public void logInit() throws LLException;

    public void logReference() throws LLException;

    public void logFinal() throws LLException;

    public BitSet parseInstance( String text );

    public BitSet generateInstance();

}
