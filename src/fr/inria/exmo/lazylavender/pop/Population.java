/*
 * Copyright (C) INRIA, 2019-2021, 2024
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 */

package fr.inria.exmo.lazylavender.pop;

import fr.inria.exmo.lazylavender.model.LLException;
import fr.inria.exmo.lazylavender.agent.LLAgent;
import fr.inria.exmo.lazylavender.env.LLEnvironment;

import fr.inria.exmo.lazylavender.agent.PopulationAlignmentAdjustingAgent;
import fr.inria.exmo.lazylavender.env.NOOEnvironment;
import fr.inria.exmo.lazylavender.logger.ActionLogger;

import org.semanticweb.owl.align.Alignment;
import org.semanticweb.owl.align.AlignmentException;
import org.semanticweb.owl.align.Evaluator;
import org.semanticweb.owl.align.Cell;
import org.semanticweb.owl.align.Relation;
import fr.inrialpes.exmo.align.impl.rel.EquivRelation;
import fr.inrialpes.exmo.align.impl.rel.SubsumeRelation;
import fr.inrialpes.exmo.align.impl.rel.SubsumedRelation;
import fr.inrialpes.exmo.align.impl.ObjectAlignment;

import fr.inrialpes.exmo.align.impl.eval.SemPRecEvaluator;
import fr.inrialpes.exmo.align.impl.eval.PRecEvaluator;
import fr.inrialpes.exmo.align.impl.ObjectAlignment;

import fr.inrialpes.exmo.ontowrap.HeavyLoadedOntology;

import org.semanticweb.HermiT.Reasoner;

import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.IRI;
import fr.inria.exmo.lazylavender.env.features.OWLOntologyHelper;

import org.semanticweb.owlapi.reasoner.SimpleConfiguration;
import org.semanticweb.owlapi.reasoner.OWLReasoner;
import org.semanticweb.owlapi.reasoner.OWLReasonerFactory;
import org.semanticweb.owlapi.model.OWLDataFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Properties;
import java.util.Random;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java. util. Iterator;

import javax.swing.text.StyledEditorKit.AlignmentAction;

import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.net.URI;
import java.net.URISyntaxException;

/**
 * Describes a population of agents sharing the same ontology
 * but having their own alignments.
 */

public class Population {
    final static Logger logger = LoggerFactory.getLogger( Population.class );

    // From agent
    public NOOEnvironment env = null;
    public ActionLogger alog = null;

    // Ontology
    public URI ontologyURI = null;
    public OWLOntology ontology = null; // useless for the moment
    private OWLDataFactory factory = null;
    private OWLReasoner reasoner = null;
    private OWLClass top = null;

    // Agents
    /**
     * Number of agents/populations to play (-DnbAgents)
     */
    public int numberOfAgents = 4;
    
    protected LLAgent[] agents = null;

    private int id; // identifier of the population for the experiment
    private int envid; // identified of the first agent for the environment

    private Random rand = null;

    public enum SynchronisationMode { MAJORITY, GENERAL, SPECIFIC };
    protected SynchronisationMode syncMode  = SynchronisationMode.MAJORITY;

    // initialisation
    /**
     * Where (and if) to save data to
     */
    File refDir = null;
    File initDir = null;
    File finalDir = null;

    /**
     * Where (and if) to load data from
     */
    String loadDir = null;
    boolean loadAgents = false;
    boolean loadEnv = false; // Check if useful?

    public boolean syntacticEvaluation = false;

    //public Population() {}

    public Population( NOOEnvironment e, ActionLogger a, int rank, URI o ) {
	env = e;
	alog = a;
	id = rank;
	ontologyURI = o;
	rand = new Random(); // could use setSeed( long seed );
    }

    public void init( Properties params ) throws LLException {
	if ( params.getProperty( "nbAgents" ) != null ) {
	    numberOfAgents = Integer.parseInt( params.getProperty( "nbAgents" ) );
	} else { // Set the default for sub processes
	    params.setProperty( "nbAgents", Integer.toString( numberOfAgents ) );
	}
	String mod = params.getProperty( "synchronisationMode" );
	if ( mod == null ) mod = params.getProperty( "synchronizationMode" );
	if ( mod != null ) {
	    if ( mod.equals("general") ) syncMode  = SynchronisationMode.GENERAL;
	    else if ( mod.equals("specific") ) syncMode  = SynchronisationMode.SPECIFIC;
	    else if ( mod.equals("majority") ) syncMode  = SynchronisationMode.MAJORITY;
	    else logger.warn( "IGNORED: Unknown synchronisation mode: {}", mod );
	}
	syntacticEvaluation = ( params.getProperty( "syntactic" ) != null );
	// params/load
	if ( params.getProperty( "initDir" ) != null ) { // Ugly
	    initDir = new File( params.getProperty( "initDir" )+"/population", ""+envid );
	}
	if ( params.getProperty( "finalDir" ) != null ) {
	    finalDir = new File( params.getProperty( "finalDir" )+"/population", ""+envid );
	}
	// LOAD
	if ( params.getProperty( "loadRunDir" ) != null ) {
	    loadDir = params.getProperty( "loadRunDir" ) + "/init/";
	    if ( params.getProperty( "loadAgents" ) != null ) loadAgents = true;
	}

	// nbAgents
	// typeOfAgent (defined by the expe)
	agents = new LLAgent[ numberOfAgents ]; 
	envid = id*numberOfAgents;
	// get the ontology from the NOO (always, even when loaded)
	// Unclear that it should not be in the Population...
	ontology = env.getOntology( ontologyURI ).getOntology();
	// create the agents
	for ( int ag = 0; ag < numberOfAgents; ag++ ) {
	    agents[ag] = new PopulationAlignmentAdjustingAgent( env, alog, envid+ag, this );
	    agents[ag].init( params );
	}
	// intialise the ontology
	precomputeOntologies();
	// initialise the alignments
	for( LLAgent agent: agents ) {
	    ((PopulationAlignmentAdjustingAgent)agent).initAlignments();
	}
	initStore();
    }

    public LLAgent getRandomAgent() {
	return agents[ rand.nextInt( numberOfAgents ) ];
    }

    public LLAgent getAgent( int id ) {
	return agents[id-envid];
    }

    public int getId() {
	return id;
    }

    public URI getOntologyURI() {
	return ontologyURI;
    }

    public OWLOntology getOntology() {
	return ontology;
    }

    public OWLDataFactory getFactory() {
	return factory;
    }

    public OWLReasoner getReasoner() {
	return reasoner;
    }

    public OWLClass getTop() {
	return top;
    }

    public void precomputeOntologies() throws LLException {
	// get the ontology from the environment
	ontology = env.getOntology( ontologyURI ).getOntology();
	factory = ontology.getOWLOntologyManager().getOWLDataFactory();
	OWLReasonerFactory reasonerFactory = new Reasoner.ReasonerFactory();
	reasoner = reasonerFactory.createReasoner( ontology, new SimpleConfiguration() );
	reasoner.precomputeInferences();
	top = factory.getOWLClass(IRI.create( ontologyURI+"#top" ));
    }
    
    // This is cl1 subClassOf c2
    protected boolean isSubClassOf( OWLClass cl1, OWLClass cl2 ) {
	if ( cl1 == cl2 ) return true;
	if ( reasoner.getSubClasses( cl2, false ).getFlattened().contains( cl1 ) ) return true;
	else return false;
    }

    protected boolean isDisjoint( OWLClass cl1, OWLClass cl2 ) {
	if ( cl1 == cl2 ) return false;
	if ( reasoner.getDisjointClasses( cl2 ).getFlattened().contains( cl1 ) ) return true;
	else return false;
    }

    // cl1 and cl2 are disjoint classes
    protected OWLClass commonSuperClass( OWLClass cl1, OWLClass cl2 ) {
	Set<OWLClass> superClasses = reasoner.getSuperClasses( cl1, false ).getFlattened();
	// They cannot be explored in order! Then
	OWLClass sup = null;
	Set<OWLClass> supersup = null;
	for ( OWLClass sc : superClasses ) {
	    if ( isSubClassOf( cl2, sc ) ) {
		if ( sup == null || !supersup.contains( sc ) ) {
		    sup = sc;
		    supersup = reasoner.getSuperClasses( sc, false ).getFlattened();
		}
	    }
	}
	return sup;
    }

    // synchronisation
    public void synchronizeAlignments() throws LLException  {
    	for ( URI otherOntologyURI: env.getOntologies() ) {
	    if ( !otherOntologyURI.equals( ontologyURI ) ){ // JE: ==?
		logger.debug( "Synchronisation {} <<>> {}", ontologyURI, otherOntologyURI );
		HashMap<OWLClass, ArrayList<Cell>> targetCorrespondences = new HashMap<OWLClass, ArrayList<Cell>>();
		OWLOntology otherOntology = env.getOntology( otherOntologyURI ).getOntology();
		OWLDataFactory otherFactory = otherOntology.getOWLOntologyManager().getOWLDataFactory();
		OWLClass othertop = otherFactory.getOWLClass(IRI.create( otherOntologyURI +"#top" ));
		// For each agent: get the correct alignment and collect all correspondences
		for( LLAgent agent: agents ) {
		    Alignment alignment = ((PopulationAlignmentAdjustingAgent)agent).getDirectAlignment(otherOntologyURI);
		    if ( alignment != null ) {
			logger.debug( " Local to {}", agent );
            	    	for ( Cell c : alignment ) {
			    logger.debug("  -- {} {} {}", c.getObject1(), c.getRelation().getRelation(), c.getObject2());
			    OWLClass targetClass = (OWLClass)c.getObject2();
			    // To suppress
			    if ( ( c.getObject2() == othertop && c.getObject1() != top )
				 || ( c.getObject2() != othertop && c.getObject1() == top ) ) {
				System.err.println("***** Top not aligned with top in local\n");
			    }
			    ArrayList<Cell> corres;
			    if ( targetCorrespondences.containsKey(targetClass) ) {
				corres = targetCorrespondences.get(targetClass);
			    } else {
				corres = new ArrayList<Cell>();
				targetCorrespondences.put(targetClass, corres);
			    }
			    corres.add(c);
            	    	}
            	    }
            	}
		// Ready to synchronize
		ObjectAlignment consensusAlignment = new ObjectAlignment();
		try {
		    consensusAlignment.init( ontologyURI, otherOntologyURI );
		} catch ( AlignmentException alex ) {
		    throw new LLException( "Cannot initialize consensus alignment", alex );
		}
		try {
		    for ( OWLClass targetClass : targetCorrespondences.keySet() ) {
			logger.debug(" Target Class: {} going to sync", targetClass);
			// JE: apparently this decides what is the replacing correspondence
			OWLClass resultClass = synchronizeCorrespondences(targetCorrespondences.get(targetClass));
			if ( resultClass != null ) { //give it to all agents in the population
			    consensusAlignment.addAlignCell( resultClass, targetClass, "<", 1.0);
			}
			//else resultClass = null, then do nothing;
		    }
		} catch ( AlignmentException alex ) {
		    throw new LLException( "Cannot add consensus cell", alex );
		}
		logger.debug(" Resulting consensus:" );
		for ( Cell c : consensusAlignment ) {
		    logger.debug("  -- {} {} {}", c.getObject1(), c.getRelation().getRelation(), c.getObject2());
		    // To suppress
		    if ( ( c.getObject2() == othertop && c.getObject1() != top )
			 || ( c.getObject2() != othertop && c.getObject1() == top ) ) {
			System.err.println("***** Top not aligned with top in consensus\n");
		    }
		}
		// Agents can now integrate the result
		// There should be an abstraction...
		for ( LLAgent ag : agents ) {
		    ((PopulationAlignmentAdjustingAgent)ag).syncAlignment( otherOntologyURI, consensusAlignment );
		}
		// Store the consensus in the environment
		env.setAlignment( consensusAlignment );
	    }
    	}
    }

    // Merges correspondences arriving to the same object (o1)
    public OWLClass synchronizeCorrespondences( ArrayList<Cell> correspondences) {
    	OWLClass ClassToMakeCommon = null;
    	ArrayList<OWLClass> classes = new ArrayList<OWLClass>();
    	logger.debug( "The array list of classes for the target class, contains the following : ");
    	for ( Cell c : correspondences ) {
	    logger.debug("  -- {} {} {}", c.getObject1(), c.getRelation().getRelation(), c.getObject2());
	    // just to be sure, but not necessary
	    if ( c.getRelation() instanceof SubsumedRelation || c.getRelation() instanceof EquivRelation ) {
		classes.add( (OWLClass)c.getObject1() );
		logger.debug( "{}, {}",c.getObject1(), c.getRelation() );
	    }
    	}
    	if ( classes.size() > 1 ) {
	    if (syncMode == SynchronisationMode.MAJORITY) {
		int threshold = (correspondences.size())/2; // or (nbagents/2)+1
		int max = 1;
		for ( int i = 0 ; i < classes.size(); i++ ) {
		    int counter = 1;
		    for ( int j = i+1; j < classes.size(); j++ ) {
			if ( classes.get(i) == classes.get(j) ) counter ++;
		    }
		    if ( (counter > threshold) && (threshold > 0) ) {
			if ( counter > max ) { max = counter; ClassToMakeCommon = classes.get(i); }
		    }
		    counter = 1;
		}
		logger.debug( "====================> MAJORITY DECIDED ClassToMakeCommon : {} ", ClassToMakeCommon);
	    } else if ( syncMode == SynchronisationMode.GENERAL ) {
		OWLClass LeastCommonSubsumer = classes.get(0);
		for ( int j=classes.size()-1; j > 0; j-- ) {
		    OWLClass cl = classes.get(j);
		    if ( cl != LeastCommonSubsumer ) {
			if ( isSubClassOf( LeastCommonSubsumer, cl ) ) {
			    LeastCommonSubsumer = cl;
			} else if ( !isSubClassOf( cl, LeastCommonSubsumer ) ) {
			    OWLClass sup = commonSuperClass( LeastCommonSubsumer, cl );
			    if ( sup == top ) { LeastCommonSubsumer = null; break; }
			    if ( sup != null ) LeastCommonSubsumer = sup;
			}	
		    }
		}
		ClassToMakeCommon = LeastCommonSubsumer;
		logger.debug( "====================> GENERAL DECIDED ClassToMakeCommon : {}", ClassToMakeCommon );
	    } else { //syncMode == SynchronisationMode.SPECIFIC
		OWLClass GreatestCommonSubsumee = classes.get(0);
		for ( int j=classes.size()-1; j > 0; j-- ) {
		    OWLClass cl = classes.get(j);
		    if ( cl != GreatestCommonSubsumee ) {
			if ( isSubClassOf( cl, GreatestCommonSubsumee ) ) {
			    GreatestCommonSubsumee = cl;
			} else if ( !isSubClassOf( GreatestCommonSubsumee, cl ) ) {
			    GreatestCommonSubsumee = null; break;
			}	
		    }
		}
		ClassToMakeCommon = GreatestCommonSubsumee;
		logger.debug( "====================> SPECIFIC DECIDED ClassToMakeCommon : {}", ClassToMakeCommon );
	    }
    	} else if ( classes.size() == 1 ) {
	    ClassToMakeCommon = classes.get(0);
	    logger.debug( "====================> SINGLE ClassToMakeCommon : {}", ClassToMakeCommon );
	}
	return ClassToMakeCommon;
    }
    
    // provide a new generation
    public void newGeneration() {
    }

    /**
     * Computing measures for the network
     * Everything is here cached...
     */
    private HashMap<URI,float[]> ontoMeasures;
    // Not sure that all are needed
    protected int size = 0;
    protected int inc = 1;
    protected int prec = 2;
    protected int rec = 3;
    protected int fmeas = 4;

    protected void initStore() {
	ontoMeasures = new HashMap<URI,float[]>();
	for ( URI u : env.getOntologies() ) {
	    if ( !ontologyURI.equals( u ) ) {
		float[] measures = new float[5];
		ontoMeasures.put( u, measures );
		updateStore( measures, u );
	    }
	}
    }

    // To do for the correct dependency
    public void updateStore( final URI u ) {
	updateStore( ontoMeasures.get( u ), u );
    }

    protected Alignment getReferenceAlignment( final URI src, final URI dst ) {
	Set<Alignment> alignments = env.getReferenceNetwork().getAlignments( src, dst );
	if ( alignments.isEmpty() ) return null;
	else return alignments.iterator().next(); // the first, but unique
    }
    
    public void updateStore( float[] measures, final URI u ) {
	// Measure size
	measures[size] = computeSizeAlignments( u );
	//
	logger.debug( "Updating store for population {} with URI {} of size {}", id, u, measures[size] );
	// Compute precision and recall
	int nbexpected = 0; // expected so far
	int nbfound = 0; // found so far
	int nbfoundentailed = 0;
	int nbexpectedentailed = 0;
	int nbcorrect = 0; // correct so far
	double incoherence = 0.;
	// get the reference
	Alignment ref = getReferenceAlignment( ontologyURI, u );
	if ( ref == null ) logger.error( "Cannot retrieve reference between {} and {}", ontologyURI, u );
	// for ( Cell c: ref ) {
	//     System.err.println( "    "+c.getObject1()+" "+c.getRelation().getRelation()+" "+c.getObject2() );
	// }
	boolean fromHere = true; // useless
	// for each agent
	for ( int ag = 0; ag < numberOfAgents; ag++ ) {
	    Alignment al = ((PopulationAlignmentAdjustingAgent)agents[ ag ]).getDirectAlignment( u );
	    if ( al == null ) logger.error( "Cannot retrieve alignment between {} and {} for agent", ontologyURI, u, ag );
	    // System.err.println( " Agent "+ag );
	    // for ( Cell c: al ) {
	    // 	System.err.println( "    "+c.getObject1()+" "+c.getRelation().getRelation()+" "+c.getObject2() );
	    // }
	    Evaluator eval = null;
	    // count values for prec/rec/fmeasure
	    try {
		if ( syntacticEvaluation ) {
		    eval = new PRecEvaluator( ref, al ).init();
		} else {
		    eval = new SemPRecEvaluator( ref, al ).init((Properties)null);
		}
		eval.eval( (Properties)null ) ;
		nbexpected += ((PRecEvaluator)eval).getExpected();
		if ( syntacticEvaluation ) {
		    nbfound += ((PRecEvaluator)eval).getFound();
		    nbcorrect += ((PRecEvaluator)eval).getCorrect();
		    //logger.trace("Prec/rec {} <=> {} == {} / {}", ontologyURI, u, ((PRecEvaluator)eval).getPrecision(), ((PRecEvaluator)eval).getRecall() );
		} else {
		    nbfound += ((SemPRecEvaluator)eval).getFound();
		    nbfoundentailed += ((SemPRecEvaluator)eval).getFoundEntailed();
		    nbexpectedentailed += ((SemPRecEvaluator)eval).getExpectedEntailed();
		}
	    } catch ( AlignmentException alex ) { // should not happen
		logger.warn( "IGNORED: AlignmentException", alex );
	    }
	    // compute incoherence
		// JE 2019: neutralized because it satures memory
		// It is to suspect that Alcomo launches too many threads about it
	    //try {
	    //incoherence += env.incoherenceDegree( (HeavyLoadedOntology<Object>)((ObjectAlignment)al).getOntologyObject1(), (HeavyLoadedOntology<Object>)((ObjectAlignment)al).getOntologyObject2(), al );
	    //} catch ( AlignmentException alex ) { // should not happen
	    //logger.warn( "IGNORED: AlignmentException", alex );
	    //} catch ( LLException llex ) { // should not happen
	    //	logger.warn( "IGNORED: LLException", llex );
	    //}
	}
	// aggregate
	float precision = (float)0.;
	float recall = (float)0.;
	if ( syntacticEvaluation ) {
	    precision = (float)nbcorrect/nbfound;
	    recall = (float)nbcorrect/nbexpected;
	} else {
	    precision = (float)nbfoundentailed / (float)nbfound;
	    recall = (float)nbexpectedentailed / (float)nbexpected;
	    // System.err.println( " [ "+nbfound+" ] "+nbexpectedentailed+" / "+nbexpected+" = "+recall );
	}
	measures[prec] = precision;
	measures[rec] = recall;
	// Compute Fmeasure
	measures[fmeas] = (2 * precision * recall) / (precision + recall);
	// Compute incoherence (average)
	// Generate a NaN because Inc is not available
	//measures[inc] = (float)(incoherence/(float)numberOfAgents);
	measures[inc] = Float.NaN;
    }

    // Actually computes the incoherence rate
    // Used to 
    public float computeIncoherenceRate() {
	float result = (float)0.0;
	Set<URI> ontos = env.getOntologies();
	for ( URI u : ontos ) {
	    if ( !ontologyURI.equals( u ) ) {
		for ( int ag = 0; ag < numberOfAgents; ag++ ) {
		    try {
			ObjectAlignment al = (ObjectAlignment)((PopulationAlignmentAdjustingAgent)agents[ ag ]).getDirectAlignment( u );
			result += env.incoherenceDegree( (HeavyLoadedOntology<Object>)(al.getOntologyObject1()), (HeavyLoadedOntology<Object>)(al.getOntologyObject2()), al );
		    } catch ( AlignmentException alex ) { // should not happen
			logger.warn( "IGNORED: AlignmentException", alex );
		    } catch ( LLException llex ) { // should not happen
			logger.warn( "IGNORED: LLException in incoherenceDegree", llex );
		    }
		}
	    }
	}
	return result/(numberOfAgents*(ontos.size()-1)); // average
    }

    public float computeSizeAlignments( URI u ) {
	int localSize = 0;
	for ( int ag = 0; ag < numberOfAgents; ag++ ) {
	    localSize += ((PopulationAlignmentAdjustingAgent)agents[ ag ]).getDirectAlignment( u ).nbCells();
	}
	return localSize / numberOfAgents; // average size
    }

    // Aggregates value for all ontologies
    public float getPrecision() {
	float result = (float)0.0;
	Set<URI> ontos = env.getOntologies();
	for ( URI u : ontos ) {
	    if ( !ontologyURI.equals( u ) ) {
		result += ontoMeasures.get( u )[prec];
	    }
	}
	return result/(ontos.size()-1); // average
    }

    public float getRecall() {
	float result = (float)0.0;
	Set<URI> ontos = env.getOntologies();
	for ( URI u : ontos ) {
	    if ( !ontologyURI.equals( u ) ) {
		result += ontoMeasures.get( u )[rec];
	    }
	}
	return result/(ontos.size()-1); // average
    }
    public float getFMeasure() {
	float result = (float)0.0;
	Set<URI> ontos = env.getOntologies();
	for ( URI u : ontos ) {
	    if ( !ontologyURI.equals( u ) ) {
		result += ontoMeasures.get( u )[fmeas];
	    }
	}
	return result/(ontos.size()-1); // average
    }
    public float getIncoherenceRate() {
	float result = (float)0.0;
	Set<URI> ontos = env.getOntologies();
	for ( URI u : ontos ) {
	    if ( !ontologyURI.equals( u ) ) {
		result += ontoMeasures.get( u )[inc];
	    }
	}
	return result/(ontos.size()-1); // average
    }

    public float getSizeAlignments() {
	float result = (float)0.0;
	for ( URI u : env.getOntologies() ) {
	    if ( !ontologyURI.equals( u ) ) {
		result += ontoMeasures.get( u )[size];  // sum
	    }
	}
	return result;
    }

    // Save population
    public void save( File dir ) throws LLException {
	// Save the ontology
	try(  PrintWriter out = new PrintWriter( new File( dir, "sig.txt" ) )  ){
	    out.println( ontologyURI );
	    out.println( env.signatureToString( ontologyURI ) );
	} catch ( FileNotFoundException fnfex ) {
	    throw new LLException( "Cannot open file "+dir+"/sig.txt" );
	}
	// Use it because manager is not here (but could be at the environment)
	OWLOntologyHelper.saveOntologyToFile( ontology, new File( dir, "onto.owl" ) );
	// Save the agents...
	File agentsdir = new File( dir, "agent" );
	agentsdir.mkdir();
	for ( int ag = 0; ag < numberOfAgents; ag++ ) {
	    ((PopulationAlignmentAdjustingAgent)agents[ag]).save( agentsdir );
	}
    }
    
    // Load population
    public void load( File dir ) throws LLException {
	// Useless because everything is made in the environment
    }
    
}
