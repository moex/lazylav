/*

 * Copyright (C) INRIA, 2014-2020, 2022, 2024
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 */

package fr.inria.exmo.lazylavender.agent;

import fr.inria.exmo.lazylavender.model.LLException;
import fr.inria.exmo.lazylavender.expe.LLGame;
import fr.inria.exmo.lazylavender.expe.InstanceIdGame;
import fr.inria.exmo.lazylavender.expe.InstanceIdByAlignmentGame;
import fr.inria.exmo.lazylavender.env.NOOEnvironment;
import fr.inria.exmo.lazylavender.logger.ActionLogger;
import fr.inria.exmo.lazylavender.env.features.OWLOntologyHelper;

import br.uniriotec.ppgi.alignmentRevision.revision.RevisorInterface;

import fr.inrialpes.exmo.align.impl.rel.EquivRelation;
import fr.inrialpes.exmo.align.impl.rel.SubsumeRelation;
import fr.inrialpes.exmo.align.impl.rel.SubsumedRelation;
import fr.inrialpes.exmo.align.impl.ObjectAlignment;

import org.semanticweb.owl.align.Alignment;
import org.semanticweb.owl.align.Cell;
import org.semanticweb.owl.align.Relation;
import org.semanticweb.owl.align.AlignmentException;

import org.semanticweb.HermiT.Reasoner;

import org.semanticweb.owlapi.reasoner.SimpleConfiguration;
import org.semanticweb.owlapi.reasoner.OWLReasoner;
import org.semanticweb.owlapi.reasoner.OWLReasonerFactory;

import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.IRI;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.BitSet;
import java.util.Set;
import java.util.List;
import java.util.Arrays;
import java.util.Map;
import java.util.Map.Entry;
import java.util.HashMap;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.Vector;
import java.util.Properties;
import java.util.Random;
import java.lang.NumberFormatException;
import java.io.PrintWriter;
import java.io.FileNotFoundException;
import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;

public class AlignmentAdjustingAgent implements LLAgent {
    final static Logger logger = LoggerFactory.getLogger( AlignmentAdjustingAgent.class );

    public enum Explore { RANDOM, CURIOUS, CAUTIOUS, ULTRACAUTIOUS };
    public enum Expand { NONE, RANDOM, PROTECTED, CLEVER };
    public enum Mod { DONOTHING, DELETE, REPLACE, ADD, ADDJOIN, REFINE, REFADD };
    public enum Strengthen { NONE, MOSTSPECIFIC, RANDOM, MOSTGENERAL };

    public NOOEnvironment env = null;
    public ActionLogger alog = null;
    public int envid = 0;
    public OWLOntology ontology = null;
    public URI ontologyURI = null;
    public Alignment[] alignments = null;

    protected Properties parameters;
    
    // Action parameters
    protected Explore explorationmod = null;
    protected Mod revisionmod = Mod.DELETE;
    private int immediateRatio = 100; // Percentage of correspondences systematically applied
    protected Expand expand = Expand.NONE;
    protected Strengthen strengthen = Strengthen.NONE;

    protected boolean memory = false; // Keep memory of discarded correspondences
    protected boolean nonred = false; // Non redundant alignments
    protected boolean complete = false; // Use the ontology of the other (simulate dialogue steps)

    protected boolean theoryRevision = false; // do we proceed to theory revision
    protected RevisorInterface revisor = null;
    
    protected boolean generative = false;

    // Must there be only one cell per class?
    protected boolean uniqueCellPerClass = false;

    // Cache of already discarded correspondences
    public IdentityHashMap<Alignment,Alignment> cellCache = null;

    // Cache of correspondences which succedded for objects
    protected Vector<Map<BitSet,Cell>> objectCache = null;
    protected Vector<BitSet> toremove = null;
    
    protected OWLDataFactory factory = null;
    protected OWLReasoner reasoner = null;
    protected OWLClass top = null;

    private Random rand = null;

    public int numberOfFeatures = 0;
    public Object ontologySignature = null;
    public boolean ontologyPermutation = true;

    /**
     * Where (and if) to save data to
     */
    File initDir = null;
    File finalDir = null;

    // It is now fully disconnected from the ontology URI which is given otherwise
    public AlignmentAdjustingAgent( NOOEnvironment e, ActionLogger a, int id ) { // To be generalised
		env = e;
		alog = a;
		envid = id;
		rand = new Random();
    }

    public AlignmentAdjustingAgent( NOOEnvironment e, ActionLogger a, int id, URI onto ) { // To be generalised
		this( e, a, id );
		ontologyURI = onto;
		ontologySignature = env.getSignature( onto );
    }

    public AlignmentAdjustingAgent init( Properties param ) {
		parameters = param;
		numberOfFeatures = env.numberOfFeatures();
		if ( param.getProperty( "initDir" ) != null ) { // Ugly
			initDir = new File( param.getProperty( "initDir" )+"/agent", ""+envid );
		}
		if ( param.getProperty( "finalDir" ) != null ) {
			finalDir = new File( param.getProperty( "finalDir" )+"/agent", ""+envid );
		}

		if ( param.getProperty( "tRevision" ) != null ) {
			theoryRevision = true;
			revisor = env.getRevisor();
		}
	
		String pexplore = param.getProperty( "explorationMode" );
		if ( pexplore != null ) {
			if ( pexplore.equals( "curious") ) explorationmod = Explore.CURIOUS;
			else if ( pexplore.equals( "cautious") ) explorationmod = Explore.CAUTIOUS;
			else if ( pexplore.equals( "ultracautious") ) explorationmod = Explore.ULTRACAUTIOUS;
			//else if ( pexplore.equals( "random") ) explorationmod = null;
		}
		if ( explorationmod != null ) {
			// JE: it is initialised on the number of ontologies
			// but filled based on the agentId... shoud work.
			objectCache = new Vector<Map<BitSet,Cell>>();
			for ( int i=0; i < env.numberOfOntologies; i++ ) {
				objectCache.add( i, new HashMap<BitSet,Cell>() );
			}
			toremove = new Vector<BitSet>();
		}	    

		if ( param.getProperty( "generative" ) != null ) {
			generative = true;
		}
		if ( param.getProperty( "noo" ) != null && param.getProperty( "noo" ).equals( "random" ) ) ontologyPermutation = false;
		String pexpand = param.getProperty( "expandAlignments" );
		if ( pexpand != null ) {
			if ( pexpand.equals( "random") ) expand = Expand.RANDOM;
			else if ( pexpand.equals( "protected") ) expand = Expand.PROTECTED;
			else if ( pexpand.equals( "clever") ) {
				expand = Expand.CLEVER;
				memory = true;
			}
		}
		String uniq = param.getProperty( "expansionPolicy" );
		if ( uniq != null && uniq.equals( "unique" ) ) {
			uniqueCellPerClass = true;
		}

		String ratio = param.getProperty( "immediateRatio" );
		if ( ratio != null ) {
			try {
				immediateRatio = Integer.parseInt( ratio );
				if ( immediateRatio < 0 || immediateRatio > 100 ) 
					throw new NumberFormatException();
			} catch (NumberFormatException nfex) {
				logger.warn( "bad immediateRatio: {} ignored; must be an integer between 0 and 100 (percentage)", ratio );
				immediateRatio = 100;
			}
		}

		if ( param.getProperty( "nonRedundancy" ) != null ) {
			nonred = true;
		}
		if ( param.getProperty( "ontoLookup" ) != null ) {
			complete = true;
		}

		String mod = param.getProperty( "revisionModality" );
		if ( mod != null ) {
			if ( mod.equals("delete") ) revisionmod = Mod.DELETE;
			else if ( mod.equals("nothing") ) revisionmod = Mod.DONOTHING;
			else if ( mod.startsWith("repl") ) revisionmod = Mod.REPLACE;
			else if ( mod.startsWith("refine") ) revisionmod = Mod.REFINE;
			else if ( mod.startsWith("refadd") ) revisionmod = Mod.REFADD;
			else if ( mod.equals("add") ) revisionmod = Mod.ADD;
			else if ( mod.equals("addjoin") ) revisionmod = Mod.ADDJOIN;
			else logger.warn( "IGNORED: Unknown revision modality: {}", mod );
		}

		String pstrengthen = param.getProperty( "strengthen" );
		if ( pstrengthen != null ) {
			if ( pstrengthen.equals( "mostspecific" ) ) {
				strengthen = Strengthen.MOSTSPECIFIC;
			} else if ( pstrengthen.equals( "mostgeneral" ) ) {
				strengthen = Strengthen.MOSTGENERAL;
			} else if ( pstrengthen.equals( "random" ) ) {
				strengthen = Strengthen.RANDOM;
			} else {
				logger.warn( "incorrect strengthening option: {} ignored", pstrengthen );
			}
			memory = true;
		}

		if ( param.getProperty( "memory" ) != null ) {
			memory = true;
		}

		if ( memory ) cellCache = new IdentityHashMap<Alignment,Alignment>();

		return this;
    }

    public void precomputeOntologies() throws LLException {
		// get the ontology from the environment
		ontology = env.getOntology( ontologyURI ).getOntology();
		factory = ontology.getOWLOntologyManager().getOWLDataFactory();
		OWLReasonerFactory reasonerFactory = new Reasoner.ReasonerFactory();
		reasoner = reasonerFactory.createReasoner( ontology, new SimpleConfiguration() );
		reasoner.precomputeInferences();
		top = factory.getOWLClass(IRI.create( ontologyURI+"#top" ));
    }

    public int getId() { return envid; }
    public URI getOntologyURI() { return ontologyURI; }

    public Alignment getDirectAlignment( URI uri ) {
		return env.getAlignment( ontologyURI, uri );
    }
    public Alignment getReverseAlignment( URI uri ) {
		return env.getAlignment( uri, ontologyURI );
    }

    public boolean quickCheck( AlignmentAdjustingAgent interloccutor, BitSet instance ) throws LLException {
		URI uri = interloccutor.getOntologyURI();
		Alignment al = getDirectAlignment( uri );
		boolean alignmentFromHere = true;
		if ( al == null ) {
			al = getReverseAlignment( uri );
			alignmentFromHere = false;
		}
		Cell c = interloccutor.applicableCorrespondence( al, instance, !alignmentFromHere );
		// Should not happen, no correspondence (counted false)
		if ( c == null ) return false;
		if ( alignmentFromHere ) {
			return !isDisjoint( classOf( instance ), (OWLClass)c.getObject1() );
		} else {
			return !isDisjoint( classOf( instance ), (OWLClass)c.getObject2() );
		}
    }

    // JE201905: Touched TOCHECK COMPLETELY (aFH=OK)
    public OWLClass identifyObject( LLGame game ) throws LLException {
		if ( !( game instanceof InstanceIdByAlignmentGame ) )
			throw new LLException( "Can only play InstanceIdByAlignmentGame "+game );
		InstanceIdByAlignmentGame theGame = (InstanceIdByAlignmentGame)game;
		URI targetOntologyURI = ((AlignmentAdjustingAgent)(theGame.getFirstAgent())).getOntologyURI();
		Alignment al = getDirectAlignment( targetOntologyURI );
		boolean alignmentFromHere = true;
		if ( al == null ) {
			al = getReverseAlignment( targetOntologyURI );
			alignmentFromHere = false;
		}
		theGame.setAlignment( al );
		OWLClass sourceClass = classOf( theGame.getInstance() );
		theGame.setSourceClass( sourceClass );
		Cell c = applicableCorrespondence( al, sourceClass, alignmentFromHere ); //SYM.
		if ( c == null ) {
			String logmessage = "non applicable";
			alog.logResult( "FAILURE", false, "(correspondence "+logmessage+")" );
			return null;
		}
		theGame.setCell( c );
		OWLClass targetClass = null;
		if ( alignmentFromHere ) {
			targetClass = (OWLClass)c.getObject2();
			logger.debug( "  {} <- {} - {}", targetClass, c.getRelation().inverse().getRelation(), c.getObject1() );
		} else {
			targetClass = (OWLClass)c.getObject1();
			logger.debug( "  {} <- {} - {}", targetClass, c.getRelation().getRelation(), c.getObject2() );
		}
		return targetClass;
    }

    /*
     * If the agent is self-motivated, it can choose which object to play with which agent
	 * JE: These manipulations around ontologySignature are doubtfull
	 * See intrinsic motivation instead
     */
    public double computeRisk(BitSet instance, LLGame game) {
    	AlignmentAdjustingAgent interloccutor = (AlignmentAdjustingAgent)((InstanceIdByAlignmentGame)game).getSecondAgent();
    	if(objectCache.get(interloccutor.getId()).get(instance) != null)
    		return 0;
    	
    	BitSet nemesis = (BitSet)instance.clone();
    	int i;
    	
    	if ( ontologyPermutation ) {
    	    i = ((Integer)ontologySignature).intValue();
    	    if(i == 0)
    	    	i=numberOfFeatures;
    	    else
    	    	i--;
    	} else { // random
    	    // Simpler because no modulo!
    		List<int[]> sig = Arrays.asList( ((int[])ontologySignature) );
            for (i=0; i < numberOfFeatures; i++ ) {
                if ( ! sig.contains( i ) ) break;
            }
    	}
    	nemesis.flip(i);
    	if(objectCache.get(interloccutor.getId()).get(nemesis) != null)
    		return 0.5;
    	else
    		return 1;
    }
    
    public double computeGain(BitSet instance, LLGame game) throws LLException {
    	if ( !( game instanceof InstanceIdByAlignmentGame ) )
    	    throw new LLException( "Can only play InstanceIdByAlignmentGame "+game );
    	
    	AlignmentAdjustingAgent interloccutor = (AlignmentAdjustingAgent)((InstanceIdByAlignmentGame)game).getSecondAgent();
    	if(objectCache.get(interloccutor.getId()).get(instance) != null)
    		return 0;
    	
    	URI uri = interloccutor.getOntologyURI();
    	Alignment al = getDirectAlignment( uri );
    	boolean alignmentFromHere = true;
    	if ( al == null ) {
    	    al = getReverseAlignment( uri );
    	    alignmentFromHere = false;
    	}
    	
    	OWLClass cl = classOf(instance);
    	int count = 0;
    	try {
    	    while ( cl != null ) {
    		if ( cl == top ) { // relaxed and nothing shadowed
    		    //logger.debug( "*** IMMEDIATE reached top: release {}", fetched );
    		    break;
    		}
    		Set<Cell> cs = null;
    		if ( alignmentFromHere ) cs = al.getAlignCells1( cl );
    		else cs = al.getAlignCells2( cl );
    		if ( cs != null ) {
    		    // get the class (only one valid in principle by injectivity)
    		    for ( Cell cell : cs ) {
    			Relation rel = cell.getRelation();
    			if ( rel instanceof EquivRelation || 
    			     ( !alignmentFromHere && rel instanceof SubsumeRelation ) ||
    			     ( alignmentFromHere && rel instanceof SubsumedRelation ) ) {
    				count++;
    			   
    			}
    		    }
    		}
    		
    		
    		cl = superClass( cl );
    	    }
    	} catch (AlignmentException alex) {
    	    throw new LLException( "Unexpected cell retrieval error", alex );
    	}
    	return 1.0/count;
    }
    
    public InstanceIdGame chooseGame( InstanceIdGame game ) {
		if ( explorationmod == null || explorationmod == Explore.RANDOM ) return game;
		// Esdras here below, you can add stuff
		// objectCache.get( second.getId() ).get( BitSet object ) --> une correspondance appliquée à cet objet (ou null)
		// in the end game.indiv = instance;
		// To retrieve the other agent AlignmentAdjustingAgent interloccutor = (AlignmentAdjustingAgent)((InstanceIdByAlignmentGame)game).getSecondAgent();
		logger.debug( "Here we have asked for exploration {} instead of object {} ", explorationmod, ((InstanceIdByAlignmentGame)game).getInstance() );
		//Compute risk for objects
	
		BitSet instance = new BitSet( numberOfFeatures );
		BitSet selectedinstance = new BitSet( numberOfFeatures );
		double bestRisk;
		double bestGain;
	
	
		if ( explorationmod == Explore.CAUTIOUS ) {
			bestRisk = 1.0;
			bestGain = 1.0;
			for(int i=0; i< Math.pow(2, numberOfFeatures); i++) {
				instance = new BitSet(numberOfFeatures);
				int val = i;
				int index = 0;
				while (val != 0) {
					if (val % 2 != 0) {
						instance.set(index);
					}
					++index;
					val = val >>> 1;
				}
	        
	        
				try {
					double risk = computeRisk(instance, game);
					double gain = computeGain(instance, game);
					if(risk < bestRisk && risk != 0) {
						selectedinstance = instance;
						bestRisk = risk;
						bestGain = gain;
					
					
					}else if(risk == bestRisk) {
						if(gain > bestGain) {
							selectedinstance = instance;
							bestRisk = risk;
							bestGain = gain;
						}
					}
				
				} catch (LLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
			
			}
			if(bestRisk == 0 || bestRisk == 1) {
				return game;
			}
			InstanceIdByAlignmentGame newgame = new InstanceIdByAlignmentGame(game.getFirstAgent(), game.getSecondAgent(), selectedinstance);
			return newgame;
		}else if(explorationmod == Explore.ULTRACAUTIOUS) {
			bestRisk = 1.0;
			bestGain = 1.0;
			for(int i=0; i< Math.pow(2, numberOfFeatures); i++) {
				instance = new BitSet(numberOfFeatures);
				int val = i;
				int index = 0;
				while (val != 0) {
					if (val % 2 != 0) {
						instance.set(index);
					}
					++index;
					val = val >>> 1;
				}
	        
	        
				try {
					double risk = computeRisk(instance, game);
					double gain = computeGain(instance, game);
					if(risk < bestRisk) {
						selectedinstance = instance;
						bestRisk = risk;
						bestGain = gain;
					
					}else if(risk == bestRisk) {
						if(gain > bestGain) {
							selectedinstance = instance;
							bestRisk = risk;
							bestGain = gain;
						}
					}
				
				} catch (LLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
			
			}
		
			InstanceIdByAlignmentGame newgame = new InstanceIdByAlignmentGame(game.getFirstAgent(), game.getSecondAgent(), selectedinstance);
			return newgame;
		
		}else { // CURIOUS
			bestRisk = 0.0;
			bestGain = 0.0;
			for(int i=0; i< Math.pow(2, numberOfFeatures); i++) {
				instance = new BitSet(numberOfFeatures);
				int val = i;
				int index = 0;
				while (val != 0) {
					if (val % 2 != 0) {
						instance.set(index);
					}
					++index;
					val = val >>> 1;
				}
	        
	        
				try {
					double risk = computeRisk(instance, game);
					double gain = computeGain(instance, game);
					if(gain > bestGain) {
						selectedinstance = instance;
						bestRisk = risk;
						bestGain = gain;
					}else if(gain == bestGain) {
						if(risk <= bestRisk && risk != 0) {
							selectedinstance = instance;
							bestRisk = risk;
							bestGain = gain;
						}
					}
				
				} catch (LLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
	       	}
			if(bestGain == 0) {
				return game;
			}
			InstanceIdByAlignmentGame newgame = new InstanceIdByAlignmentGame(game.getFirstAgent(), game.getSecondAgent(), selectedinstance);
			return newgame;

		}
	
    }
    
    public void playGame( int remaining, LLGame game ) throws LLException {
		if ( !( game instanceof InstanceIdByAlignmentGame ) )
			throw new LLException( "Can only play InstanceIdByAlignmentGame "+game );
		AlignmentAdjustingAgent interloccutor = (AlignmentAdjustingAgent)((InstanceIdByAlignmentGame)game).getSecondAgent();
		BitSet instance = ((InstanceIdByAlignmentGame)game).getInstance();
		OWLClass sourceClass = classOf( instance );
		logger.debug( "From: {} - [{}] -> {} : {}", envid, instance, ontologyURI, sourceClass );
	
		OWLClass targetClass = interloccutor.identifyObject( game );
		logger.debug( "  {} comparedTo {}", sourceClass, targetClass );
		// Compare the target class with our class for instance...
		if ( isDisjoint( sourceClass, targetClass ) ) {
			if ( theoryRevision ) addExample( false, remaining, interloccutor.classOf( instance ), sourceClass );
			if ( explorationmod != null ) {
				Map<BitSet,Cell> map = objectCache.get( interloccutor.getId() );
				for ( Map.Entry<BitSet, Cell> e: map.entrySet() ) {
					if ( e.getValue() == ((InstanceIdByAlignmentGame)game).getCell() )
						toremove.add( e.getKey() );
				}
				for ( BitSet o: toremove ) map.remove( o );
				toremove.clear();
			}
			String logmessage = "preserved";
			logmessage = interloccutor.failed( (InstanceIdByAlignmentGame)game );
			// Compute F-measure (ask the environment)
			alog.logResult( "FAILURE", false, "(correspondence "+logmessage+")" );
		} else if ( isSubClassOf( sourceClass, targetClass ) ) { // we have a success
			if ( theoryRevision ) addExample( true, remaining, interloccutor.classOf( instance ), sourceClass);
			if ( explorationmod != null ) objectCache.get( interloccutor.getId() ).put( instance, ((InstanceIdByAlignmentGame)game).getCell() );
			alog.logResult( "SUCCESS", true, null );
			logger.debug( "===> success" );
			OWLClass newClass = null;
			// The initiator find a correspondence strengthening the one used
			if ( strengthen != Strengthen.NONE ) {
				ObjectAlignment al = (ObjectAlignment)((InstanceIdByAlignmentGame)game).getAlignment();
				OWLClass othertop = factory.getOWLClass(IRI.create( interloccutor.getOntologyURI()+"#top" ));
				boolean alignmentFromHere = ( al.getOntology1() == ontology );
				Cell c = ((InstanceIdByAlignmentGame)game).getCell();
				if ( ( alignmentFromHere && c.getObject1() != top && c.getObject2() != othertop )
					 || ( !alignmentFromHere && c.getObject2() != top && c.getObject1() != othertop ) )
					newClass = findStrongerClass( al, c, interloccutor, alignmentFromHere, sourceClass, targetClass );
			}
			interloccutor.succeeded( (InstanceIdByAlignmentGame)game, newClass );
		} else { // Source class is always a leaf... hence, there is no isSuperClassOf
			alog.logResult( "Non comp (SUCCESS)", true, null );
			logger.debug( "===> non comp" );
		}
    }
    
    public String failed( InstanceIdByAlignmentGame game ) throws LLException {
		String logmessage = null;
		Alignment al = game.getAlignment();
		boolean alignmentFromHere = ( (OWLOntology)((ObjectAlignment)al).getOntology1() == ontology );
		Cell c = game.getCell();
		if ( memory ) {
			Alignment cached = getCachedAlignment( al );
			// The cell may contain '=' but only the corresponding relation is to be retracted
			cacheDeletedCorrespondence( cached, c, alignmentFromHere?"<":">" );
		}
		if ( revisionmod != Mod.DONOTHING ) {
			int toexpand = 0;
			// initiator repairs... the ADD, that I can do...
			try {
				AlignmentAdjustingAgent interloccutor = (AlignmentAdjustingAgent)game.getFirstAgent();
		// default: removal of the faulty cell
		al.remCell( c );
		toexpand++;
		logmessage = "removed";
		logger.debug( "===> {}", logmessage );
		// option REPLACE/ADD/ADDJOIN/REFINE/REFADD: replacement by a weaker cell
		if ( c.getRelation() instanceof EquivRelation ) {
		    if ( revisionmod != Mod.DELETE ) {
			al.addAlignCell( c.getObject1(), c.getObject2(), alignmentFromHere?">":"<", c.getStrength() );
			logmessage = "weakened";
			logger.debug( "===> {}", logmessage );
			logger.debug( "**ADDED: {} {} {}", c.getObject1(), alignmentFromHere?">":"<", c.getObject2() );
		    } else toexpand++;
		}
		// These two below are to be asked to the host, simply...
		OWLClass sup = null;
		if ( revisionmod == Mod.ADD ) { // option ADD: addition of a reverse weaker cell
		    sup = interloccutor.nonTopSuperClass( (OWLClass)(alignmentFromHere?c.getObject2():c.getObject1()) );
		    logmessage = "added";
		} else if ( revisionmod == Mod.ADDJOIN || revisionmod == Mod.REFADD ) { // option ADDJOIN: addition of the most specific possible reverse weaker cell
		    sup = interloccutor.commonSuperClass( interloccutor.classOf( game.getInstance() ), (OWLClass)(alignmentFromHere?c.getObject2():c.getObject1()) );
		    logmessage = "join-replaced";
		}
		//
		if ( sup != null ) {
		    if ( addCorrespondence( al, c, sup, alignmentFromHere, logmessage ) ) {
			logmessage = "";
			toexpand--;
		    }
		}
	    } catch (AlignmentException alex) {
		throw new LLException( "Unexpected cell removal error", alex );
	    }
	    // my repairs
	    try {
		logmessage = null;
		// option REFINE: ADDITION of downward weaker cells
		if ( revisionmod == Mod.REFINE || revisionmod == Mod.REFADD ) {
		    int nbrefine = refineCorrespondence( al, c, game.getInstance(), alignmentFromHere );
		    if ( nbrefine > 0 ) {
			toexpand -= nbrefine;
			logmessage = "refined("+nbrefine+")";
			logger.debug( "===> {}", logmessage );
		    }
		}
	    } catch (AlignmentException alex) {
		throw new LLException( "Unexpected cell removal error", alex );
	    }
	    // expansions
	    if ( toexpand > 0 && expand != Expand.NONE ) { 
		replaceCell( al, c, toexpand, alignmentFromHere );
		logmessage = "expanded";
		logger.debug( "===> {}", logmessage );
	    }
	}
	return logmessage;
    }

    /**
     * On sucess with strenghen, the successful correspondence is replaced by a stronger one with new targe newCl
     */
    public void succeeded( InstanceIdByAlignmentGame game, OWLClass newCl ) throws LLException {
		// if newCl != null, then Strengthen if proposed, and so is applicable
		if ( newCl != null ) { // strengthen != Strengthen.NONE
			Cell c = game.getCell();
			Alignment al = game.getAlignment();
			boolean alignmentFromHere = ( (OWLOntology)((ObjectAlignment)al).getOntology1() == ontology );
			try {
				if ( c.getRelation() instanceof EquivRelation ) { // Leave the non operative one
					al.remCell( c );
					if ( alignmentFromHere ) {
						al.addAlignCell( c.getObject1(), c.getObject2(), ">", c.getStrength() );
					} else {
						al.addAlignCell( c.getObject1(), c.getObject2(), "<", c.getStrength() );
					}
					logger.debug( "===> replaced" );
				} else { // Remove to replace by the other
					al.remCell( c );
					logger.debug( "===> removed" );
				}
				addCorrespondence( al, c, newCl, alignmentFromHere, "strengthened" );
			} catch (AlignmentException alex) {
				logger.error( "Error strengthening correspondences {}", alex );
			};
		} // else, no change
    }

    // * c1 != top && c2 != othertop means that we have achieved the topmost correspondence
    // two attitudes. (a) leave it alone -like here-, (b) try to strengthen -see below-
    // * this is not correctly implemented. If there were '=' correspondences, they would be just replaced by a < or > correspondences.
    // * The cache is tested against the _interloccutor_ cache... this involves looking in its cache
    public OWLClass findStrongerClass( final Alignment al, final Cell c, final AlignmentAdjustingAgent interloccutor, final boolean alignmentFromHere, OWLClass sourceClass, OWLClass targetClass ) throws LLException {
		// Because we did not implemented a specific cache for strengthening,
		// we have to rely on the interloccutor's cache
		// This is faster but weird...
		OWLClass otherClass = (OWLClass)(alignmentFromHere?c.getObject2():c.getObject1());
		OWLClass newCl = null;
		
		if ( targetClass != sourceClass ) {
			switch (strengthen) {
			case MOSTSPECIFIC:
				newCl = findMostSpecific( interloccutor, al, sourceClass, targetClass, otherClass );
				break;
			case MOSTGENERAL:
				newCl = findMostGeneral( interloccutor, al, sourceClass, targetClass, otherClass );
				break;
			case RANDOM:
				newCl = findRandom( interloccutor, al, sourceClass, targetClass, otherClass );
				break;
			}
		}
		return newCl;
    }

    protected OWLClass findMostSpecific( final AlignmentAdjustingAgent interloccutor, final Alignment al, final OWLClass lowest, final OWLClass highest, final OWLClass otherClass ) {
		OWLClass newClass = lowest;
		while ( newClass != highest ) {
			if ( interloccutor.isCachedCorrespondence( al, otherClass, newClass ) ) {
				newClass = nonTopSuperClass( newClass );
			} else {
				return newClass; }
		}
		return null;
    }

    protected OWLClass findMostGeneral( final AlignmentAdjustingAgent interloccutor, final Alignment al, final OWLClass lowest, final OWLClass highest, final OWLClass otherClass ) {
		OWLClass newClass = lowest;
		OWLClass candidate = null;
		while ( newClass != highest ) {
			if ( !interloccutor.isCachedCorrespondence( al, otherClass, newClass ) ) {
				candidate = newClass;
			}
			newClass = nonTopSuperClass( newClass );
		}
		return candidate;
    }
	
    protected OWLClass findRandom( final AlignmentAdjustingAgent interloccutor, final Alignment al, final OWLClass lowest, final OWLClass highest, final OWLClass otherClass ) {
		OWLClass newClass = lowest;
		Vector<OWLClass> candidates = new Vector<OWLClass>();
		while ( newClass != highest ) {
			if ( !interloccutor.isCachedCorrespondence( al, otherClass, newClass ) ) {
				candidates.add( newClass );
			}
			newClass = nonTopSuperClass( newClass );
		}
		if ( candidates.size() == 0 ) return null;
		else return candidates.get( rand.nextInt( candidates.size() ) );
    }

    /**
     * Add a new correspondence while replacing the target
     */
    protected boolean addCorrespondence( Alignment al, Cell c, OWLClass sup, boolean alignmentFromHere, String logmessage ) throws AlignmentException {
		if ( alignmentFromHere ) {
			if ( !nonred || !isEntailedCorrespondence( al, (OWLOntology)al.getOntology1(), (OWLOntology)al.getOntology2(), (OWLClass)c.getObject1(), false, sup, alignmentFromHere ) ) {
				al.addAlignCell( c.getObject1(), sup, "<", c.getStrength() );
				logger.debug( "===> {}", logmessage );
				logger.debug( "**ADDED: {} {} {}", c.getObject1(), "<", sup );
				return true;
			}
		}  else if ( !nonred || !isEntailedCorrespondence( al, (OWLOntology)al.getOntology1(), (OWLOntology)al.getOntology2(), sup, true, (OWLClass)c.getObject2(), alignmentFromHere ) ) {
			al.addAlignCell( sup, c.getObject2(), ">", c.getStrength() );
			logger.debug( "===> {}", logmessage );
			logger.debug( "**ADDED: {} {} {}", sup, ">", c.getObject2() );
			return true;
		}
		return false; // nothing added, so can be expanded (JE2017: rather be return toexpand)
    }
	
    /**
     * Add in al a correspondence for replacing c which has been faulty because of instance
     * General
     * Specific
     * Complete, we will do complete
     */
    public int refineCorrespondence( final Alignment al, final Cell c, final BitSet instance, final boolean alignmentFromHere ) throws AlignmentException {
		return refineCorrespondence( al, c, classOf( instance ), 0, alignmentFromHere );
    }

    public int refineCorrespondence( final Alignment al, final Cell c, final OWLClass faulty, int nbadd, final boolean alignmentFromHere ) throws AlignmentException {
		// If the faulty class is not directly the extremity of the cell
		logger.trace( "FAULTLY: {}", faulty );
		if ( alignmentFromHere ) {
			logger.trace( "TARGET: {}", c.getObject1() );
		} else {
			logger.trace( "TARGET: {}", c.getObject2() );
		}
		if ( faulty == null
			 || ( alignmentFromHere && faulty == c.getObject1() ) 
			 || ( !alignmentFromHere && faulty == c.getObject2() ) ) return nbadd;
		// Find the father
		OWLClass clsup = superClass( faulty );
		// Find the brother (ad hoc)
		String pref = clsup.getIRI().toString();
		logger.trace( "FATHER: {}", pref );
		OWLClass clsib;
		if ( faulty.getIRI().toString().equals( pref+"1") ) clsib = factory.getOWLClass( IRI.create(pref+"0") );
		else clsib = factory.getOWLClass( IRI.create(pref+"1") );
		logger.trace( "BROTHER({}): {}", nbadd, clsib );
		// Add the correspondence to it to the alignment (beware this is inverted wrt add because we switched agents)
		logger.debug( "===> refined" );
		if ( alignmentFromHere && functionalClass( al, clsib, alignmentFromHere ) ) {
			al.addAlignCell( clsib, c.getObject2(), "<", c.getStrength() );//"<"
			logger.debug( "**ADDED: {} {} {}", clsib, "<", c.getObject2() );
			nbadd++;
		} else if ( !alignmentFromHere && functionalClass( al, clsib, alignmentFromHere ) ) {
			al.addAlignCell( c.getObject1(), clsib, ">", c.getStrength() );//">"
			logger.debug( "**ADDED: {} {} {}", c.getObject1(), ">", clsib );
			nbadd++;
		}
		// If it is not the extremity of the cell recurse
		return refineCorrespondence( al, c, clsup, nbadd, alignmentFromHere );
    }

    public boolean functionalClass( final Alignment al, final OWLClass c, final boolean alignmentFromHere ) {
		try {
			Set<Cell> cs = alignmentFromHere?al.getAlignCells1( c ):al.getAlignCells2( c );
			if ( cs == null ) return true;
			for ( Cell cp: cs ) {
				String rel = cp.getRelation().getRelation(); // TODO OPTIMIZE
				if ( rel.equals( "=" ) 
					 || ( alignmentFromHere && rel.equals( "<" ) )
					 || ( !alignmentFromHere && rel.equals( ">" ) ) ) return false;
			}
			return true;
		} catch (Exception ex) { ex.printStackTrace(); System.exit(-1); return false; }
    }

    /**
     * Replaces a (faulty) cell c in alignment al, by nb new cells
     * if PROTECTED, then the new cells should not be entailed
     * if CLEVER, they should not have already been tried
     * if complete, then the agent event test the other agent ontology...
     * if nonred, then the new correspondence should not be entailed
     * JE201905-ERR: It is not correct because of the isSubClassOf in the other ontology
     * Covered by the isSubClassOf
     */
    public Cell replaceCell( final Alignment al, final Cell c, int nbcells, final boolean alignmentFromHere ) {
		logger.debug( "ReplaceCall: {}, {}", c, alignmentFromHere );
		Cell result = null;
		OWLOntology o1 = (OWLOntology)al.getOntology1();
		OWLOntology o2 = (OWLOntology)al.getOntology2();
		// draw a cell at random
		OWLClass c1 = null;
		OWLClass c2 = null;
		for ( ; nbcells > 0; nbcells-- ) {
			int nbTry = numberOfFeatures*numberOfFeatures;
			try {
				while ( c1 == null || c2 == null ) {
					// draw a first class
					if ( c1 == null ) c1 = drawClass( al, alignmentFromHere, true, nbTry );
					// draw a second class
					if ( c2 == null ) c2 = drawClass( al, !alignmentFromHere, false, nbTry );
					// Check that the new cell does not entail by the suppressed one
					if ( expand == Expand.PROTECTED && c != null ) {
						if ( ( alignmentFromHere
							   && isSubClassOf( (OWLClass)c.getObject1(), c1 )
							   && ( c2 == (OWLClass)c.getObject2() 
									|| ( complete && isSubClassOf( o2, c2, (OWLClass)c.getObject2() ) ) ) )
							 || (!alignmentFromHere
								 && isSubClassOf( (OWLClass)c.getObject2(), c2 )
								 && ( c1 == (OWLClass)c.getObject1() 
									  || ( complete && isSubClassOf( o1, c1, (OWLClass)c.getObject1() ) ) ) ) ) {
							c1 = null; c2 = null; //System.err.println( "protected" );
						}
					} else if ( expand == Expand.CLEVER ) { // here we do not put memory
						// because that would transform an non expand with memory in CLEVER
						if ( alreadyDeletedCorrespondence( al, o1, o2, c1, !alignmentFromHere, c2, alignmentFromHere ) ) {
							c1 = null; c2 = null; //System.err.println( "cached" );
							break;
						}
						// Actually this is redundant, because the new correspondence may entail others
						if ( nonred && c1 != null && c2 != null ) {
							if ( isEntailedCorrespondence( al, o1, o2, c1, !alignmentFromHere, c2, alignmentFromHere ) ) {
								c1 = null; c2 = null;
							}
						}
					}
					if ( ( c1 == null || c2 == null ) && nbTry-- == 0 ) {
						throw new LLException( "Cannot find suitable class" );
						//logger.debug( "**UNABLE to expand alignment: {}  {}", o1, o2 );
						//break;
					}
				} // otherwise draw a new one (when to stop?)
				if ( c1 != null && c2 != null ) {
					String rel = alignmentFromHere?"<":">";
					logger.debug( "**ADDED: {} {} {}", c1, rel, c2 );
					result = al.addAlignCell( c1, c2, rel, 1.0 );
				}
			} catch ( LLException llex ) {
				logger.debug( "Cannot generate new correspondences between {} and {}", o1, o2 );
			} catch ( AlignmentException alex ) {
				logger.debug( "Cannot generate new correspondences between {} and {}", o1, o2 );
			}
		}
		return result;
    }

    /**
     * Generate a new cell, subsuming class clinit
     * Could be improved by returning the top one if necessary
     */
    public Cell generateCell( final Alignment al, final boolean alignmentFromHere, final OWLClass clint, Cell result ) {
		logger.debug( "GenerateCall: clinit: {}, ", clint );
		OWLOntology o1 = (OWLOntology)al.getOntology1();
		OWLOntology o2 = (OWLOntology)al.getOntology2();
		// draw a cell at random
		OWLClass c1 = null;
		OWLClass c2 = null;
		if ( alignmentFromHere ) {
			c1 = clint;
		} else {
			c2 = clint;
		}
		int nbTry = numberOfFeatures*numberOfFeatures;
		try {
			while ( c1 == null || c2 == null ) {
				// draw a first class
				if ( c1 == null ) c1 = drawClass( al, alignmentFromHere, true, nbTry );
				// draw a second class
				if ( c2 == null ) c2 = drawClass( al, !alignmentFromHere, false, nbTry );
				// Check that the alignment is not been covered
				if ( memory ) {
					if ( alreadyDeletedCorrespondence( al, o1, o2, c1, !alignmentFromHere, c2, alignmentFromHere ) ) {
						if ( alignmentFromHere ) c2 = null;
						else c1 = null;
						//System.err.println( "cached" );
						break;
					}
					if ( nonred && c1 != null && c2 != null ) {
						if ( isEntailedCorrespondence( al, o1, o2, c1, !alignmentFromHere, c2, alignmentFromHere ) ) {
							if ( alignmentFromHere ) c2 = null;
							else c1 = null;
						}
					}
				}
				if ( ( c1 == null || c2 == null ) && nbTry-- == 0 ) {
					// Exception is caught below
					throw new LLException( "Cannot find suitable class" );
				}
			} // otherwise draw a new one (when to stop?)
			if ( c1 != null && c2 != null ) {
				String rel = alignmentFromHere?"<":">";
				logger.debug( "===> generated" );
				logger.debug( "**ADDED: {} {} {}", c1, rel, c2 );
				result = al.addAlignCell( c1, c2, rel, 1.0 );
			}
		} catch ( LLException llex ) {
			logger.debug( "Cannot generate new correspondences between {} and {}", o1, o2 );
		} catch ( AlignmentException alex ) {
			logger.debug( "Cannot generate new correspondences between {} and {}", o1, o2 );
		}
		return result;
    }

    public Alignment getCachedAlignment( final Alignment al ) {
		Alignment result = cellCache.get( al );
		if ( result == null ) {
			try {
				result = new ObjectAlignment( (ObjectAlignment)al );
				for ( Cell c : al ) { result.remCell( c ); }
				cellCache.put( al, result );
			} catch (AlignmentException alex) {
				logger.error( "Cannot cache alignment {}", alex ); 
			}
		}
		return result;
    }

    public void cacheDeletedCorrespondence( final Alignment cache, final Cell c, final String rel ) {
		try {
			// cannot do addCell...
			cache.addAlignCell( c.getObject1(), c.getObject2(), rel, c.getStrength() );
		} catch (AlignmentException alex) {
			logger.error( "Cannot cache correspondence {}", alex );
		};
    }

    public boolean alreadyDeletedCorrespondence( final Alignment al, final OWLOntology o1, final OWLOntology o2, final OWLClass c1, final boolean subsumes, final OWLClass c2, final boolean alignmentFromHere ) {
		Alignment cached = getCachedAlignment( al );
		//for ( Cell cc : cached ) {
		//System.err.println( "  -- "+cc.getObject1()+" "+cc.getRelation().getRelation()+" "+cc.getObject2() );
		//}
		return isEntailedCorrespondence( cached, o1, o2, c1, subsumes, c2, alignmentFromHere );
    }

    /**
     * Is the correspondence c1 subsume?>:< c2 already entailed by Alignment al?
     * JE201905-ERR: It is not correct because of the isSubClassOf in the other ontology
     * Covered by the complete
     * Knowing if entailed can only be cooperative!
     */
    public boolean isEntailedCorrespondence( final Alignment al, final OWLOntology o1, final OWLOntology o2, final OWLClass c1, final boolean subsumes, final OWLClass c2, final boolean alignmentFromHere ) {
		for ( Cell aligned : al ) {
			if ( ( subsumes
				   && ( aligned.getRelation() instanceof EquivRelation
						|| aligned.getRelation() instanceof SubsumeRelation )
				   && ( c1 == (OWLClass)aligned.getObject1()
						|| ( !alignmentFromHere && complete && isSubClassOf( o1, (OWLClass)aligned.getObject1(), c1 ) )
						|| ( alignmentFromHere && isSubClassOf( (OWLClass)aligned.getObject1(), c1 ) ) )
				   && ( c2 == (OWLClass)aligned.getObject2()
						|| ( alignmentFromHere && complete && isSubClassOf( o2, c2, (OWLClass)aligned.getObject2() ) )
						|| ( !alignmentFromHere && isSubClassOf( c2, (OWLClass)aligned.getObject2() ) ) ) )
				 || ( !subsumes
					  && ( aligned.getRelation() instanceof EquivRelation
						   || aligned.getRelation() instanceof SubsumedRelation )
					  && ( c1 == (OWLClass)aligned.getObject1()
						   || ( !alignmentFromHere && complete && isSubClassOf( o1, c1, (OWLClass)aligned.getObject1() ) )
						   || ( alignmentFromHere && isSubClassOf( c1, (OWLClass)aligned.getObject1() ) ) )
					  && ( c2 == (OWLClass)aligned.getObject2()
						   ||  ( alignmentFromHere && complete && isSubClassOf( o2, (OWLClass)aligned.getObject2(), c2 ) )
						   || ( !alignmentFromHere && isSubClassOf( (OWLClass)aligned.getObject2(), c2 ) ) ) ) )
				return true;
		}
		return false;
	}

    // Not the most efficient implementation as it is used, but the most agent...
    public boolean isCachedCorrespondence( final Alignment al, final OWLClass c1, final OWLClass c2 ) {
		boolean alignmentFromHere = ( al.getOntology1() == ontology );
		Alignment cache = getCachedAlignment( al );
		if ( alignmentFromHere ) {
			for ( Cell aligned : cache ) {
				if ( c1 == (OWLClass)aligned.getObject1() && c2 == (OWLClass)aligned.getObject2() )
					return true;
			}
		} else {
			for ( Cell aligned : cache ) {
				if ( c1 == (OWLClass)aligned.getObject2() && c2 == (OWLClass)aligned.getObject1() ) 
					return true;
			}
		}
        return false;
    }

    public void addExample( final boolean success, final int remaining, final OWLClass initialClass, final OWLClass sourceClass ) throws LLException {
		try {// this is the same assertion
			// in which instance is the representant of the most specific class to which instance belongs (not easy to get)
			String ontologyIdentifierInstance = "o" + env.stripOntoPrefix(initialClass.getIRI().toString()).substring(0, env.stripOntoPrefix(initialClass.getIRI().toString()).indexOf("#"));
			String statement = "belongsTo(inst"+remaining+","+ontologyIdentifierInstance+","+env.shortClassName( initialClass.getIRI() )+").";
			logger.debug( statement );
			revisor.addAssertion( statement );
			// If success, it is a possible example, if failure, a negative one
			// it should be isA( instance, targetClass )
			String ontologyIdentifierConcept = "o" + env.stripOntoPrefix(sourceClass.getIRI().toString()).substring(0, env.stripOntoPrefix(sourceClass.getIRI().toString()).indexOf("#"));
			String example = ((success)?"+":"-") + "isA("+ontologyIdentifierInstance+",inst"+remaining+","+ontologyIdentifierConcept+","+env.shortClassName( sourceClass.getIRI() )+").";
			logger.debug( example );
			revisor.addExample( example );
		} catch (Exception ex) {
			logger.debug( "IGNORED Exception: incorrect example", ex );
		}
    }

    /**
     * Returns the correspondence in al connecting classname
     * first is true if classname has to be found in ontology1 of the alignment
     * 
     * It may not be the most specific correspondence due to immedate ratio
     * It may be null because LogMap may suppress top correspondences (and other reasons)
     */
    protected Cell applicableCorrespondence( final Alignment al, final BitSet instance, final boolean alignmentFromHere ) throws LLException {
		logger.debug( "AC: {} - [{}] -> {} : {}", envid, instance, ontologyURI, classOf( instance ) );
		return applicableCorrespondence( al, classOf( instance ), alignmentFromHere );
    }

    protected Cell applicableCorrespondence( final Alignment al, final OWLClass clinit, final boolean alignmentFromHere ) throws LLException {
		Cell fetched = null;
		OWLClass cl = clinit;
		try {
			while ( cl != null ) {
				if ( cl == top && fetched != null ) { // relaxed and nothing shadowed
					//logger.debug( "*** IMMEDIATE reached top: release {}", fetched );
					return fetched;
				}
				Set<Cell> cs = null;
				if ( alignmentFromHere ) cs = al.getAlignCells1( cl );
				else cs = al.getAlignCells2( cl );
				if ( cs != null ) {
					// get the class (only one valid in principle by injectivity)
					for ( Cell cell : cs ) {
						Relation rel = cell.getRelation();
						if ( rel instanceof EquivRelation || 
							 ( !alignmentFromHere && rel instanceof SubsumeRelation ) ||
							 ( alignmentFromHere && rel instanceof SubsumedRelation ) ) {
							// Here we have an applicable correspondence
							if ( cl == top && generative ) {
								//logger.debug( "GENERATED CORRESPONDENCE {} at {}", cell, cl );
								return generateCell( al, alignmentFromHere, clinit, cell );
							} else if ( cl == top || immediateRatio == 100 || rand.nextInt( 100 ) < immediateRatio ) {
								//logger.debug( "SELECTED CORRESPONDENCE {} at {}", cell, cl );
								return cell; // selected
							} else { // relaxed and bypassed
								//logger.debug( "*** IMMEDIATE, bypass {} at {}", cell, cl );
								fetched = cell;
								break; // and go one level above
							}
						}
					}
				}
				// else get superclass (there is only one).
				cl = superClass( cl );
			}
		} catch (AlignmentException alex) {
			throw new LLException( "Unexpected cell retrieval error", alex );
		}
		// Here: I run to the top and never encountered an applicable correpondence
		// In priciple, this is not possible (because of the top correspondences)
		logger.warn( "Reached top class {} without correspondence", cl );
		return null;
    }

    /**
     * Draw a class at random in one ontology (first=1, second=2) of the alignment al
     * if subsumed, check that the alignment remains functional
     */
    protected OWLClass drawClass( final Alignment al, final boolean subsumed, final boolean first, int nbTry ) throws LLException {
		OWLClass cl = null;
		URI prefix = null;
		try {
			prefix = first?al.getOntology1URI():al.getOntology2URI();
			while ( cl == null ) {
				cl = factory.getOWLClass( IRI.create( prefix+"#"+env.generateClassName() ) );
				// check if not already aligned
				if ( subsumed || uniqueCellPerClass ) {
					if ( first && al.getAlignCells1( cl ) != null ) {
						for ( Cell cc : al.getAlignCells1( cl ) ) {
							if ( uniqueCellPerClass || cc.getRelation() instanceof SubsumedRelation || cc.getRelation() instanceof EquivRelation ) { cl = null; break; }
						}
					} else if ( !first && al.getAlignCells2( cl ) != null ) {
						for ( Cell cc : al.getAlignCells2( cl ) ) {
							if ( uniqueCellPerClass || cc.getRelation() instanceof SubsumeRelation || cc.getRelation() instanceof EquivRelation ) { cl = null; break; }
						}
					}
					if (  cl == null && nbTry-- == 0 ) throw new LLException( "Cannot find suitable class" );
				}
			}
		} catch ( AlignmentException alex) { // should not occur
			throw new LLException( "Ontology problem", alex );
		}
		return cl;
    }

    public OWLClass classOf( final BitSet instance ) {
	String classname = "";
	if ( ontologyPermutation ) {
	    int i = ((Integer)ontologySignature).intValue();
	    int remaining=numberOfFeatures-1;
	    while ( remaining > 0 ) {
		if ( i<numberOfFeatures ) {
		    classname += instance.get( i )?"1":"0";
		    remaining--;
		    i++;
		} else {
		    i=0;
		}
	    }
	    //logger.trace( " --> {}#{}", ontologyURI, classname );
	} else { // random
	    // Simpler because no modulo!
	    for ( int i=0; i < numberOfFeatures-1; i++ ) {
		classname += instance.get( ((int[])ontologySignature)[i] )?"1":"0";
	    }
	}
	return factory.getOWLClass( IRI.create( ontologyURI+"#"+classname ) );
    }

    public BitSet getAltern( final OWLClass cl, final BitSet obj ) {
		String name = env.stripOntoPrefix( cl.getIRI().toString() );
		BitSet bs = (BitSet)obj.clone();
		int idx;
		if ( ontologyPermutation ) {
			idx = ((Integer)ontologySignature).intValue();
			if ( idx == 0 ) idx = numberOfFeatures-1;
		} else {
			List<int[]> sig = Arrays.asList( ((int[])ontologySignature) );
			for ( idx=0; idx < numberOfFeatures; idx++ ) {
				if ( ! sig.contains( idx ) ) break;
			}
			// Put the missing one in i
		}
		bs.flip( idx );
		return bs;
    }

    // Hence, 'complete' asking for comparing classes of the other ontology should now be wrong
    // This is cl1 subClassOf c2
    // Since it is protected, then this should be doable for other agents!
    protected boolean isSubClassOf( final OWLClass cl1, final OWLClass cl2 ) {
		if ( cl1 == cl2 ) return true;
		if ( reasoner.getSubClasses( cl2, false ).getFlattened().contains( cl1 ) ) return true;
		else return false;
    }

    // JE201905-TOSUPPRESS This is here for covering obsolete code
    protected boolean isSubClassOf( final OWLOntology ontology, final OWLClass cl1, final OWLClass cl2 ) {
		return isSubClassOf( cl1, cl2 );
    }

    protected boolean isDisjoint( final OWLClass cl1, final OWLClass cl2 ) {
		if ( cl1 == cl2 ) return false;
		if ( reasoner.getDisjointClasses( cl2 ).getFlattened().contains( cl1 ) ) return true;
		else return false;
    }

    // direct super class
    // returns null if top
    protected OWLClass nonTopSuperClass( final OWLClass cl ) { // true=direct
		OWLClass sup = superClass( cl );
		if ( sup != top ) return sup;
		else return null;
    }

    protected OWLClass superClass( final OWLClass cl ) { // true=direct
		Set<OWLClass> superClasses = reasoner.getSuperClasses( cl, true ).getFlattened();
		return superClasses.isEmpty()?null:superClasses.iterator().next();
    }

    // cl1 and cl2 are disjoint classes
    // returns null if top
    protected OWLClass commonSuperClass( final OWLClass cl1, final OWLClass cl2 ) {
		Set<OWLClass> superClasses = reasoner.getSuperClasses( cl1, false ).getFlattened();
		// They cannot be explored in order! Then
		OWLClass sup = null;
		Set<OWLClass> supersup = null;
		for ( OWLClass sc : superClasses ) {
			if ( isSubClassOf( cl2, sc ) ) {
				if ( sup == null || !supersup.contains( sc ) ) {
					sup = sc;
					supersup = reasoner.getSuperClasses( sc, false ).getFlattened();
				}
			}
		}
		if ( sup != top ) return sup;
		else return null;
    }

    public void save( File dir ) throws LLException {
		try(  PrintWriter out = new PrintWriter( new File( dir, "sig.txt" ) )  ){
			out.println( ontologyURI );
			out.println( env.signatureToString( ontologyURI ) );
		} catch ( FileNotFoundException fnfex ) {
			throw new LLException( "Cannot open file "+dir+"/sig.txt" );
		}
		// Use it because manager is not here (but could be at the environment)
		OWLOntologyHelper.saveOntologyToFile( ontology, new File( dir, "onto.owl" ) );
    }

    public void load( File dir ) throws LLException {
		// should load the ontology instead of the environment ??
		// Not really
    }
}
