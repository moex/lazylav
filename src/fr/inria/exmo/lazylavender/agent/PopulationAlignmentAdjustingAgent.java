/*
 * Copyright (C) INRIA, 2019-2021
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 */

package fr.inria.exmo.lazylavender.agent;

import fr.inria.exmo.lazylavender.model.LLException;
import fr.inria.exmo.lazylavender.env.NOOEnvironment;
import fr.inria.exmo.lazylavender.logger.ActionLogger;
import fr.inria.exmo.lazylavender.pop.Population;

import br.uniriotec.ppgi.alignmentRevision.revision.RevisorInterface;

import fr.inrialpes.exmo.align.impl.Namespace;
import fr.inrialpes.exmo.align.impl.Annotations;
import fr.inrialpes.exmo.align.impl.ObjectAlignment;
import fr.inrialpes.exmo.align.impl.URIAlignment;
import fr.inrialpes.exmo.align.impl.rel.SubsumedRelation;
import fr.inrialpes.exmo.align.impl.rel.EquivRelation;
import fr.inrialpes.exmo.align.impl.renderer.RDFRendererVisitor;
import fr.inrialpes.exmo.align.parser.AlignmentParser;

import org.semanticweb.owl.align.Evaluator;
import fr.inrialpes.exmo.align.impl.eval.SemPRecEvaluator;
import fr.inrialpes.exmo.align.impl.eval.PRecEvaluator;

import org.semanticweb.owl.align.Alignment;
import org.semanticweb.owl.align.Cell;
import org.semanticweb.owl.align.Relation;
import org.semanticweb.owl.align.AlignmentException;

import org.semanticweb.HermiT.Reasoner;

import org.semanticweb.owlapi.reasoner.SimpleConfiguration;
import org.semanticweb.owlapi.reasoner.OWLReasoner;
import org.semanticweb.owlapi.reasoner.OWLReasonerFactory;
import org.semanticweb.owlapi.reasoner.OWLReasonerConfiguration;

import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.IRI;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.BitSet;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Vector;
import java.util.Properties;
import java.util.Hashtable;
import java.util.IdentityHashMap;
import java.util.Random;
import java.lang.NumberFormatException;
import java.io.File;
import java.io.File;
import java.io.OutputStreamWriter;
import java.io.BufferedWriter;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.net.URI;
import java.net.URISyntaxException;

public class PopulationAlignmentAdjustingAgent extends AlignmentAdjustingAgent {
    final static Logger logger = LoggerFactory.getLogger(PopulationAlignmentAdjustingAgent.class);
    protected IntegrationMode intMode = IntegrationMode.FATIMA;
    protected Random randomGenerator = null;
    // Where (and if) to load data from
    String loadDir = null;
    private Population population;
    private boolean startempty = false;
    /**
     * Probability of agent exhibit non-Confirmist behaviour (-convertProbability)
     */
    private float convertProbability = (float) 0;
    /**
     * Maximum alignmnt distance allowed for agents in a population
     */
    private float maxAlignmentDistance = (float) 0;
    /**
     * Controles memory feature for syncronisation
     */

    private boolean syntacticDistance = true;

    private HashMap<URI, Alignment> alignments;

    // It is now fully disconnected from the ontology URI which is given otherwise
    public PopulationAlignmentAdjustingAgent(NOOEnvironment e, ActionLogger a, int id) { // To be generalised
        super(e, a, id);
        alignments = new HashMap<URI, Alignment>();
    }

    public PopulationAlignmentAdjustingAgent(NOOEnvironment e, ActionLogger a, int id, Population pop) { // To be generalised
        this(e, a, id);
        population = pop;
    }

    public PopulationAlignmentAdjustingAgent init(Properties param) {
        super.init(param);
        String proba = param.getProperty("convertProbability");
        if (proba != null) {
            convertProbability = Float.parseFloat(proba);
        }

        randomGenerator = new Random();

        String distance = param.getProperty("maxAlignmentDistance");
        if (distance != null) {
            maxAlignmentDistance = Float.parseFloat(distance);
        }


        syntacticDistance = (param.getProperty("syntacticDistance") == null);

        if (param.getProperty("startempty") != null || param.getProperty("startEmpty") != null) startempty = true;
        String integrationMode = param.getProperty("integrationMode");
        if (integrationMode != null) {
            if (integrationMode.equals("consprior")) {
                intMode = IntegrationMode.CONSENSUS_FIRST;
            } else if (integrationMode.equals("consonly")) {
                intMode = IntegrationMode.CONSENSUS_ONLY;
            } else if (integrationMode.equals("locprior")) {
                intMode = IntegrationMode.LOCAL_FIRST;
            } else if (integrationMode.equals("loconly")) {
                intMode = IntegrationMode.LOCAL_ONLY;
            } else if (integrationMode.equals("random")) {
                intMode = IntegrationMode.RANDOM;
                randomGenerator = new Random();
            } else if (integrationMode.equals("proba")) {
                intMode = IntegrationMode.PROBABLISTIC;
            } else if (integrationMode.equals("aligndist")) {
                intMode = IntegrationMode.ALIGNMENT_DISTANCE;
            } else if (integrationMode.equals("meminter")) {
                intMode = IntegrationMode.MEMORY_INTER;
            } else
                logger.debug("Ingnored unknown integration mode: {}", integrationMode);
        }

        // LOAD
        if (param.getProperty("loadRunDir") != null &&
                param.getProperty("loadAgents") != null &&
                !startempty) {
            loadDir = param.getProperty("loadRunDir") + "/init/population/" + population.getId() + "/agent/" + envid + "/";
        }
        ontologyURI = population.getOntologyURI();
        ontologySignature = env.getSignature(ontologyURI);

        return this;
    }

    // The ontology and ontology factories are in the populations...
    public void initAlignments() throws LLException {
        // get the ontology from the environment
        ontology = population.getOntology();
        factory = population.getFactory();
        reasoner = population.getReasoner();
        reasoner.precomputeInferences();
        top = population.getTop();
        AlignmentParser aparser = null;
        for (URI u : env.getOntologies()) {
            String oI = env.stripOntoPrefix(ontologyURI.toString());
            if (!ontologyURI.equals(u)) {
                String oJ = env.stripOntoPrefix(u.toString());
                Alignment newal;
                if (loadDir != null) {
                    if (aparser == null) aparser = new AlignmentParser();
                    //System.err.println( " >> "+loadDir+"/NOOEnvironment/"+oI+"___"+oJ+".rdf" );
                    try {
                        Alignment ual = aparser.parse("file:" + loadDir + oI + "___" + oJ + ".rdf");
                        newal = env.makeOneWay(ual);
                    } catch (AlignmentException alex) {
                        throw new LLException("Cannot parse alignment", alex);
                    }
                } else if (startempty) {
                    newal = env.initAlignment(ontologyURI, u, true);
                } else {
                    newal = env.createInitialOneWayAlignment(ontologyURI, u);
                }
                newal.setExtension(Namespace.ALIGNMENT.uri, Annotations.ID, "file:" + oI + "___" + oJ + ".rdf");
                //newal.setExtension( Namespace.ALIGNMENT.uri, Annotations.ID, al.getExtension( Namespace.ALIGNMENT.uri, Annotations.ID ) );
                alignments.put(u, newal);
            }
        }
    }

    public Alignment getDirectAlignment(URI u) {
        return alignments.get(u);
    }

    public Population getPopulation() {
        return population;
    }

    private float alignmentDistance(Alignment local, Alignment consensus) throws LLException {

        int nbexpected = 0; // expected so far
        int nbfound = 0; // found so far
        int nbfoundentailed = 0;
        int nbexpectedentailed = 0;
        int nbcorrect = 0; // correct so far

        Evaluator eval = null;
        // count values for prec/rec/fmeasure
        try {
            if (syntacticDistance) {
                eval = new PRecEvaluator(consensus, local).init();
            } else {
                eval = new SemPRecEvaluator(consensus, local).init((Properties) null);
            }
            eval.eval((Properties) null);
            nbexpected += ((PRecEvaluator) eval).getExpected();
            if (syntacticDistance) {
                nbfound += ((PRecEvaluator) eval).getFound();
                nbcorrect += ((PRecEvaluator) eval).getCorrect();
                //logger.trace("Prec/rec {} <=> {} == {} / {}", ontologyURI, u, ((PRecEvaluator)eval).getPrecision(), ((PRecEvaluator)eval).getRecall() );
            } else {
                nbfound += ((SemPRecEvaluator) eval).getFound();
                nbfoundentailed += ((SemPRecEvaluator) eval).getFoundEntailed();
                nbexpectedentailed += ((SemPRecEvaluator) eval).getExpectedEntailed();
            }
        } catch (AlignmentException alex) { // should not happen
            logger.warn("IGNORED: AlignmentException", alex);
        }


        if (syntacticDistance) {
            return 1 - (float) nbcorrect / (float) (nbfound + nbexpected - nbcorrect);
        } else {
            return 1 - (float) (nbfoundentailed + nbexpectedentailed) / (float) (nbfound + nbexpected);
        }
    }

    public void syncAlignment(URI otherOntologyURI, ObjectAlignment syncedAlignment) throws LLException {

        ObjectAlignment consensus = null;
        try {
            consensus = new ObjectAlignment(syncedAlignment);
        } catch (AlignmentException alex) {
            throw new LLException("Cannot create consensus alignment for caculating alignment distance", alex);
        }
        Alignment al = getDirectAlignment(otherOntologyURI);
        if (al == null)
            logger.error("Cannot retrieve alignment between {} and {} for agent", ontologyURI, otherOntologyURI);
        if (consensus == null)
            logger.error("Cannot construct consensus alignment between {} and {} for agent", ontologyURI, otherOntologyURI);

        // If LOCAL_ONLY, do nothing
        if (intMode != IntegrationMode.LOCAL_ONLY) {
            if (intMode == IntegrationMode.CONSENSUS_ONLY) {
                // Just copy the consensus
                try {
                    alignments.replace(otherOntologyURI, new ObjectAlignment(consensus));
                } catch (AlignmentException alex) {
                    throw new LLException("Cannot create alignment", alex);
                }
            } else if (intMode == IntegrationMode.MEMORY_INTER) {
                //If use memory in sync, check correpondances in memroy first
                Alignment cached = getCachedAlignment(al);
                OWLClass c1 = null;
                OWLClass c2 = null;

                OWLOntology o1 = (OWLOntology) al.getOntology1();
                OWLOntology o2 = (OWLOntology) al.getOntology2();
                boolean alignmentFromHere = (al.getOntology1() == ontology);
                for (Cell cc : syncedAlignment) {
                    c1 = (OWLClass) cc.getObject1();
                    c2 = (OWLClass) cc.getObject2();
                    if (alreadyDeletedCorrespondence(al, o1, o2, c1, !alignmentFromHere, c2, alignmentFromHere)) {
                        try {
                            consensus.remCell(cc);
                        } catch (AlignmentException alex) {
                            throw new LLException("Unexpected cell removal error", alex);
                        }
                    }
                }

                for (Cell c : consensus) {
                    try {
                        Set<Cell> conflicts = al.getAlignCells1(c.getObject1());
                        if (conflicts != null) {
                            if (conflicts.size() > 1) {
                                // ConcurrentModificationException crazyness
                                HashSet<Cell> torem = new HashSet<Cell>();
                                for (Cell cc : conflicts) torem.add(cc);
                                for (Cell cc : torem) al.remCell(cc);
                            } else {
                                al.remCell(conflicts.iterator().next());
                            }
                            // add c
                            al.addAlignCell(c.getObject1(), c.getObject2(), c.getRelation().getRelation(), 1.0);
                            break;
                        }
                    } catch (AlignmentException alex) {
                        throw new LLException("Cannot update alignment", alex);
                    }
                }
            } else {
                //Alignment al = getDirectAlignment(otherOntologyURI);
                // PROP: The consensus is injective by construction (one per target)
                for (Cell c : consensus) {
                    // Get correspondences with the same ontology
                    // PROP: There is only one, otherwise the alignment was not injective...
                    // if conflicting correspondence (same o1, diff o2, subsumed or equiv)
                    // JE: REDO by collecting only the relevant ones
                    try {
                        Set<Cell> conflicts = al.getAlignCells1(c.getObject1());
                        if (conflicts != null) {
                            // Possibilities
                            switch (intMode) {
			    case FATIMA:
				// Take the subsume(d) if it exists (this is always the case)
				// otherwise, take at random... but the same across all agents
				// which is NOT possible without agents sharing their correspondences
				break;
			    case CONSENSUS_FIRST:
				// remove the conflicts
				// JE: TODO: Note that the equiv should be split if not selected
				// (in principle we do not have EQUIV here...)
				if (conflicts.size() > 1) {
				    // ConcurrentModificationException crazyness
				    HashSet<Cell> torem = new HashSet<Cell>();
				    for (Cell cc : conflicts) torem.add(cc);
				    for (Cell cc : torem) al.remCell(cc);
				} else {
				    al.remCell(conflicts.iterator().next());
				}
				// add c
				al.addAlignCell(c.getObject1(), c.getObject2(), c.getRelation().getRelation(), 1.0);
				break;
			    case LOCAL_FIRST:
				// do not add c
				break;
			    case PROBABLISTIC:
				if (randomGenerator.nextFloat() > convertProbability) {
				    //same as in CONSENSUS_FIRST
				    if (conflicts.size() > 1) {
					HashSet<Cell> torem = new HashSet<Cell>();
					for (Cell cc : conflicts) torem.add(cc);
					for (Cell cc : torem) al.remCell(cc);
				    } else {
					al.remCell(conflicts.iterator().next());
				    }
				    // add c
				    al.addAlignCell(c.getObject1(), c.getObject2(), c.getRelation().getRelation(), 1.0);
				}

                    break;
			    case ALIGNMENT_DISTANCE:
				if (alignmentDistance(al, consensus) > maxAlignmentDistance) {
				    //same as in CONSENSUS_FIRST
				    if (conflicts.size() > 1) {
					HashSet<Cell> torem = new HashSet<Cell>();
					for (Cell cc : conflicts) torem.add(cc);
					for (Cell cc : torem) al.remCell(cc);
				    } else {
					al.remCell(conflicts.iterator().next());
				    }
				    // add c
				    al.addAlignCell(c.getObject1(), c.getObject2(), c.getRelation().getRelation(), 1.0);
				}
                    break;
			    case RANDOM:
				// check the one to add, and implement randomly
				if (randomGenerator.nextBoolean()) {
				    // remove the conflicts
				    // JE: TODO: Note that the equiv should be split if not selected
				    if (conflicts.size() > 1) {
					// ConcurrentModificationException crazyness
					HashSet<Cell> torem = new HashSet<Cell>();
					for (Cell cc : conflicts) torem.add(cc);
					for (Cell cc : torem) al.remCell(cc);
				    } else {
					al.remCell(conflicts.iterator().next());
				    }
				    // add c
				    al.addAlignCell(c.getObject1(), c.getObject2(), c.getRelation().getRelation(), 1.0);
				} // else do nothing, the conflicting is preserved
				break;
                            }
                        } else { // there is no conflict
			    if ( intMode == IntegrationMode.LOCAL_FIRST ) { // add it
				al.addAlignCell(c.getObject1(), c.getObject2(), c.getRelation().getRelation(), 1.0);
			    }
			}
                    } catch (AlignmentException alex) {
                        throw new LLException("Cannot update alignment", alex);
                    }
                }
            }
        }
    }

    public void save(File dir) throws LLException {
        File agdir = new File(dir, "" + envid);
        agdir.mkdir();
        // Here it should save the alignments LLAgent should have this method
        int lenToStrip = 5; // size of "file:"
        if (agdir != null) {
            PrintWriter pr = null;
            try {
                for (URI u : env.getOntologies()) {
                    if (!ontologyURI.equals(u)) {
                        Alignment al = alignments.get(u);
                        String uri = al.getExtension(Namespace.ALIGNMENT.uri, Annotations.ID);
                        pr = new PrintWriter(
                                new BufferedWriter(
                                        new OutputStreamWriter(new FileOutputStream(agdir + "/" + uri.substring(lenToStrip)), StandardCharsets.UTF_8.name())), true);
                        RDFRendererVisitor renderer = new RDFRendererVisitor(pr);
                        renderer.init(new Properties());
                        al.render(renderer);
                        pr.close();
                    }
                }
            } catch (UnsupportedEncodingException ueex) {
                logger.debug("IGNORED Exception", ueex);
            } catch (FileNotFoundException fnfex) {
                logger.debug("IGNORED Exception", fnfex);
            } catch (AlignmentException alex) {
                logger.warn("IGNORED Exception", alex);
            } finally {
                pr.close();
            }
        }
    }

    public void load(File dir) {
        // All is above
    }

    public enum IntegrationMode {FATIMA, CONSENSUS_ONLY, CONSENSUS_FIRST, LOCAL_ONLY, LOCAL_FIRST, RANDOM, PROBABLISTIC, ALIGNMENT_DISTANCE, MEMORY_INTER}
}
