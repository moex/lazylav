/*
 * Copyright (C) INRIA, 2016-2019
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 */

package fr.inria.exmo.lazylavender.agent;

import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeSet;
import java.util.UUID;
import java.util.Properties;


import org.semanticweb.HermiT.Reasoner;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLClassAssertionAxiom;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLEntity;
import org.semanticweb.owlapi.model.OWLIndividual;
import org.semanticweb.owlapi.model.OWLNamedIndividual;
import org.semanticweb.owlapi.model.OWLObjectProperty;
import org.semanticweb.owlapi.model.OWLObjectPropertyAssertionAxiom;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.reasoner.Node;
import org.semanticweb.owlapi.reasoner.NodeSet;
import org.semanticweb.owlapi.reasoner.OWLReasoner;
import org.semanticweb.owlapi.reasoner.OWLReasonerFactory;
import org.semanticweb.owlapi.reasoner.SimpleConfiguration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.inria.exmo.lazylavender.env.features.FeatureEnvironment;
import fr.inria.exmo.lazylavender.env.features.OWLOntologyHelper;
import fr.inria.exmo.lazylavender.model.LLException;
import fr.inria.exmo.lazylavender.agent.LLAgent;
import fr.inria.exmo.lazylavender.expe.LLGame;
import fr.inria.exmo.lazylavender.expe.ObjectIdGame;

public class OntologyEvolvingAgent implements LLAgent {
    private final static Logger logger = LoggerFactory.getLogger(OntologyEvolvingAgent.class);

    private int envid;
    private OWLOntology ontology;
    private OWLOntologyManager manager;
    private OWLDataFactory factory;
    private OWLReasoner reasoner;
    private String categoryNamesPrefix;
    
    private int initNbCategories;
    private int nbCategoriesLearnedFromOtherAgents;
    private int nbCategoriesCreatedByIntersection;

    public OntologyEvolvingAgent( OWLOntology ontology, int index ) throws LLException {
	this.envid = index;
	this.ontology = ontology;
	this.manager = ontology.getOWLOntologyManager();
	this.factory = manager.getOWLDataFactory();
	
	this.categoryNamesPrefix = FeatureEnvironment.uriprefix + FeatureEnvironment.agent_prefix + getId() + "#";
	
	OWLReasonerFactory reasonerFactory = new Reasoner.ReasonerFactory();
	reasoner = reasonerFactory.createReasoner(ontology, new SimpleConfiguration());
	
	if (!reasoner.isConsistent()) {
	    logger.error("Inconsistent Ontology: " + OWLOntologyHelper.getOntologyToString(ontology));
	    throw new LLException("Initial agent[" + getId() + "] ontology is not consistent! ");
	}
	
	this.initNbCategories = getNbOfCategories();
    }

    public OntologyEvolvingAgent init( Properties param ) {
	return this;
    }
    
    public void playGame( final int remaining, final LLGame game ) throws LLException {
	if ( !( game instanceof ObjectIdGame ) )
	    throw new LLException( "Can only play InstanceIdGame "+game );
    }

    public boolean equivalentCategoryAlreadyExists(OWLClassExpression category) {
	reasoner.flush();
	Node<OWLClass> equivalentClasses = reasoner.getEquivalentClasses(category);
	return equivalentClasses.getSize() > 0;
    }
    
    /**
     * Returns the number of categories of interest in the agents ontology, including the initial, shared category named "all". The categories of interest are all the subclasses of "all".
     * Thus, an agent can have at least 1 category of interest (which is "all")
     * @return
     * @throws LLException
     */
    public int getNbOfCategories() throws LLException {
	OWLClassExpression allObjectsCategory = FeatureEnvironment.getAllObjectsFromTheEnvironmentCategory(ontology);
	reasoner.flush();
	NodeSet<OWLClass> subClasses = reasoner.getSubClasses(allObjectsCategory, false);
	// nb of subclasses of "all" + 1 (OWL:Nothing) - 1 ("all")
	int nbCategories = subClasses.getNodes().size();
	return nbCategories;
    }
    
    public int getNbOfClasses() {
	if (ontology != null) {
	    return ontology.getClassesInSignature().size();
	}
	return 0;
    }
    
    /**
     * return the name of the most specific class this object belongs to; if there are more, create intersection class
     * 
     * @param object
     * @return null if the most specific class is owl:Thing
     * @throws LLException
     */
    public OWLClass classify(HashMap<String, String> object) throws LLException {
	OWLClass result = null;
	
	NodeSet<OWLClass> mostSpecificClassesNodeSet = getMostSpecificClasses(object);
	Set<Node<OWLClass>> mostSpecificClassesSet = mostSpecificClassesNodeSet.getNodes();
	if (mostSpecificClassesSet == null || mostSpecificClassesSet.isEmpty()) {
	    throw new LLException("No class found for object [" + object + "] !");
	}
	if (mostSpecificClassesSet.size() > 1) {
	    
	    Set<String> mostSpecificClassesNames = new TreeSet<>();
	    Set<OWLClassExpression> mostSpecificClassesDefinitions = new TreeSet<>();
	    
	    for (Node<OWLClass> node : mostSpecificClassesSet) {
		Set<OWLClass> entities = node.getEntities();
		if (entities.size() != 1) {
		    throw new LLException("Node<OWLClass> contains more than one class: [" + entities + "] !");
		}
		OWLClass category = entities.iterator().next();
		
		String categoryName = getEntityName(category);
		mostSpecificClassesNames.add(categoryName);
		
		OWLClassExpression categoryDefinition = FeatureEnvironment.getCategoryDefinition(ontology, category);
		mostSpecificClassesDefinitions.add(categoryDefinition);
		
	    }
	    String newCategoryName = mostSpecificClassesNames.toString().replace(" ", "");
	    OWLClass newCategoryClass = factory.getOWLClass(getEntityIRI(newCategoryName));
	    
	    logger.debug("AGENT " + envid + "CREATES AN NEW INTERSECTION CLASS: " + newCategoryClass);
	    setNbCategoriesCreatedByIntersection(getNbCategoriesCreatedByIntersection() + 1);
	    
	    OWLOntologyHelper.addConjunctionClass(ontology, newCategoryClass, mostSpecificClassesDefinitions);
	    result = newCategoryClass;
	} else {
	    result = mostSpecificClassesSet.iterator().next().getEntities().iterator().next();
	    if (result.equals(factory.getOWLThing())) {
		throw new LLException(" AGENT" + envid + "CLASSIFIES object " + object + " as OWL:THING!!");
	    }
	}
	
	return result;
    }
    
    public String getEntityName(OWLEntity entity) throws LLException {
	String prefix;
	if (entity.equals(FeatureEnvironment.getAllObjectsFromTheEnvironmentCategory(ontology))) {
	    prefix = FeatureEnvironment.uriprefix + "#";
	} else {
	    prefix = categoryNamesPrefix;
	}
	
	String entityName = entity.getIRI().toString().substring(prefix.length());
	return entityName;
    }
    
    public IRI getEntityIRI(String entityName) {
	String iri;
	if (entityName.equals(FeatureEnvironment.all_objects_in_environment)) {
	    iri = FeatureEnvironment.uriprefix + "#" + FeatureEnvironment.all_objects_in_environment;
	} else {
	    iri = categoryNamesPrefix + entityName;
	}
	return IRI.create(iri);
    }
    
    /**
     * Returns the signatures of the most specific classes the object belongs to
     * 
     * @param object
     * @return
     * @throws LLException
     */
    public NodeSet<OWLClass> getMostSpecificClasses(HashMap<String, String> object) throws LLException {
	
	OWLNamedIndividual namedIndividual = createNamedIndividual(object);
	
	reasoner.flush();
	NodeSet<OWLClass> types = reasoner.getTypes(namedIndividual, true);
	
	logger.debug("AGENT " + envid + " CLASSIFIES OBJECT " + object + "AS: " + types.getFlattened() + "and registers it as: " + namedIndividual.getIRI());
	
	return types;
    }
    
    public OWLNamedIndividual createNamedIndividual(HashMap<String, String> object) throws LLException {
	
	long objectName = UUID.randomUUID().getLeastSignificantBits();
	OWLNamedIndividual namedIndividual = factory.getOWLNamedIndividual(getEntityIRI(String.valueOf(objectName)));
	
	// object belongs to class all
	OWLClass all = FeatureEnvironment.getAllObjectsFromTheEnvironmentCategory(ontology);
	OWLClassAssertionAxiom classAssertion = factory.getOWLClassAssertionAxiom(all, namedIndividual);
	manager.addAxiom(ontology, classAssertion);
	
	Set<Entry<String, String>> objectProperties = object.entrySet();
	for (Entry<String, String> entry : objectProperties) {
	    // XXX for agents that know a limited set of features, check existence in their onto
	    String featureName = entry.getKey();
	    String featureValueName = entry.getValue();
	    
	    OWLObjectProperty featureObjectProperty = FeatureEnvironment.getFeatureObjectProperty(ontology, featureName);
	    OWLNamedIndividual featureValueIndividual = FeatureEnvironment.getFeatureValueIndividual(ontology, featureValueName);
	    OWLObjectPropertyAssertionAxiom owlObjectPropertyAssertionAxiom = factory.getOWLObjectPropertyAssertionAxiom(featureObjectProperty, namedIndividual, featureValueIndividual);
	    manager.addAxiom(ontology, owlObjectPropertyAssertionAxiom);
	}
	
	return namedIndividual;
    }
    
    public boolean isIndividualInstanceOf(OWLIndividual individual, OWLClass category) {
	reasoner.flush();
	OWLClassAssertionAxiom classAssertion = factory.getOWLClassAssertionAxiom(category, individual);
	boolean isInstanceOf = reasoner.isEntailed(classAssertion);
	return isInstanceOf;
    }
    
    public OWLOntology getOntology() {
	return ontology;
    }
    
    public void setOntology(OWLOntology ontology) {
	this.ontology = ontology;
    }
    
    public int getId() {
	return envid;
    }
    
    public int getInitNbCategories() {
	return initNbCategories;
    }
    
    public int getNbCategoriesLearnedFromOtherAgents() {
	return nbCategoriesLearnedFromOtherAgents;
    }
    
    public void setNbCategoriesLearnedFromOtherAgents(int nbCategoriesLearnedFromOtherAgents) {
	this.nbCategoriesLearnedFromOtherAgents = nbCategoriesLearnedFromOtherAgents;
    }
    
    public int getNbCategoriesCreatedByIntersection() {
	return nbCategoriesCreatedByIntersection;
    }
    
    public void setNbCategoriesCreatedByIntersection(int nbCategoriesCreatedByIntersection) {
	this.nbCategoriesCreatedByIntersection = nbCategoriesCreatedByIntersection;
    }
    
//	FIXME 
//	category1Name: [[color_black,letter_a,shape_square],[letter_a,size_s]]
//	category2Name: [[color_black,letter_b,size_s],[shape_square]]
//	category1SubsumesCategory2: false, category2SubsumesCategory1: false, category1DisjointFromCategory2: false
	
//	category1Name: [[color_white,letter_b],[shape_triangle,size_s]]
//	category2Name: [[color_black,letter_b,size_s],[color_black,shape_triangle,size_s]]
//	category1SubsumesCategory2: false, category2SubsumesCategory1: false, category1DisjointFromCategory2: false
	
//	TODO use this; test why it isn't working 
    public void determineRelationship(String category1Name, String category2Name ) throws LLException {
	OWLClass category1 = FeatureEnvironment.getCategory(ontology, envid, category1Name);
	//			OWLClassExpression category1Definition = FeatureEnvironment.getCategoryDefinition(ontology, category1);
	
	OWLClass category2 = FeatureEnvironment.getCategory(ontology, envid, category2Name);
	//			OWLClassExpression category2Definition = FeatureEnvironment.getCategoryDefinition(ontology, category2);
	
	OWLAxiom category1SubsumesCategory2Axiom = factory.getOWLSubClassOfAxiom(category2, category1) ;
	OWLAxiom category2SubsumesCategory1Axiom = factory.getOWLSubClassOfAxiom(category1, category2) ;
	OWLAxiom category1DisjointFromCategory2Axiom=factory.getOWLDisjointClassesAxiom(category1, category2);
	
	logger.trace("category1Name: {}", category1Name);
	logger.trace("category2Name: {}", category2Name);
	
	boolean category1SubsumesCategory2 = reasoner.isEntailed(category1SubsumesCategory2Axiom);
	boolean category2SubsumesCategory1 = reasoner.isEntailed(category2SubsumesCategory1Axiom);
	boolean category1DisjointFromCategory2 = reasoner.isEntailed(category1DisjointFromCategory2Axiom);
	
	logger.trace("category1SubsumesCategory2: {}, category2SubsumesCategory1: {}, category1DisjointFromCategory2: {}", category1SubsumesCategory2,  category2SubsumesCategory1, category1DisjointFromCategory2 );
    }

}
