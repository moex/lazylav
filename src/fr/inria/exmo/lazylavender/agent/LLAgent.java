/*
 * Copyright (C) INRIA, 2019
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 */

package fr.inria.exmo.lazylavender.agent;

import fr.inria.exmo.lazylavender.model.LLException;
import fr.inria.exmo.lazylavender.expe.LLGame;
import fr.inria.exmo.lazylavender.logger.ActionLogger;

import java.util.Properties;

public interface LLAgent {

    public LLAgent init( Properties param );

    // honestly, why an int! Should be at worse java.io.Serializable (does not work)
    public int getId();
    
    public void playGame( int remaining, final LLGame game ) throws LLException;
    
}
