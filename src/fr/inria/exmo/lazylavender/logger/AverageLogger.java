/*
 * Copyright (C) INRIA, 2014, 2019, 2021
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 */

package fr.inria.exmo.lazylavender.logger;

import java.io.PrintWriter;
import java.util.Iterator;
import java.util.Vector;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AverageLogger implements LLLogger { // should extend actionlogger
    final static Logger logger = LoggerFactory.getLogger( AverageLogger.class );

    boolean report = false;

    int nbActions = 0;
    int nbSuccess = 0;

    String[] measureNames; // could be common to ActionLogger

    public String[] getMeasureNames() {
	return measureNames;
    }

    Vector<Point> plot = null;

    public AverageLogger( boolean immediateDisplay ) {
	report = immediateDisplay;
    }

    public void aggregate( ActionLogger[] loggers ) {
	int nbruns = loggers.length;
	if ( nbruns > 0 ) {
	    // JE: They must have the same length
	    int nbiter = loggers[0].plot.size();
	    int nbchars = nbiter==0 ? 0 :loggers[0].plot.get(0).values.length;
	    //logger.debug( "{} / {} / {}", nbruns, nbiter, nbchars );
	    if ( nbchars > 0 ) { // If nothing to average, forget it
		// Create the measure names
		measureNames = new String[3*nbchars];
		int z=0;
		for( String name : loggers[0].getMeasureNames() ){
		    measureNames[z++] = "min-"+name;
		    measureNames[z++] = "avg-"+name;
		    measureNames[z++] = "max-"+name;
		}
		// Aggregate the values
		Measure[] aggregator = new Measure[nbchars];
		for ( int k=0; k < nbchars; k++ ) aggregator[k] = new Measure();
		plot = new Vector<Point>();
		for ( int i=0; i < nbiter; i++ ) {
		    for ( int k=0; k < nbchars; k++ ) aggregator[k].init();
		    for ( int j=0; j < nbruns; j++ ) {
			for ( int k=0; k < nbchars; k++ ) {
			    aggregator[k].add( loggers[j].plot.get(i).values[k] );
			}
		    }
		    double[] values = new double[3*nbchars];
		    for ( int k=0; k < nbchars; k++ ) {
			int n = 3*k;
			values[n] = aggregator[k].min;
			values[n+1] = aggregator[k].sum/nbruns;
			values[n+2] = aggregator[k].max;
		    }
		    plot.add( new Point( loggers[0].plot.get(i).rank, values ) );
		}
	    }
	}
	// With history = rank + array of values
	// Returning history = rank + array of mean, array of max, array of average
    }

    public void fullReport( PrintWriter output ) {
	if ( plot != null ) {
	    output.print( "rank" );
	    for ( String name : getMeasureNames() ) {
		output.print( " " );
		output.print( name );
	    }
	    output.println();
	    for ( Iterator<Point> itplot = plot.iterator(); itplot.hasNext(); ) {
		Point pt = itplot.next();
		output.print( pt.rank );
		for ( int i = 0; i < pt.values.length; i++ ) output.print( " "+pt.values[i] );
		output.println();
	    }
	}
    }
}

class Measure {
    public double min = 1; // That only works when values are below 1, otherwise this will return 1.
    public double sum = 0;
    public double max = 0;

    public Measure () {};

    public void init() {
	min = 1;
	sum = 0;
	max = 0;
    }

    public void add ( double val ) {
	sum += val;
	min = Math.min( min, val );
	max = Math.max( max, val );
    }
}
