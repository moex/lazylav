/*
 *
 * Copyright (C) INRIA, 2014, 2019, 2021
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 */

package fr.inria.exmo.lazylavender.logger;

import java.io.PrintWriter;
import java.util.Iterator;
import java.util.Vector;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

// Everything is string-based to allow garbage collection.

public class ActionLogger implements LLLogger {
    final static Logger logger = LoggerFactory.getLogger( ActionLogger.class );

    boolean report = false;

    int nbActions = 0;
    int nbSuccess = 0;

    String[] measureNames;

    Vector<Entry> entries = null;
    public Vector<Point> plot = null;

    public ActionLogger( boolean immediateDisplay, String[] measure ) {
	report = immediateDisplay;
	entries = new Vector<Entry>();
	measureNames = measure;
    }

    public void log() { nbActions++; }

    public void log( String act ) {
	log();
	if ( report ) logger.info( act );
    }

    // Agent
    public void log( int ag1, int ag2, String query, String answer, boolean success, String repair ) {
	double[] measures = {0};
	log( ag1, ag2, query, answer, success, repair, measures );
    }

    // Agent = logGame + logResult + logMeasure
    public void log( int ag1, int ag2, String query, String answer, boolean success, String repair, double[] measures ) {
		log();
		if ( success ) nbSuccess++;
		double rate = (double)nbSuccess/nbActions;
		if ( report ) {
			logger.info( "{} => {}: {}", ag1, ag2, query );
			if ( success ) {
			logger.info( " <- {}: SUCCESS [{}]", answer, rate );
			} else {
			logger.info( " <- {}: FAILURE: {} [{}]", answer, repair, rate );
			}
		}
		measures[0] = rate;
		entries.add( new Entry( ag1, ag2, query, answer, success, repair, measures ) );
    }

    public void logGame( int ag1, int ag2, String query ) {
	log();
	entries.add( new Entry( ag1, ag2, query ) ); // Why not game?
    }

	public void logGame( int ag1, int[] ag2, String query ) {
		log();
		entries.add( new Entry( ag1, ag2, query ) );
	}
    
    // Agent
    public void logResult( String answer, boolean success, String repair ) {
		Entry game = entries.lastElement();
		if ( success ) {
			nbSuccess++;
		}
		game.answer = answer;
		game.success = success;
		game.repair = repair;
    }

    public void logMeasures( double[] measures ) {
		Entry game = entries.lastElement();
		game.measures = measures;
		double rate = (double)nbSuccess/nbActions;
		if ( report ) { // Could have gone above... and not in the logger of the logger...
			logger.info( "{} => {}: {}", game.ag1, game.ag2, game.query );
			if ( game.success ) {
				logger.info( " <- {}: SUCCESS [{}]", game.answer, rate );
			} else {
				logger.info( " <- {}: FAILURE: {} [{}]", game.answer, game.repair, rate );
			}
		}
		measures[0] = rate;
    }

	public void logMeasures2( double[] measures ) {
		Entry game = entries.lastElement();
		game.measures = measures;
		double rate = (double)nbSuccess/nbActions;
		if ( report ) { // Could have gone above... and not in the logger of the logger...
			logger.info( "{} => {}: {}", game.ag1, game.ag2, game.query );
			if ( game.success ) {
				logger.info( " <- {}: SUCCESS [{}]", game.answer, rate );
			} else {
				logger.info( " <- {}: FAILURE: {} [{}]", game.answer, game.repair, rate );
			}
		}
	}

	public String[] getMeasureNames() {
		return measureNames;
    }

    public void report() {
	logger.info( "Nb games={}, Success rate= {}", nbActions, (double)nbSuccess/nbActions );
    }

    public void fullReport( PrintWriter output ) {
	compPlot();
	output.print( "rank" );
	for ( String name : getMeasureNames() ) {
	    output.print( " " );
	    output.print( name );
	}
	output.println();
	for ( Iterator<Point> itplot = plot.iterator(); itplot.hasNext(); ) {
	    Point pt = itplot.next();
	    output.print( pt.rank );
	    for ( int i = 0; i < pt.values.length; i++ ) output.print( " "+pt.values[i] );
	    output.println();
	}
    }

    public void plotReport( PrintWriter output ) {
	compPlot();
	output.println( "rank rate" );
	for ( Iterator<Point> itplot = plot.iterator(); itplot.hasNext(); ) {
	    Point pt = itplot.next();
	    output.print( pt.rank );
	    for ( int i = 0; i < pt.values.length; i++ ) output.print( " "+pt.values[i] );
	    output.println();
	}
    }

    public void compPlot() {
		if ( plot == null ) {
			plot = new Vector<>();
			int rank = 0;
			for ( Iterator<Entry> itent = entries.iterator(); itent.hasNext(); ) {
				Entry entry = itent.next();
				rank++;
				plot.add( new Point( rank, entry.measures ) );
			}
		}
    }
}

class Entry {
    public int ag1;
    public int ag2;
	public int[] agn;
    public String query;
    public String answer;
    public boolean success;
    public String repair;
    public double[] measures;

    public Entry ( int a1, int a2, String q ) {
	ag1 = a1; ag2 = a2; query = q;
    }

	public Entry ( int a1, int[] an, String q ) {
		ag1 = a1; ag2 = -1; query = q; agn = an;
	}

	public Entry ( int a1, int a2, String q, String a, boolean s, String r ) {
	ag1 = a1; ag2 = a2; query = q; answer = a; success = s; repair = r;
    } 
		  
    public Entry ( int a1, int a2, String q, String a, boolean s, String r, double[] m ) {
	ag1 = a1; ag2 = a2; query = q; answer = a; success = s; repair = r; measures = m;
    } 
		  
}
