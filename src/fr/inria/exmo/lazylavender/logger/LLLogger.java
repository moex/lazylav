/*
 *
 * Copyright (C) INRIA, 2021
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 */

package fr.inria.exmo.lazylavender.logger;

import java.io.PrintWriter;

/**
 * This interface is currently useless and empty...
 */

public interface LLLogger {

    /*
    public void log();

    public void log( String act );

    public void log( int ag1, int ag2, String query, String answer, boolean success, String repair );

    public void log( int ag1, int ag2, String query, String answer, boolean success, String repair, double[] measures );

    public void logGame( int ag1, int ag2, String query );
    
    public void logResult( String answer, boolean success, String repair );

    public void logMeasures( double[] measures );

    public String[] getMeasureNames();

    public void report();

    public void fullReport( PrintWriter output );

    public void plotReport( PrintWriter output );

    public void compPlot();
    */
}

