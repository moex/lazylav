package fr.inria.exmo.lazylavender.engine.resultorganiser;

import fr.inria.exmo.lazylavender.engine.ExperimentalPlan;
import fr.inria.exmo.lazylavender.logger.ActionLogger;
import fr.inria.exmo.lazylavender.logger.AverageLogger;
import fr.inria.exmo.lazylavender.logger.Point;

import java.io.*;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class GDPlanResults extends ExperimentalPlan.PlanResultsOrganiser {

    private static final int NONAME = 1;
    private static final int NAME_DIRECTORIES = 2;
    private static final int NAME_FILES = 3;

    Map<String, Integer> nameCounter = new HashMap<>();


    int index = 1;
    int namingMethod = NAME_DIRECTORIES;

    @Override
    public void report() {
        new File(outputDirectory).mkdirs();
        for(ExperimentalPlan.ExperimentResult r: results) {
            String dir = getPrefix(r);
            report(r,dir);
            r.writePrams(dir+"parameters.csv");
            index ++;
        }
        results.clear();
    }

    protected String getPrefix(ExperimentalPlan.ExperimentResult r) {
        if(namingMethod == NONAME) {
            String dir = outputDirectory + "params" + index + "/";
            new File(dir).mkdirs();
            return dir;
        }
        String dir = outputDirectory + getNameFromParams(r);
        if(namingMethod == NAME_DIRECTORIES) {
            dir += "/";
            new File(dir).mkdirs();
        }
        else
            dir += "-";
        return dir;
    }

    protected String getNameFromParams(ExperimentalPlan.ExperimentResult r) {
        String name = fnamingPattern;
        Matcher m = Pattern.compile("@(.+?)@")
                .matcher(fnamingPattern);
        while (m.find()) {
            name = name.replaceAll(m.group(),r.planParams.getProperty(m.group(1)));
        }

        // managing duplicate names
        if(!nameCounter.containsKey(name))
            nameCounter.put(name, 1);
        else {
            int count = nameCounter.get(name);
            count ++;
            nameCounter.put(name, count);
            name += "(" + count + ")";
        }
        return name;
    }



    public void report(ExperimentalPlan.ExperimentResult r, String directory) {
        PrintWriter writer = null;
        if (directory == null) {
            writer = new PrintWriter(System.out, true);
        } else {
            try {
                writer = new PrintWriter(
                        new BufferedWriter(
                                new OutputStreamWriter(
                                        new FileOutputStream(directory + "average.csv"),
                                        "UTF-8")),
                        true);
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
        for (int i = 0; i < r.loggers.length; i++) r.loggers[i].compPlot();

        for (int i = 0; i < r.loggers.length; i++) {
            fullReport(r.loggers[i], directory + "run"+(i+1)+".csv");
        }
        AverageLogger avglog = new AverageLogger(false);
        avglog.aggregate(r.loggers);
        avglog.fullReport(writer);
        writer.close();
    }

    public void fullReport(ActionLogger logger, String file ) {
        if(file == null) return;
        try {
            PrintWriter output = new PrintWriter(
                    new BufferedWriter(
                            new OutputStreamWriter(
                                    new FileOutputStream(file),
                                    "UTF-8")),
                    true);
            if ( logger.plot != null ) {
                output.print( "rank" );
                for ( String name : logger.getMeasureNames() ) {
                    output.print( " " );
                    output.print( name );
                }
                output.println();
                for (Iterator<Point> itplot = logger.plot.iterator(); itplot.hasNext(); ) {
                    Point pt = itplot.next();
                    output.print( pt.rank );
                    for ( int i = 0; i < pt.values.length; i++ ) output.print( " "+pt.values[i] );
                    output.println();
                }
            }
            output.close();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

}
