/*
 *
 * Copyright (C) INRIA, 2014, 2016-2018, 2021
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 */

package fr.inria.exmo.lazylavender.engine;

import java.io.File;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.Properties;
import java.lang.reflect.Constructor;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.HelpFormatter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.inrialpes.exmo.align.cli.CommonCLI;

import fr.inria.exmo.lazylavender.model.LLException;
import fr.inria.exmo.lazylavender.expe.LLExperiment;
import fr.inria.exmo.lazylavender.expe.AlignmentRevisionExperiment;
import fr.inria.exmo.lazylavender.expe.OntologyEvolutionExperiment;
import fr.inria.exmo.lazylavender.logger.ActionLogger;
import fr.inria.exmo.lazylavender.logger.AverageLogger;

public class Monitor extends CommonCLI { // extendss LLEngine
    final static Logger logger = LoggerFactory.getLogger( Monitor.class );
    
    public static final String EXPERIMENT_PARAMETER_NAME="experiment";
    public static final String EXPERIMENT_ALIGNMENT_REVISION="NOORevision";
    public static final String EXPERIMENT_ONTOLOGY_EVOLVE="OntologyEvolution";
    
    /**
     * The number of experiments to be run (-DnbRuns=)
     */
    private int nbRuns = 5;

    // Reuses CommonCLI, create our own if needed
    public Monitor() {
	super();
    }

    // Should be in LLEngineImpl
    File saveDir = null;
    File loadDir = null;

    boolean saveRun = false;

    // This one has a main()
    public static void main( String[] args ) {
	try { new Monitor().run( args ); }
	catch ( Exception ex ) {
	    ex.printStackTrace();
	    System.exit(-1);
	};
    }

    // 2020: split into parse+init+process+report
    // It is unclear if all the treatment 
    public void run( String[] args ) throws Exception {
	CommandLine line = null;
	logger.debug( "Parsing arguments..." );
	try { 
	    line = parseCommandLine( args );
	    if ( line == null ) System.exit(1); // --help
	    //parameters.list( System.err );
	} catch( ParseException exp ) {
	    logger.error( "Cannot parse command line", exp );
	    usage();
	    System.exit(-1);
	}
	logger.debug( "Initializing..." );
	// Initialize (already done here with the parsed parameters)
	init( parameters );
	// Run expe
	ActionLogger[] loggers = process();
	// Report
	logger.debug( "Reporting..." );
	report( loggers );
    }

    public void init( Properties params ) throws IOException, FileNotFoundException {
	if ( parameters != params ) parameters = params; // called from outside
	// Get load directory
	if ( parameters.getProperty( "loadDir" ) != null ) {
	    loadDir = new File( parameters.getProperty( "loadDir" ) );
	    if ( parameters.getProperty( "loadParams" ) != null ) {
		// load them from file
		Properties storedParams = new Properties();
		storedParams.loadFromXML( new FileInputStream( new File( loadDir, "params.xml" ) ) );
		// == add new params... but loadParams??? to loaded ones?
		storedParams.list( System.err );
		// merge both parameter sets... (maybe not savedir)
		storedParams.forEach( (k, v) ->
				      { if ( !((String)k).equals("saveDir") )
					      parameters.setProperty( (String)k, (String)v ); });
		//parameters.list( System.err );
	    }
	}
	// Get save directory
	if ( parameters.getProperty( "saveDir" ) != null ) {
	    saveDir = new File( parameters.getProperty( "saveDir" ) );
	    saveDir.mkdir();
	    if ( parameters.getProperty( "saveParams" ) != null ) {
		//store the properties detail into a pre-defined XML file
		parameters.storeToXML( new FileOutputStream( new File( saveDir, "params.xml" ) ),
				       "Lazy lavender session parameters", "UTF-8");
	    }
	}
	// Get nbRuns
	if ( parameters.getProperty( "saveEachRun" ) != null ) {
	    if ( saveDir != null ) {
		saveRun = true;
	    } else {
		logger.warn( "saveEachRun option not honoured: it requires saveDir to be set" );
	    };
	}
	// Get nbRuns
	if ( parameters.getProperty( "nbRuns" ) != null ) {
	    nbRuns = Integer.parseInt( parameters.getProperty( "nbRuns" ) );
	}
    }
    
    public ActionLogger[] process() throws LLException {
	// The real work
	ActionLogger[] loggers = new ActionLogger[nbRuns];
	for ( int i = 0; i < nbRuns; i++ ) {
	    logger.debug( "Initialising iteration {}...", i );
	    if ( loadDir != null ) {
		parameters.setProperty( "loadRunDir", loadDir.toString()+"/"+i );
	    }
	    LLExperiment expe = createExperiment( parameters );
	    // create save directory
	    if ( saveDir != null ) {
		parameters.setProperty( "runDir", saveDir.toString()+"/"+i );
	    }
	    loggers[i] = expe.init( parameters );
	    logger.debug( "Processing experiment #{} ...", i );
	    expe.process();
	    if ( saveRun ) {
		File runDirD = new File( parameters.getProperty( "runDir" ) );
		runDirD.mkdir();
		// Set output file
		PrintWriter writer = null;
		try {
		    writer = new PrintWriter (
			           new BufferedWriter(
			                 new OutputStreamWriter( 
					     new FileOutputStream( runDirD+"/results.tsv" ),
					     "UTF-8" )), 
				   true);
		    loggers[i].compPlot();
		    loggers[i].fullReport( writer );
		} catch (FileNotFoundException fnfex) {
		    logger.error( "Cannot write output in {}: file not found", runDirD + "/results.tsv" );
		} catch (UnsupportedEncodingException ueex) {
		    logger.debug( "IGNORED UnsupportedEncodingException" );
		} finally {
		    writer.close();
		}
	    }
	}
	return loggers;
    }
    
    private LLExperiment createExperiment( Properties parameters ) throws LLException {
	String expeType = parameters.getProperty( EXPERIMENT_PARAMETER_NAME );
	LLExperiment experiment = null;
	try {
	    if ( expeType == null || expeType.equals( EXPERIMENT_ALIGNMENT_REVISION ) ) {
		experiment = new AlignmentRevisionExperiment();
	    } else if( expeType.equals(EXPERIMENT_ONTOLOGY_EVOLVE) ){
		experiment = new OntologyEvolutionExperiment();
	    } else { // classname
		Class<?> experimentClass = Class.forName( expeType );
		Class<?>[] cparams = {};
		Constructor<?> experimentConstructor = experimentClass.getConstructor(cparams);
		Object[] mparams = {};
		experiment = (LLExperiment)experimentConstructor.newInstance( mparams );
	    }
	    return experiment;
	} catch ( Exception ex ) { // ClassNotFoundException+NoSuchMethodException+InstantiationException
	    throw new LLException( "Cannot create experiment "+expeType, ex );
	}
    }
   
    public void report( ActionLogger[] loggers ) {
	// Create filename
	PrintWriter writer = null;
	if ( outputfilename == null ) {
	    writer = new PrintWriter( System.out, true );
	} else {
	    try {
		writer = new PrintWriter (
			           new BufferedWriter(
			                 new OutputStreamWriter( 
					     new FileOutputStream( outputfilename ),
					     "UTF-8" )), 
				   true);
	    } catch (FileNotFoundException fnfex) {
		logger.error( "Cannot write output in {}: file not found", outputfilename );
	    } catch (UnsupportedEncodingException ueex) {
		logger.debug( "IGNORED UnsupportedEncodingException" );
	    }
	}
	// Aggregate results
	for ( int i = 0; i < loggers.length; i++ ) loggers[i].compPlot();
	AverageLogger avglog = new AverageLogger( false );
	avglog.aggregate( loggers );
	// Print
	avglog.fullReport( writer );
	writer.close();
    }    

    public void usage() {
	new HelpFormatter().printHelp( 80, "java "+this.getClass().getName()+" [options]", "\nOptions:", options, "\nSee https://gitlab.inria.fr/moex/lazylav/-/blob/master/doc/parameters.md for an extensive list of properties\n" );
    }
}
