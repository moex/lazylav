/*
 *
 * Copyright (C) INRIA, 2020
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 */

package fr.inria.exmo.lazylavender.engine;

import fr.inria.exmo.lazylavender.engine.resultorganiser.GDPlanResults;
import fr.inria.exmo.lazylavender.logger.ActionLogger;
import fr.inria.exmo.lazylavender.logger.AverageLogger;
import fr.inria.exmo.lazylavender.model.LLException;
import fr.inrialpes.exmo.align.cli.CommonCLI;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.regex.Pattern;

public class ExperimentalPlan extends CommonCLI {
    final static Logger logger = LoggerFactory.getLogger(ExperimentalPlan.class);
    static Class<?>[] organisers = {PlanResultsOrganiser.class, GDPlanResults.class};
    static String[] organisersNames = {"console", "godecisions"};

    // Reuses CommonCLI
    // For Yasser this means that we already know how to parse the command line!
    public ExperimentalPlan() {
        super();
    }

    // The code launched from CLI
    public static void main(String[] args) {
        long startTime = System.nanoTime();
        try {
            new ExperimentalPlan().run(args);
        } catch (Exception ex) {
            ex.printStackTrace();
            System.exit(-1);
        }
        logger.debug("ExperimentalPlan finished in {} s", (System.nanoTime() - startTime) / 1_000_000_000);
    }

    public void run(String[] args) throws LLException {
        // Read and extract the command line (in args)
        // they go in the parameter variable because we extend CommonCLI
        CommandLine line = null; // This line is useless: this is juste the return result
        logger.debug("Parsing arguments...");
        try {
            line = parseCommandLine(args);
            if (line == null) System.exit(1); // --help
        } catch (ParseException exp) {
            logger.error("Cannot parse command line", exp);
            usage();
            System.exit(-1);
        }
        // Create the iterator that contains the stuff to be run in the plan
        // Not easy to know if those are for the ExperimentalPlan or for other stuff... anyway
        // Extract the experiment to run
        ParamsIterator iterator = new ParamsIterator(parameters);

        // YB: to organise output files
        PlanResultsOrganiser resultsOrganiser = createOrganiser(parameters);
        resultsOrganiser.init(parameters, iterator.namingPattern);
        int count = 0;
        // Iterate on this iterator to lanch experiments through monitor (no
        while (iterator.hasNext()) {
            // would be better (for this I guess ParamsIteraor should better be iterable)
            //for ( Properties param: iterator ) {
            try {
                Monitor mon = new Monitor();
                // Here this should be the equivalent of
                // Initialize (already done here with the parsed parameters)
                Properties p = iterator.next();
                // YB: init(p) means that iterator returns all parameters, not only experiment plan ones
                mon.init(p);
                logger.debug("Running monitor " + (++count));
                // Run expe
                long startTime = System.nanoTime();
                ActionLogger[] loggers = mon.process();
                logger.debug("Monitor {} finished in {} s", count, (System.nanoTime() - startTime) / 1_000_000_000);
                // The loggers (results) must be stored in a structure
                // I suggest to put it in the ParamsIterator
                // (so the parameter and the results would be there).
                // ...
                resultsOrganiser.addResult(new ExperimentResult(p, loggers, iterator.commandParams));
                resultsOrganiser.report();

            } catch (Exception ex) {
                ex.printStackTrace();
                System.exit(-1);
            }


        }
        // Here process the results the way you like.
        // This may be something like Monitor.report( loggers );
        // But taking the loggers from the structure

    }


    private int indexOf(String s, String[] strings) {
        for (int i = 0; i < strings.length; i++)
            if (s.equals(strings[i]))
                return i;
        return -1;
    }

    // creates organiser extending PlanResultsOrganiser, either give full class name in -Dorganiser
    // or abbreviation that is stated in organisersNames
    public PlanResultsOrganiser createOrganiser(Properties p) {
        PlanResultsOrganiser organiser = null;
        if (p.containsKey("organiser")) {
            try {
                String name = p.getProperty("organiser");
                Class<?> c;
                int index = indexOf(name, organisersNames);
                if (index > 0)
                    c = organisers[index];
                else
                    c = Class.forName(name);

                Class<?>[] params = {};
                Constructor<?> constructor = c.getConstructor(params);
                Object[] oparams = {};
                organiser = (PlanResultsOrganiser) constructor.newInstance(oparams);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else
            organiser = new PlanResultsOrganiser();
        return organiser;
    }


    // a class to organise experiment plan output, extend this class and redefine methods
    public static class PlanResultsOrganiser {
        protected List<ExperimentResult> results = new ArrayList<>();

        private final String patternValue = "([^,]+,)+[^,]+";

        // parameters
        // directory name
        protected String outputDirectory = "planResults/";

        // pattern can have any string value.
        // parameter values should be written between @
        // for example: p@p@-@p2@ would yield a result like p0.5-30-run1.csv
        protected String fnamingPattern = null;
        Properties params;

        // redefine this eventually
        public void init(Properties params, String fnamingPattern) {
            this.params = params;
            if (params.containsKey("resultDir"))
                outputDirectory = params.getProperty("resultDir");

            if (outputDirectory.charAt(outputDirectory.length() - 1) != '/')
                outputDirectory += "/";

            this.fnamingPattern = fnamingPattern;


        }

        private boolean isPlanParameterValue(String value) {
            Pattern pattern = Pattern.compile(patternValue);
            return pattern.matcher(value).matches();
        }

        // redefine this
        public void report() {
            for (ExperimentResult r : results)
                r.report(null);
        }

        public void addResult(ExperimentResult r) {
            results.add(r);
        }

    }


    // YB: Similarly to PlanResultsOrganiser, this also could be extended and sent as a parameter to PlanResults
    // I did not make the code for this one because I basically kept the same output format as in Monitor
    // Eventually, the output format could be specified in the class extended from Organiser since its variables are public
    public static class ExperimentResult {
        public Properties planParams;
        public ActionLogger[] loggers;


        public ExperimentResult(Properties parameters, ActionLogger[] loggers, Properties allParameters) {
            this.planParams = parameters;
            this.loggers = loggers;
            for(Object key: allParameters.keySet())
                if(!planParams.containsKey(key) && !key.equals("loadXPlanDir"))
                    planParams.put(key, allParameters.get(key));
        }

        public void report(String file) {
            PrintWriter writer = null;
            if (file == null) {
                writer = new PrintWriter(System.out, true);
            } else {
                try {
                    writer = new PrintWriter(
                            new BufferedWriter(
                                    new OutputStreamWriter(
                                            new FileOutputStream(file),
                                            "UTF-8")),
                            true);
                } catch (FileNotFoundException fnfex) {
                    logger.error("Cannot write output in {}: file not found", file);
                } catch (UnsupportedEncodingException ueex) {
                    logger.debug("IGNORED UnsupportedEncodingException");
                }
            }
            for (int i = 0; i < loggers.length; i++) loggers[i].compPlot();
            AverageLogger avglog = new AverageLogger(false);
            avglog.aggregate(loggers);
            avglog.fullReport(writer);
            writer.close();
        }

        public void writePrams(String file) {
            String sep = " ";
            try {
                FileWriter f = new FileWriter(file);
                for (Object k : planParams.keySet()) {
                    f.write(k + sep + planParams.get(k) + "\n");
                }
                f.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    // Parameters of an experimental plan
    private static class ParamsIterator implements Iterator<Properties> {

        // JE: The structures I put here may not be the best but they will work
        String[] parameter; // From index -> key
        String[][] values; // From index -> values
        int[] valueIndices; // From index -> currentIndex (I have lost its declaration)
        int numberOfParameters;
        private final String patternKey = "PP.+";
        private final String patternValue = "([^,]+,)+[^,]+";

        Properties commandParams = null;
        protected List<Properties> loadedProperties = null; // if loadDir is given, load parameters from directories
        protected int loadedPropertiesIndex = 0;
        protected String namingPattern = null;

        String saveDir = null;

        public ParamsIterator(Properties param) {
            int index = 0;
            commandParams = param;
            File saveDirectory = null;
            if(param.containsKey("saveXPlanDir")) {
                saveDir = param.getProperty("saveXPlanDir");
                saveDirectory = new File(saveDir);
                saveDirectory.mkdirs();

            }
            if(param.containsKey("loadXPlanDir")) { // load parameters from directory
                loadedProperties = new ArrayList<>();

                String loadDir = param.getProperty("loadXPlanDir");
                File loadDirectory = new File(loadDir);
                try {
                    FileInputStream paramsFile = new FileInputStream(new File(loadDirectory, "commandParams.xml"));
                    Properties toCopy = new Properties();
                    toCopy.loadFromXML(paramsFile);

                    toCopy.forEach((k, v) ->
                    { if ( !k.equals("saveXPlanDir") && !k.equals("saveDir") )
                        commandParams.setProperty( (String)k, (String)v ); });
//                    commandParams.loadFromXML(new FileInputStream(new File(loadDirectory, "commandParams.xml")));
                    paramsFile.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                if (commandParams.containsKey("fnamingPattern"))
                    namingPattern = commandParams.getProperty("fnamingPattern");
                else {
                    logger.debug("No naming pattern in command params");
                    initFNamingPattern(commandParams);
                    commandParams.setProperty("fnamingPattern", namingPattern);
                }

                List<File> directoryListing = loadMonitorDirectories(loadDir);
                for(File monitorRun: directoryListing) {
                    Properties p = new Properties();
                    param.forEach((k, v) ->
                    { if ( !k.equals("saveXPlanDir") && !k.equals("saveDir") && !isPlanParameterValue(v.toString()))
                        p.setProperty( (String)k, (String)v ); });

                    try {
                        FileInputStream paramsFile = new FileInputStream(new File(monitorRun, "params.xml"));
                        Properties toCopy = new Properties();
                        toCopy.loadFromXML(paramsFile);

                        toCopy.forEach((k, v) ->
                        { if ( !k.equals("saveXPlanDir") && !k.equals("saveDir") && !p.containsKey(k))
                            p.setProperty( (String)k, (String)v ); });


                        p.setProperty("loadDir", monitorRun.getPath());
                        loadedProperties.add(p);

                        paramsFile.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
            else {
                if (param.containsKey("fnamingPattern"))
                    namingPattern = param.getProperty("fnamingPattern");
                else {
                    initFNamingPattern(param);
                    param.setProperty("fnamingPattern", namingPattern);
                }

                numberOfParameters = param.stringPropertyNames().size(); // ? determine the number of parameters
                parameter = new String[numberOfParameters]; // the number of params we have
                values = new String[numberOfParameters][]; // the number of params we have
                valueIndices = new int[numberOfParameters];

                for (String key : param.stringPropertyNames()) { // For each parameter
                    parameter[index] = key;
                    // This can retrieve the various params passed to it
                    // Recognize those wich are plan params -DPPparam=value?

                    // YB: Do we need to recognize them? if we recognize a value in form {.*}, then, it should be varied.
                    // the others are taken as singleton arrays so they stay fixed.

                    // Let see if we can do that
                    // Parse the value in a list of values
                    // Store them in the corresponding parameter structure...
                    values[index] = parseValues(param.getProperty(key));

                    valueIndices[index] = 0;
                    index++;
                }
                if (valueIndices.length > 0)
                    valueIndices[0] = -1;


            }

            if(saveDirectory != null) {
                try {
                    commandParams.storeToXML(new FileOutputStream(new File(saveDirectory,"commandParams.xml")),
                            "Lazy lavender session parameters", "UTF-8");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            logger.debug("Naming pattern is: " + namingPattern);
        }

        List<File> loadMonitorDirectories(String loadDir) {
            List<File> directories = new ArrayList<>();
            File loadDirectory = new File(loadDir);
            int index = 1;
            while (true){
                File directory = new File(loadDirectory, "monitorRun"+index);
                if(directory.exists())
                    directories.add(directory);
                else
                    break;
                index++;
            }
            return directories;
        }

        protected void initFNamingPattern(Properties params) {
            if(namingPattern == null) {
                namingPattern = "";
                for (String key : params.stringPropertyNames())
                    if (isPlanParameterValue(params.getProperty(key)))
                        namingPattern += "@"+ key + "@-";
                if(namingPattern.length() == 0)
                    namingPattern = "_";
                else
                    namingPattern = namingPattern.substring(0, namingPattern.length() - 1);
            }
        }

        private boolean isPlanParameter(String key) {
            Pattern pattern = Pattern.compile(patternKey);
            return pattern.matcher(key).matches();
        }

        private boolean isPlanParameterValue(String value) {
            Pattern pattern = Pattern.compile(patternValue);
            return pattern.matcher(value).matches();
        }

        // Create a array of values from "1, 2, 3"
        public String[] parseValues(String valueSet) {
            // basically all parameters could be varied, if a parameter is not written in the form of
            // "1,2,3", it is taken as a fixed parameter.
            if (!isPlanParameterValue(valueSet))
                return new String[]{valueSet};
            // split by ","
            String[] values = valueSet.split(",");
            // trim()
            // add
            for (int i = 0; i < values.length; i++) values[i] = values[i].trim();
            return values;
        }

        @Override
        public boolean hasNext() {
            if(loadedProperties != null) {
                return loadedPropertiesIndex < loadedProperties.size();
            }
            for (int i = 0; i < valueIndices.length; i++)
                if (valueIndices[i] < values[i].length - 1)
                    return true;
            return false;
        }

        public void setProperties() {
            int i = 0;
            valueIndices[i]++;
            while (valueIndices[i] == values[i].length) {
                valueIndices[i] = 0;
                i++;
                valueIndices[i]++;
            }
        }

        @Override
        public Properties next() {
            Properties p = new Properties();
            if(loadedProperties != null) {
                p = loadedProperties.get(loadedPropertiesIndex++);
                for(Object key: p.keySet())
                    if(p.getProperty((String) key).equals("_"))
                        p.remove(key);
            }
            else {
                setProperties();

                for (int i = 0; i < numberOfParameters; i++) {
                    String value = values[i][valueIndices[i]];

                    if (!value.equals("_"))
                        p.setProperty(parameter[i], values[i][valueIndices[i]]);
                }
                loadedPropertiesIndex++;
            }

            if(saveDir != null) {
                File savePath = new File(saveDir,"monitorRun" + loadedPropertiesIndex);

                savePath.mkdirs();
                try {
                    p.storeToXML( new FileOutputStream( new File( savePath, "params.xml" ) ),
                            "Lazy lavender session parameters", "UTF-8");
                } catch (IOException e) {
                    e.printStackTrace();
                }

                p.setProperty("saveDir", savePath.getPath());
            }
            return p;
        }
    }

    // This is an example of using the interface
    public void usage() {
        System.err.println(" Usage: to be implemented");
    }

    ;

}
