package fr.inria.exmo.lazylavender.decisiontaking.multitask;

import java.util.Comparator;

public class AgentAgeComparator implements Comparator<MultitaskingAgent> {
    @Override
    public int compare(MultitaskingAgent agent1, MultitaskingAgent agent2) {
        if (agent1.getPeriodsAlive() != agent2.getPeriodsAlive()) {
            return agent1.getPeriodsAlive()<agent2.getPeriodsAlive() ? -1 : 1;
        }
        return 0;
    }

}
