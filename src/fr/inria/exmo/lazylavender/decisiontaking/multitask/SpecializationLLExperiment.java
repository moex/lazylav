package fr.inria.exmo.lazylavender.decisiontaking.multitask;

import java.util.ArrayList;
import java.util.HashMap;

public interface SpecializationLLExperiment extends MultitaskLLExperiment {
    public int getPickedTasks();
    public double getSpecializedGamesRatio();
    public double getSpecializedAgentsRatio();
    public ArrayList<HashMap<Task, Integer>> getPreferencesPermutations();
    public int getMaxAdaptingRank();


}
