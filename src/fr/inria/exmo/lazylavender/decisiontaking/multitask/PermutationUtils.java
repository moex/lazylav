package fr.inria.exmo.lazylavender.decisiontaking.multitask;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class PermutationUtils {

    public static ArrayList<HashMap<Task, Integer>> getAllPreferences(ArrayList<Task> candidateTasks, int numberOfAgents){

        ArrayList<HashMap<Task, Integer>> preferencesList = new ArrayList<>();

        int agentsForEachPermutation = numberOfAgents/getNumberOfPermutations(candidateTasks.size(), candidateTasks.size());

        System.out.println("Agents for each permutation: "+agentsForEachPermutation);

        HashMap<String, ArrayList<Task>> allPermutations = PermutationUtils.getAllPermutations(candidateTasks);

        for (Map.Entry<String, ArrayList<Task>> entry : allPermutations.entrySet()) {
            ArrayList<Task> permList = entry.getValue();

            HashMap<Task, Integer> tempPref = PermutationUtils.arraylistToRankedTasks(permList);
            for (int i = 0; i < agentsForEachPermutation; i++) {
                preferencesList.add(tempPref);
            }

        }

        return preferencesList;
    }

    private static HashMap<String, ArrayList<Task>> getAllPermutations(ArrayList<Task> candidateTasks) {
        HashMap<String, ArrayList<Task>> permutationsMap = new HashMap<>();

        int n = candidateTasks.size();
        int r = candidateTasks.size();

        int numberOfPermutations = PermutationUtils.getNumberOfPermutations(n, r);

        System.out.println("Number of permutations: " + numberOfPermutations);

        while(permutationsMap.size()<numberOfPermutations){
            ArrayList<Task> tempList = new ArrayList<>(candidateTasks);
            Collections.shuffle(tempList);
            permutationsMap.put(tempList.toString(), tempList);
        }

        for (String key : permutationsMap.keySet()) {
            System.out.println("Perm: "+key);
        }

        return permutationsMap;

    }

    private static int fact(int number) {
        int f = 1;
        int j = 1;
        while(j <= number) {
            f = f * j;
            j++;
        }
        return f;
    }

    private static int getNumberOfPermutations(int n, int r){
        return fact(n) / fact(n-r);
    }

    private static HashMap<Task, Integer> arraylistToRankedTasks(ArrayList<Task> list){
        HashMap<Task, Integer> rankedTasks = new HashMap<>();
        for (int i = 0; i < list.size(); i++) {
            rankedTasks.put(list.get(i), i);
        }
        return rankedTasks;
    }

}
