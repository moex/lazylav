package fr.inria.exmo.lazylavender.decisiontaking.multitask;

import fr.inria.exmo.lazylavender.decisiontaking.treeclassifier.NoClassException;

import java.util.*;

public interface MultitaskTreeClassifier
{
    /**
     * Apply a change on the tree classifier
     */

    interface MultiChanger {
        public MultitaskTreeClassifier.MultiChange change(MultitaskTreeClassifier tree);
    }

    /**
     * Creates a node with a condition on one attribute, a decision class and a parent node
     */

    interface MultiNodeCreator {
        public MultitaskTreeClassifier.MultiTaskNode multitaskNode(MultitaskDecision multitaskDecision, int attribute, boolean value, MultitaskTreeClassifier.MultiTaskNode parent);
    }

    class MultiChange {
        List<MultitaskTreeClassifier.MultiTaskNode> added = new ArrayList<>(), deleted = new ArrayList<>(), changed = new ArrayList<>();

        public void addAdded(MultitaskTreeClassifier.MultiTaskNode... nodes) {
            added.addAll(Arrays.asList(nodes));
        }

        public void addDeleted(MultitaskTreeClassifier.MultiTaskNode... nodes) {
            deleted.addAll(Arrays.asList(nodes));
        }

        public void addChanged(MultitaskTreeClassifier.MultiTaskNode... nodes) {
            changed.addAll(Arrays.asList(nodes));
        }

        public List<MultitaskTreeClassifier.MultiTaskNode> getAdded() {
            return added;
        }

        public void setAdded(List<MultitaskTreeClassifier.MultiTaskNode> added) {
            this.added = added;
        }

        public List<MultitaskTreeClassifier.MultiTaskNode> getDeleted() {
            return deleted;
        }

        public void setDeleted(List<MultitaskTreeClassifier.MultiTaskNode> deleted) {
            this.deleted = deleted;
        }

        public List<MultitaskTreeClassifier.MultiTaskNode> getChanged() {
            return changed;
        }

        public void setChanged(List<MultitaskTreeClassifier.MultiTaskNode> changed) {
            this.changed = changed;
        }
    }

    interface MultiCondition {
        boolean evaluate(BitSet instance);
        void and(MultitaskTreeClassifier.MultiCondition condition);
        void not();
        MultitaskTreeClassifier.MultiCondition copy();
        boolean subsumes(MultitaskTreeClassifier.MultiCondition condition);
        boolean disjoint(MultitaskTreeClassifier.MultiCondition condition);
    }

    interface MultiTaskNode {
        public void addChild(MultiTaskNode child);
        public boolean isLeaf();
        public List<MultiTaskNode> getChildren();
        public int getChildrenDifferences();
        public MultiTaskNode getInstanceNode(BitSet instance);
        public MultitaskTreeClassifier.MultiCondition getCondition();
        public MultitaskTreeClassifier.MultiCondition getRecursiveCondition();
        public MultiTaskNode getParent();
        public MultitaskDecision getMultiDecision();
        public void setMultiDecision(MultitaskDecision multitaskDecision);
        public MultiTaskNode copy();
        public HashSet<BitSet> getAssociatedInstances();
        public void removeAllAssociatedInstances();
        public void addAssociatedInstance(BitSet instance);
        public void removeAssociatedInstance(BitSet instance);
    }

    void train(ArrayList<TrainingExample> labeledExamples) throws Exception;

    /**
     * classifies an instance
     * @param input an instance
     * @return integer ID value of the class
     */
    MultitaskDecision getClass(BitSet input) throws NoClassException;

    /**
     * gets the decision node of the input instance
     * @param instance
     * @return
     */
    MultiTaskNode getNode(BitSet instance) throws NoClassException;

    /**
     * gets the path condition of the input instance
     * @param instance
     * @return
     */
    MultitaskTreeClassifier.MultiCondition getCondition(BitSet instance) throws NoClassException;

    /**
     * applies a modification on the tree classifier
     * @param changer
     */

    MultitaskTreeClassifier.MultiChange applyModification(MultiChanger changer);

    MultiTaskNode createNode(MultiTaskNode parent, MultitaskTreeClassifier.MultiCondition condition, MultitaskDecision multitaskDecision);

    /**
     * returns the root of the tree
     * @return Node
     */
    MultiTaskNode getRoot();

    /**
     * returns true if the tree classifier has been trained
     * @return boolean
     */
    boolean isTrained();

    MultiNodeCreator getNodeCreator();
}
