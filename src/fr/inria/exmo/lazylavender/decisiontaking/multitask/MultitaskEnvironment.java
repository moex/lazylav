/*
 * Copyright (C) INRIA, 2021-2024
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 */

package fr.inria.exmo.lazylavender.decisiontaking.multitask;

import fr.inria.exmo.lazylavender.env.LLEnvironment;
import fr.inria.exmo.lazylavender.model.LLException;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MultitaskEnvironment implements LLEnvironment {

    private int numberOfFeatures, numberOfIrrelevantFeatures, numberOfTasks, numberOfDecisions;

    private HashMap<String, Task> environmentTasks;

    private HashMap<BitSet, EnvironmentExample> environmentExamples;

    private HashMap<String, BitSet> bitsetIndex;

    private ArrayList<BitSet> lottery;

    private HashMap<Task, HashMap<String, Integer>> decisionValidationMap;

    public MultitaskEnvironment(int numberOfFeatures, int numberOfIrrelevantFeatures, int numberOfTasks, int numberOfDecisions) {
        this.numberOfFeatures = numberOfFeatures;
        this.numberOfIrrelevantFeatures = numberOfIrrelevantFeatures;
        this.numberOfTasks = numberOfTasks;
        this.numberOfDecisions = numberOfDecisions;
        environmentTasks = new HashMap<>();
        environmentExamples = new HashMap<>();
        bitsetIndex = new HashMap<>();
        lottery = new ArrayList<>();
    }

    public MultitaskDecision getDecision(BitSet instance) {
        return environmentExamples.get(instance).getMultiDecision();
    }

    public int getNumberOfFeatures() {
        return numberOfFeatures;
    }

    public int getNumberOfTasks() {
        return numberOfTasks;
    }

    @Override
    public LLEnvironment init() throws LLException {
        return init(new Properties());
    }

    @Override
    public LLEnvironment init(Properties param) throws LLException {

        String loadEnvDir = param.getProperty("loadEnvDir");

        File tempFile = new File(loadEnvDir+File.separator+"tasks.tsv");

        System.out.println("TempFilePath : "+tempFile.getAbsolutePath());

        if (param.containsKey("loadEnvDir") && tempFile.exists()) {

            try {
                Iterator<String> inputLines = Files.lines(Paths.get(loadEnvDir, "tasks.tsv")).iterator();
                initTasksFromFile(inputLines);
            } catch (IOException e) {
                e.printStackTrace();
            }

            try {
                Iterator<String> inputLines = Files.lines(Paths.get(loadEnvDir, "instances.tsv")).iterator();
                initExamplesFromFile(inputLines);
            } catch (IOException e) {
                e.printStackTrace();
            }

        }

        String allTasksIrrelevantFeatures = (param.containsKey("allTasksIrrelevantFeatures") ? param.getProperty("allTasksIrrelevantFeatures") : "");

        if(environmentTasks.isEmpty()){
            initTasksRandomly(allTasksIrrelevantFeatures);
        }

        if(environmentExamples.entrySet().isEmpty()){
            initDecisionValidationMap();
            initExamplesRandomly(0, new BitSet());
        }

        if (param.containsKey("runDir")) {
            if(saveTasksToFile(param)) {
                saveExamplesToFile(param);
            }
        }

        return this;
    }

    private void initDecisionValidationMap(){
        decisionValidationMap = new HashMap<>();

        for (Map.Entry<String, Task> entry : environmentTasks.entrySet()) {
            decisionValidationMap.put(entry.getValue(), new HashMap<>());
        }

    }

    private void initTasksRandomly(String allTasksIrrelevantFeatures){

        System.out.println("allTasksIrrelevantFeatures: "+allTasksIrrelevantFeatures);

        ArrayList<HashSet<Integer>> irrelevantFeaturesSets = new ArrayList<>();

        String[] tokens;

        if(allTasksIrrelevantFeatures.split("-").length!=numberOfTasks){
            allTasksIrrelevantFeatures = "";
        }

        if(!allTasksIrrelevantFeatures.equalsIgnoreCase("")){
            tokens = allTasksIrrelevantFeatures.split("-");

            System.out.println("tokens length: "+tokens.length);

            for (int i = 0; i < tokens.length; i++) {
                HashSet<Integer> taskSet = new HashSet<Integer>();
                String[] subtokens = tokens[i].split(":");
                System.out.println("subtokens length: "+subtokens.length);
                for (int j = 0; j < subtokens.length; j++) {
                    System.out.println("number of features:"+numberOfFeatures);
                    System.out.println("subtoken:"+j+" feature:"+String.valueOf(Integer.parseInt(subtokens[j])));
                    if(Integer.parseInt(subtokens[j])<=numberOfFeatures){
                        taskSet.add(Integer.parseInt(subtokens[j]));
                    }
                }
                System.out.println("taskSet size:"+taskSet.size());
                irrelevantFeaturesSets.add(taskSet);
            }

        }

        Random rand = new Random();

        for (int i = 0; i < numberOfTasks; i++) {

            String name = "Task"+" "+String.valueOf(i);
            double rewardCoefficient = 0.5 + (1.5 - 0.5) * rand.nextDouble();
            int reward = ThreadLocalRandom.current().nextInt(1,10);
            int penalty = ThreadLocalRandom.current().nextInt(1,10)*-1;

            Task task;
            if(allTasksIrrelevantFeatures.equalsIgnoreCase("")) {
                task = new Task(name, rewardCoefficient, numberOfDecisions, reward, penalty, numberOfFeatures, numberOfIrrelevantFeatures);
            }else{
                //System.out.println("It entered here, having subtokens!");
                task = new Task(name, rewardCoefficient, numberOfDecisions, reward, penalty, numberOfFeatures, irrelevantFeaturesSets.get(i));
            }
            environmentTasks.put(name, task);
        }
    }

    private void initTasksFromFile(Iterator<String> inputLines) {

        if (inputLines != null) {

            //System.out.println("Init Tasks from File!");

            int counter = 0;

            while (inputLines.hasNext()) {

                String[] line = inputLines.next().split("\t");

                String taskName = line[0];

                double rewardCoefficient = (Double.parseDouble(line[1]) <= 1.0) ? Double.parseDouble(line[1]) : ThreadLocalRandom.current().nextDouble(0, 1.0);

                int decision = (Integer.parseInt(line[2]) <= numberOfDecisions) ? Integer.parseInt(line[2]) : ThreadLocalRandom.current().nextInt(0, numberOfDecisions + 1);

                int reward = Integer.parseInt(line[3]);

                int penalty = Integer.parseInt(line[4]);

                String sRelevantFeatures = line[5];

                String[] relevantFeaturesTokens = sRelevantFeatures.split(":");

                Task newTask = new Task(taskName, rewardCoefficient, decision, reward, penalty, numberOfFeatures, 0);

                for (int i = 0; i < relevantFeaturesTokens.length; i++) {
                    newTask.addIrrelevantFeature(Integer.parseInt(relevantFeaturesTokens[i]));
                }

                environmentTasks.put(taskName, newTask);

            }

            numberOfTasks = environmentTasks.size();

        }

    }

    private boolean saveTasksToFile(Properties param){
        String runDir = param.getProperty("runDir");
        try {
            PrintWriter writer = new PrintWriter(new File(runDir , "tasks.tsv"));
            if(writer != null) {

                StringBuilder lineBuilder = new StringBuilder();

                for (Map.Entry<String, Task> taskEntry : environmentTasks.entrySet()) {

                    lineBuilder.setLength(0);

                    lineBuilder.append(taskEntry.getKey()).append("\t").append(String.valueOf(taskEntry.getValue().getRewardCoefficient())).append("\t").append(String.valueOf(taskEntry.getValue().getNumberOfDecisions())).append("\t").append(taskEntry.getValue().getReward()).append("\t").append(taskEntry.getValue().getPenalty()).append("\t").append(pickTask().irrelevantFeaturesToString());

                    writer.println(lineBuilder.toString().trim());

                }

                writer.flush();
                writer.close();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    private void initExamplesFromFile(Iterator<String> inputLines) {

        if (inputLines != null) {

            //System.out.println("Init examples from File!");

            int counter = 0;

            while (inputLines.hasNext()) {

                String[] line = inputLines.next().split("\t");

                //Create a labeledExample by parsing the line

                BitSet instance = parseInstance(line[0]);

                //Create a MultiDecision based on the number of Tasks. If decision > number of decision choose a random decision

                MultitaskDecision multitaskDecision = new MultitaskDecision();

                for (int i = 1; i < (1 + numberOfTasks); i++) {

                    String[] taskDecisionPair = line[i].split(":");

                    String taskName = taskDecisionPair[0];

                    String decisionString = taskDecisionPair[1];

                    //System.out.println("Line[i]:"+line[i]+" decisionString:"+decisionString);

                    if (environmentTasks.containsKey(taskName)) {

                        Task task = environmentTasks.get(taskName);

                        multitaskDecision.putDecision(task, (Integer.parseInt(decisionString) <= task.getNumberOfDecisions()) ? Integer.parseInt(decisionString) : ThreadLocalRandom.current().nextInt(0, task.getNumberOfDecisions()));

                    }
                }

                EnvironmentExample newEnvironmentExample = new EnvironmentExample(instance, multitaskDecision);

                environmentExamples.put(instance, newEnvironmentExample);
                bitsetIndex.put(instance.toString(), instance);

            }
        }
    }

    private void initExamplesRandomly(int level, BitSet parentBitSet) {

        BitSet firstBitSet = new BitSet();
        BitSet secondBitSet = new BitSet();

        firstBitSet.or(parentBitSet);
        secondBitSet.or(parentBitSet);

        firstBitSet.set(level);

        if (level == numberOfFeatures - 1) {

            MultitaskDecision firstMultitaskDecision = new MultitaskDecision();
            MultitaskDecision secondMultitaskDecision = new MultitaskDecision();

            String firstBitSetString = firstBitSet.toString();
            String secondBitSetString = secondBitSet.toString();

            for (Map.Entry<String, Task> entry : environmentTasks.entrySet()) {

                //System.out.println("Task: "+entry.getValue().getName());

                //if(entry.getValue().getIrrelevantFeatures().size()>0){

                if(false){

                    //System.out.println("Task: "+entry.getValue().getName()+" has " + entry.getValue().getIrrelevantFeatures().size() + " irrelevant features!");

                    String taskFirstBitSetString = firstBitSetString;
                    String taskSecondBitSetString = secondBitSetString;

                    HashSet<Integer> taskIrrelevantFeatures = entry.getValue().getIrrelevantFeatures();

                    for (Integer irrelevantFeature : taskIrrelevantFeatures) {
                        taskFirstBitSetString.replace(String.valueOf(irrelevantFeature), "");
                        taskFirstBitSetString.replace(" ,", "");

                        taskSecondBitSetString.replace(String.valueOf(irrelevantFeature), "");
                        taskSecondBitSetString.replace(" ,", "");
                    }

                    HashMap<String, Integer> taskMap = decisionValidationMap.get(entry.getValue());

                    if(taskMap.containsKey(taskFirstBitSetString)){
                        System.out.println("First contained!");
                        firstMultitaskDecision.putDecision(entry.getValue(), taskMap.get(taskFirstBitSetString));
                    }else{
                        int randomDecision = ThreadLocalRandom.current().nextInt(0, entry.getValue().getNumberOfDecisions() + 1);
                        firstMultitaskDecision.putDecision(entry.getValue(), randomDecision);
                        taskMap.put(taskFirstBitSetString, randomDecision);
                    }

                    if(taskMap.containsKey(taskSecondBitSetString)){
                        System.out.println("Second contained!");
                        secondMultitaskDecision.putDecision(entry.getValue(), taskMap.get(taskSecondBitSetString));
                    }else{
                        int randomDecision = ThreadLocalRandom.current().nextInt(0, entry.getValue().getNumberOfDecisions() + 1);
                        secondMultitaskDecision.putDecision(entry.getValue(), randomDecision);
                        taskMap.put(taskSecondBitSetString, randomDecision);
                    }

                }else {
                    firstMultitaskDecision.putDecision(entry.getValue(), ThreadLocalRandom.current().nextInt(0, entry.getValue().getNumberOfDecisions() + 1));
                    secondMultitaskDecision.putDecision(entry.getValue(), ThreadLocalRandom.current().nextInt(0, entry.getValue().getNumberOfDecisions() + 1));
                }
            }

            environmentExamples.put(firstBitSet, new EnvironmentExample(firstBitSet, firstMultitaskDecision));
            environmentExamples.put(secondBitSet, new EnvironmentExample(secondBitSet, secondMultitaskDecision));

            bitsetIndex.put(firstBitSet.toString(), firstBitSet);
            bitsetIndex.put(secondBitSet.toString(), secondBitSet);

            return;
        }
        initExamplesRandomly(level + 1, firstBitSet);
        initExamplesRandomly(level + 1, secondBitSet);
    }

    private void saveExamplesToFile(Properties param){
        String runDir = param.getProperty("runDir");
        try {
            PrintWriter writer = new PrintWriter(new File(runDir , "instances.tsv"));
            if(writer != null) {

                StringBuilder lineBuilder = new StringBuilder();

                for (Map.Entry<BitSet, EnvironmentExample> exampleEntry : environmentExamples.entrySet()) {

                    lineBuilder.setLength(0);

                    lineBuilder.append(exampleEntry.getKey().toString());

                    SortedSet<String> sortedTaskNames = new TreeSet<>(environmentTasks.keySet());

                    Iterator<String> iterator = sortedTaskNames.iterator();
                    while (iterator.hasNext()) {
                        String taskName = iterator.next();
                        lineBuilder.append("\t").append(taskName).append(":").append(String.valueOf(exampleEntry.getValue().getMultiDecision().getDecision(environmentTasks.get(taskName))));

                    }

                    writer.println(lineBuilder.toString().trim());
                }

                writer.flush();
                writer.close();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public HashMap<BitSet, EnvironmentExample> getSamples(double sampleRatio) {

        HashMap<BitSet, EnvironmentExample> sampleExamples = new HashMap<>();

        Random rand = new Random();

        for (Map.Entry<BitSet,EnvironmentExample> entry : this.getEnvironmentExamples().entrySet()) {
            if (rand.nextDouble() <= sampleRatio) {
                sampleExamples.put(entry.getKey(), entry.getValue());
            }
        }

        if (sampleExamples.isEmpty()) {
            BitSet[] keys = this.getEnvironmentExamples().keySet().toArray(new BitSet[this.getEnvironmentExamples().keySet().size()]);
            BitSet randomKey = keys[rand.nextInt(keys.length)];
            sampleExamples.put(randomKey, this.getEnvironmentExamples().get(randomKey));
        }

        return sampleExamples;

    }

    @Override
    public void logInit() throws LLException {
    }

    @Override
    public void logReference() throws LLException {
    }

    @Override
    public void logFinal() throws LLException {
    }


    //TODAND VERY IMPORTANT
    @Override
    public BitSet parseInstance(String text) {
        Pattern pattern = Pattern.compile("\\{([0-9,\\s]*)\\}");
        Matcher matcher = pattern.matcher( text );
        if ( matcher.find() ) {
            BitSet instance = new BitSet( numberOfFeatures );
            for ( String item : matcher.group(1).split("\\s*,\\s*") ) {
                if ( !("".equals(item)) )
                    instance.set( Integer.parseInt( item ) );
            }
            return instance;
        } else return null;
    }

    @Override
    public BitSet generateInstance() {
        if(lottery.size()==0){
            lottery.addAll(environmentExamples.keySet());
        }
        //The drawn example - instance is removed so that it is not chosen again
        return lottery.remove(ThreadLocalRandom.current().nextInt(0, lottery.size()));
    }

    public Task pickTask(){
        Random generator = new Random();
        Object[] values = environmentTasks.values().toArray();
        Object randomValue = values[generator.nextInt(values.length)];
        return (Task) randomValue;
    }

    public ArrayList<Task> pickMultipleTasks(int pickedTasks){

        //System.out.println("pickMultipleTasks got the following parameter value : " + pickedTasks);

        ArrayList<Task> proposedTasks = new ArrayList<>();
        Random generator = new Random();

        if(pickedTasks==1){
            proposedTasks.add(pickTask());
        }else if(pickedTasks>1 && pickedTasks<=getNumberOfTasks()) {

            double pickRatio = (double)pickedTasks/(double)getNumberOfTasks();

            for (Task task : environmentTasks.values()) {
                if (generator.nextDouble() <= pickRatio) {
                    proposedTasks.add(task);
                }
            }
        }

        if(proposedTasks.isEmpty()){
            proposedTasks.add(pickTask());
        }

        return proposedTasks;
    }

    public HashMap<BitSet, EnvironmentExample> getEnvironmentExamples() {
        return environmentExamples;
    }


    public HashMap<String, Task> getEnvironmentTasks() {
        return environmentTasks;
    }

    public BitSet getBitSet(String s){
        return bitsetIndex.get(s);
    }

}
