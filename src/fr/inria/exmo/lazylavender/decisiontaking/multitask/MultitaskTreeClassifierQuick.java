package fr.inria.exmo.lazylavender.decisiontaking.multitask;

import fr.inria.exmo.lazylavender.decisiontaking.treeclassifier.NoClassException;
import fr.inria.exmo.lazylavender.decisiontaking.treeclassifier.dttrainers.InterleavedTrainer;


import java.util.ArrayList;
import java.util.BitSet;
import java.util.HashSet;

public class MultitaskTreeClassifierQuick implements MultitaskTreeClassifier {

    private MultitaskTreeClassifier.MultiTaskNode root = null;

    private int classesLimit = 2048;

    public MultitaskTreeClassifierQuick(int classesLimit) {
        this.classesLimit = classesLimit;
    }

    @Override
    public void train(ArrayList<TrainingExample> trainingExamples) throws Exception {

        //System.out.println("I am trying to be trained with: "+trainingExamples.size()+ " examples");

        int numberOfAttributes = 0;

        for(TrainingExample trainingExample : trainingExamples){
            int lastSet = trainingExample.getInstance().previousSetBit(1000) + 1;
            if(lastSet > numberOfAttributes) {
                numberOfAttributes = lastSet;
            }
        }

        ArrayList<Integer> attributes = new ArrayList<>();
        for (int i = 0; i < numberOfAttributes; i++) {
            attributes.add(i);
        }

        InterleavedTrainer trainer = new InterleavedTrainer(this, this.getNodeCreator(), classesLimit);
        root = trainer.train(trainingExamples, attributes, null);

    }

    @Override
    public MultiNodeCreator getNodeCreator() {
        return MultiBitSetNode::new;
    }

    @Override
    public String toString()
    {
        String result = "tree:\n"+root;

        return result;
    }

    @Override
    public MultitaskDecision getClass(BitSet instance) throws NoClassException {
        if(root==null){
            System.out.println("Root was null!");
        }
        if(instance==null){
            System.out.println("Instance was null!");
        }
        return ((MultiBitSetNode)root).classify(instance);
    }

    @Override
    public MultiTaskNode getNode(BitSet instance) throws NoClassException {
        return ((MultiBitSetNode)root).getNode(instance);
    }

    @Override
    public MultitaskTreeClassifier.MultiCondition getCondition(BitSet instance) {
        return ((MultiBitSetNode)root).getConditionInstance(instance);
    }

    @Override
    public MultitaskTreeClassifier.MultiChange applyModification(MultitaskTreeClassifier.MultiChanger changer) {
        //System.out.println("Applying modification!");
        return changer.change(this);
    }

    @Override
    public MultiTaskNode createNode(MultiTaskNode parent, MultitaskTreeClassifier.MultiCondition condition, MultitaskDecision multitaskDecision) {
        MultiBitSetNode node = new MultiBitSetNode(multitaskDecision, condition, parent);
        parent.addChild(node);
        return node;
    }

    @Override
    public MultiTaskNode getRoot() {
        return root;
    }

    @Override
    public boolean isTrained() {
        return false;
    }


    public static class BitsetCondition implements MultitaskTreeClassifier.MultiCondition {
        /**
         * denotes the attributes on which there is a condition
         * a bit is set if there is a condition on it, unset if there is no condition on it
         */
        BitSet conditionedAttributes = new BitSet();
        /**
         * denotes the conditions on the attributes
         * each bit represent the value of that attribute
         */
        BitSet conditions = new BitSet();
        /**
         * returns not this condition if not is true
         */
        boolean not;
        /**
         * and conditions must be true for this condition to be true
         */
        ArrayList<MultitaskTreeClassifier.MultiCondition> andConditions = new ArrayList<>();
        /**
         * breaks a loop of and conditions
         */
        boolean loopBreaker = false;

        public BitsetCondition(BitSet conditionedAttributes, BitSet conditions, BitsetCondition parentCondition, boolean not) {
            this.conditionedAttributes.or(conditionedAttributes);
            this.conditions.or(conditions);
            this.conditions.and(conditionedAttributes);
            this.not = not;
            if(parentCondition != null) {
                and(parentCondition);
            }
        }

        @Override
        public boolean evaluate(BitSet instance)
        {

            if(loopBreaker) // this condition called itself in a loop to evaluate itself
                return true;
            boolean value = true;
            if(!conditionedAttributes.isEmpty()) {

                BitSet toEvaluate = new BitSet();
                toEvaluate.clear();

                toEvaluate.or(instance);
                toEvaluate.and(conditionedAttributes);
                toEvaluate.xor(conditions);
                value = toEvaluate.isEmpty();
            }
            loopBreaker = true;

            for(MultitaskTreeClassifier.MultiCondition c: andConditions)
                if(!c.evaluate(instance)) {
                    value = false;
                    break;
                }
            loopBreaker = false;
            if(not) {
                return !value;
            }
            return value;
        }

        @Override
        public void and(MultitaskTreeClassifier.MultiCondition condition) {
            if(not) {
                MultitaskTreeClassifier.MultiCondition c = copy();
                // make this condition true
                andConditions.clear();
                conditionedAttributes.clear();
                conditions.clear();
                not = false;
                // add its copy to and conditions
                andConditions.add(c);
            }
            andConditions.add(condition);
        }

        @Override
        public void not() {
            this.not = ! this.not;
        }

        @Override
        public String toString()
        {
            String neg = "\\neg ", land = "\\land ";
            int index = 0;
            ArrayList<Integer> atts = new ArrayList<>();
            while (conditionedAttributes.nextSetBit(index) != -1)
            {
                atts.add(conditionedAttributes.nextSetBit(index));
                index = conditionedAttributes.nextSetBit(index) + 1;
            }
            String result = "";
            if(not)
                result = neg+"(";
            for(Integer att: atts)
            {
                if(conditions.get(att))
                    result += (char)('a'+att) + land;
                else
                    result += neg+(char)('a'+att)+ land;
            }

            for (MultitaskTreeClassifier.MultiCondition cond: andConditions)
                if(!cond.toString().equals(""))
                    result += cond + land;
            if(result.length() == 0)
                result = "";
            else
                result = result.substring(0, result.length()-6);
            if(not)
                result += ")";
            return result;
        }

        @Override
        public MultitaskTreeClassifier.MultiCondition copy() {
            BitsetCondition condition = new BitsetCondition(conditionedAttributes, conditions, null, not);
            for(MultitaskTreeClassifier.MultiCondition c: andConditions) {
                condition.andConditions.add(c.copy());
            }
            return condition;
        }

        boolean lastBitLoopBreaker = false;

        public int getLastSetBit()
        {
            int lastSet = conditionedAttributes.previousSetBit(1000);
            if(lastBitLoopBreaker) {
                return lastSet;
            }
            lastBitLoopBreaker = true;
            for(MultitaskTreeClassifier.MultiCondition and: andConditions) {
                int bit = ((BitsetCondition) and).getLastSetBit();
                if(bit > lastSet) {
                    lastSet = bit;
                }
            }
            lastBitLoopBreaker = false;
            return lastSet;
        }

        @Override
        public boolean subsumes(MultitaskTreeClassifier.MultiCondition condition) {
            int lastSet = getLastSetBit();
            BitsetCondition con = (BitsetCondition) condition;
            lastSet = Math.max(lastSet, con.getLastSetBit());
            for (int i = 0; i < (1<<(lastSet+1)); i++) {
                BitSet instance = BitSet.valueOf(new long[]{i});
                if(con.evaluate(instance) && !evaluate(instance))
                    return false;
            }
            return true;
        }

        @Override
        public boolean disjoint(MultitaskTreeClassifier.MultiCondition condition) {
            int lastSet = getLastSetBit();
            BitsetCondition con = (BitsetCondition) condition;
            lastSet = Math.max(lastSet, con.getLastSetBit());
            for (int i = 0; i < (1<<(lastSet+1)); i++) {
                BitSet instance = BitSet.valueOf(new long[]{i});
                if(con.evaluate(instance) && evaluate(instance)) {
                    return false;
                }
            }
            return true;
        }

    }

        private static class MultiBitSetNode implements MultiTaskNode {

        private ArrayList<MultiTaskNode> children = new ArrayList<>();
        private MultitaskDecision multitaskDecision;
        private MultitaskTreeClassifier.MultiCondition condition;
        private MultiTaskNode parent;
        private HashSet<BitSet> associatedInstances;

        MultiBitSetNode(MultitaskDecision multitaskDecision, int attribute, boolean value, MultiTaskNode parent) {

            associatedInstances = new HashSet<BitSet>();

            BitSet att = new BitSet();

            if(attribute >= 0) {
                att.set(attribute);
            }

            BitSet val = new BitSet();
            if(value) {
                val.set(attribute);
            }

            condition = new BitsetCondition(att, val, null, false);

            this.multitaskDecision = multitaskDecision;

            this.parent = parent;
        }

        MultiBitSetNode(MultitaskDecision multitaskDecision, MultitaskTreeClassifier.MultiCondition condition, MultiTaskNode parent) {
            this.condition = condition;
            this.multitaskDecision = multitaskDecision;
            this.parent = parent;

            associatedInstances = new HashSet<BitSet>();
        }

        MultitaskDecision classify(BitSet instance) throws NoClassException {
            return getNode(instance).getMultiDecision();
        }

        MultiTaskNode getNode(BitSet instance) throws NoClassException {
            if(isLeaf()) {
                return this;
            }else {
                for (MultiTaskNode c : children) {
                    MultiBitSetNode child = (MultiBitSetNode) c;
                    if (child.containsInstance(instance)) {
                        return child.getNode(instance);
                    }
                }
            }

            throw new NoClassException("not leaf and instance satisfies no child");
        }

        @Override
        public MultiTaskNode getInstanceNode(BitSet instance)
        {
            for(MultiTaskNode c: children)
            {
                MultiBitSetNode child = (MultiBitSetNode) c;
                if(child.containsInstance(instance))
                    return child.getInstanceNode(instance); // root has no condition
            }
            return this;
        }


        public MultitaskTreeClassifier.MultiCondition getConditionInstance(BitSet instance)
        {
            for(MultiTaskNode c: children)
            {
                MultiBitSetNode child = (MultiBitSetNode) c;
                if(child.containsInstance(instance))
                    return child.getConditionInstance(instance, null); //ROOT HAS NO CONDITION
            }
            return condition;
        }

        MultitaskTreeClassifier.MultiCondition getConditionInstance(BitSet instance, MultitaskTreeClassifier.MultiCondition parentCondition)
        {
            MultitaskTreeClassifier.MultiCondition condition = getCondition().copy();

            if(parentCondition != null) {
                condition.and(parentCondition);
            }

            for(MultiTaskNode c: children) {
                MultiBitSetNode child = (MultiBitSetNode) c;
                if(child.containsInstance(instance)) {
                    return child.getConditionInstance(instance, condition);
                }
            }
            return condition; // case this is leaf
        }

        boolean containsInstance(BitSet instance) {
            return condition.evaluate(instance);
        }

        @Override
        public void addChild(MultiTaskNode child) {
            children.add(child);
        }

        @Override
        public boolean isLeaf() {
            return children.isEmpty();
        }

        @Override
        public MultitaskTreeClassifier.MultiCondition getCondition() {
            return condition;
        }

        @Override
        public MultitaskTreeClassifier.MultiCondition getRecursiveCondition() {
            if(getParent() == null) {
                return getCondition().copy();
            }
            MultitaskTreeClassifier.MultiCondition condition = getCondition().copy();
            condition.and(parent.getRecursiveCondition());
            return condition;
        }

        public void setCondition(BitsetCondition condition) {
            this.condition = condition;
        }

        @Override
        public MultitaskDecision getMultiDecision() {
            return multitaskDecision;
        }

        public void setMultiDecision(MultitaskDecision multitaskDecision) {
            this.multitaskDecision = multitaskDecision;
        }

        @Override
        public MultiTaskNode copy() {
            return copy(null);
        }

        @Override
        public HashSet<BitSet> getAssociatedInstances() {
            return associatedInstances;
        }

        @Override
        public void removeAllAssociatedInstances() {
            associatedInstances.clear();
        }

        @Override
        public void addAssociatedInstance(BitSet instance) {
            associatedInstances.add(instance);
        }

        @Override
        public void removeAssociatedInstance(BitSet instance) {
            associatedInstances.remove(instance);
        }


        public MultiTaskNode copy(MultiTaskNode parent) {
            MultiTaskNode node = new MultiBitSetNode(multitaskDecision, condition.copy(), parent);
            for(MultiTaskNode child: children) {
                node.addChild(((MultiBitSetNode) child).copy(node));
            }
            return node;
        }

        @Override
        public String toString() {
            String tabs = "";
            for(MultiTaskNode p = parent; p != null; p = p.getParent()) {
                tabs += "----";
            }

            String result = tabs+">("+condition+")";
            if(children.size() == 0) {
                result += ": " + multitaskDecision;
            }

            result += "\n";
            for (MultiTaskNode child: children) {
                result += child;
            }

            return result;
        }

        public ArrayList<MultiTaskNode> getChildren() {
            return children;
        }

            @Override
            public int getChildrenDifferences() {
                if(this.isLeaf()){
                    return -1;
                }else{
                    MultiTaskNode leftChild = this.getChildren().get(0);
                    MultiTaskNode rightChild = this.getChildren().get(1);
                    return leftChild.getMultiDecision().getDifference(rightChild.getMultiDecision());
                }
            }

            @Override
        public MultiTaskNode getParent() {
            return parent;
        }
    }
}
