package fr.inria.exmo.lazylavender.decisiontaking.multitask;

import java.util.*;

public class SpecializationGameIterator implements Iterator<String> {

    private int maxIter, currentIter;
    private SpecializationLLExperiment experiment;
    private Random rand;
    private List<Integer> agentsIDs;
    private boolean onlyAcquaintances;
    private int numberOfUncertainBits;
    private int numberOfFeatures;

    public SpecializationGameIterator(SpecializationLLExperiment experiment, boolean onlyAcquaintances, int numberOfUncertainBits, int numberOfFeatures) {
        this.experiment = experiment;
        this.onlyAcquaintances = onlyAcquaintances;
        this.numberOfUncertainBits = numberOfUncertainBits;
        this.numberOfFeatures = numberOfFeatures;
        initIterator();
        rand = new Random();
    }

    @Override
    public boolean hasNext() {
        return currentIter < maxIter;
    }

    public void initIterator() {
        if(agentsIDs == null){
            agentsIDs = new ArrayList<Integer>(experiment.getAliveAgents().keySet());
        }
        currentIter = 0;
        maxIter = experiment.getNumberOfIterations();
        if(experiment instanceof SelectiveAcceptanceSpecializationExperiment || experiment instanceof DelegationSpecializationExperiment){
            maxIter *= 20;
        }
    }

    @Override
    public String next() {

        int firstIndex = rand.nextInt(agentsIDs.size());
        int secondAgentId;


        if(experiment.getAliveAgents().get(agentsIDs.get(firstIndex)).getAcquaintances().isEmpty() || !onlyAcquaintances){
            int secondIndex = rand.nextInt(agentsIDs.size());
            while (secondIndex == firstIndex) {
                secondIndex = rand.nextInt(agentsIDs.size());
            }
            secondAgentId = agentsIDs.get(secondIndex);
        }else{
            ArrayList<MultitaskingAgent> acquaintances = new ArrayList<MultitaskingAgent>(experiment.getAliveAgents().get(agentsIDs.get(firstIndex)).getAcquaintances().keySet());
            Collections.shuffle(acquaintances);
            secondAgentId = acquaintances.get(0).getId();
        }

        BitSet instance = experiment.getEnvironment().generateInstance();

        ArrayList<Task> pickedTasks = experiment.getEnvironment().pickMultipleTasks((rand.nextDouble() <= experiment.getSpecializedGamesRatio()) ? experiment.getPickedTasks() : 1);

        currentIter += 1;

        StringBuilder taskStringBuilder = new StringBuilder();
        for (int i = 0; i < pickedTasks.size(); i++) {
            taskStringBuilder.append(pickedTasks.get(i).getName()).append(":");
        }

        HashSet<Integer> uncertainBits = new HashSet<>();
        StringBuilder uncertainStringBuilder = new StringBuilder();

        for (int i = 0; i < numberOfUncertainBits; i++) {
            int bitIndex = rand.nextInt(numberOfFeatures);
            while(uncertainBits.contains(bitIndex)){
                bitIndex = rand.nextInt(numberOfFeatures);
            }
            uncertainBits.add(bitIndex);
            uncertainStringBuilder.append(String.valueOf(bitIndex)).append(":");
        }

        if(uncertainStringBuilder.toString().isEmpty()){
            uncertainStringBuilder.append(":");
        }

        return "" + agentsIDs.get(firstIndex) + "\t" + secondAgentId + "\t" + instance + "\t" + taskStringBuilder.toString() + "\t" + uncertainStringBuilder.toString();
    }

}
