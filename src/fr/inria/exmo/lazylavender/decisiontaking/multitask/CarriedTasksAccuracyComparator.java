package fr.inria.exmo.lazylavender.decisiontaking.multitask;

import java.util.ArrayList;
import java.util.Comparator;

public class CarriedTasksAccuracyComparator implements Comparator<MultitaskingAgent> {

    private ArrayList<EnvironmentExample> examples;

    public CarriedTasksAccuracyComparator(ArrayList<EnvironmentExample> examples){
        this.examples = examples;
    }

    @Override
    public int compare(MultitaskingAgent agent1, MultitaskingAgent agent2) {
        if (agent1.getAccuracy(agent1.getPreferredTasks(), examples) != agent2.getAccuracy(agent2.getPreferredTasks(), examples)) {
            return agent1.getAccuracy(agent1.getPreferredTasks(), examples)<agent2.getAccuracy(agent2.getPreferredTasks(), examples) ? -1 : 1;
        }
        return 0;
    }

    public ArrayList<EnvironmentExample> getExamples(){
        return examples;
    }

}
