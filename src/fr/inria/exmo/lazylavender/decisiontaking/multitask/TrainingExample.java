package fr.inria.exmo.lazylavender.decisiontaking.multitask;

import java.util.BitSet;

public class TrainingExample {

    private BitSet instance;
    private Task task;
    private int correctDecision;

    public TrainingExample(BitSet instance, Task task, int correctDecision){
        this.instance = instance;
        this.task = task;
        this.correctDecision = correctDecision;
    }

    public BitSet getInstance(){
        return this.instance;
    }

    public Task getTask(){
        return this.task;
    }

    public int getCorrectDecision(){
        return this.correctDecision;
    }

}
