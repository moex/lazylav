package fr.inria.exmo.lazylavender.decisiontaking.multitask;

import java.util.*;

public final class GraphUtils {

    public static void initGraph(HashMap<Integer, MultitaskingAgent> agents, double edgePercentage){

        ArrayList<String> candidateEdges = new ArrayList<>();
        ArrayList<String> finalEdges = new ArrayList<>();

        for (Map.Entry<Integer, MultitaskingAgent> entry : agents.entrySet()) {
            entry.getValue().getAcquaintances().clear();
        }

        for (Map.Entry<Integer, MultitaskingAgent> externalEntry : agents.entrySet()) {
            for (Map.Entry<Integer, MultitaskingAgent> internalEntry : agents.entrySet()) {
                if(externalEntry.getKey()!=internalEntry.getKey()){
                    candidateEdges.add(String.valueOf(externalEntry.getKey())+":"+String.valueOf(internalEntry.getKey()));
                }
            }
        }

        Collections.shuffle(candidateEdges);

        double requiredEdges = candidateEdges.size()*edgePercentage;

        for (int i = 0; i < candidateEdges.size(); i++) {
            finalEdges.add(candidateEdges.get(i));
            if(finalEdges.size()>=requiredEdges){
                break;
            }
        }

        for (int i = 0; i < finalEdges.size(); i++) {
            String[] tokens = finalEdges.get(i).split(":");
            MultitaskingAgent agent1 = agents.get(Integer.parseInt(tokens[0]));
            MultitaskingAgent agent2 = agents.get(Integer.parseInt(tokens[1]));
            agent1.addAcquaintance(agent2, 1);
        }

    }
}
