package fr.inria.exmo.lazylavender.decisiontaking.multitask;

import java.util.BitSet;

public class EnvironmentExample {

    private BitSet instance;
    private MultitaskDecision multiLabel;

    public EnvironmentExample(BitSet instance, MultitaskDecision multiLabel){
        this.instance = instance;
        this.multiLabel = multiLabel;
    }

    public BitSet getInstance(){
        return this.instance;
    }

    public MultitaskDecision getMultiDecision(){
        return this.multiLabel;
    }

}
