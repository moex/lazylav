package fr.inria.exmo.lazylavender.decisiontaking.multitask;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Random;

public class Task {

    private String name;
    private double rewardCoefficient;
    private int numberOfDecisions;
    private int reward;
    private int penalty;
    private HashSet<Integer> irrelevantFeatures;

    private int numberOfFeatures;

    public Task(String name, double rewardCoefficient, int numberOfDecisions, int reward, int penalty, int numberOfFeatures){
        this.name = name;
        this.numberOfDecisions = numberOfDecisions;
        this.rewardCoefficient = rewardCoefficient;
        this.reward = reward;
        this.penalty = penalty;
        this.numberOfFeatures = numberOfFeatures;
    }

    public Task(String name, double rewardCoefficient, int numberOfDecisions, int reward, int penalty, int numberOfFeatures, HashSet<Integer> irrelevantFeaturesList) {
        this(name, rewardCoefficient, numberOfDecisions, reward, penalty, numberOfFeatures);
        irrelevantFeatures = irrelevantFeaturesList;
    }

    public Task(String name, double rewardCoefficient, int numberOfDecisions, int reward, int penalty, int numberOfFeatures, int numberOfIrrelevantFeatures) {
        this(name, rewardCoefficient, numberOfDecisions, reward, penalty, numberOfFeatures);

        irrelevantFeatures = new HashSet<>();

        Random random = new Random();

        for (int i = 0; i < numberOfIrrelevantFeatures; i++) {
            int bitIndex = random.nextInt(numberOfFeatures);
            while(irrelevantFeatures.contains(bitIndex)){
                bitIndex = random.nextInt(numberOfFeatures);
            }
            irrelevantFeatures.add(bitIndex);
        }
    }

    public String getName(){
        return this.name;
    }

    public int getNumberOfDecisions(){
        return this.numberOfDecisions;
    }

    public double getRewardCoefficient(){
        return this.rewardCoefficient;
    }

    public int getReward(){
        return this.reward;
    }

    public int getPenalty(){
        return this.penalty;
    }

    public void setNumberOfFeatures(int numberOfFeatures){
        this.numberOfFeatures = numberOfFeatures;
    }

    public int getNumberOfFeatures(){
        return numberOfFeatures;
    }

    public HashSet<Integer> getIrrelevantFeatures(){
        return irrelevantFeatures;
    }

    public String irrelevantFeaturesToString(){
        StringBuilder stringBuilder = new StringBuilder();
        Iterator<Integer> iterator = irrelevantFeatures.iterator();
        while(iterator.hasNext()){
            stringBuilder.append(iterator.next()).append(":");
        }
        return stringBuilder.toString();
    }

    public void addIrrelevantFeature(int relevantFeature){
        irrelevantFeatures.add(relevantFeature);
    }

}
