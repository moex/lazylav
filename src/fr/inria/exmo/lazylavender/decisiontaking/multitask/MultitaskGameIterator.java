package fr.inria.exmo.lazylavender.decisiontaking.multitask;

import java.util.*;

public class MultitaskGameIterator implements Iterator<String> {
    private int maxIter, currentIter;
    private MultitaskLLExperiment experiment;
    private Random rand;
    private List<Integer> agentsIDs;
    private boolean onlyAcquaintances;
    private int numberOfUncertainBits;
    private int numberOfFeatures;

    MultitaskGameIterator(MultitaskLLExperiment experiment, boolean onlyAcquaintances, int numberOfUncertainBits, int numberOfFeatures) {
        this.experiment = experiment;
        this.onlyAcquaintances = onlyAcquaintances;
        this.numberOfUncertainBits = numberOfUncertainBits;
        this.numberOfFeatures = numberOfFeatures;
        initIterator();
        rand = new Random();
    }

    @Override
    public boolean hasNext() {
        return currentIter < maxIter;
    }

    public void initIterator() {
        if(agentsIDs == null){
            agentsIDs = new ArrayList<Integer>(experiment.getAliveAgents().keySet());
        }
        currentIter = 0;
        maxIter = experiment.getNumberOfIterations();
        if(experiment instanceof SelectiveAcceptanceSpecializationExperiment || experiment instanceof DelegationSpecializationExperiment){
            maxIter *= 20;
        }
        System.out.println("maxIter:"+maxIter);
    }

    public void updateAgentIDs(){
        agentsIDs = new ArrayList<Integer>(experiment.getAliveAgents().keySet());
    }

    @Override
    public String next() {

        //System.out.println("agentsIDs size: "+agentsIDs.size());

        int firstIndex = rand.nextInt(agentsIDs.size());

        int secondAgentId;

        if(experiment.getAliveAgents().get(agentsIDs.get(firstIndex)).getAcquaintances().isEmpty() || !onlyAcquaintances){
            int secondIndex = rand.nextInt(agentsIDs.size());
            while (secondIndex == firstIndex) {
                secondIndex = rand.nextInt(agentsIDs.size());
            }
            secondAgentId = agentsIDs.get(secondIndex);
        }else{
            ArrayList<MultitaskingAgent> acquaintances = new ArrayList<MultitaskingAgent>(experiment.getAliveAgents().get(agentsIDs.get(firstIndex)).getAcquaintances().keySet());
            Collections.shuffle(acquaintances);
            secondAgentId = acquaintances.get(0).getId();
        }

        BitSet instance = experiment.getEnvironment().generateInstance();

        Task task = experiment.getEnvironment().pickTask();

        currentIter += 1;

        HashSet<Integer> uncertainBits = new HashSet<>();
        StringBuilder uncertainStringBuilder = new StringBuilder();

        for (int i = 0; i < numberOfUncertainBits; i++) {
            int bitIndex = rand.nextInt(numberOfFeatures);
            while(uncertainBits.contains(bitIndex)){
                bitIndex = rand.nextInt(numberOfFeatures);
            }
            uncertainBits.add(bitIndex);
            uncertainStringBuilder.append(String.valueOf(bitIndex)).append(":");
        }

        if(uncertainStringBuilder.toString().isEmpty()){
            uncertainStringBuilder.append(":");
        }

        return agentsIDs.get(firstIndex) + "\t" + secondAgentId + "\t" + instance + "\t" + task.getName() + "\t" + uncertainStringBuilder.toString();
    }

}
