/*
 * Copyright (C) INRIA, 2021-2024
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 */

package fr.inria.exmo.lazylavender.decisiontaking.multitask;

import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

public class ExampleUtils {

    public static ArrayList<TrainingExample> environmentExamplesToTrainingExamples(ArrayList<EnvironmentExample> environmentExamples, int numberOfFeatures){

        //It works as long as all environment examples have all tasks!
        Task[] tasks = environmentExamples.get(0).getMultiDecision().getTasks().toArray(new Task[environmentExamples.get(0).getMultiDecision().getTasks().size()]);

        HashMap<Task, ArrayList<Integer>> availableDecisionsPerTask = new HashMap<>();
        for (int i = 0; i < tasks.length; i++) {

            int multiplier = (int) Math.pow(2,(numberOfFeatures-tasks[i].getIrrelevantFeatures().size()))/tasks[i].getNumberOfDecisions();

            ArrayList<Integer> availableDecisions = new ArrayList<>();
            for (int j = 0; j < tasks[i].getNumberOfDecisions(); j++) {
                for (int k = 0; k < multiplier; k++) {
                    availableDecisions.add(j);
                }
            }
            availableDecisionsPerTask.put(tasks[i], availableDecisions);
//            System.out.println("Available decisions for task:"+tasks[i].getName()+" are:"+availableDecisions.size());
//            System.out.println("These are:");
//            for (int j = 0; j < availableDecisions.size(); j++) {
//                System.out.println(availableDecisions.get(j));
//            }
        }

        //USED TO STORE PREVIOUSLY ASSIGNED DECISIONS
        HashMap<Task, HashMap<String, Integer>> previouslyAssignedDecisions = new HashMap<>();

        ArrayList<TrainingExample> trainingExamples = new ArrayList<TrainingExample>();

        Random rand = new Random();

        //System.out.println("Environment examples : "+environmentExamples.size());

        for (int i = 0; i < environmentExamples.size(); i++) {

            BitSet currentInstance = environmentExamples.get(i).getInstance();

            for (int j = 0; j < tasks.length; j++) {

                BitSet taskSpecificInstance = (BitSet) currentInstance.clone();

                if(tasks[j].getIrrelevantFeatures().size()>0){

                    //System.out.println("The task:"+j+" has the following irrelevant features:"+tasks[j].irrelevantFeaturesToString());

                    for (int k = 0; k < taskSpecificInstance.size(); k++) {
                        if(tasks[j].getIrrelevantFeatures().contains(k)){
                            taskSpecificInstance.set(k, false);
                        }
                    }

                    if(previouslyAssignedDecisions.containsKey(tasks[j])){

                        HashMap<String, Integer> bitsetMap = previouslyAssignedDecisions.get(tasks[j]);

                        if(bitsetMap.containsKey(taskSpecificInstance.toString())){
                            int existingDecision = bitsetMap.get(taskSpecificInstance.toString());
                            environmentExamples.get(i).getMultiDecision().putDecision(tasks[j], existingDecision);
                        }else{

                            int randomIndex = rand.nextInt(availableDecisionsPerTask.get(tasks[j]).size());
                            int randomDecision = availableDecisionsPerTask.get(tasks[j]).get(randomIndex);

                            availableDecisionsPerTask.get(tasks[j]).remove(randomIndex);

                            //randomDecision = ThreadLocalRandom.current().nextInt(0, tasks[j].getNumberOfDecisions() + 1);

                            bitsetMap.put(taskSpecificInstance.toString(), randomDecision);
                            previouslyAssignedDecisions.put(tasks[j], bitsetMap);
                            environmentExamples.get(i).getMultiDecision().putDecision(tasks[j], randomDecision);
                        }
                    }else{

                        int randomIndex = rand.nextInt(availableDecisionsPerTask.get(tasks[j]).size());
                        int randomDecision = availableDecisionsPerTask.get(tasks[j]).get(randomIndex);

                        availableDecisionsPerTask.get(tasks[j]).remove(randomIndex);

                        //randomDecision = ThreadLocalRandom.current().nextInt(0, tasks[j].getNumberOfDecisions() + 1);

                        HashMap<String, Integer> bitsetMap = new HashMap<>();
                        bitsetMap.put(taskSpecificInstance.toString(), randomDecision);
                        previouslyAssignedDecisions.put(tasks[j], bitsetMap);
                        environmentExamples.get(i).getMultiDecision().putDecision(tasks[j], randomDecision);
                    }

                }

                trainingExamples.add(new TrainingExample(currentInstance, tasks[j], environmentExamples.get(i).getMultiDecision().getDecision(tasks[j])));

            }

        }

//        System.out.println("All training examples just after their creation");
//        for (int i = 0; i < trainingExamples.size(); i++) {
//            System.out.println("Example:"+i+" Object:"+trainingExamples.get(i).getInstance().toString()+" Task:"+trainingExamples.get(i).getTask().getName()+" Correct decision:"+trainingExamples.get(i).getCorrectDecision());
//        }

        return trainingExamples;
    }

}
