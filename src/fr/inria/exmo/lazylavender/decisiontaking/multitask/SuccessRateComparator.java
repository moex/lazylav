package fr.inria.exmo.lazylavender.decisiontaking.multitask;

import java.util.ArrayList;
import java.util.Comparator;

public class SuccessRateComparator implements Comparator<MultitaskingAgent> {

    private ArrayList<EnvironmentExample> examples;
    private ArrayList<Task> tasks;

    public SuccessRateComparator(ArrayList<Task> tasks){
        this.tasks = tasks;
    }

    @Override
    public int compare(MultitaskingAgent agent1, MultitaskingAgent agent2) {

        double agent1Sum = 0;
        double agent2Sum = 0;

        for (int i = 0; i < tasks.size(); i++) {
            agent1Sum += agent1.getTaskSuccessRate(tasks.get(i));
            agent2Sum += agent2.getTaskSuccessRate(tasks.get(i));
        }

        double agent1SuccessRate = agent1Sum/tasks.size();
        double agent2SuccessRate = agent2Sum/tasks.size();

        if (agent1SuccessRate != agent2SuccessRate) {
            return agent1SuccessRate<agent2SuccessRate ? -1 : 1;
        }
        return 0;
    }

}
