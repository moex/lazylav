package fr.inria.exmo.lazylavender.decisiontaking.multitask;

import java.util.ArrayList;
import java.util.Comparator;

public class MaximumAccuracyComparator implements Comparator<MultitaskingAgent> {

    private ArrayList<Task> tasks;

    public MaximumAccuracyComparator(ArrayList<Task> tasks){
        this.tasks = tasks;
    }

    @Override
    public int compare(MultitaskingAgent agent1, MultitaskingAgent agent2) {
        if (agent1.getMaxAccuracy(tasks) != agent2.getMaxAccuracy(tasks)) {
            return agent1.getMaxAccuracy(tasks)<agent2.getMaxAccuracy(tasks) ? -1 : 1;
        }
        return 0;
    }


}
