package fr.inria.exmo.lazylavender.decisiontaking.multitask;

import fr.inria.exmo.lazylavender.decisiontaking.measurements.RecordingManager;
import fr.inria.exmo.lazylavender.expe.LLExperiment;

import java.util.HashMap;
import java.util.Properties;

public interface MultitaskLLExperiment extends LLExperiment {

    public MultitaskEnvironment getEnvironment();
    public HashMap<Integer, MultitaskingAgent> getAliveAgents();
    public HashMap<Integer, MultitaskingAgent> getDeadAgents();
    public int getNumberOfIterations();
    public int getNumberOfAgents();
    public Properties getParams();
    public int getMaxAgentId();
    public void setMaxAgentId(int maxAgentId);
    public double getRatio();
    public int getGenerationInterval();
    public void setGenerationInterval(int generationInterval);
    public int getNumberOfBirths();
    public int getNumberOfDeaths();
    public MultitaskGameIterator getGameIterator();
    public int getCurrentGeneration();
    public RecordingManager getRecordingManager();
    public HashMap<Task, Integer> getGamesPlayed();
    public double getSampleRatio();

}
