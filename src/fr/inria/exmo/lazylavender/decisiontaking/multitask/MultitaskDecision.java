package fr.inria.exmo.lazylavender.decisiontaking.multitask;

import fr.inria.exmo.lazylavender.decisiontaking.treeclassifier.Decision;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ThreadLocalRandom;

public class MultitaskDecision implements Decision {

    private HashMap<Task, Integer> decisions;

    public MultitaskDecision(){
        decisions = new HashMap<Task, Integer>();
    }

    public HashMap<Task, Integer> getDecisions(){
        return decisions;
    }

    public HashMap<Task, Integer> copyDecisions(){
        HashMap<Task, Integer> copiedDecisions = new HashMap<>();
        copiedDecisions.putAll(decisions);
        return copiedDecisions;
    }

    public void setDecisions(HashMap<Task, Integer> decisions){
        this.decisions = decisions;
    }

    public void putDecision(Task task, int d){
        decisions.put(task, d);
    }

    public int getDecision(Task task){
        return decisions.get(task);
    }

    public int getDecision(Task task, MultitaskingAgent.UnknownDecisionStrategy strategy){
        if(!decisions.containsKey(task)) {
            if (strategy == MultitaskingAgent.UnknownDecisionStrategy.RANDOM) {
                this.putDecision(task, ThreadLocalRandom.current().nextInt(0, task.getNumberOfDecisions()));
            } else if (strategy == MultitaskingAgent.UnknownDecisionStrategy.FIRST) {
                this.putDecision(task, 1);
            } else if (strategy == MultitaskingAgent.UnknownDecisionStrategy.NONE) {
                this.putDecision(task, -1);
            }
        }
        return decisions.get(task);
    }

    public boolean hasTask(Task task){
        return decisions.containsKey(task);
    }

    public Set<Task> getTasks(){
        return decisions.keySet();
    }

    public MultitaskDecision copy() {
        MultitaskDecision multitaskDecision = new MultitaskDecision();
        for(Task key: decisions.keySet()) {
            multitaskDecision.putDecision(key, decisions.get(key));
        }
        return multitaskDecision;
    }

    @Override
    public String toString() {
        String str = "[";
        for(Map.Entry<Task, Integer> set: decisions.entrySet()) {
            //str += set.getKey().getName() + " -> " + set.getValue() + ",";
            str += set.getValue() + ",";
        }
        str = str.substring(0, str.length()-1);
        str += "]";
        return str;
    }

    public int getDifference(MultitaskDecision anotherMultitaskDecision){
        int differences = 0;
        HashSet<Task> allTasks = new HashSet<>();
        allTasks.addAll(this.getTasks());
        allTasks.addAll(anotherMultitaskDecision.getTasks());

        for (Task task : allTasks) {
            if(this.hasTask(task) && anotherMultitaskDecision.hasTask(task)){
                if(this.getDecision(task) != anotherMultitaskDecision.getDecision(task)){
                    differences++;
                }
            }else{
                differences++;
            }
        }
        return differences;
    }

}
