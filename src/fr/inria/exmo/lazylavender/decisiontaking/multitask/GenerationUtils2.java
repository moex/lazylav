/*
 * Copyright (C) INRIA, 2023-2024
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 */

package fr.inria.exmo.lazylavender.decisiontaking.multitask;

import fr.inria.exmo.lazylavender.decisiontaking.measurements.Recorder;

import java.util.Map;
import java.util.Set;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Collection;
import java.util.Collections;
import java.util.Random;

public class GenerationUtils2 {

    enum ParentSelectionPolicy {
        RANDOM,
        BEST_PARENTS,
        BEST_PARENT_BEST_MATE
    }

    enum BirthCriteria {
        ACCURACY,
        COMPENSATION,
        AGE,
        SUCCESS_RATE
    }

    enum BirthSortingOrder{
        ASCENDING,
        DESCENDING
    }

    enum EliminationCriteria {
        ACCURACY,
        COMPENSATION,
        AGE,
        SUCCESS_RATE
    }

    enum EliminationSortingOrder{
        ASCENDING,
        DESCENDING
    }

    enum RenewalDomain {
        PERMUTATION,
        POPULATION
    }

    public static void updateAgentPeriodsAlive(MultitaskLLExperiment exp){
        for (Map.Entry<Integer, MultitaskingAgent> set : exp.getAliveAgents().entrySet()) {
            set.getValue().setPeriodsAlive(set.getValue().getPeriodsAlive()+1);
        }
    }

    private static ArrayList<EnvironmentExample> getEnvironmentExamples(MultitaskLLExperiment exp, double ratio){

        Random rand = new Random();
        ArrayList<EnvironmentExample> environmentExamples = new ArrayList<EnvironmentExample>(exp.getEnvironment().getEnvironmentExamples().values());
        ArrayList<EnvironmentExample> finalEnvironmentExamples = new ArrayList<>();

        Collections.shuffle(environmentExamples);

        for (int i = 0; i < ratio*environmentExamples.size(); i++) {
            finalEnvironmentExamples.add(environmentExamples.get(i));
        }

        return finalEnvironmentExamples;
    }

    private static HashMap<HashMap<Task, Integer>, AccuracyComparator> initAccuracyComparators(ArrayList<HashMap<Task, Integer>> permutationsList, int maxAdaptingRank, MultitaskLLExperiment exp){
        HashMap<HashMap<Task, Integer>, AccuracyComparator> comparatorsMap = new HashMap<>();

        for (int i = 0; i < permutationsList.size(); i++) {
            ArrayList<Task> comparatorTasks = new ArrayList<>();
            for (Map.Entry<Task, Integer> set : permutationsList.get(i).entrySet()) {
                if(set.getValue()<=maxAdaptingRank){
                    comparatorTasks.add(set.getKey());
                }
            }
            AccuracyComparator comparator = new AccuracyComparator(comparatorTasks, GenerationUtils2.getEnvironmentExamples(exp, exp.getSampleRatio()));
            comparatorsMap.put(permutationsList.get(i),comparator);
        }

        return comparatorsMap;
    }


    private static HashMap<HashMap<Task, Integer>, SuccessRateComparator> initSuccessRateComparators(ArrayList<HashMap<Task, Integer>> permutationsList, int maxAdaptingRank, MultitaskLLExperiment exp){
        HashMap<HashMap<Task, Integer>, SuccessRateComparator> comparatorsMap = new HashMap<>();

        for (int i = 0; i < permutationsList.size(); i++) {
            ArrayList<Task> comparatorTasks = new ArrayList<>();
            for (Map.Entry<Task, Integer> set : permutationsList.get(i).entrySet()) {
                if(set.getValue()<=maxAdaptingRank){
                    comparatorTasks.add(set.getKey());
                }
            }
            SuccessRateComparator comparator = new SuccessRateComparator(comparatorTasks);
            comparatorsMap.put(permutationsList.get(i),comparator);
        }

        return comparatorsMap;
    }

    public static void populationRenewal(int numberOfBirths, int numberOfDeaths, MultitaskLLExperiment exp, ParentSelectionPolicy policy, BirthCriteria birthCriteria, EliminationCriteria eliminationCriteria, RenewalDomain renewalDomain, BirthSortingOrder birthSortingOrder, EliminationSortingOrder eliminationSortingOrder){

        //System.out.println("\n\nNew generation\n\n");

        int birthsPerPermutation = -1;

        Collection<MultitaskingAgent> aliveAgentsInCollection = exp.getAliveAgents().values();
        ArrayList<MultitaskingAgent> aliveAgentsInList = new ArrayList<>(aliveAgentsInCollection);

        HashMap<HashMap<Task, Integer>,ArrayList<MultitaskingAgent>> agentsPerDistinctPermutation = new HashMap<>();
        HashMap<HashMap<Task, Integer>,Integer> counterPerDistinctPermutation = new HashMap<>();
        if(exp instanceof SpecializationLLExperiment) {
            for (int i = 0; i < aliveAgentsInList.size(); i++) {
                MultitaskingAgent currAgent = aliveAgentsInList.get(i);
                if(agentsPerDistinctPermutation.containsKey(currAgent.getAdaptingTasksRanking())){
                    ArrayList<MultitaskingAgent> previousAgents = agentsPerDistinctPermutation.get(currAgent.getAdaptingTasksRanking());
                    previousAgents.add(currAgent);
                }else{
                    ArrayList<MultitaskingAgent> tempList = new ArrayList<>();
                    tempList.add(currAgent);
                    agentsPerDistinctPermutation.put(currAgent.getAdaptingTasksRanking(), tempList);
                    counterPerDistinctPermutation.put(currAgent.getAdaptingTasksRanking(), 0);
                }
            }
            birthsPerPermutation = numberOfBirths / counterPerDistinctPermutation.size();
        }

        Set<HashMap<Task, Integer>> permutationKeySet = agentsPerDistinctPermutation.keySet();
        ArrayList<HashMap<Task, Integer>> permutationKeySetList = new ArrayList<>(permutationKeySet);

        int maxAdaptingRank = exp.getEnvironment().getNumberOfTasks();

        if(exp instanceof SpecializationLLExperiment){
            maxAdaptingRank = ((SpecializationLLExperiment)exp).getMaxAdaptingRank();
        }

        //TODO INIT COMPARATORS

        HashMap<HashMap<Task, Integer>, AccuracyComparator> accuracyComparators = GenerationUtils2.initAccuracyComparators(permutationKeySetList, maxAdaptingRank, exp);

        HashMap<HashMap<Task, Integer>, SuccessRateComparator> successRateComparators = GenerationUtils2.initSuccessRateComparators(permutationKeySetList, maxAdaptingRank, exp);

        SuccessRateComparator averageSuccessRateComparator = new SuccessRateComparator(new ArrayList<Task>(exp.getEnvironment().getEnvironmentTasks().values()));

        AccuracyComparator averageAccuracyComparator = new AccuracyComparator(new ArrayList<Task>(exp.getEnvironment().getEnvironmentTasks().values()), new ArrayList<EnvironmentExample>(exp.getEnvironment().getEnvironmentExamples().values()));

        AgentAgeComparator ageComparator = new AgentAgeComparator();

        CompensationComparator compensationComparator = new CompensationComparator();

        //TODO GIVING BIRTH

        ArrayList<MultitaskingAgent> newDeads = new ArrayList<>();
        ArrayList<MultitaskingAgent> newBorns = new ArrayList<>();

        for (int i = 0; i < numberOfBirths; i++) {

            MultitaskingAgent newAgent = new MultitaskingAgent(exp.getMaxAgentId()+1, exp.getEnvironment(), exp, exp.getParams());

            exp.setMaxAgentId(exp.getMaxAgentId()+1);

            MultitaskingAgent parent1 = null;
            MultitaskingAgent parent2 = null;

            Random rand = new Random();

            HashMap<Task, Integer> randomPermutation = new HashMap<>();

            if(exp instanceof SpecializationLLExperiment){

                if(renewalDomain == RenewalDomain.PERMUTATION) {

                    randomPermutation = permutationKeySetList.get(rand.nextInt(permutationKeySetList.size()));
                    while (counterPerDistinctPermutation.get(randomPermutation) > birthsPerPermutation - 1) {
                        System.out.println("CounterPerDistinctPermutation:"+counterPerDistinctPermutation.get(randomPermutation));
                        System.out.println("BirthsPerPermutation:"+birthsPerPermutation);
                        randomPermutation = permutationKeySetList.get(rand.nextInt(permutationKeySetList.size()));
                    }
                    counterPerDistinctPermutation.put(randomPermutation, counterPerDistinctPermutation.get(randomPermutation) + 1);

                    ArrayList<MultitaskingAgent> agentsForRandomPermutation = agentsPerDistinctPermutation.get(randomPermutation);

                    if(birthCriteria==BirthCriteria.AGE) {
                        Collections.sort(agentsForRandomPermutation, ageComparator);
                    } else if (birthCriteria==BirthCriteria.COMPENSATION) {
                        Collections.sort(agentsForRandomPermutation, compensationComparator);
                    }else if (birthCriteria==BirthCriteria.ACCURACY){
                        AccuracyComparator accuracyComparatorForRandomPermutation = accuracyComparators.get(randomPermutation);
                        Collections.sort(agentsForRandomPermutation, accuracyComparatorForRandomPermutation);
                    }else if (birthCriteria==BirthCriteria.SUCCESS_RATE) {
                        SuccessRateComparator successRateComparator = successRateComparators.get(randomPermutation);
                        Collections.sort(agentsForRandomPermutation, successRateComparator);
                    }

                    if(birthSortingOrder==BirthSortingOrder.DESCENDING){
                        Collections.reverse(agentsForRandomPermutation);
                    }

                    if (policy == ParentSelectionPolicy.BEST_PARENTS) {
                        parent1 = agentsForRandomPermutation.get((counterPerDistinctPermutation.get(randomPermutation) * 2) - 2);
                        parent2 = agentsForRandomPermutation.get((counterPerDistinctPermutation.get(randomPermutation) * 2) - 1);
                    } else if(policy==ParentSelectionPolicy.BEST_PARENT_BEST_MATE){
                        parent1 = agentsForRandomPermutation.get((counterPerDistinctPermutation.get(randomPermutation) * 2) - 2);
                        double maxSuccessRate = -1;
                        MultitaskingAgent bestMatch = null;
                        for (Map.Entry<Integer, MultitaskingAgent> entry : exp.getAliveAgents().entrySet()) {
                            MultitaskingAgent value = entry.getValue();

                            if(value.getId()== parent1.getId())
                                continue;

                            double successfulInteractionsWithCurrentAgent = (parent1.getSuccessfulInteractions().get(value)!=null) ? parent1.getSuccessfulInteractions().get(value) : 0;
                            double totalInteractionsWithCurrentAgent = (parent1.getTotalInteractions().get(value)!=null) ? parent1.getTotalInteractions().get(value) : 1;
                            double currentSuccessRate = successfulInteractionsWithCurrentAgent/totalInteractionsWithCurrentAgent;

                            if(currentSuccessRate>maxSuccessRate){
                                maxSuccessRate = currentSuccessRate;
                                bestMatch = value;
                            }
                        }
                        parent2 = bestMatch;
                    }  else {
                        parent1 = agentsForRandomPermutation.get(rand.nextInt(agentsForRandomPermutation.size()));
                        parent2 = agentsForRandomPermutation.get(rand.nextInt(agentsForRandomPermutation.size()));
                        while (parent1.getId() == parent2.getId()) {
                            parent2 = agentsPerDistinctPermutation.get(randomPermutation).get(rand.nextInt(agentsPerDistinctPermutation.get(randomPermutation).size()));
                        }
                    }

                }else if(renewalDomain==RenewalDomain.POPULATION){
                    if(birthCriteria==BirthCriteria.AGE) {
                        Collections.sort(aliveAgentsInList, ageComparator);
                    } else if (birthCriteria==BirthCriteria.COMPENSATION) {
                        Collections.sort(aliveAgentsInList, compensationComparator);
                    }else if (birthCriteria==BirthCriteria.ACCURACY){
                        Collections.sort(aliveAgentsInList, averageAccuracyComparator);
                    }else if (birthCriteria==BirthCriteria.SUCCESS_RATE) {
                        Collections.sort(aliveAgentsInList, averageSuccessRateComparator);
                    }

                    if(birthSortingOrder==BirthSortingOrder.DESCENDING){
                        Collections.reverse(aliveAgentsInList);
                    }

                    if (policy == ParentSelectionPolicy.BEST_PARENTS) {
                        parent1 = aliveAgentsInList.get((numberOfBirths * 2) - 2);
                        parent2 = aliveAgentsInList.get((numberOfBirths * 2) - 1);
                    } else if(policy==ParentSelectionPolicy.BEST_PARENT_BEST_MATE){
                        parent1 = aliveAgentsInList.get((numberOfBirths * 2) - 2);
                        double maxSuccessRate = -1;
                        MultitaskingAgent bestMatch = null;
                        for (Map.Entry<Integer, MultitaskingAgent> entry : exp.getAliveAgents().entrySet()) {
                            MultitaskingAgent value = entry.getValue();

                            if(value.getId()== parent1.getId())
                                continue;

                            double successfulInteractionsWithCurrentAgent = (parent1.getSuccessfulInteractions().get(value)!=null) ? parent1.getSuccessfulInteractions().get(value) : 0;
                            double totalInteractionsWithCurrentAgent = (parent1.getTotalInteractions().get(value)!=null) ? parent1.getTotalInteractions().get(value) : 1;
                            double currentSuccessRate = successfulInteractionsWithCurrentAgent/totalInteractionsWithCurrentAgent;

                            if(currentSuccessRate>maxSuccessRate){
                                maxSuccessRate = currentSuccessRate;
                                bestMatch = value;
                            }
                        }
                        parent2 = bestMatch;
                    } else {
                        parent1 = aliveAgentsInList.get(rand.nextInt(aliveAgentsInList.size()));
                        parent2 = aliveAgentsInList.get(rand.nextInt(aliveAgentsInList.size()));
                        while (parent1.getId() == parent2.getId()) {
                            parent2 = aliveAgentsInList.get(rand.nextInt(aliveAgentsInList.size()));
                        }
                    }
                }
            } else if(exp instanceof MultitaskLLExperiment) {

                parent1 = aliveAgentsInList.get(rand.nextInt(aliveAgentsInCollection.size()));
                parent2 = aliveAgentsInList.get(rand.nextInt(aliveAgentsInCollection.size()));
                while (parent1.getId() == parent2.getId()) {
                    parent2 = aliveAgentsInList.get(rand.nextInt(aliveAgentsInCollection.size()));
                }
            }

            newAgent.addParent(parent1);
            newAgent.addParent(parent2);

            //TODO DOWN OF HERE EXAMPLES CREATION!

            ArrayList<TrainingExample> finalExamples = new ArrayList<>();

            ArrayList<TrainingExample> parentsExamples = new ArrayList<>();

            ArrayList<EnvironmentExample> environmentExamples = GenerationUtils2.getEnvironmentExamples(exp, 1.0);

            ArrayList<TrainingExample> parent1Examples = parent1.createExamples(environmentExamples);
            ArrayList<TrainingExample> parent2Examples = parent2.createExamples(environmentExamples);

            parentsExamples.addAll(parent1Examples);
            parentsExamples.addAll(parent2Examples);

            Collections.shuffle(parentsExamples);

            while(finalExamples.isEmpty()) {
                for (int j = 0; j < parentsExamples.size(); j++) {
                    finalExamples.add(parentsExamples.get(j));
                }
            }

            Collections.shuffle(finalExamples);

            //System.out.println("The new agent will be trained with "+finalExamples.size()+" parent examples!");
            try {
                newAgent.getClassifier().train(finalExamples);
            } catch (Exception e) {
                e.printStackTrace();
            }

            newAgent.getClassifier().toString();

            boolean parentCoinToss = rand.nextBoolean();

            newAgent.setAdaptingTasksRanking((parentCoinToss) ? parent1.getAdaptingTasksRanking() : parent2.getAdaptingTasksRanking());
            newAgent.setPreferredTasks((parentCoinToss) ? parent1.getPreferredTasks() :parent2.getPreferredTasks());
            newAgent.setFavoriteTask((parentCoinToss) ? parent1.getFavoriteTask() : parent2.getFavoriteTask());

            newBorns.add(newAgent);
        }

        if(renewalDomain == RenewalDomain.PERMUTATION) {
            //TODO KILL AGENTS PER PERMUTATION
            for (int i = 0; i < permutationKeySetList.size(); i++) {
                HashMap<Task, Integer> currentPermutation = permutationKeySetList.get(i);
                ArrayList<MultitaskingAgent> agentsForCurrentPermutation = agentsPerDistinctPermutation.get(currentPermutation);

                if(eliminationCriteria==EliminationCriteria.AGE) {
                    Collections.sort(agentsForCurrentPermutation, ageComparator);
                } else if (eliminationCriteria==EliminationCriteria.COMPENSATION) {
                    Collections.sort(agentsForCurrentPermutation, compensationComparator);
                }else if (eliminationCriteria==EliminationCriteria.ACCURACY){
                    AccuracyComparator accuracyComparatorForRandomPermutation = accuracyComparators.get(currentPermutation);
                    Collections.sort(agentsForCurrentPermutation, accuracyComparatorForRandomPermutation);
                }else if (eliminationCriteria==EliminationCriteria.SUCCESS_RATE) {
                    SuccessRateComparator successRateComparator = successRateComparators.get(currentPermutation);
                    Collections.sort(agentsForCurrentPermutation, successRateComparator);
                }

                if(eliminationSortingOrder==EliminationSortingOrder.ASCENDING){
                    Collections.reverse(agentsForCurrentPermutation);
                }

                for (int j = agentsForCurrentPermutation.size() - 1; j >= agentsForCurrentPermutation.size() - birthsPerPermutation; j--) {
                    MultitaskingAgent dyingAgent = agentsForCurrentPermutation.get(j);
                    dyingAgent.setAlive(false);
                    exp.getDeadAgents().put(dyingAgent.getId(), dyingAgent);
                    exp.getAliveAgents().remove(dyingAgent.getId());
                    newDeads.add(dyingAgent);
                }
            }
        }else if(renewalDomain==RenewalDomain.POPULATION){

            if(eliminationCriteria==EliminationCriteria.AGE) {
                Collections.sort(aliveAgentsInList, ageComparator);
            } else if (eliminationCriteria==EliminationCriteria.COMPENSATION) {
                Collections.sort(aliveAgentsInList, compensationComparator);
            }else if(eliminationCriteria==EliminationCriteria.ACCURACY){
                Collections.sort(aliveAgentsInList, averageAccuracyComparator);
            }else if (eliminationCriteria==EliminationCriteria.SUCCESS_RATE) {
                Collections.sort(aliveAgentsInList, averageSuccessRateComparator);
            }

            if(eliminationSortingOrder==EliminationSortingOrder.ASCENDING){
                Collections.reverse(aliveAgentsInList);
            }

            for (int j = aliveAgentsInList.size() - 1; j >= aliveAgentsInList.size() - numberOfDeaths; j--) {
                MultitaskingAgent dyingAgent = aliveAgentsInList.get(j);
                dyingAgent.setAlive(false);
                exp.getDeadAgents().put(dyingAgent.getId(), dyingAgent);
                exp.getAliveAgents().remove(dyingAgent.getId());
                newDeads.add(dyingAgent);
            }
        }

        //TODO EnD KILL AGENTS PER PERMUTATION

        //System.out.println("after killing agents, the agents were : " + exp.getAliveAgents().size());

        StringBuilder idsOfAgentsAlive = new StringBuilder();
        for (Map.Entry<Integer, MultitaskingAgent> agentsSet : exp.getAliveAgents().entrySet()) {
            idsOfAgentsAlive.append(agentsSet.getKey()).append(" ");
        }
        System.out.println(idsOfAgentsAlive);

        System.out.println(newBorns.size()+" new agents were created!");

        System.out.println(newDeads.size()+ " agents were killed!");

        for (int i = 0; i < newBorns.size(); i++) {
            //System.out.println("New agent's favorite task:"+newBorns.get(i).getFavoriteTask().getName());
            exp.getAliveAgents().put(newBorns.get(i).getId(), newBorns.get(i));
        }

        exp.getGameIterator().updateAgentIDs();

        //System.out.println("Recorders:"+exp.getRecordingManager().getRecorders().size());

        for(Recorder rec: exp.getRecordingManager().getRecorders()) {
            if(rec instanceof RecorderUpdater){
                ((RecorderUpdater) rec).updateRecorder(newBorns, newDeads);
            }
        }

    }

    private static void printSorted(ArrayList<MultitaskingAgent> sortedAgents, BirthCriteria birthCriteria){

        for (int i = 0; i < sortedAgents.size(); i++) {
            if(birthCriteria==BirthCriteria.AGE) {
                System.out.println(sortedAgents.get(i).getPeriodsAlive());
            } else if (birthCriteria==BirthCriteria.COMPENSATION) {
                System.out.println(sortedAgents.get(i).getCompensation());
            }else if(birthCriteria==BirthCriteria.ACCURACY){
                System.out.println("IT WAS ACCURACY");
            }else if (birthCriteria==BirthCriteria.SUCCESS_RATE)
                System.out.println(sortedAgents.get(i).getTotalSuccesses());
        }
    }

}
