/*
 * Copyright (C) INRIA, 2021-2024
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 */

package fr.inria.exmo.lazylavender.decisiontaking.multitask;

import fr.inria.exmo.lazylavender.decisiontaking.measurements.RecordingManager;
import fr.inria.exmo.lazylavender.logger.ActionLogger;
import fr.inria.exmo.lazylavender.model.LLException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Stream;

public class SelectiveAdaptationSpecializationExperiment implements SpecializationLLExperiment {

    final static Logger logger = LoggerFactory.getLogger(SelectiveAdaptationSpecializationExperiment.class);

    public static final int SAMPLE_SCORE = 1;
    public static final int SUCCESS_SCORE = 2;
    public static final int SUCCESS_SCORE_M = 3;

    private int assessment = SAMPLE_SCORE;
    private boolean assessOnce = false;

    private MultitaskEnvironment environment;
    private int numberOfFeatures = 4;
    private int numberOfIrrelevantFeatures = 0;
    private int numberOfDeficientSensors = 0;
    private int numberOfAgents = 20;
    private int numberOfIterations = 80000;
    private int numberOfClasses = 4;
    private int numberOfTasks = 4;

    private double ratio = 0.1;
    private double sampleRatio = 0.6;
    private double specializedGamesRatio = 1.0;
    private double specializedAgentsRatio = 1.0;
    private boolean splitTrain = false;
    private String runDir = null;
    private String loadRunDir = null;
    private int targetClass = -1;

    private HashSet<Task> trainedTaskSet = null;
    private String trainedTasks = null;

    private HashSet<Task> playedTaskSet = null;
    private String playedTasks = null;

    private HashSet<Task> recordedTaskSet = null;
    private String recordedTasks = null;

    private String recordMode = "accuracySuccessRate";

    private HashMap<Integer, MultitaskingAgent> aliveAgents = new HashMap<>();
    private HashMap<Integer, MultitaskingAgent> deadAgents = new HashMap<>();

    private HashMap<MultitaskingAgent, ArrayList<TrainingExample>> agentEnvironmentExamples = new HashMap<MultitaskingAgent, ArrayList<TrainingExample>>();

    private Iterator<String> gameIt;

    private RecordingManager rManager = new RecordingManager();

    private ActionLogger alogger;

    private Properties params;

    private HashMap<Task, Integer> gamesPlayed = new HashMap<Task, Integer>();

    private HashMap<Task, Integer> examplesTrained = new HashMap<Task, Integer>();

    private int maxAdaptationRank = numberOfTasks-1;

    private String winCondition = "task";

    private boolean onlyAcquaintances = false;
    private double edgePercentage = 0.5;
    private int numberOfUncertainBits = 0;
    private boolean deficiencyBased = false;
    private double knowledgeLimit = 1.0;
    private ArrayList<HashMap<Task, Integer>> preferencesPermutations;
    private int generationInterval = numberOfIterations;
    private int currentGeneration = 1;
    private int maxAgentId = 0;
    private int numberOfBirths = numberOfAgents/2;
    private int numberOfDeaths = numberOfBirths;

    protected void loadParams(Properties p) {
        params = p;
        if(p.containsKey("numberOfTasks"))
            numberOfTasks = Integer.parseInt(p.getProperty("numberOfTasks"));
        if (p.containsKey("numberOfFeatures"))
            numberOfFeatures = Integer.parseInt(p.getProperty("numberOfFeatures"));
        if(p.containsKey("numberOfIrrelevantFeatures"))
            numberOfIrrelevantFeatures = Integer.parseInt(p.getProperty("numberOfIrrelevantFeatures")) > numberOfFeatures ? numberOfFeatures : Integer.parseInt(p.getProperty("numberOfIrrelevantFeatures"));
        if(p.containsKey("numberOfDeficientSensors"))
            numberOfDeficientSensors = Integer.parseInt(p.getProperty("numberOfDeficientSensors")) > numberOfFeatures ? numberOfFeatures : Integer.parseInt(p.getProperty("numberOfDeficientSensors"));
        if (p.containsKey("numberOfAgents"))
            numberOfAgents = Integer.parseInt(p.getProperty("numberOfAgents"));
        if (p.containsKey("numberOfIterations"))
            numberOfIterations = Integer.parseInt(p.getProperty("numberOfIterations"));
        if (p.containsKey("nbIterations"))
            numberOfIterations = Integer.parseInt(p.getProperty("nbIterations"));
        if (p.containsKey("numberOfClasses"))
            numberOfClasses = Integer.parseInt(p.getProperty("numberOfClasses"));
        if (p.containsKey("assessment"))
            assessment = Integer.parseInt(p.getProperty("assessment"));
        if (p.containsKey("assessOnce"))
            assessOnce = Boolean.parseBoolean(p.getProperty("assessOnce"));
        if (p.containsKey("ratio"))
            ratio = Double.parseDouble(p.getProperty("ratio"));
        if (p.containsKey("sampleRatio"))
            sampleRatio = Double.parseDouble(p.getProperty("sampleRatio"));
        if (p.containsKey("specializedGamesRatio"))
            specializedGamesRatio = Double.parseDouble(p.getProperty("specializedGamesRatio"));
        if (p.containsKey("specializedAgentsRatio"))
            specializedAgentsRatio = Double.parseDouble(p.getProperty("specializedAgentsRatio"));
        if (p.containsKey("runDir"))
            runDir = p.getProperty("runDir");
        if (p.containsKey("loadRunDir"))
            loadRunDir = p.getProperty("loadRunDir");
        if (p.containsKey("splitTrain"))
            splitTrain = true;
        if (p.containsKey("targetClass"))
            targetClass = Integer.parseInt(p.getProperty("targetClass"));
        if (!p.containsKey("loadEnvDir") && p.containsKey("loadRunDir"))
            p.setProperty("loadEnvDir", loadRunDir);
        if (p.containsKey("trainedTasks"))
            trainedTasks = p.getProperty("trainedTasks");
        if (p.containsKey("playedTasks"))
            playedTasks = p.getProperty("playedTasks");
        if (p.containsKey("recordedTasks"))
            recordedTasks = p.getProperty("recordedTasks");
        if (p.containsKey("recordMode"))
            recordMode = p.getProperty("recordMode");
        if (p.containsKey("onlyAcquaintances"))
            onlyAcquaintances = Boolean.parseBoolean(p.getProperty("onlyAcquaintances"));
        if (p.containsKey("edgePercentage"))
            edgePercentage = Double.parseDouble(p.getProperty("edgePercentage"));
        if(p.containsKey("maxAdaptationRank"))
            maxAdaptationRank = Integer.parseInt(p.getProperty("maxAdaptationRank"));
        if(p.containsKey("winCondition"))
            winCondition = p.getProperty("winCondition");
        if(p.containsKey("numberOfUncertainBits"))
            numberOfUncertainBits = Integer.parseInt(p.getProperty("numberOfUncertainBits"));
        if(p.containsKey("deficiencyBased"))
            deficiencyBased = Boolean.parseBoolean(p.getProperty("deficiencyBased"));
        if(p.containsKey("knowledgeLimit"))
            knowledgeLimit = Double.parseDouble(p.getProperty("knowledgeLimit"));
        if(p.containsKey("generationInterval"))
            generationInterval = Integer.parseInt(p.getProperty("generationInterval"));
        if(p.containsKey("numberOfBirths"))
            numberOfBirths = Integer.parseInt(p.getProperty("numberOfBirths"));
        if(p.containsKey("numberOfDeaths"))
            numberOfDeaths = Integer.parseInt(p.getProperty("numberOfDeaths"));
    }

    private void initRecordedTasksList(String taskNames) {
        recordedTaskSet = new HashSet<Task>();

        String[] tokens = taskNames.split(":");

        for (int i = 0; i < tokens.length; i++) {
            if (this.getEnvironment().getEnvironmentTasks().containsKey(tokens[i])) {
                recordedTaskSet.add(this.getEnvironment().getEnvironmentTasks().get(tokens[i]));
            }
        }
    }

    private void initTrainedTasksList(String taskNames) {
        trainedTaskSet = new HashSet<Task>();

        String[] tokens = taskNames.split(":");

        for (int i = 0; i < tokens.length; i++) {
            if (this.getEnvironment().getEnvironmentTasks().containsKey(tokens[i])) {
                trainedTaskSet.add(this.getEnvironment().getEnvironmentTasks().get(tokens[i]));
            }
        }
    }

    private void initPlayedTaskList(String taskNames) {
        playedTaskSet = new HashSet<Task>();

        String[] tokens = taskNames.split(":");

        for (int i = 0; i < tokens.length; i++) {
            if (this.getEnvironment().getEnvironmentTasks().containsKey(tokens[i])) {
                playedTaskSet.add(this.getEnvironment().getEnvironmentTasks().get(tokens[i]));
            }
        }
    }

    @Override
    public ActionLogger init(Properties param) throws LLException {
        loadParams(param);

        if (runDir != null) {
            new File(runDir).mkdirs();
        }

        environment = new MultitaskEnvironment(numberOfFeatures, numberOfIrrelevantFeatures, numberOfTasks, numberOfClasses);
        environment.init(param);

        if (trainedTasks != null) {
            initTrainedTasksList(trainedTasks);
        }

        if (playedTasks != null) {
            initPlayedTaskList(playedTasks);
        }

        if (recordedTasks != null) {
            initRecordedTasksList(recordedTasks);
        }

        initPreferencesPermutations();

        Random generator = new Random();

        for (int i = 0; i < numberOfAgents; i++) {
            aliveAgents.put(i, new MultitaskingAgent(i, environment, this, param));
        }

        maxAgentId = numberOfAgents-1;

        loadAgentParamsFromFile();
        loadTrainingSets();
        trainAgents();

        if (onlyAcquaintances) {
            GraphUtils.initGraph(this.getAliveAgents(), edgePercentage);
        }

        if (runDir != null) {
            saveAgentParamsToFile();
            saveTrainingSets();
        }

        if (loadRunDir == null) {
            gameIt = new MultitaskGameIterator(this, onlyAcquaintances, numberOfUncertainBits, numberOfFeatures);
//            gameIt = MultitaskGameIterator.getInstance(this);
        } else {
            try {
                //System.out.println("Trying to create iterator from games.tsv");
                gameIt = Files.lines(Paths.get(loadRunDir, "games.tsv")).iterator();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }


        if (recordMode.equalsIgnoreCase("accuracyPerTask")) {

            MultitaskRecorders.AverageAccuracy[] recorders = new MultitaskRecorders.AverageAccuracy[this.getEnvironment().getEnvironmentTasks().size()];

            int i = 0;

            for (Map.Entry<String, Task> entrySet : environment.getEnvironmentTasks().entrySet()) {
                ArrayList<Task> taskList = new ArrayList<>();
                taskList.add(entrySet.getValue());

                recorders[i] = new MultitaskRecorders.AverageAccuracy(new ArrayList<>(this.getAliveAgents().values()), taskList, new ArrayList<>(environment.getEnvironmentExamples().values()), "Accuracy_for_" + entrySet.getKey().replaceAll(" ", "_"));

                i++;
            }

            alogger = rManager.init(recorders);

        } else if (recordMode.equalsIgnoreCase("accuracyPerTrainedTask")) {

            MultitaskRecorders.AverageAccuracy[] recorders = new MultitaskRecorders.AverageAccuracy[trainedTaskSet.size()];

            int i = 0;

            for (Task trainedTask : trainedTaskSet) {
                ArrayList<Task> taskList = new ArrayList<>();
                taskList.add(trainedTask);

                recorders[i] = new MultitaskRecorders.AverageAccuracy(new ArrayList<>(this.getAliveAgents().values()), taskList, new ArrayList<>(environment.getEnvironmentExamples().values()), "Accuracy_for_" + trainedTask.getName().replace(" ", "_"));

                i++;
            }

            alogger = rManager.init(recorders);

        } else if (recordMode.equalsIgnoreCase("successRatePerTask")) {


            MultitaskRecorders.SuccessRate[] recorders = new MultitaskRecorders.SuccessRate[this.getEnvironment().getEnvironmentTasks().size()];

            int i = 0;

            for (Map.Entry<String, Task> entrySet : environment.getEnvironmentTasks().entrySet()) {

                HashSet<Task> taskSet = new HashSet<>();
                taskSet.add(entrySet.getValue());

                recorders[i] = new MultitaskRecorders.SuccessRate("Srate_for " + entrySet.getKey().replace(" ", "\t"), taskSet);
                i++;
            }

            alogger = rManager.init(recorders);


        } else if (recordMode.equalsIgnoreCase("successRatePerTrainedTask")) {


            MultitaskRecorders.SuccessRate[] recorders = new MultitaskRecorders.SuccessRate[trainedTaskSet.size()];

            int i = 0;

            for (Task trainedTask : trainedTaskSet) {
                ArrayList<Task> taskList = new ArrayList<>();
                taskList.add(trainedTask);
                recorders[i] = new MultitaskRecorders.SuccessRate("Srate_for_" + trainedTask.getName().replace(" ", "\t"), trainedTaskSet);
                i++;
            }

            alogger = rManager.init(recorders);


        }else if(recordMode.equalsIgnoreCase("accuracySuccessRate")){
            alogger = rManager.init(new MultitaskRecorders.SuccessRate("Success_rate_t="+numberOfTasks, new HashSet<Task>(environment.getEnvironmentTasks().values())), new MultitaskRecorders.AverageAccuracy(new ArrayList<>(this.getAliveAgents().values()), new ArrayList<>(environment.getEnvironmentTasks().values()), new ArrayList<>(environment.getEnvironmentExamples().values()),"Avg_Accuracy_t="+numberOfTasks));
        }else if(recordMode.equalsIgnoreCase("accuracySuccessRateCollisionRateSuccessRateWithUncertainBits")){
            alogger = rManager.init(new MultitaskRecorders.SuccessRate("Success_rate_t="+numberOfTasks+"_ub="+numberOfUncertainBits, new HashSet<Task>(environment.getEnvironmentTasks().values())), new MultitaskRecorders.AverageAccuracy(new ArrayList<>(this.getAliveAgents().values()), new ArrayList<>(environment.getEnvironmentTasks().values()), new ArrayList<>(environment.getEnvironmentExamples().values()),"Avg_Accuracy_t="+numberOfTasks+"_ub="+numberOfUncertainBits),new MultitaskRecorders.CollisionRate("Collision_rate_t="+numberOfTasks+"_ub="+numberOfUncertainBits), new MultitaskRecorders.SuccessRateWithUncertainBits("Success_rate_uncertain_bits_t="+numberOfTasks+"_ub="+numberOfUncertainBits, new HashSet<Task>(environment.getEnvironmentTasks().values())));
        }else if(recordMode.equalsIgnoreCase("accuracySuccessRateRelativeStandardDeviations")){
            alogger = rManager.init(new MultitaskRecorders.SuccessRate("Success_rate_t="+numberOfTasks, new HashSet<Task>(environment.getEnvironmentTasks().values())),
                    new MultitaskRecorders.AverageAccuracy(new ArrayList<>(this.getAliveAgents().values()), new ArrayList<>(environment.getEnvironmentTasks().values()), new ArrayList<>(environment.getEnvironmentExamples().values()),"Accuracy_t="+numberOfTasks),
                    new MultitaskRecorders.GamesStandardDeviation("Relative_standard_deviation(Attempts)_t="+numberOfTasks, this));
        }else if(recordMode.equalsIgnoreCase("minMaxAvgAccuracy")){
            alogger = rManager.init(new MultitaskRecorders.WorstTaskAccuracy(this,"Avg_minimum_accuracy_t="+numberOfTasks), new MultitaskRecorders.BestTaskAccuracy(new ArrayList<>(this.getAliveAgents().values()), new ArrayList<>(environment.getEnvironmentTasks().values()), new ArrayList<>(environment.getEnvironmentExamples().values()),"Avg_maximum_accuracy_t="+numberOfTasks), new MultitaskRecorders.AverageAccuracy(new ArrayList<>(this.getAliveAgents().values()), new ArrayList<>(environment.getEnvironmentTasks().values()), new ArrayList<>(environment.getEnvironmentExamples().values()),"Avg_accuracy_t="+numberOfTasks));
        }else if(recordMode.equalsIgnoreCase("allMeasures")) {

            ArrayList<Task> allTasks = new ArrayList<>(this.getEnvironment().getEnvironmentTasks().values());

            MultitaskRecorders.MultiDolaRecorder[] recorders = new MultitaskRecorders.MultiDolaRecorder[19+ allTasks.size()];

            recorders[0] = new MultitaskRecorders.CollisionRate("Collision_rate_t="+numberOfTasks+"_ub="+numberOfUncertainBits);
            recorders[1] = new MultitaskRecorders.SuccessRate("Success_rate_t="+numberOfTasks+"_ub="+numberOfUncertainBits, new HashSet<Task>(environment.getEnvironmentTasks().values()));
            recorders[2] = new MultitaskRecorders.SuccessRateWithUncertainBits("Success_rate_uncertain_bits_t="+numberOfTasks+"_ub="+numberOfUncertainBits, new HashSet<Task>(environment.getEnvironmentTasks().values()));
            recorders[3] = new MultitaskRecorders.WorstTaskAccuracy(this, "Avg_minimum_accuracy_t=" + numberOfTasks);
            recorders[4] = new MultitaskRecorders.AverageAccuracy(new ArrayList<>(this.getAliveAgents().values()), new ArrayList<>(environment.getEnvironmentTasks().values()), new ArrayList<>(environment.getEnvironmentExamples().values()), "Avg_accuracy_t=" + numberOfTasks);
            recorders[5] = new MultitaskRecorders.BestTaskAccuracy(new ArrayList<>(this.getAliveAgents().values()), new ArrayList<>(environment.getEnvironmentTasks().values()), new ArrayList<>(environment.getEnvironmentExamples().values()), "Avg_maximum_accuracy_t=" + numberOfTasks);
            recorders[6] = new MultitaskRecorders.GamesStandardDeviation("Games_standard_deviation_t="+numberOfTasks, this);
            recorders[7] = new MultitaskRecorders.Distance(new ArrayList<>(this.getAliveAgents().values()),"Distance_t=" + numberOfTasks);
            recorders[8] = new MultitaskRecorders.AverageLastAdaptation(new ArrayList<>(this.getAliveAgents().values()),"Avg_last_adaptation_t=" + numberOfTasks);
            recorders[9] = new MultitaskRecorders.RandomAdaptationRate("Random_adaptation_rate_t="+numberOfTasks+"_ub="+numberOfUncertainBits, new HashSet<Task>(environment.getEnvironmentTasks().values()));
            recorders[10] = new MultitaskRecorders.RandomAgreementRate("Random_agreement_rate_t="+numberOfTasks+"_ub="+numberOfUncertainBits, new HashSet<Task>(environment.getEnvironmentTasks().values()));
            recorders[11] = new MultitaskRecorders.AveragePreferredTasksAccuracy(new ArrayList<>(this.getAliveAgents().values()), new ArrayList<>(environment.getEnvironmentExamples().values()), "Avg_spec_accuracy_m=" + maxAdaptationRank);
            recorders[12] = new MultitaskRecorders.CorrectDecisionRate("Correct_decision_rate_t="+numberOfTasks+"_knowledge_limit="+knowledgeLimit, new HashSet<Task>(environment.getEnvironmentTasks().values()));
            recorders[13] = new MultitaskRecorders.DelegationRate("Delegation_rate_t="+numberOfTasks+"_knowledge_limit="+knowledgeLimit, new HashSet<Task>(environment.getEnvironmentTasks().values()));
            recorders[14] = new MultitaskRecorders.TotalCompensation(new ArrayList<>(this.getAliveAgents().values()),"Total_compensation=" + numberOfTasks);
            recorders[15] = new MultitaskRecorders.DecileRatio(allTasks, new ArrayList<>(this.getAliveAgents().values()),0.1,0.1,"Compensation_decile_ratio=" + numberOfTasks, new CompensationComparator());
            recorders[16] = new MultitaskRecorders.DecileRatio(allTasks, new ArrayList<>(this.getAliveAgents().values()),0.1,0.1,"Carried_tasks_accuracy_decile_ratio=" + numberOfTasks, new CarriedTasksAccuracyComparator(new ArrayList<>(environment.getEnvironmentExamples().values())));
            recorders[17] = new MultitaskRecorders.DecileRatio(allTasks, new ArrayList<>(this.getAliveAgents().values()),0.1,0.1,"MaxAccuracy_decile_ratio=" + numberOfTasks, new MaximumAccuracyComparator(allTasks));
            recorders[18] = new MultitaskRecorders.AccuracyStandardDeviation(this,new ArrayList<>(environment.getEnvironmentTasks().values()),new ArrayList<>(environment.getEnvironmentExamples().values()), "Accuracy_standard_deviation_t=" + numberOfTasks);

            int i = 19;

            for (Map.Entry<String, Task> entrySet : environment.getEnvironmentTasks().entrySet()) {
                ArrayList<Task> taskList = new ArrayList<>();
                taskList.add(entrySet.getValue());

                recorders[i] = new MultitaskRecorders.AverageAccuracy(new ArrayList<>(this.getAliveAgents().values()), taskList, new ArrayList<>(environment.getEnvironmentExamples().values()),"Accuracy_for_"+entrySet.getKey().replaceAll(" ", "_")+"="+maxAdaptationRank);

                i++;
            }

            alogger = rManager.init(recorders);
        }
        return alogger;
    }

    @Override
    public void process() throws LLException {

        PrintWriter prGames = null;
        if (runDir != null) {
            try {
                prGames = new PrintWriter(new File(runDir, "games.tsv"));
            } catch (FileNotFoundException fnfex) {
                System.out.println(fnfex.getMessage());
            }
        }

        int remaining = numberOfIterations;
        scoreAgents(new ArrayList<MultitaskingAgent>(aliveAgents.values()));

        int count = 0;
        while (gameIt.hasNext()) {
            count++;
            BitSet instance;
            String g = gameIt.next();
            String[] spec = g.split("\t");
            MultitaskingAgent firstAgent = aliveAgents.get(Integer.parseInt(spec[0]));
            MultitaskingAgent secondAgent = aliveAgents.get(Integer.parseInt(spec[1]));
            instance = environment.parseInstance(spec[2]);
            Task gameTask = environment.getEnvironmentTasks().get(spec[3]);
            String sUncertainBits = spec[4];
            HashSet<Integer> uncertainBits = new HashSet<>();
            String[] ubTokens = sUncertainBits.split(":");
            for (int i = 0; i < ubTokens.length; i++) {
                uncertainBits.add(Integer.parseInt(ubTokens[i]));
            }


            if (playedTasks != null) {
                if (!playedTaskSet.contains(gameTask)) {
                    continue;
                }
            }


            if (!assessOnce) {
                scoreAgents(new ArrayList<>(aliveAgents.values()));
            }

            MultitaskGame game = new MultitaskGame(count, firstAgent, secondAgent, instance, this, gameTask, uncertainBits);

            firstAgent.playGame(remaining--, game);

            if (gamesPlayed.containsKey(game.getTask())) {
                gamesPlayed.put(game.getTask(), gamesPlayed.get(game.getTask()) + 1);
            } else {
                gamesPlayed.put(game.getTask(), 1);
            }

            updateAgentSuccess(game);

            rManager.update(game);

            alogger.logGame(firstAgent.getId(), secondAgent.getId(), game.getInstance() + "");
            alogger.logResult(game.isSuccess() ? "SUCCESS" : "FAILURE", game.isSuccess(), game.getAdaptation() + "");

            rManager.log(game);
            if (prGames != null) {
                game.save(prGames);
            }
        }

        if (prGames != null) {
            prGames.close();
        }
    }


    /////////////////////// Saving experiment ///////////////////////////

    protected boolean saveAgentParamsToFile() {

        String runDir = params.getProperty("runDir");
        try {
            PrintWriter writer = new PrintWriter(new File(runDir, "agents.tsv"));
            if (writer != null) {

                StringBuilder lineBuilder = new StringBuilder();

                for (Map.Entry<Integer, MultitaskingAgent> currAgent : aliveAgents.entrySet()) {

                    lineBuilder.setLength(0);

                    lineBuilder.append(currAgent.getKey()).append("\t").append(currAgent.getValue().isSpecialized()).append("\t").append(currAgent.getValue().getTaskSelectionStrategy()).append("\t").append(currAgent.getValue().getUnknownDecisionStrategy()).append("\t").append(currAgent.getValue().getWinningCondition());

                    writer.println(lineBuilder.toString().trim());

                }

                writer.flush();
                writer.close();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return false;
        }
        return true;

    }

    private void loadAgentParamsFromFile() {
        String loadEnvDir = params.getProperty("loadEnvDir");

        File tempFile = new File(loadEnvDir + File.separator + "agents.tsv");

        System.out.println("TempFilePath : " + tempFile.getAbsolutePath());

        if (params.containsKey("loadEnvDir") && tempFile.exists()) {

            try {
                Iterator<String> inputLines = Files.lines(Paths.get(loadEnvDir, "agents.tsv")).iterator();

                if (inputLines != null) {

                    while (inputLines.hasNext()) {

                        String[] line = inputLines.next().split("\t");

                        String agentId = String.valueOf(line[0]);

                        if (aliveAgents.containsKey(agentId)) {

                            MultitaskingAgent currAgent = aliveAgents.get(agentId);

                            currAgent.setSpecialized(Boolean.parseBoolean(line[1]));

                            String sTaskSelectionStrategy = line[2];
                            switch (sTaskSelectionStrategy) {
                                case "MAXIMAX":
                                    currAgent.setTaskSelectionStrategy(MultitaskingAgent.TaskSelectionStrategy.MAXIMAX);
                                    break;
                                case "MAXIMIN":
                                    currAgent.setTaskSelectionStrategy(MultitaskingAgent.TaskSelectionStrategy.MAXIMIN);
                                    break;
                                case "MINIMAX_REGRET":
                                    currAgent.setTaskSelectionStrategy(MultitaskingAgent.TaskSelectionStrategy.MINIMAX_REGRET);
                                    break;
                                default:
                                    currAgent.setTaskSelectionStrategy(MultitaskingAgent.TaskSelectionStrategy.MINIMAX_REGRET);
                                    break;
                            }

                            String sUnknownDecisionStrategy = line[3];
                            switch (sUnknownDecisionStrategy) {
                                case "FIRST":
                                    currAgent.setUnknownDecisionStrategy(MultitaskingAgent.UnknownDecisionStrategy.FIRST);
                                    break;
                                case "RANDOM":
                                    currAgent.setUnknownDecisionStrategy(MultitaskingAgent.UnknownDecisionStrategy.RANDOM);
                                    break;
                                case "NONE":
                                    currAgent.setUnknownDecisionStrategy(MultitaskingAgent.UnknownDecisionStrategy.NONE);
                                    break;
                                default:
                                    currAgent.setUnknownDecisionStrategy(MultitaskingAgent.UnknownDecisionStrategy.RANDOM);
                                    break;
                            }

                            String sWinningCondition = line[4];
                            switch (sWinningCondition) {
                                case "GLOBAL":
                                    currAgent.setWinningCondition(MultitaskingAgent.WinningCondition.GLOBAL);
                                    break;
                                case "TASK":
                                    currAgent.setWinningCondition(MultitaskingAgent.WinningCondition.TASK);
                                    break;
                                default:
                                    currAgent.setWinningCondition(MultitaskingAgent.WinningCondition.TASK);
                                    break;
                            }

                            String[] deficientSensorTokens = line[5].split(":");
                            for (int i = 0; i < deficientSensorTokens.length; i++) {
                                currAgent.addDeficientFeature(Integer.parseInt(deficientSensorTokens[i]));
                            }

                        }
                    }
                }

            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }

    protected void saveTrainingSets() {

        String saveDir = runDir;

        if (saveDir.charAt(saveDir.length() - 1) == '/') {
            saveDir += "training";
        } else {
            saveDir += "/training";
        }

        new File(saveDir).mkdirs();

        for (MultitaskingAgent currentAgent : aliveAgents.values()) {

            String filename = String.valueOf(currentAgent.getId()) + ".tsv";

            try {

                PrintWriter printWriter = new PrintWriter(new File(saveDir, filename));

                ArrayList<TrainingExample> currentTrainingSet = agentEnvironmentExamples.get(currentAgent);

                for (int j = 0; j < currentTrainingSet.size(); j++) {
                    printWriter.println(currentTrainingSet.get(j).getInstance() + "\t" + currentTrainingSet.get(j).getTask().getName() + "\t" + String.valueOf(currentTrainingSet.get(j).getCorrectDecision()));
                }

                printWriter.close();

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
    }


    private void loadTrainingSets() {

        String loadDir = loadRunDir;
        if (loadRunDir != null) {
            if (loadDir.charAt(loadDir.length() - 1) == '/') {
                loadDir += "training";
            } else {
                loadDir += "/training";
            }
        }

        int index = 0;

        ArrayList<EnvironmentExample> environmentExamples = new ArrayList<EnvironmentExample>(environment.getEnvironmentExamples().values());

        ArrayList<TrainingExample> totalTrainingExamples = ExampleUtils.environmentExamplesToTrainingExamples(environmentExamples, numberOfFeatures);

        for (Map.Entry<Integer, MultitaskingAgent> agentEntry : aliveAgents.entrySet()) {

            Collections.shuffle(totalTrainingExamples);

            ArrayList<TrainingExample> agentExamples = new ArrayList<TrainingExample>();

            if (loadRunDir == null) {

                //System.out.println("Total training examples are :"+totalTrainingExamples.size());

                if (!splitTrain) {

                    Random rand = new Random();

                    for (int j = 0; j < totalTrainingExamples.size(); j++) {
                        if (rand.nextDouble() <= ratio && totalTrainingExamples.get(j) != null) {
                            agentExamples.add(totalTrainingExamples.get(j));
                        }
                    }

                    //TODO VERIFY THAT THIS IS NEEDED.
                    if (agentExamples.size() == 0) {
                        int randomIndex = rand.nextInt(totalTrainingExamples.size());
                        agentExamples.add(totalTrainingExamples.get(randomIndex));
                    }

                } else { //NO OVERLAP AMONG THE AGENTS' TRAINING SETS

                    System.out.println("Split training!");

                    int sizePerAgent = totalTrainingExamples.size() / numberOfAgents;

                    System.out.println("Size per agent should be :" + sizePerAgent);

                    for (int j = 0; j < sizePerAgent; j++) {
                        agentExamples.add(totalTrainingExamples.get(j + index));
                    }

                    index += sizePerAgent;

                    if (totalTrainingExamples.size() - index < sizePerAgent) {// the rest are added to the last agents
                        for (int j = index; j < totalTrainingExamples.size(); j++) {
                            agentExamples.add(totalTrainingExamples.get(j));
                        }
                    }

                }
            } else { //RETRIEVE THE FILES CONTAINING THE TRAINING SETS
                //TODO SHOULD THIS BE FILE BY FILE PER AGENT?

                System.out.println("Reading training set files!");

                String filename = agentEntry.getKey() + ".tsv";

                try {

                    Stream<String> stream = Files.lines(Paths.get(loadDir, filename));
                    Iterator<String> lines = stream.iterator();

                    while (lines.hasNext()) {

                        String currentLine = lines.next();

                        String[] splits = currentLine.split("\t");

                        BitSet instance = environment.parseInstance(splits[0]);

                        Task task = environment.getEnvironmentTasks().get(splits[1]);

                        if (trainedTaskSet != null && !trainedTaskSet.isEmpty()) {
                            if (!trainedTaskSet.contains(task)) {
                                continue;
                            }
                        }

                        //TODO, WHAT IF THE LABEL IS INCORRECT IN FILE AND IS HIGHER THAN THE NUMBER_OF_DECISIONS?
                        int correctDecision = Integer.parseInt(splits[2]);

                        agentExamples.add(new TrainingExample(instance, task, correctDecision));

                    }
                    stream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }

            agentEnvironmentExamples.put(agentEntry.getValue(), agentExamples);

        }
    }

    /**
     * Train agent classifiers
     */

    protected void trainAgents() {
        for (Map.Entry<Integer, MultitaskingAgent> agentEntry : aliveAgents.entrySet()) {

            if (agentEnvironmentExamples.get(agentEntry.getValue()).size() == 0) {
                ArrayList<TrainingExample> runnerUpExamples = ExampleUtils.environmentExamplesToTrainingExamples(new ArrayList<EnvironmentExample>(environment.getEnvironmentExamples().values()), numberOfFeatures);

                if (trainedTaskSet != null && !trainedTaskSet.isEmpty()) {
                    for (int i = 0; i < runnerUpExamples.size(); i++) {
                        if (trainedTaskSet.contains(runnerUpExamples.get(i).getTask())) {
                            ArrayList<TrainingExample> runnerUpList = new ArrayList<>();
                            runnerUpList.add(runnerUpExamples.get(i));
                            agentEnvironmentExamples.put(agentEntry.getValue(), runnerUpList);
                            break;
                        }
                    }
                } else {
                    ArrayList<TrainingExample> randomExampleList = new ArrayList<>();
                    randomExampleList.add(runnerUpExamples.get(0));
                    agentEnvironmentExamples.put(agentEntry.getValue(), randomExampleList);
                }
            }

            List<TrainingExample> set = agentEnvironmentExamples.get(agentEntry.getValue());
            //System.out.println("Agent to be trained, id : " +agentEntry.getValue().getId() +" with : " +set.size()+ " examples");

            try {
                agentEntry.getValue().getClassifier().train(agentEnvironmentExamples.get(agentEntry.getValue()));

                for (int i = 0; i < agentEnvironmentExamples.get(agentEntry.getValue()).size(); i++) {
                    Task task = agentEnvironmentExamples.get(agentEntry.getValue()).get(i).getTask();
                    if (examplesTrained.containsKey(task)) {
                        examplesTrained.put(task, examplesTrained.get(task) + 1);
                    } else {
                        examplesTrained.put(task, 1);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    /**
     * Generate samples for tasks (save them in disk if loadRunDir parameter is set to rerun the exact same experiment)
     *
     * @return
     */


    /////////////////// Agent Payoff ////////////////////////
    protected void updateAgentSuccess(MultitaskGame game) {
        try {

            updateAgentSuccess((MultitaskingAgent) game.getFirstAgent(), game.isSuccess(), true, game.getTask());
            updateAgentSuccess((MultitaskingAgent) game.getSecondAgent(), game.isSuccess(), true, game.getTask());

            //CATCH SHOULD LATER BE REPLACED WITH NoClassException

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void updateAgentSuccess(MultitaskingAgent agent, boolean isGameSuccessful, boolean correct, Task task) {
        agent.setTotalSuccesses(agent.getTotalSuccesses() + ((!isGameSuccessful) ? 0 : (correct || assessment != SUCCESS_SCORE_M) ? 1 : 0.5));
        agent.setNumberOfAttempts(agent.getNumberOfAttempts() + 1);
        agent.setTaskSuccessfulAttempts(task, (agent.getTaskSuccessfulAttempts(task) + (isGameSuccessful ? 1 : 0)));
        agent.setTaskTotalGames(task, agent.getTaskTotalGames(task) + 1);
        //System.out.println("Current specialized success rate : " + (agent.getTaskSuccessfulAttempts(task)/agent.getTaskTotalAttempts(task)));
    }

    protected void scoreAgents(ArrayList<MultitaskingAgent> agents) {
        if (assessment == SAMPLE_SCORE) {
            HashMap<BitSet, EnvironmentExample> sampleFromEnvironment = environment.getSamples(sampleRatio);
            for (MultitaskingAgent agent : agents) {
                agent.setScore(agent.getAccuracy(new ArrayList<>(environment.getEnvironmentTasks().values()), new ArrayList<>(sampleFromEnvironment.values())));
            }
        } else if (assessment == SUCCESS_SCORE || assessment == SUCCESS_SCORE_M) {
            for (MultitaskingAgent agent : agents) {
                if (agent.getNumberOfAttempts() > 0) {
                    agent.setScore( agent.getTotalSuccesses() / (double) agent.getNumberOfAttempts());
                } else {
                    agent.setScore(0);
                }
            }
        }
    }

    public MultitaskEnvironment getEnvironment() {
        return environment;
    }

    public void setEnvironment(MultitaskEnvironment environment) {
        this.environment = environment;
    }

    public int getNumberOfFeatures() {
        return numberOfFeatures;
    }

    public void setNumberOfFeatures(int numberOfFeatures) {
        this.numberOfFeatures = numberOfFeatures;
    }

    @Override
    public MultitaskGameIterator getGameIterator() {
        return (MultitaskGameIterator) gameIt;
    }

    @Override
    public int getCurrentGeneration() {
        return currentGeneration;
    }

    @Override
    public RecordingManager getRecordingManager() {
        return rManager;
    }

    @Override
    public HashMap<Task, Integer> getGamesPlayed() {
        return gamesPlayed;
    }

    @Override
    public double getSampleRatio() {
        return sampleRatio;
    }

    public int getNumberOfAgents() {
        return numberOfAgents;
    }

    @Override
    public Properties getParams() {
        return params;
    }

    @Override
    public int getMaxAgentId() {
        return maxAgentId;
    }

    @Override
    public void setMaxAgentId(int maxAgentId) {
        this.maxAgentId = maxAgentId;
    }

    @Override
    public double getRatio() {
        return ratio;
    }

    @Override
    public int getGenerationInterval() {
        return generationInterval;
    }

    @Override
    public void setGenerationInterval(int generationInterval) {
        this.generationInterval = generationInterval;
    }

    @Override
    public int getNumberOfBirths() {
        return numberOfBirths;
    }

    @Override
    public int getNumberOfDeaths() {
        return numberOfDeaths;
    }

    public void setNumberOfAgents(int numberOfAgents) {
        this.numberOfAgents = numberOfAgents;
    }

    public int getNumberOfIterations() {
        return numberOfIterations;
    }

    public HashMap<Integer, MultitaskingAgent> getAliveAgents() {
        return aliveAgents;
    }

    @Override
    public HashMap<Integer, MultitaskingAgent> getDeadAgents() {
        return deadAgents;
    }


    /////// TEST DEBUG ONE EXPERIMENT ///////////

    public static void main(String[] args) throws LLException {
        SelectiveAdaptationSpecializationExperiment exp = new SelectiveAdaptationSpecializationExperiment();
        Properties p = new Properties();
        //p.setProperty("recordMode", "maxAccuracyAverageAccuracySuccessRate");
        //p.setProperty("numberOfTasks", "4");
        //p.setProperty("adaptationParam", "task");
        //p.setProperty("maxAdaptationRank", "0");
        //p.setProperty("winCondition", "task");
        //p.setProperty("op","oneCom");
        exp.init(p);

        ArrayList<MultitaskingAgent> agents = new ArrayList<MultitaskingAgent>(exp.getAliveAgents().values());

        double avg = 0;
        for (MultitaskingAgent a : agents) {
            double perf = a.getAccuracy(new ArrayList<Task>(exp.getEnvironment().getEnvironmentTasks().values()), new ArrayList<EnvironmentExample>(exp.getEnvironment().getEnvironmentExamples().values()));
            avg += perf;
            System.out.println("accuracy " + a.getId() + ": " + perf);
        }
        avg /= agents.size();
        System.out.println("average accuracy: " + avg);

        for (MultitaskingAgent a : agents) {
            //System.out.println(a.getClassifier());
        }
        System.out.println("============== START ==============");

        exp.process();

        agents = new ArrayList<MultitaskingAgent>(exp.getAliveAgents().values());

        avg = 0;
        for (MultitaskingAgent agent : agents) {
            double perf = agent.getAccuracy(new ArrayList<Task>(exp.getEnvironment().getEnvironmentTasks().values()), new ArrayList<EnvironmentExample>(exp.getEnvironment().getEnvironmentExamples().values()));
            avg += perf;
            System.out.println(agent.toString(new ArrayList<Task>(exp.getEnvironment().getEnvironmentTasks().values()), new ArrayList<EnvironmentExample>(exp.getEnvironment().getEnvironmentExamples().values())));
        }
        avg /= agents.size();

        System.out.println("average accuracy: " + avg);

        for (MultitaskingAgent a : agents) {
            //System.out.println(a.getClassifier());
        }
        System.out.println("==============  END  ==============");

        try {
            PrintWriter pw = new PrintWriter(exp.recordMode + "_results_" + "a=" + exp.numberOfAgents + "_" + "t=" + exp.numberOfTasks + "_" + "f=" + exp.numberOfFeatures + "_" + "c=" + exp.numberOfClasses + "_" + "i=" + exp.numberOfIterations + ".csv");
            exp.alogger.fullReport(pw);
            pw.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        exp.printGamesPlayed();

        exp.printExamplesTrained();

    }

    private void printGamesPlayed() {
        for (Map.Entry<Task, Integer> entry : gamesPlayed.entrySet()) {
            Task task = entry.getKey();
            Integer counter = entry.getValue();
            System.out.println("Task : " + task.getName() + " Games : " + counter);
        }
        System.out.println("============ GAMES COUNTERS ============");
    }

    private void printExamplesTrained() {
        for (Map.Entry<Task, Integer> entry : examplesTrained.entrySet()) {
            Task task = entry.getKey();
            Integer counter = entry.getValue();
            System.out.println("Task : " + task.getName() + " Games : " + counter);
        }
        System.out.println("============ EXAMPLES COUNTERS ============");
    }


    @Override
    public int getPickedTasks() {
        return 1;
    }

    @Override
    public double getSpecializedGamesRatio() {
        return specializedGamesRatio;
    }

    @Override
    public double getSpecializedAgentsRatio() {
        return specializedAgentsRatio;
    }

    private void initPreferencesPermutations() {
        ArrayList<Task> tasks = new ArrayList<>(this.getEnvironment().getEnvironmentTasks().values());
        preferencesPermutations = PermutationUtils.getAllPreferences(tasks, numberOfAgents);
        System.out.println("Preferences Permutations size: "+preferencesPermutations.size());
    }
    @Override
    public ArrayList<HashMap<Task, Integer>> getPreferencesPermutations() {
        return preferencesPermutations;
    }

    @Override
    public int getMaxAdaptingRank() {
        return maxAdaptationRank;
    }
}
