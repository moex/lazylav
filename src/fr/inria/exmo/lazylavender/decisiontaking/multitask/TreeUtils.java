package fr.inria.exmo.lazylavender.decisiontaking.multitask;

import org.semanticweb.owlapi.util.EscapeUtils;
import uk.ac.manchester.cs.bhig.util.Tree;

import java.util.*;

public class TreeUtils {

    public static ArrayList<MultitaskTreeClassifier.MultiTaskNode> getParentsUnsorted(MultitaskTreeClassifier.MultiTaskNode root) {

        ArrayList<MultitaskTreeClassifier.MultiTaskNode> parents = new ArrayList<>();

        if (root == null) {
            return parents;
        }

        // create an empty stack and push the root node
        Stack<MultitaskTreeClassifier.MultiTaskNode> stack = new Stack<>();
        stack.push(root);

        // create another stack to store postorder traversal
        Stack<MultitaskTreeClassifier.MultiTaskNode> out = new Stack<>();

        // loop till stack is empty
        while (!stack.empty())
        {
            // pop a node from the stack and push the data into the output stack
            MultitaskTreeClassifier.MultiTaskNode curr = stack.pop();
            out.push(curr);

            // push the left and right child of the popped node into the stack
            if(!curr.isLeaf()) {
                if (curr.getChildren().get(0) != null) {
                    stack.push(curr.getChildren().get(0));
                }

                if (curr.getChildren().get(1) != null) {
                    stack.push(curr.getChildren().get(1));
                }
            }
        }

        // print postorder traversal
        while (!out.empty()) {
            MultitaskTreeClassifier.MultiTaskNode temp = out.pop();
            if(!temp.isLeaf()){
                parents.add(temp);
            }
        }

        return parents;
    }

    public static ArrayList<MultitaskTreeClassifier.MultiTaskNode> getLeafs(MultitaskTreeClassifier.MultiTaskNode root) {

        ArrayList<MultitaskTreeClassifier.MultiTaskNode> leafs = new ArrayList<>();

        if (root == null) {
            return leafs;
        }

        // create an empty stack and push the root node
        Stack<MultitaskTreeClassifier.MultiTaskNode> stack = new Stack<>();
        stack.push(root);

        // create another stack to store postorder traversal
        Stack<MultitaskTreeClassifier.MultiTaskNode> out = new Stack<>();

        // loop till stack is empty
        while (!stack.empty())
        {
            // pop a node from the stack and push the data into the output stack
            MultitaskTreeClassifier.MultiTaskNode curr = stack.pop();
            out.push(curr);

            // push the left and right child of the popped node into the stack
            if(!curr.isLeaf()) {
                if (curr.getChildren().get(0) != null) {
                    stack.push(curr.getChildren().get(0));
                }

                if (curr.getChildren().get(1) != null) {
                    stack.push(curr.getChildren().get(1));
                }
            }
        }

        // print postorder traversal
        while (!out.empty()) {
            MultitaskTreeClassifier.MultiTaskNode temp = out.pop();
            if(temp.isLeaf()){
                leafs.add(temp);
            }
        }

        return leafs;
    }

    public static List<Map.Entry<MultitaskTreeClassifier.MultiTaskNode, Integer>> getParentsSorted(MultitaskTreeClassifier.MultiTaskNode root){
        ArrayList<MultitaskTreeClassifier.MultiTaskNode> unsortedList = TreeUtils.getParentsUnsorted(root);
        HashMap<MultitaskTreeClassifier.MultiTaskNode, Integer> map = new HashMap<>();
        for (int i = 0; i < unsortedList.size(); i++) {
            map.put(unsortedList.get(i), unsortedList.get(i).getChildrenDifferences());
        }

        List<Map.Entry<MultitaskTreeClassifier.MultiTaskNode, Integer>> sortedList = new LinkedList<Map.Entry<MultitaskTreeClassifier.MultiTaskNode, Integer>>(map.entrySet());
        Collections.sort(sortedList, Comparator.comparing(Map.Entry::getValue));

        return sortedList;
    }

    public static MultitaskTreeClassifier.MultiTaskNode getRandomNodeToMerge(MultitaskTreeClassifier.MultiTaskNode root){
        //System.out.println("Is root null?"+root==null ? "true" : "false");
        ArrayList<MultitaskTreeClassifier.MultiTaskNode> list = TreeUtils.getParentsUnsorted(root);
        //System.out.println("list size:"+list.size());
        Random rand = new Random();
        return list.isEmpty() ? null : list.get(rand.nextInt(list.size()));
    }

    public static MultitaskTreeClassifier.MultiTaskNode getRandomGeneralizableNodeToMerge(MultitaskTreeClassifier.MultiTaskNode root, Task task){
        ArrayList<MultitaskTreeClassifier.MultiTaskNode> list = TreeUtils.getParentsUnsorted(root);
        ArrayList<MultitaskTreeClassifier.MultiTaskNode> finalList = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {

            boolean leftChildIsLeaf = list.get(i).getChildren().get(0).isLeaf();
            boolean rightChildIsLeaf = list.get(i).getChildren().get(1).isLeaf();

            boolean leftChildHasTask = list.get(i).getChildren().get(0).getMultiDecision().hasTask(task);
            boolean rightChildHasTask = list.get(i).getChildren().get(1).getMultiDecision().hasTask(task);

            if(leftChildIsLeaf && rightChildIsLeaf && leftChildHasTask && rightChildHasTask && (list.get(i).getChildren().get(0).getMultiDecision().getDecision(task) == list.get(i).getChildren().get(1).getMultiDecision().getDecision(task))){
                finalList.add(list.get(i));
            }
        }
        //System.out.println("List size: " + list.size() + " FinalList size: "+finalList.size());
        Random rand = new Random();
        return (finalList.isEmpty()) ? null : finalList.get(rand.nextInt(finalList.size()));
    }

    public static MultitaskTreeClassifier.MultiTaskNode getRandomGeneralizableNodeToMerge2(MultitaskTreeClassifier.MultiTaskNode root, ArrayList<Task> tasks){
        //System.out.println("Tasks size: "+tasks.size());

        ArrayList<MultitaskTreeClassifier.MultiTaskNode> list = TreeUtils.getParentsUnsorted(root);
        ArrayList<MultitaskTreeClassifier.MultiTaskNode> finalList = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {

            boolean leftChildIsLeaf = list.get(i).getChildren().get(0).isLeaf();
            boolean rightChildIsLeaf = list.get(i).getChildren().get(1).isLeaf();

            boolean allDecisionsEqual = true;

            for (int j = 0; j < tasks.size(); j++) {
                boolean leftChildHasTask = list.get(i).getChildren().get(0).getMultiDecision().hasTask(tasks.get(j));
                boolean rightChildHasTask = list.get(i).getChildren().get(1).getMultiDecision().hasTask(tasks.get(j));

                if(leftChildHasTask && rightChildHasTask){
                    if(list.get(i).getChildren().get(0).getMultiDecision().getDecision(tasks.get(j)) != list.get(i).getChildren().get(1).getMultiDecision().getDecision(tasks.get(j))){
                        allDecisionsEqual = false;
                        break;
                    }
                }
            }

            if(leftChildIsLeaf && rightChildIsLeaf && allDecisionsEqual){
                finalList.add(list.get(i));
            }
        }
        Random rand = new Random();
        return (finalList.isEmpty()) ? null : finalList.get(rand.nextInt(finalList.size()));
    }

    public static MultitaskTreeClassifier.MultiTaskNode getBestNodeToMerge(MultitaskTreeClassifier tree){
        List<Map.Entry<MultitaskTreeClassifier.MultiTaskNode, Integer>> list = TreeUtils.getParentsSorted(tree.getRoot());
        return list.get(list.size()-1).getKey();
    }

}
