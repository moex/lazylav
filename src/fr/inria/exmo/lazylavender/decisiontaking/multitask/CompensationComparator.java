package fr.inria.exmo.lazylavender.decisiontaking.multitask;

import java.util.Comparator;

public class CompensationComparator implements Comparator<MultitaskingAgent> {
    @Override
    public int compare(MultitaskingAgent agent1, MultitaskingAgent agent2) {
        if (agent1.getCompensation() != agent2.getCompensation()) {
            return agent1.getCompensation()<agent2.getCompensation() ? -1 : 1;
        }
        return 0;
    }

}
