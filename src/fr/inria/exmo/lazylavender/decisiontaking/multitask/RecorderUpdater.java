package fr.inria.exmo.lazylavender.decisiontaking.multitask;

import java.util.ArrayList;

public interface RecorderUpdater {
    public void updateRecorder(ArrayList<MultitaskingAgent> births, ArrayList<MultitaskingAgent> deaths);

}
