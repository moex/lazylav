package fr.inria.exmo.lazylavender.decisiontaking.multitask;

import fr.inria.exmo.lazylavender.agent.LLAgent;
import fr.inria.exmo.lazylavender.decisiontaking.Common;
import fr.inria.exmo.lazylavender.expe.InstanceIdGame;
import fr.inria.exmo.lazylavender.expe.LLExperiment;
import fr.inria.exmo.lazylavender.model.LLException;

import java.io.PrintWriter;
import java.util.BitSet;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Random;

public class MultitaskGame extends InstanceIdGame {

    private boolean success, successWithUncertainBits, isWinnerDecisionRandom, actualAdaptation, isAgreementRandom;
    private Task task;
    private int numberOfCollisions;
    private MultitaskingAgent winner, loser;
    private MultitaskTreeClassifier.MultiChanger adaptation;
    private LLExperiment experiment;
    private MultitaskTreeClassifier.MultiChange change;
    private HashSet<Integer> globalUncertainBits;
    private int iteration;
    private boolean isDecisionCorrect;

    private boolean isDelegated;

    public MultitaskGame(LLAgent firstAgent, LLAgent secondAgent, BitSet instance, LLExperiment experiment, Task task) {
        super(firstAgent, secondAgent, instance);
        this.experiment = experiment;
        this.task = task;
        this.iteration = -1;
    }

    public MultitaskGame(int iteration, LLAgent firstAgent, LLAgent secondAgent, BitSet instance, LLExperiment experiment, Task task) {
        super(firstAgent, secondAgent, instance);
        this.iteration = iteration;
        this.experiment = experiment;
        this.task = task;
    }

    public MultitaskGame(LLAgent firstAgent, LLAgent secondAgent, BitSet instance, LLExperiment experiment, Task task, HashSet<Integer> globalUncertainBits) {
        this(firstAgent, secondAgent, instance, experiment, task);
        this.globalUncertainBits = globalUncertainBits;
        this.iteration = -1;
    }

    public MultitaskGame(int iteration, LLAgent firstAgent, LLAgent secondAgent, BitSet instance, LLExperiment experiment, Task task, HashSet<Integer> globalUncertainBits) {
        this(iteration, firstAgent, secondAgent, instance, experiment, task);
        this.globalUncertainBits = globalUncertainBits;
    }

    Common.Pair<MultitaskingAgent, MultitaskingAgent> getRandomWinnerLoser() {
        Random random = new Random();
        return (random.nextInt(2)==1) ? new Common.Pair<>((MultitaskingAgent) getFirstAgent(), (MultitaskingAgent) getSecondAgent()) : new Common.Pair<>((MultitaskingAgent) getSecondAgent(), (MultitaskingAgent) getFirstAgent());
    }

    Common.Pair<MultitaskingAgent, MultitaskingAgent> getTaskWinnerLoser() {
        return (((MultitaskingAgent) getFirstAgent()).getTaskScore(task) > ((MultitaskingAgent) getSecondAgent()).getTaskScore(task)) ? new Common.Pair<>((MultitaskingAgent) getFirstAgent(), (MultitaskingAgent) getSecondAgent()) : new Common.Pair<>((MultitaskingAgent) getSecondAgent(), (MultitaskingAgent) getFirstAgent());
    }

    Common.Pair<MultitaskingAgent, MultitaskingAgent> getWinnerLoser() {
        return (((MultitaskingAgent) getFirstAgent()).getScore(getInstance()) > ((MultitaskingAgent) getSecondAgent()).getScore(getInstance())) ? new Common.Pair<>((MultitaskingAgent) getFirstAgent(), (MultitaskingAgent) getSecondAgent()) : new Common.Pair<>((MultitaskingAgent) getSecondAgent(), (MultitaskingAgent) getFirstAgent());
    }

    public int getNumberOfCollisions(){
        return numberOfCollisions;
    }

    public void setNumberOfCollisions(int numberOfCollisions){
        this.numberOfCollisions = numberOfCollisions;
    }

    public void setActualAdaptation(boolean actualAdaptation){
        this.actualAdaptation = actualAdaptation;
    }

    public boolean isActualAdaptation(){
        return actualAdaptation;
    }

    public boolean isDecisionCorrect(){
        return isDecisionCorrect;
    }

    public void setDecisionCorrect(boolean decisionCorrect){
        this.isDecisionCorrect = decisionCorrect;
    }

    public boolean isSuccessWithUncertainBits(){
        return successWithUncertainBits;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public void setSuccessWithUncertainBits(boolean successWithUncertainBits){
        this.successWithUncertainBits = successWithUncertainBits;
    }

    public MultitaskingAgent getWinner() {
        return winner;
    }

    public void setWinner(MultitaskingAgent winner) {
        this.winner = winner;
    }

    public MultitaskingAgent getLoser() {
        return loser;
    }

    public void setLoser(MultitaskingAgent loser) {
        this.loser = loser;
    }

    public MultitaskTreeClassifier.MultiChanger getAdaptation() {
        return adaptation;
    }

    public void setAdaptation(MultitaskTreeClassifier.MultiChanger adaptation) {
        this.adaptation = adaptation;
    }

    public void setChange(MultitaskTreeClassifier.MultiChange change) {
        this.change = change;
    }

    public MultitaskTreeClassifier.MultiChange getChange() {
        return change;
    }

    public Task getTask(){
        return this.task;
    }

    public void setTask(Task task){
        this.task = task;
    }

    public void setIteration(int iteration){
        this.iteration = iteration;
    }

    public int getIteration(){
        return this.iteration;
    }

    public void setGlobalUncertainBits(HashSet<Integer> globalUncertainBits){
        this.globalUncertainBits = globalUncertainBits;
    }

    public HashSet<Integer> getGlobalUncertainBits(){
        return globalUncertainBits;
    }

    private String uncertainBitsToString(){
        if(globalUncertainBits.size()>0) {
            StringBuilder sb = new StringBuilder();
            Iterator<Integer> it = globalUncertainBits.iterator();
            while (it.hasNext()) {
                sb.append(String.valueOf(it.next())).append(":");
            }
            return sb.toString();
        }else{
            return ":";
        }
    }

    public void setWinnerDecisionRandom(boolean isWinnerDecisionRandom){
        /*
        if(isWinnerDecisionRandom){
            System.out.println("Winner decision is random");
        }

         */
        this.isWinnerDecisionRandom = isWinnerDecisionRandom;
    }

    public boolean isWinnerDecisionRandom(){
        return isWinnerDecisionRandom;
    }

    public void setAgreementRandom(boolean isAgreementRandom){
        this.isAgreementRandom = isAgreementRandom;
    }

    public boolean isAgreementRandom(){
        return isAgreementRandom;
    }

    public void setDelegated(boolean isDelegated){
        this.isDelegated = isDelegated;
    }

    public boolean isDelegated(){
        return isDelegated;
    }

    @Override
    public void save(PrintWriter file ) throws LLException {
        file.printf( "%d\t%d\t%s\t%s\t%s\n", getFirstAgent().getId(), getSecondAgent().getId(), getInstance(), getTask().getName(), uncertainBitsToString() );
    }

    public LLExperiment getExperiment(){
        return experiment;
    }


}
