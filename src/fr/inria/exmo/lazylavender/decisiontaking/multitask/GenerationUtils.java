package fr.inria.exmo.lazylavender.decisiontaking.multitask;

import fr.inria.exmo.lazylavender.decisiontaking.generations.DolaGenRecorders;
import fr.inria.exmo.lazylavender.decisiontaking.measurements.Recorder;

import java.util.*;

public class GenerationUtils {

    enum ParentSelectionPolicy {
        RANDOM,
        BEST_MATE,
        BEST_MATE_SINGLE_CHILD
    }

    public static void updateAgentPeriodsAlive(MultitaskLLExperiment exp){
        for (Map.Entry<Integer, MultitaskingAgent> set : exp.getAliveAgents().entrySet()) {
            set.getValue().setPeriodsAlive(set.getValue().getPeriodsAlive()+1);
        }
    }

    public static void populationRenewal(int numberOfBirths, MultitaskLLExperiment exp, ParentSelectionPolicy policy){

        //System.out.println("\n\nNew generation\n\n");

        AgentAgeComparator ageComparator = new AgentAgeComparator();

        Random rand = new Random();

        //TODO GIVING BIRTH

        //System.out.println("Number of expected births: "+numberOfBirths);

        ArrayList<MultitaskingAgent> newDeads = new ArrayList<>();
        ArrayList<MultitaskingAgent> newBorns = new ArrayList<>();

        HashSet<MultitaskingAgent> parents = new HashSet<>();

        for (int i = 0; i < numberOfBirths; i++) {
            MultitaskingAgent newAgent = new MultitaskingAgent(exp.getMaxAgentId()+1, exp.getEnvironment(), exp, exp.getParams());

            exp.setMaxAgentId(exp.getMaxAgentId()+1);

            Collection<MultitaskingAgent> aliveAgentsInCollection = exp.getAliveAgents().values();
            ArrayList<MultitaskingAgent> aliveAgentsInList = new ArrayList<>(aliveAgentsInCollection);

            MultitaskingAgent parent1 = aliveAgentsInList.get(rand.nextInt(aliveAgentsInCollection.size()));
            if(policy == ParentSelectionPolicy.BEST_MATE_SINGLE_CHILD){
                while(parents.contains(parent1)){
                    parent1 = aliveAgentsInList.get(rand.nextInt(aliveAgentsInCollection.size()));
                }
            }

            MultitaskingAgent parent2 = null;

            if(policy == ParentSelectionPolicy.RANDOM) {
                parent2 = aliveAgentsInList.get(rand.nextInt(aliveAgentsInCollection.size()));
                while (parent1.getId() == parent2.getId()) {
                    parent2 = aliveAgentsInList.get(rand.nextInt(aliveAgentsInCollection.size()));
                }
            }else if(policy == ParentSelectionPolicy.BEST_MATE){
                double maxSuccessRate = -1;
                MultitaskingAgent bestMatch = null;
                for (Map.Entry<Integer, MultitaskingAgent> entry : exp.getAliveAgents().entrySet()) {
                    MultitaskingAgent value = entry.getValue();

                    if(value.getId()== parent1.getId())
                        continue;

                    double successfulInteractionsWithCurrentAgent = (parent1.getSuccessfulInteractions().get(value)!=null) ? parent1.getSuccessfulInteractions().get(value) : 0;
                    //System.out.println("successful interactions with current agent: "+successfulInteractionsWithCurrentAgent);
                    double totalInteractionsWithCurrentAgent = (parent1.getTotalInteractions().get(value)!=null) ? parent1.getTotalInteractions().get(value) : 1;
                    //System.out.println("total interactions with current agent: "+parent1.getTotalInteractions().get(value));
                    double currentSuccessRate = successfulInteractionsWithCurrentAgent/totalInteractionsWithCurrentAgent;

                    if(currentSuccessRate>maxSuccessRate){
                        maxSuccessRate = currentSuccessRate;
                        bestMatch = value;
                    }
                }
                parent2 = bestMatch;
            }else{
                double maxSuccessRate = -1;
                MultitaskingAgent bestMatch = null;
                for (Map.Entry<Integer, MultitaskingAgent> entry : exp.getAliveAgents().entrySet()) {
                    MultitaskingAgent value = entry.getValue();

                    if(value.getId() == parent1.getId() || parents.contains(value))
                        continue;

                    double successfulInteractionsWithCurrentAgent = (parent1.getSuccessfulInteractions().get(value)!=null) ? parent1.getSuccessfulInteractions().get(value) : 0;
                    //System.out.println("successful interactions with current agent: "+successfulInteractionsWithCurrentAgent);
                    double totalInteractionsWithCurrentAgent = (parent1.getTotalInteractions().get(value)!=null) ? parent1.getTotalInteractions().get(value) : 1;
                    //System.out.println("total interactions with current agent: "+parent1.getTotalInteractions().get(value));
                    double currentSuccessRate = successfulInteractionsWithCurrentAgent/totalInteractionsWithCurrentAgent;

                    if(currentSuccessRate>maxSuccessRate){
                        maxSuccessRate = currentSuccessRate;
                        bestMatch = value;
                    }
                }
                parent2 = bestMatch;

                parents.add(parent1);
                parents.add(parent2);
            }

            newAgent.addParent(parent1);
            newAgent.addParent(parent2);

            //TODO DOWN OF HERE EXAMPLES CREATION!

            ArrayList<EnvironmentExample> environmentExamples = new ArrayList<EnvironmentExample>(exp.getEnvironment().getEnvironmentExamples().values());
            ArrayList<EnvironmentExample> finalEnvironmentExamples = new ArrayList<>();
            while(finalEnvironmentExamples.isEmpty()) {
                for (int j = 0; j < environmentExamples.size(); j++) {
                    if (rand.nextDouble() <= exp.getRatio()) {
                        finalEnvironmentExamples.add(environmentExamples.get(j));
                    }
                }
            }

            ArrayList<TrainingExample> finalExamples = new ArrayList<>();

            ArrayList<TrainingExample> parent1Examples = parent1.createExamples(finalEnvironmentExamples);
            ArrayList<TrainingExample> parent2Examples = parent2.createExamples(finalEnvironmentExamples);

            if(parent2Examples.size() != parent1Examples.size()){
                System.out.println("parent2Examples.size() != parent1Examples.size()");
            }

            for (int j = 0; j < parent1Examples.size(); j++) {
                if(rand.nextBoolean()){
                    finalExamples.add(parent1Examples.get(j));
                }else{
                    finalExamples.add(parent2Examples.get(j));
                }
            }

            try {
                newAgent.getClassifier().train(finalExamples);
            } catch (Exception e) {
                e.printStackTrace();
            }

            newAgent.getClassifier().toString();

            if(exp instanceof SpecializationLLExperiment) {

                HashMap<Task, Integer> parent1Rankings = parent1.getAdaptingTasksRanking();
                System.out.println("parent1Rankings size: " + parent1.getAdaptingTasksRanking().size());
                for (Map.Entry<Task, Integer> p1EntrySet : parent1Rankings.entrySet()) {
                    System.out.println("p1EntrySet: " + p1EntrySet.getKey().getName() + " - " + p1EntrySet.getValue());
                }

                HashMap<Task, Integer> parent2Rankings = parent2.getAdaptingTasksRanking();
                System.out.println("parent2Rankings size: " + parent2.getAdaptingTasksRanking().size());
                for (Map.Entry<Task, Integer> p2EntrySet : parent2Rankings.entrySet()) {
                    System.out.println("p2EntrySet: " + p2EntrySet.getKey().getName() + " - " + p2EntrySet.getValue());
                }

            }

            boolean parentCoinToss = rand.nextBoolean();
            newAgent.setAdaptingTasksRanking((parentCoinToss) ? parent1.getAdaptingTasksRanking() : parent2.getAdaptingTasksRanking());
            newAgent.setPreferredTasks((parentCoinToss) ? parent1.getPreferredTasks() :parent2.getPreferredTasks());
            newAgent.setFavoriteTask((parentCoinToss) ? parent1.getFavoriteTask() : parent2.getFavoriteTask());

            newBorns.add(newAgent);
        }

        //TODO KILLING

        Collection<MultitaskingAgent> values = exp.getAliveAgents().values();

        ArrayList<MultitaskingAgent> listOfAgentsAlive = new ArrayList<>(values);

        Collections.sort(listOfAgentsAlive, ageComparator);

        for (int j = listOfAgentsAlive.size() - 1; j >= listOfAgentsAlive.size()-numberOfBirths; j--) {
            MultitaskingAgent dyingAgent = listOfAgentsAlive.get(j);
            dyingAgent.setAlive(false);
            exp.getDeadAgents().put(dyingAgent.getId(), dyingAgent);
            exp.getAliveAgents().remove(dyingAgent.getId());
            newDeads.add(dyingAgent);
        }

        //System.out.println("after killing agents, the agents were : " + exp.getAliveAgents().size());

        StringBuilder idsOfAgentsAlive = new StringBuilder();
        for (Map.Entry<Integer, MultitaskingAgent> agentsSet : exp.getAliveAgents().entrySet()) {
            idsOfAgentsAlive.append(agentsSet.getKey()).append(" ");
        }
        System.out.println(idsOfAgentsAlive.toString());

        for (int i = 0; i < newBorns.size(); i++) {
            exp.getAliveAgents().put(newBorns.get(i).getId(), newBorns.get(i));
        }

        exp.getGameIterator().updateAgentIDs();

        for(Recorder rec: exp.getRecordingManager().getRecorders()) {
            if(rec instanceof RecorderUpdater){
                ((RecorderUpdater) rec).updateRecorder(newBorns, newDeads);
            }
        }

    }

}
