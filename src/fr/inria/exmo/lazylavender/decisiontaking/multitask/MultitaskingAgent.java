/*
 * Copyright (C) INRIA, 2021-2024
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 */

package fr.inria.exmo.lazylavender.decisiontaking.multitask;

import fr.inria.exmo.lazylavender.agent.LLAgent;
import fr.inria.exmo.lazylavender.decisiontaking.Common;
import fr.inria.exmo.lazylavender.decisiontaking.treeclassifier.*;
import fr.inria.exmo.lazylavender.decisiontaking.treeclassifier.changers.MergeMultiTaskNodeChanger;
import fr.inria.exmo.lazylavender.decisiontaking.treeclassifier.changers.MultiTaskNodeClassChanger;
import fr.inria.exmo.lazylavender.decisiontaking.treeclassifier.changers.SplitMultiTaskNodeChanger;
import fr.inria.exmo.lazylavender.expe.LLGame;
import fr.inria.exmo.lazylavender.model.LLException;

import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

public class MultitaskingAgent implements LLAgent {

    enum TaskSelectionStrategy {
        SIMPLISTIC,
        BALANCED,
        RANDOM,
        MAXIMAX,
        MAXIMIN,
        MINIMAX_REGRET
    }

    public enum UnknownDecisionStrategy {
        FIRST,
        RANDOM,
        NONE
    }

    enum WinningCondition {
        GLOBAL,
        TASK
    }

    enum ForgettingMechanism {
        RANDOM,
        GENERALIZATION,
        NO_FORGETTING,
        PROBABILISTIC_GENERALIZATION,
        MOST_ACCURATE_TASK_GENERALIZATION
    }

    private ForgettingMechanism forgettingMechanism = ForgettingMechanism.GENERALIZATION;

    private UnknownDecisionStrategy unknownDecisionStrategy = UnknownDecisionStrategy.RANDOM;

    private WinningCondition winningCondition = WinningCondition.GLOBAL;

    private TaskSelectionStrategy taskSelectionStrategy;

    private MultitaskTreeClassifier classifier;

    private MultitaskEnvironment environment;

    private int id;

    private double score;

    private HashMap<Task, Double> taskRewardCoefficients;

    private HashMap<Task, Double> taskPenaltyCoefficients;

    private HashMap<Task, Double> taskScores;

    private HashMap<Task, Double> taskSpecialization;

    private HashMap<MultitaskingAgent, Double> acquaintances;

    private boolean useNodeConf = false;

    private Map<MultitaskTreeClassifier.MultiTaskNode, Integer> successful;

    private Map<MultitaskTreeClassifier.MultiTaskNode, Integer> tries;

    private String adaptOp = "allCom";

    private double totalSuccesses;

    private HashMap<Task, Double> taskSuccessfulAttempts;

    private HashMap<Task, Double> taskTotalGames;

    private HashMap<Task, Double> taskTotalAdaptations;

    private HashMap<Task, Double> taskTotalLessons;

    private HashMap<Task, Double> taskTotalLosses;

    private int numberOfAttempts;

    private SplitMultiTaskNodeChanger.Adaptation adaptation;

    private boolean specialized = false;

    private HashMap<Task, Integer> adaptingTasksRanking;

    private int maxAdaptationRank;

    private int numberOfUncertainBits;

    private MultitaskLLExperiment experiment;

    private ArrayList<MultitaskGame> actualAdaptations;

    private HashSet<Integer> deficientSensors;

    private Task favoriteTask;

    private double knowledgeLimit = 1.0;

    private double specializedAgentsRatio = 1.0;

    private ArrayList<Task> preferredTasks;

    private ArrayList<Task> generalizableTasks;

    private ArrayList<Task> bestTask;

    private int classesLimit = 2048;

    private int money = 1;

    private HashMap<MultitaskingAgent, Double> successfulInteractions;

    private HashMap<MultitaskingAgent, Double> totalInteractions;

    private int maxMerges = 10000;

    private int maxSubstitutions = 10000;

    private int actualMerges = 0;

    private int actualSubstitutions = 0;

    private boolean continuousMerging = true;

    private double compensation = 0;

    private double defaultCompensation = 10;

    private double maxAccuracy = 0;

    private boolean alive = true;

    private int periodsAlive = 0;

    private ArrayList<MultitaskingAgent> parents;

    public MultitaskingAgent(int id, MultitaskEnvironment environment, MultitaskLLExperiment experiment, Properties params) {
        this.id = id;
        this.environment = environment;
        this.experiment = experiment;
        this.init(params);
    }

    public void initDeficientSensors(int numberOfDeficientSensors) {
        Random random = new Random();
        int numberOfFeatures = environment.getNumberOfFeatures();
        for (int i = 0; i < numberOfDeficientSensors; i++) {
            int bitIndex = random.nextInt(numberOfFeatures);
            while (this.deficientSensors.contains(bitIndex)) {
                bitIndex = random.nextInt(numberOfFeatures);
            }
            this.deficientSensors.add(bitIndex);
        }
    }

    public void initPreferences(){
        //System.out.println("Specialized: "+specialized);
        preferredTasks = new ArrayList<>();
        if (specialized && (experiment instanceof DelegationSpecializationExperiment || experiment instanceof SelectiveAdaptationSpecializationExperiment || experiment instanceof SelectiveAcceptanceSpecializationExperiment || experiment instanceof  SelectiveMentorSpecializationExperiment)) {
            //System.out.println("TaskSelectionStrategy: "+this.taskSelectionStrategy.name());
            adaptingTasksRanking = this.rankTasks(new ArrayList<>(environment.getEnvironmentTasks().values()), isSpecialized(), false);
            System.out.println("Task ranking initialized..");
            for (Map.Entry<Task, Integer> entry : adaptingTasksRanking.entrySet()) {
                Integer rank = entry.getValue();
                if(rank<=maxAdaptationRank){
                    preferredTasks.add(entry.getKey());
                }
                if(rank==0){
                    favoriteTask = entry.getKey();
                    //System.out.println("Favorite task is: "+favoriteTask.getName());
                }
                System.out.println("taskId : " + entry.getKey().getName() + " rank : " + rank);
            }
        }
    }

    public void setMoney(int money){
        this.money = money;
    }

    public int getMoney(){
        return money;
    }

    public void setTaskSelectionStrategy(TaskSelectionStrategy taskSelectionStrategy) {
        this.taskSelectionStrategy = taskSelectionStrategy;
    }

    public TaskSelectionStrategy getTaskSelectionStrategy() {
        return (this.taskSelectionStrategy)!=null ? this.taskSelectionStrategy : TaskSelectionStrategy.SIMPLISTIC;
    }

    public int getTaskRank(Task key){
        if(adaptingTasksRanking.containsKey(key)){
            return adaptingTasksRanking.get(key);
        }else{
            return -1;
        }
    }

    public HashMap<Task, Integer> getAdaptingTasksRanking(){
        return adaptingTasksRanking;
    }

    public void setAdaptingTasksRanking(HashMap<Task,Integer> adaptingTasksRanking){
        this.adaptingTasksRanking = adaptingTasksRanking;
    }

    public double getTotalSuccesses() {
        return totalSuccesses;
    }

    public void setTotalSuccesses(double totalSuccesses) {
        //System.out.println("Setting success rate : "+ totalSuccesses);
        this.totalSuccesses = totalSuccesses;
    }

    public int getNumberOfAttempts() {
        return numberOfAttempts;
    }

    public void setNumberOfAttempts(int numberOfAttempts) {
        this.numberOfAttempts = numberOfAttempts;
    }

    public double getTaskTotalGames(Task task) {
        if (taskTotalGames.containsKey(task)) {
            return taskTotalGames.get(task);
        } else {
            return 1;
        }
    }

    public double getTaskTotalAdaptations(Task task) {
        if (taskTotalAdaptations.containsKey(task)) {
            return taskTotalAdaptations.get(task);
        } else {
            return 0;
        }
    }

    public double getTotalAdaptations() {
        double sum = 0;
        for (Map.Entry<Task, Double> entry : taskTotalAdaptations.entrySet()) {
            sum += entry.getValue();
        }
        return sum;
    }

    public double getTaskTotalLessons(Task task) {
        if (taskTotalLessons.containsKey(task)) {
            return taskTotalLessons.get(task);
        } else {
            return 0;
        }
    }

    public double getTotalLessons() {
        double sum = 0;
        for (Map.Entry<Task, Double> entry : taskTotalLessons.entrySet()) {
            sum += entry.getValue();
        }
        return sum;
    }

    public double getTaskTotalLosses(Task task) {
        if (taskTotalLosses.containsKey(task)) {
            return taskTotalLosses.get(task);
        } else {
            return 0;
        }
    }

    public void setTaskTotalGames(Task task, double attempts) {
        taskTotalGames.put(task, attempts);
    }

    public void setTaskTotalAdaptations(Task task, double adaptations) {
        taskTotalAdaptations.put(task, adaptations);
    }

    public void setTaskTotalLessons(Task task, double lessons) {
        taskTotalLessons.put(task, lessons);
    }

    public void setTaskTotalLosses(Task task, double losses) {
        taskTotalLosses.put(task, losses);
    }

    public String printTotalAttemptsPerTask() {
        String output = "";
        for (Map.Entry<Task, Double> entry : taskTotalGames.entrySet()) {
            Task task = entry.getKey();
            Double totalGames = entry.getValue();
            Double totalAdaptations = this.getTaskTotalAdaptations(task);
            Double totalLosses = this.getTaskTotalLosses(task);
            output += "Task : " + task.getName() + "(" + getTaskRewardCoefficient(task) + "," + getTaskPenaltyCoefficient(task) + ")" + " played : " + totalGames + " and adapted : " + ((totalLosses == 0 && totalAdaptations == 0) ? 0 : totalAdaptations / totalLosses) + "\n";
        }
        return output;
    }

    @Override
    public LLAgent init(Properties param) {

        parents = new ArrayList<>();

        acquaintances = new HashMap<>();

        deficientSensors = new HashSet<>();

        taskRewardCoefficients = new HashMap<>();
        for (Task task : environment.getEnvironmentTasks().values()) {
            taskRewardCoefficients.put(task, ThreadLocalRandom.current().nextDouble(0, 1));
        }

        taskPenaltyCoefficients = new HashMap<>();
        for (Task task : environment.getEnvironmentTasks().values()) {
            taskPenaltyCoefficients.put(task, ThreadLocalRandom.current().nextDouble(0, 1));
        }

        taskScores = new HashMap<>();
        taskSpecialization = new HashMap<>();

        successful = new HashMap<>();
        tries = new HashMap<>();

        taskSuccessfulAttempts = new HashMap<>();
        taskTotalGames = new HashMap<>();
        taskTotalAdaptations = new HashMap<>();
        taskTotalLessons = new HashMap<>();
        taskTotalLosses = new HashMap<>();

        actualAdaptations = new ArrayList<>();

        totalSuccesses = 0;
        numberOfAttempts = 0;

        if (param.containsKey("forgettingMechanism")) {
            String fmec = param.getProperty("forgettingMechanism");
            if (fmec.equalsIgnoreCase("random")) {
                forgettingMechanism = ForgettingMechanism.RANDOM;
            } else if (fmec.equalsIgnoreCase("generalization")) {
                forgettingMechanism = ForgettingMechanism.GENERALIZATION;
            } else if (fmec.equalsIgnoreCase("no_forgetting")) {
                forgettingMechanism = ForgettingMechanism.NO_FORGETTING;
            } else if(fmec.equalsIgnoreCase("probabilistic_generalization")){
                forgettingMechanism = ForgettingMechanism.PROBABILISTIC_GENERALIZATION;
            } else if(fmec.equalsIgnoreCase("most_accurate_task_generalization")){
                forgettingMechanism = ForgettingMechanism.MOST_ACCURATE_TASK_GENERALIZATION;
            }
        }

        if (param.containsKey("winCondition")) {
            String winCondition = param.getProperty("winCondition");
            if (winCondition.equalsIgnoreCase("global")) {
                winningCondition = WinningCondition.GLOBAL;
            } else if (winCondition.equalsIgnoreCase("task")) {
                winningCondition = WinningCondition.TASK;
            }
        }else{
            winningCondition = WinningCondition.TASK;
        }

        if (param.containsKey("unknownDecisionStrategy")) {
            String unknownDecisionStrategy = param.getProperty("unknownDecisionStrategy");
           if(param.containsKey("runDir") || param.containsKey("loadRunDir")){
               this.unknownDecisionStrategy = UnknownDecisionStrategy.FIRST;
           } else if (unknownDecisionStrategy.equalsIgnoreCase("first")) {
                this.unknownDecisionStrategy = UnknownDecisionStrategy.FIRST;
            } else if (unknownDecisionStrategy.equalsIgnoreCase("random")) {
                this.unknownDecisionStrategy = UnknownDecisionStrategy.RANDOM;
            } else if (unknownDecisionStrategy.equalsIgnoreCase("none")) {
                this.unknownDecisionStrategy = UnknownDecisionStrategy.NONE;
            } else {
                this.unknownDecisionStrategy = UnknownDecisionStrategy.RANDOM;
            }
        }else{
            this.unknownDecisionStrategy = UnknownDecisionStrategy.RANDOM;
        }

        if (param.containsKey("taskSelectionStrategy")) {
            String selectionStrategy = param.getProperty("taskSelectionStrategy");
            if (selectionStrategy.equalsIgnoreCase("MAXIMAX")) {
                taskSelectionStrategy = TaskSelectionStrategy.MAXIMAX;
            } else if (selectionStrategy.equalsIgnoreCase("MAXIMIN")) {
                taskSelectionStrategy = TaskSelectionStrategy.MAXIMIN;
            } else if (selectionStrategy.equalsIgnoreCase("MINIMAX_REGRET")) {
                taskSelectionStrategy = TaskSelectionStrategy.MINIMAX_REGRET;
            } else if (selectionStrategy.equalsIgnoreCase("RANDOM")){
                taskSelectionStrategy = getRandomTaskSelectionStrategy();
            } else if (selectionStrategy.equalsIgnoreCase("SIMPLISTIC")){
                taskSelectionStrategy = TaskSelectionStrategy.SIMPLISTIC;
            } else if(selectionStrategy.equalsIgnoreCase("BALANCED")){
                taskSelectionStrategy = TaskSelectionStrategy.BALANCED;
            } else{
                taskSelectionStrategy = TaskSelectionStrategy.SIMPLISTIC;
            }
        }else{
            taskSelectionStrategy = TaskSelectionStrategy.SIMPLISTIC;
        }

        if (param.containsKey("op")) {
            adaptOp = param.getProperty("op");
        }

        if (param.containsKey("adaptationParam")) {
            String adaptationParam = param.getProperty("adaptationParam");
            if (adaptationParam.equalsIgnoreCase("task")) {
                adaptation = SplitMultiTaskNodeChanger.Adaptation.TASK;
            } else if (adaptationParam.equalsIgnoreCase("taskPlusUnknown")) {
                adaptation = SplitMultiTaskNodeChanger.Adaptation.TASK_PLUS_UNKNOWN;
            } else if (adaptationParam.equalsIgnoreCase("allTasks")) {
                adaptation = SplitMultiTaskNodeChanger.Adaptation.ALL_TASKS;
            } else {
                adaptation = SplitMultiTaskNodeChanger.Adaptation.TASK;
            }
        }else{
            adaptation = SplitMultiTaskNodeChanger.Adaptation.TASK;
        }

        if(param.containsKey("knowledgeLimit")){
            knowledgeLimit = Double.parseDouble(param.getProperty("knowledgeLimit"));
        }else{
            knowledgeLimit = 1.0;
        }

        if(param.containsKey("numberOfDeficientSensors")) {
            this.initDeficientSensors(Integer.parseInt(param.getProperty("numberOfDeficientSensors")) > environment.getNumberOfFeatures() ? environment.getNumberOfFeatures() : Integer.parseInt(param.getProperty("numberOfDeficientSensors")));
        }

        if(param.containsKey("numberOfUncertainBits")) {
            numberOfUncertainBits = Integer.parseInt(param.getProperty("numberOfUncertainBits"));
        }else{
            numberOfUncertainBits = 0;
        }

        if(param.containsKey("maxAdaptationRank")) {
            int value = Integer.parseInt(param.getProperty("maxAdaptationRank"));
            maxAdaptationRank = (value >= environment.getNumberOfTasks()) ? environment.getNumberOfTasks()-1 : value;
        }else{
            maxAdaptationRank = environment.getNumberOfTasks()-1;
        }

        if(param.containsKey("specializedAgentsRatio")) {
            specializedAgentsRatio = Double.parseDouble(param.getProperty("specializedAgentsRatio"));
            Random generator = new Random();
            specialized = (generator.nextDouble() <= specializedAgentsRatio);
        }else{
            specialized = true;
        }

        if(param.containsKey("maxMerges")) {
            int value = Integer.parseInt(param.getProperty("maxMerges"));
            maxMerges = value;
        }else{
            maxMerges = 10000;
        }

        if(param.containsKey("maxSubstitutions")) {
            int value = Integer.parseInt(param.getProperty("maxSubstitutions"));
            maxSubstitutions = value;
        }else{
            maxSubstitutions = 10000;
        }

        if(experiment.getCurrentGeneration()==1) {
            initPreferences();
        }

        classesLimit = (int) (Math.pow(2.0, (environment.getNumberOfFeatures()))*this.getKnowledgeLimit());

        //System.out.println("Classes limit: "+classesLimit);

        classifier = new MultitaskTreeClassifierQuick(classesLimit);

        successfulInteractions = new HashMap<>();
        totalInteractions = new HashMap<>();
        //System.out.println("I DID INITIALIZE THE TWO HASHMAPS!");

        //System.out.println("Experiment number of Agents:"+experiment.getAgents().size());

        //System.out.println("successfulInteractions size:"+successfulInteractions.size());

        //System.out.println("totalInteractions size:"+totalInteractions.size());

        return this;
    }

    @Override
    public int getId() {
        return id;
    }

    public ArrayList<Task> getPreferredTasks(){
        return preferredTasks;
    }

    public ArrayList<Task> getGeneralizableTasks(){
        return generalizableTasks;
    }

    public ArrayList<Task> getBestTask(){
        return bestTask;
    }

    public void initGeneralizableTasks(){
        generalizableTasks = new ArrayList<>();
        Random rand = new Random();
        double percentagesSum = 0;
        for (Map.Entry<Task, Double> entry : taskScores.entrySet()) {
            //System.out.println("Entry value:"+entry.getValue());
            percentagesSum += entry.getValue();
        }
        for (Map.Entry<Task, Double> entry : taskScores.entrySet()) {
            if(taskScores.get(entry.getKey())/percentagesSum<=rand.nextDouble()){
                generalizableTasks.add(entry.getKey());
            }
        }
    }

    public void initBestTask(){
        bestTask = new ArrayList<>();
        Map.Entry<Task, Double> firstEntry = taskScores.entrySet().stream().findFirst().get();
        Task currentTask = firstEntry.getKey();
        double currentScore = firstEntry.getValue();

        for (Map.Entry<Task, Double> entry : taskScores.entrySet()) {
            if(entry.getValue()>=currentScore){
                currentScore = entry.getValue();
                currentTask = entry.getKey();
            }
        }
        bestTask.add(currentTask);
    }

    public void setPreferredTasks(ArrayList<Task> preferredTasks){
        this.preferredTasks = preferredTasks;
    }

    @Override
    public void playGame(int remaining, LLGame game) throws LLException {

        MultitaskGame multitaskGame = (MultitaskGame) game;

        MultitaskingAgent secondAgent = (MultitaskingAgent) multitaskGame.getSecondAgent();

        BitSet instance = multitaskGame.getInstance();

        //TODO DO WE REALLY NEED TO CREATE THESE IF THERE ARE NO UNCERTAIN BITS? PROBABLY NOT!
        BitSet firstAgentInstanceWithUncertainty = (BitSet) instance.clone();
        BitSet secondAgentInstanceWithUncertainty = (BitSet) instance.clone();

        HashSet<Integer> globalUncertainBits = new HashSet<>();
        Random random = new Random();
        int numberOfFeatures = environment.getNumberOfFeatures();

        for (int i = 0; i < numberOfUncertainBits; i++) {
            int bitIndex = random.nextInt(numberOfFeatures);
            while (globalUncertainBits.contains(bitIndex)) {
                bitIndex = random.nextInt(numberOfFeatures);
            }
            globalUncertainBits.add(bitIndex);
        }

        //CREATE UNCERTAIN BITS PER AGENT TAKING INTO ACCOUNT AGENT DEFICIENCIES
        HashSet<Integer> firstAgentUncertainBits = new HashSet<>();
        HashSet<Integer> secondAgentUncertainBits = new HashSet<>();

        firstAgentUncertainBits.addAll(globalUncertainBits);
        firstAgentUncertainBits.addAll(this.getAllDeficientSensors());

        secondAgentUncertainBits.addAll(globalUncertainBits);
        secondAgentUncertainBits.addAll(secondAgent.getAllDeficientSensors());

        multitaskGame.setGlobalUncertainBits(globalUncertainBits);

        if (firstAgentUncertainBits.size() > 0) {
            for (int index : firstAgentUncertainBits) {
                firstAgentInstanceWithUncertainty.set(index, random.nextBoolean());
            }
        }

        if (secondAgentUncertainBits.size() > 0) {
            for (int index : secondAgentUncertainBits) {
                secondAgentInstanceWithUncertainty.set(index, random.nextBoolean());
            }
        }

        MultitaskDecision firstAgentDecisionUB = classifier.getClass(firstAgentInstanceWithUncertainty);
        boolean isFirstAgentUncertainDecisionRandom = !firstAgentDecisionUB.hasTask(multitaskGame.getTask());
        MultitaskDecision firstAgentDecision = classifier.getClass(instance);
        boolean isFirstAgentDecisionRandom = !firstAgentDecision.hasTask(multitaskGame.getTask());

        MultitaskDecision secondAgentDecisionUB = secondAgent.getClassifier().getClass(secondAgentInstanceWithUncertainty);
        boolean isSecondAgentUncertainDecisionRandom = !secondAgentDecisionUB.hasTask(multitaskGame.getTask());
        MultitaskDecision secondAgentDecision = secondAgent.getClassifier().getClass(instance);
        boolean isSecondAgentDecisionRandom = !secondAgentDecision.hasTask(multitaskGame.getTask());

        //CRATE
        boolean firstAgentUncertaintyCollision;
        if (isFirstAgentDecisionRandom && isFirstAgentUncertainDecisionRandom) {
            firstAgentUncertaintyCollision = false;
        } else if (isFirstAgentDecisionRandom ^ isFirstAgentUncertainDecisionRandom) {
            firstAgentUncertaintyCollision = true;
        } else {
            if (firstAgentDecisionUB.getDecision(multitaskGame.getTask()) != firstAgentDecision.getDecision(multitaskGame.getTask())) {
                firstAgentUncertaintyCollision = true;
            } else {
                firstAgentUncertaintyCollision = false;
            }
        }

        boolean secondAgentUncertaintyCollision;
        if (isSecondAgentDecisionRandom && isSecondAgentUncertainDecisionRandom) {
            secondAgentUncertaintyCollision = false;
        } else if (isSecondAgentDecisionRandom ^ isSecondAgentUncertainDecisionRandom) {
            secondAgentUncertaintyCollision = true;
        } else {
            if (secondAgentDecisionUB.getDecision(multitaskGame.getTask()) != secondAgentDecision.getDecision(multitaskGame.getTask())) {
                secondAgentUncertaintyCollision = true;
            } else {
                secondAgentUncertaintyCollision = false;
            }
        }

        multitaskGame.setNumberOfCollisions((firstAgentUncertaintyCollision) ? 1 : 0 + ((secondAgentUncertaintyCollision) ? 1 : 0));

        //SRATE
        boolean agreement = firstAgentDecision.getDecision(multitaskGame.getTask(), this.getUnknownDecisionStrategy()) == secondAgentDecision.getDecision(multitaskGame.getTask(), secondAgent.getUnknownDecisionStrategy());

        //SHOULDN'T IT BE BASED ON UB?
        multitaskGame.setSuccess(agreement);

        //SRATE_UB
        boolean agreementWithUncertainBits = firstAgentDecisionUB.getDecision(multitaskGame.getTask(), this.getUnknownDecisionStrategy()) == secondAgentDecisionUB.getDecision(multitaskGame.getTask(), secondAgent.getUnknownDecisionStrategy());

        multitaskGame.setAgreementRandom(agreementWithUncertainBits && (isFirstAgentUncertainDecisionRandom || isSecondAgentUncertainDecisionRandom));

        //WHICH AGENT HAS THE HIGHEST PAYOFF!
        //WHICH AGENT HAS THE HIGHEST PAYOFF!
        Common.Pair<MultitaskingAgent, MultitaskingAgent> p;

        if (winningCondition == WinningCondition.GLOBAL) {
            p = multitaskGame.getWinnerLoser();
        } else if (winningCondition == WinningCondition.TASK) {
            p = multitaskGame.getTaskWinnerLoser();
        } else {
            p = multitaskGame.getRandomWinnerLoser();
        }

        if (agreementWithUncertainBits) {

            multitaskGame.setSuccessWithUncertainBits(true);
            secondAgent.addSuccess(instance);
            this.addSuccess(instance);

            //THIS AGENT
            if(totalInteractions.containsKey(secondAgent)) {
                totalInteractions.put(secondAgent, totalInteractions.get(secondAgent) + 1.0);
            }else{
                totalInteractions.put(secondAgent, 1.0);
            }
            if(successfulInteractions.containsKey(secondAgent)) {
                successfulInteractions.put(secondAgent, successfulInteractions.get(secondAgent) + 1.0);
            }else{
                successfulInteractions.put(secondAgent, 1.0);
            }

            //SECOND AGENT
            if(secondAgent.getTotalInteractions().containsKey(this)) {
                secondAgent.getTotalInteractions().put(this, secondAgent.getTotalInteractions().get(this) + 1.0);
            }else{
                secondAgent.getTotalInteractions().put(this, 1.0);
            }
            if(secondAgent.getSuccessfulInteractions().containsKey(this)) {
                secondAgent.getSuccessfulInteractions().put(this, secondAgent.getSuccessfulInteractions().get(this) + 1.0);
            }else{
                secondAgent.getSuccessfulInteractions().put(this, 1.0);
            }
        } else {

            //AGENTS DID NOT AGREE

            //We count only the decision of the more accurate agent
            multitaskGame.setDelegated(true);

            MultitaskingAgent winner = p.getKey();
            MultitaskingAgent adapter = p.getValue();

            multitaskGame.setWinner(winner);
            multitaskGame.setLoser(adapter);

            BitSet adapterInstance = (adapter == this) ? environment.getBitSet(firstAgentInstanceWithUncertainty.toString()) : environment.getBitSet(secondAgentInstanceWithUncertainty.toString());

            MultitaskDecision winnerMultitaskDecisionOnAdapterInstance = (winner == this) ? this.getClassifier().getClass(adapterInstance) : secondAgent.getClassifier().getClass(adapterInstance);
            boolean isWinnerDecisionOnAdapterInstanceRandom = false;
            if (!winnerMultitaskDecisionOnAdapterInstance.hasTask(multitaskGame.getTask())) {
                isWinnerDecisionOnAdapterInstanceRandom = true;
                winnerMultitaskDecisionOnAdapterInstance.getDecision(multitaskGame.getTask(), winner.getUnknownDecisionStrategy());
            }

            int adaptersDecisionBeforeAdaptation = (adapter == this) ? firstAgentDecisionUB.getDecision(multitaskGame.getTask(), this.getUnknownDecisionStrategy()) : secondAgentDecisionUB.getDecision(multitaskGame.getTask(), secondAgent.getUnknownDecisionStrategy());

            MultitaskTreeClassifier.MultiCondition condition = adapter.getAdaptationCondition(adapterInstance, winner, multitaskGame.getTask());

            adapter.setTaskTotalLosses(((MultitaskGame) game).getTask(), adapter.getTaskTotalLosses(((MultitaskGame) game).getTask()) + 1);

            MultitaskTreeClassifier.MultiChanger changer = null;

            changer = adapter.adaptKnowledge(condition, adapterInstance, winnerMultitaskDecisionOnAdapterInstance, multitaskGame);
            multitaskGame.setAdaptation(changer);
            adapter.setTaskTotalAdaptations(((MultitaskGame) game).getTask(), adapter.getTaskTotalAdaptations(((MultitaskGame) game).getTask()) + 1);

            int adapterDecisionAfterAdaptation = adapter.getClassifier().getClass(adapterInstance).getDecision(multitaskGame.getTask());

            //DID DECISION CHANGE?
            boolean actualAdaptation = adaptersDecisionBeforeAdaptation != adapterDecisionAfterAdaptation;

            if (actualAdaptation) {
                adapter.getActualAdaptations().add(multitaskGame);
                multitaskGame.setWinnerDecisionRandom(isWinnerDecisionOnAdapterInstanceRandom);
            }

            multitaskGame.setActualAdaptation(actualAdaptation);

            //TODO BUSINESS AS USUAL FOR NOT PROXY EXPERIMENTS
            multitaskGame.setSuccessWithUncertainBits(false);
            secondAgent.addFailure(instance);
            this.addFailure(instance);

            //ME
            if(totalInteractions.containsKey(secondAgent)) {
                totalInteractions.put(secondAgent, totalInteractions.get(secondAgent) + 1);
            }else{
                totalInteractions.put(secondAgent, 1.0);
            }

            //SECOND AGENT
            if(secondAgent.getTotalInteractions().containsKey(this)) {
                secondAgent.getTotalInteractions().put(this, secondAgent.getTotalInteractions().get(this) + 1);
            }else{
                secondAgent.getTotalInteractions().put(this, 1.0);
            }

        }

        //SET IF DECISION WAS CORRECT FOR THIS GAME
        MultitaskingAgent winner = p.getKey();
        MultitaskingAgent adapter = p.getValue();

        int winnerDecision = winner.getClassifier().getClass(instance).getDecision(multitaskGame.getTask(), this.getUnknownDecisionStrategy());
        EnvironmentExample environmentExample = ((MultitaskLLExperiment) multitaskGame.getExperiment()).getEnvironment().getEnvironmentExamples().get(instance);
        multitaskGame.setDecisionCorrect(winnerDecision==environmentExample.getMultiDecision().getDecision(multitaskGame.getTask()));

        if(multitaskGame.isDecisionCorrect()){
            if(multitaskGame.isDelegated()){
                winner.setCompensation(winner.getCompensation()+defaultCompensation*2);
            }else{
                winner.setCompensation(winner.getCompensation()+defaultCompensation);
                adapter.setCompensation(adapter.getCompensation()+defaultCompensation);
            }
        }

        firstAgentInstanceWithUncertainty = null;
        secondAgentInstanceWithUncertainty = null;
    }

    public MultitaskTreeClassifier.MultiChanger adaptKnowledge(MultitaskTreeClassifier.MultiCondition condition, BitSet instance, MultitaskDecision winnerMultitaskDecision, MultitaskGame game) throws NoClassException {

        MultitaskTreeClassifier.MultiCondition myCondition = getClassifier().getCondition(instance);
        MultitaskDecision adapterMultitaskDecision = getClassifier().getClass(instance);

        MultitaskTreeClassifier.MultiChanger changer = null;

        MultitaskTreeClassifier.MultiChange change;

        if (condition != null) {
            MultitaskTreeClassifier.MultiCondition reverse = condition.copy();
            reverse.not();
            if (reverse.subsumes(myCondition)) {
                System.out.println("Subsumes myCondition");
            }
        }

        double maximumClasses = Math.pow(2.0, ((MultitaskLLExperiment) game.getExperiment()).getEnvironment().getNumberOfFeatures());
        double actualClasses = TreeUtils.getLeafs(this.getClassifier().getRoot()).size();

        boolean eligibleForSplit = actualClasses < maximumClasses*this.getKnowledgeLimit();

        //System.out.println("Eligible for split: "+eligibleForSplit);

        if (condition == null || condition.subsumes(myCondition)) {
            if(actualSubstitutions<=maxSubstitutions) {
                changer = new MultiTaskNodeClassChanger(instance, winnerMultitaskDecision, game.getTask());
                change = classifier.applyModification(changer);
                game.setChange(change);
                actualSubstitutions++;
            }
        } else {
            if(eligibleForSplit || this.getKnowledgeLimit()==1.0) {
                //System.out.println("I was eligible for split");
                changer = new SplitMultiTaskNodeChanger(adaptation, instance, condition, winnerMultitaskDecision, adapterMultitaskDecision, game.getTask());
                change = classifier.applyModification(changer);
                game.setChange(change);
            }else{

                MultitaskTreeClassifier.MultiTaskNode mergeRoot = null;

                if(forgettingMechanism == ForgettingMechanism.GENERALIZATION){
                    //TODO Merge2 CORRESPONDS TO JEROME'S VERSION OF GENERALIZATION. DECISION MUST BE THE SAME FOR ALL TACKLED TASKS AND NOT ONLY FOR THE CURRENT ONE!
                    mergeRoot = TreeUtils.getRandomGeneralizableNodeToMerge2(this.getClassifier().getRoot(), this.getPreferredTasks());
                }else if(forgettingMechanism==ForgettingMechanism.RANDOM){
                    mergeRoot = TreeUtils.getRandomNodeToMerge(this.getClassifier().getRoot());
                }else if(forgettingMechanism==ForgettingMechanism.PROBABILISTIC_GENERALIZATION){
                    if(this.getGeneralizableTasks()==null || this.getGeneralizableTasks().isEmpty()) {
                        this.initGeneralizableTasks();
                    }
                    //TODO check whether we prefer normalized percentages for the probabilities
                    mergeRoot = TreeUtils.getRandomGeneralizableNodeToMerge2(this.getClassifier().getRoot(), this.getGeneralizableTasks());
                }else if(forgettingMechanism==ForgettingMechanism.MOST_ACCURATE_TASK_GENERALIZATION){
                    if(this.getGeneralizableTasks()==null || this.getGeneralizableTasks().isEmpty()) {
                        this.initBestTask();
                    }
                    mergeRoot = TreeUtils.getRandomGeneralizableNodeToMerge2(this.getClassifier().getRoot(), this.getBestTask());
                } else {
                    mergeRoot = null;
                }

//                if(mergeRoot!=null){
//                    System.out.println("mergeRoot was not null!!");
//                }

                int mergedSplits = 0;

                if(this.getMoney()>0){
                    this.setMoney(this.getMoney()-1);
                    game.getWinner().setMoney(game.getWinner().getMoney()+1);
                    mergeRoot = null;
                }else{
                    this.setMoney(this.getMoney()+1);
                    game.getWinner().setMoney(game.getWinner().getMoney()-1);
                }

                while(mergeRoot!=null && actualMerges<=maxMerges) {

                    mergedSplits++;

                    //System.out.println("Actual merges:"+actualMerges);

                    //TODO BUSINESS AS USUAL
                    changer = new MergeMultiTaskNodeChanger(mergeRoot, instance, winnerMultitaskDecision, game.getTask());
                    change = classifier.applyModification(changer);
                    game.setChange(change);

                    actualMerges++;

                    if(forgettingMechanism == ForgettingMechanism.GENERALIZATION){
                        //TODO Merge2 CORRESPONDS TO JEROME'S VERSION OF GENERALIZATION. DECISION MUST BE THE SAME FOR ALL TACKLED TASKS AND NOT ONLY FOR THE CURRENT ONE!
                        mergeRoot = TreeUtils.getRandomGeneralizableNodeToMerge2(this.getClassifier().getRoot(), this.getPreferredTasks());
                    }else{
                        mergeRoot = TreeUtils.getRandomNodeToMerge(this.getClassifier().getRoot());
                    }

                }
                if(mergedSplits>0){
                    //System.out.println("didMerges:"+mergedSplits);
                    MultitaskTreeClassifier.MultiChanger changer2 = new SplitMultiTaskNodeChanger(adaptation, instance, condition, winnerMultitaskDecision, adapterMultitaskDecision, game.getTask());
                    MultitaskTreeClassifier.MultiChange change2 = classifier.applyModification(changer2);
                    game.setChange(change2);
                } else{
                    if(actualSubstitutions<=maxSubstitutions) {
                        changer = new MultiTaskNodeClassChanger(instance, winnerMultitaskDecision, game.getTask());
                        change = classifier.applyModification(changer);
                        game.setChange(change);
                        actualSubstitutions++;
                    }
                }
            }
        }


        return changer;
    }

    protected void addSuccess(BitSet instance) {
        try {
            MultitaskTreeClassifier.MultiTaskNode n = this.getClassifier().getNode(instance);
            int tried = 0;
            if (tries.containsKey(n)) {
                tried = tries.get(n);
            }
            tries.put(n, tried + 1);
            int success = 0;
            if (successful.containsKey(n)) {
                success = successful.get(n);
            }
            successful.put(n, success + 1);
        } catch (NoClassException e) {
            e.printStackTrace();
        }
    }

    protected void addFailure(BitSet instance) {
        try {
            MultitaskTreeClassifier.MultiTaskNode n = this.getClassifier().getNode(instance);
            int tried = 0;
            if (tries.containsKey(n)) {
                tried = tries.get(n);
            }
            tries.put(n, tried + 1);
        } catch (NoClassException e) {
            e.printStackTrace();
        }
    }

    public MultitaskTreeClassifier getClassifier() {
        return classifier;
    }

    public void setClassifier(MultitaskTreeClassifier classifier) {
        this.classifier = classifier;
    }

    public double getScore(BitSet instance) {
        if (!useNodeConf) {
            return score;
        }
        try {
            MultitaskTreeClassifier.MultiTaskNode n = getClassifier().getNode(instance);
            if (tries.containsKey(n)) {
                double success = successful.getOrDefault(n, 0);
                return (success / (tries.get(n)));
            }
        } catch (NoClassException e) {
            e.printStackTrace();
        }
        return 0.5;
    }

    public MultitaskTreeClassifier.MultiCondition getAdaptationCondition(BitSet instance, MultitaskingAgent winner, Task t) throws NoClassException { // called by adapter agent
        if (adaptOp.equals("allCom")) {
            return winner.getClassifier().getCondition(instance);
        } else if (adaptOp.equals("oneCom")) {
            List<MultitaskTreeClassifier.MultiCondition> conditions = winner.getConditionsToInstance(instance);
            Random r = new Random();
            if (conditions.isEmpty())
                return null;
            else {
                //System.out.println(conditions.get(r.nextInt(conditions.size())).toString());
                return conditions.get(r.nextInt(conditions.size())); // one random condition from winner's conditions
            }
        }else if (adaptOp.equals("taskCom")) {
            List<MultitaskTreeClassifier.MultiCondition> conditions = winner.getConditionsToInstance(instance);

            Random r = new Random();
            if (conditions.isEmpty())
                return null;
            else {
                return conditions.get(r.nextInt(conditions.size())); // one random condition from winner's conditions
            }
        }

        else { // adaptOp == oneNoCom
            List<MultitaskTreeClassifier.MultiCondition> conditions = getConditionsFromInstance(instance);
            Random r = new Random();
            if (conditions.isEmpty())
                return null;
            else
                return conditions.get(r.nextInt(conditions.size())); // one random condition from instance that do not exist in this.classifier
        }
    }

    public List<MultitaskTreeClassifier.MultiCondition> getConditionsToInstance(BitSet instance) throws NoClassException {
        MultitaskTreeClassifier.MultiTaskNode n = getClassifier().getNode(instance);
        List<MultitaskTreeClassifier.MultiCondition> conditions = new ArrayList<>();
        while (n.getParent() != null) {
            conditions.add(n.getCondition());
            n = n.getParent();
        }
        return conditions;
    }

    public List<MultitaskTreeClassifier.MultiCondition> getConditionsFromInstance(BitSet instance) throws NoClassException {
        MultitaskTreeClassifier.MultiNodeCreator nodeCreator = getClassifier().getNodeCreator();
        List<MultitaskTreeClassifier.MultiCondition> conditions = new ArrayList<>();

        for (int i = 0; i < environment.getNumberOfFeatures(); i++) {
            MultitaskTreeClassifier.MultiCondition cond = nodeCreator.multitaskNode(null, i, instance.get(i), null).getCondition();
            conditions.add(cond);
        }
        return conditions;
    }

    public void setScore(double score) {
        this.score = score;
    }

    public double getAccuracy(ArrayList<Task> tasks, ArrayList<EnvironmentExample> examples) {

        double accuracy;

        HashMap<Task, Double> taskCorrect = new HashMap<>();
        HashMap<Task, Double> taskTries = new HashMap<>();

        double correct = 0;
        double tries = 0;

        for (int i = 0; i < examples.size(); i++) {

            BitSet instance = examples.get(i).getInstance();

            MultitaskDecision correctDecisions = examples.get(i).getMultiDecision();

            try {

                MultitaskDecision predicted = this.getClassifier().getClass(instance);

                for (int j = 0; j < tasks.size(); j++) {

                    if (correctDecisions.hasTask(tasks.get(j))) {

                        if (predicted.hasTask(tasks.get(j)) && (correctDecisions.getDecision(tasks.get(j)) == predicted.getDecision(tasks.get(j)))) {
                            if (taskCorrect.containsKey(tasks.get(j))) {
                                taskCorrect.put(tasks.get(j), taskCorrect.get(tasks.get(j)) + 1.0);
                            } else {
                                taskCorrect.put(tasks.get(j), 1.0);
                            }
                            correct++;
                        }

                        if (taskTries.containsKey(tasks.get(j))) {
                            taskTries.put(tasks.get(j), taskTries.get(tasks.get(j)) + 1.0);
                        } else {
                            taskTries.put(tasks.get(j), 1.0);
                        }

                        tries++;
                    }else{
                        System.out.println("It did not have the task in question!");
                    }
                }

            } catch (NoClassException e) {
                e.printStackTrace();
            }

        }

        accuracy = tries != 0 ? (correct / tries) : 0;

        for (Map.Entry<Task, Double> entrySet : taskCorrect.entrySet()) {
            double taskAccuracy = entrySet.getValue() / taskTries.get(entrySet.getKey());
            taskScores.put(entrySet.getKey(), taskAccuracy);
            taskSpecialization.put(entrySet.getKey(), taskAccuracy / accuracy);
        }

        return accuracy;
    }

    public void setUnknownDecisionStrategy(UnknownDecisionStrategy decisionStrategy) {
        this.unknownDecisionStrategy = decisionStrategy;
    }

    public MultitaskingAgent.UnknownDecisionStrategy getUnknownDecisionStrategy() {
        return this.unknownDecisionStrategy;
    }

    public void setWinningCondition(WinningCondition winningCondition) {
        this.winningCondition = winningCondition;
    }

    public WinningCondition getWinningCondition() {
        return this.winningCondition;
    }

    public double getTaskSuccessfulAttempts(Task t) {
        if (taskSuccessfulAttempts.containsKey(t)) {
            //System.out.println("Getting specialized success : "+ taskSuccessfulAttempts.get(t));
            return taskSuccessfulAttempts.get(t);
        } else {
            return 0;
        }
    }

    public void setTaskSuccessfulAttempts(Task t, double d) {
        //System.out.println("Setting specialized success : "+d);
        taskSuccessfulAttempts.put(t, d);
    }

    public double getTaskSuccessRate(Task t) {
        if (taskSuccessfulAttempts.containsKey(t) && taskTotalGames.containsKey(t)) {
            return taskSuccessfulAttempts.get(t) / taskTotalGames.get(t);
        } else {
            return 0;
        }
    }

    public double getTaskScore(Task t) {
        if (taskScores.containsKey(t)) {
            //System.out.println("Get specialized score : " + taskScores.get(t));
            return taskScores.get(t);
        }
        return -1;
    }

    public boolean acceptsGame(MultitaskGame multitaskGame) {
        if (experiment instanceof SelectiveAcceptanceSpecializationExperiment || experiment instanceof DelegationSpecializationExperiment) {

            if (!specialized || adaptingTasksRanking.get(multitaskGame.getTask()) <= maxAdaptationRank) {
                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }
    }


    public HashMap<Task, Integer> rankTasks(ArrayList<Task> candidateTasks, boolean isSpecialized) {
        HashMap<Task, Integer> taskTable = new HashMap<>();
        if (isSpecialized && deficientSensors.size() > 0) {
            for (int i = 0; i < candidateTasks.size(); i++) {

                int localMin = deficientSensors.size();
                Task localMinTask = null;

                for (int j = 0; j < candidateTasks.size(); j++) {
                    int impactingDeficientSensors = 0;
                    for (int deficientSensor : deficientSensors) {
                        if (!candidateTasks.get(j).getIrrelevantFeatures().contains(deficientSensor)) {
                            impactingDeficientSensors++;
                        }
                        if (impactingDeficientSensors <= localMin && taskTable.containsKey(candidateTasks.get(j))) {
                            localMin = impactingDeficientSensors;
                            localMinTask = candidateTasks.get(j);
                        }
                    }
                }

                taskTable.put(localMinTask, i);

            }
        } else {
            for (int i = 0; i < candidateTasks.size(); i++) {
                taskTable.put(candidateTasks.get(i), 0);
            }
        }

        return taskTable;
    }


    public HashMap<Task, Integer> rankTasks(ArrayList<Task> candidateTasks, boolean isSpecialized, boolean accountSuccessRate) {
        if(isSpecialized && taskSelectionStrategy==TaskSelectionStrategy.BALANCED){
            //System.out.println("Entered: isSpecialized && taskSelectionStrategy==TaskSelectionStrategy.BALANCED");
            if(experiment instanceof SpecializationLLExperiment){
                SpecializationLLExperiment specExp = (SpecializationLLExperiment) experiment;
                //System.out.println("Number of permutations: "+specExp.getPreferencesPermutations().size());
                return specExp.getPreferencesPermutations().get(this.getId());
            }else{
                ArrayList<Task> shuffleList = new ArrayList<>();
                shuffleList.addAll(candidateTasks);
                Collections.shuffle(shuffleList);
                HashMap<Task, Integer> taskTable = new HashMap<>();
                for (int i = 0; i < shuffleList.size(); i++) {
                    taskTable.put(shuffleList.get(i), i);
                }
                return taskTable;
            }
        } else if(isSpecialized && taskSelectionStrategy==TaskSelectionStrategy.SIMPLISTIC){
            //System.out.println("Entered: isSpecialized && taskSelectionStrategy==TaskSelectionStrategy.SIMPLISTIC");
            ArrayList<Task> shuffleList = new ArrayList<>();
            shuffleList.addAll(candidateTasks);
            Collections.shuffle(shuffleList);
            HashMap<Task, Integer> taskTable = new HashMap<>();
            for (int i = 0; i < shuffleList.size(); i++) {
                taskTable.put(shuffleList.get(i), i);
            }
            return taskTable;
        }else if (isSpecialized && (taskSelectionStrategy==TaskSelectionStrategy.MAXIMIN || taskSelectionStrategy==TaskSelectionStrategy.MAXIMAX && taskSelectionStrategy==TaskSelectionStrategy.MINIMAX_REGRET)) {
            //System.out.println("Entered: isSpecialized && (taskSelectionStrategy==TaskSelectionStrategy.MAXIMIN || taskSelectionStrategy==TaskSelectionStrategy.MAXIMAX && taskSelectionStrategy==TaskSelectionStrategy.MINIMAX_REGRET");

            HashMap<Task, Integer> maximaxTable = new HashMap<>();

            HashMap<Task, Integer> maximinTable = new HashMap<>();

            HashMap<Task, Integer> maximinRegretTable = new HashMap<>();

            double globalMaxReward = -10000;
            double globalMinPenalty = 10000;

            for (int i = 0; i < candidateTasks.size(); i++) {

                double maxReward = -10000;
                double minPenalty = -10000;

                Task maxRewardTask = null;
                Task minPenaltyTask = null;

                for (int j = 0; j < candidateTasks.size(); j++) {

                    if (candidateTasks.get(j).getReward() * getTaskRewardCoefficient(candidateTasks.get(j)) * ((accountSuccessRate) ? this.getTaskSuccessRate(candidateTasks.get(j)) : 1) >= maxReward && !maximaxTable.containsKey(candidateTasks.get(j))) {
                        maxRewardTask = candidateTasks.get(j);
                        maxReward = maxRewardTask.getReward() * getTaskRewardCoefficient(candidateTasks.get(j)) * ((accountSuccessRate) ? this.getTaskSuccessRate(candidateTasks.get(j)) : 1);
                    }

                    if (candidateTasks.get(j).getPenalty() * getTaskPenaltyCoefficient(candidateTasks.get(j)) * ((accountSuccessRate) ? 1.0 - this.getTaskSuccessRate(candidateTasks.get(j)) : 1) >= minPenalty && !maximinTable.containsKey(candidateTasks.get(j))) {
                        minPenaltyTask = candidateTasks.get(j);
                        minPenalty = minPenaltyTask.getPenalty() * getTaskPenaltyCoefficient(candidateTasks.get(j)) * ((accountSuccessRate) ? 1.0 - this.getTaskSuccessRate(candidateTasks.get(j)) : 1);
                    }

                }

                maximaxTable.put(maxRewardTask, i);
                maximinTable.put(minPenaltyTask, i);

                globalMaxReward = maxRewardTask.getReward() * getTaskRewardCoefficient(maxRewardTask) * ((accountSuccessRate) ? this.getTaskSuccessRate(maxRewardTask) : 1);
                globalMinPenalty = minPenaltyTask.getPenalty() * getTaskPenaltyCoefficient(minPenaltyTask) * ((accountSuccessRate) ? 1.0 - this.getTaskSuccessRate(minPenaltyTask) : 1);

            }

            HashMap<Task, Double> regretTable = new HashMap<>();

            for (int i = 0; i < candidateTasks.size(); i++) {
                double currentRegret = (globalMaxReward - candidateTasks.get(i).getReward() * getTaskRewardCoefficient(candidateTasks.get(i)) * ((accountSuccessRate) ? this.getTaskSuccessRate(candidateTasks.get(i)) : 1)) + (globalMinPenalty - candidateTasks.get(i).getPenalty() * getTaskPenaltyCoefficient(candidateTasks.get(i)) * ((accountSuccessRate) ? 1.0 - this.getTaskSuccessRate(candidateTasks.get(i)) : 1));
                regretTable.put(candidateTasks.get(i), currentRegret);
            }

            int counter = 0;

            while (counter < candidateTasks.size()) {

                Task minRegretTask = regretTable.entrySet().iterator().next().getKey();
                double minRegret = regretTable.get(minRegretTask);

                for (Map.Entry<Task, Double> regretEntry : regretTable.entrySet()) {
                    if (regretEntry.getValue() < minRegret) {
                        minRegret = regretEntry.getValue();
                        minRegretTask = regretEntry.getKey();
                    }
                }

                maximinRegretTable.put(minRegretTask, counter);
                regretTable.remove(minRegretTask);
                counter++;

            }

            switch (taskSelectionStrategy) {
                case MAXIMAX:
                    return maximaxTable;
                case MAXIMIN:
                    return maximinTable;
                case MINIMAX_REGRET:
                    return maximinRegretTable;
                default:
                    return maximinRegretTable;
            }
        } else {
            //System.out.println("Entered: else");
            HashMap<Task, Integer> taskTable = new HashMap<>();
            for (int i = 0; i < candidateTasks.size(); i++) {
                taskTable.put(candidateTasks.get(i), 0);
            }
            return taskTable;
        }
    }

    public HashMap<Task, Double> getTaskRewardCoefficients() {
        return this.taskRewardCoefficients;
    }

    public double getTaskRewardCoefficient(Task t) {
        if (taskRewardCoefficients.containsKey(t)) {
            return taskRewardCoefficients.get(t);
        } else {
            return 0;
        }
    }

    public double getTaskPenaltyCoefficient(Task t) {
        if (taskPenaltyCoefficients.containsKey(t)) {
            return taskPenaltyCoefficients.get(t);
        } else {
            return 0;
        }
    }

    public void setTaskRewardCoefficient(Task t, double rewardCoefficient) {
        if (taskRewardCoefficients.containsKey(t)) {
            taskRewardCoefficients.put(t, rewardCoefficient);
        }
    }

    public void setTaskPenaltyCoefficient(Task t, double penaltyCoefficient) {
        if (taskPenaltyCoefficients.containsKey(t)) {
            taskPenaltyCoefficients.put(t, penaltyCoefficient);
        }
    }

    public double getAccuracyRelativeStandardDeviation() {
        if (!taskTotalGames.isEmpty()) {
            double sum = 0.0;
            double dev = 0.0;

            for (Task task : environment.getEnvironmentTasks().values()) {
                if (taskScores.containsKey(task)) {
                    sum += taskScores.get(task);
                }
            }

            double mean = sum / taskTotalGames.size();

            for (Task task : environment.getEnvironmentTasks().values()) {
                if (taskScores.containsKey(task)) {
                    dev += Math.pow(taskScores.get(task) - mean, 2);
                }
            }

            return (mean != 0) ? 100 * Math.sqrt(dev / taskTotalGames.size()) / mean : 0;
        } else {
            return 0;
        }
    }

    public double getSuccessRateRelativeStandardDeviation() {
        if (!taskTotalGames.isEmpty()) {

            double sum = 0.0;
            double dev = 0.0;

            for (Task task : environment.getEnvironmentTasks().values()) {
                if (taskTotalGames.containsKey(task)) {
                    sum += taskSuccessfulAttempts.get(task) / taskTotalGames.get(task);
                }
            }

            double mean = sum / taskSuccessfulAttempts.size();

            for (Task task : environment.getEnvironmentTasks().values()) {
                if (taskTotalGames.containsKey(task)) {
                    dev += Math.pow(taskSuccessfulAttempts.get(task) / taskTotalGames.get(task) - mean, 2);
                }
            }

            return (mean != 0) ? 100 * Math.sqrt(dev / taskTotalGames.size()) / mean : 0;
        } else {
            return 0;
        }
    }

    public double getAttemptsRelativeStandardDeviation() {
        if (!taskTotalGames.isEmpty()) {

            double sum = 0.0;
            double dev = 0.0;

            for (double taskAttempts : taskTotalGames.values()) {
                sum += taskAttempts;
            }

            double mean = sum / taskTotalGames.size();

            for (double taskAttempts : taskTotalGames.values()) {
                dev += Math.pow(taskAttempts - mean, 2);
            }

            return (mean != 0) ? 100 * Math.sqrt(dev / taskTotalGames.size()) / mean : 0;
        } else {
            System.out.println("Task total games is empty!!");
            return 0;
        }
    }

    public void setSpecialized(boolean specialized) {
        this.specialized = specialized;
    }

    public boolean isSpecialized() {
        return specialized;
    }

    public HashMap<MultitaskingAgent, Double> getAcquaintances() {
        return acquaintances;
    }

    public boolean hasAcquaintance(MultitaskingAgent acquaintance) {
        return acquaintances.containsKey(acquaintance);
    }

    public void addAcquaintance(MultitaskingAgent acquaintance, double d) {
        acquaintances.put(acquaintance, d);
    }

    public void removeAcquaintance(MultitaskingAgent acquaintance) {
        acquaintances.remove(acquaintance);
    }

    private MultitaskingAgent.TaskSelectionStrategy getRandomTaskSelectionStrategy() {
        int coin = ThreadLocalRandom.current().nextInt(1, 4);
        switch (coin) {
            case 1:
                return MultitaskingAgent.TaskSelectionStrategy.MAXIMAX;
            case 2:
                return MultitaskingAgent.TaskSelectionStrategy.MAXIMIN;
            case 3:
                return MultitaskingAgent.TaskSelectionStrategy.MINIMAX_REGRET;
            default:
                return MultitaskingAgent.TaskSelectionStrategy.MINIMAX_REGRET;
        }
    }

    public double getNumberOfTackledTasks(){
        return taskTotalGames.size();
    }

    public ArrayList<MultitaskGame> getActualAdaptations() {
        return actualAdaptations;
    }

    public void addDeficientFeature(int feature) {
        deficientSensors.add(feature);
    }

    public HashSet<Integer> getAllDeficientSensors() {
        return deficientSensors;
    }

    public boolean isDeficient(int feature) {
        return deficientSensors.contains(feature);
    }

    public String toString(ArrayList<Task> tasks, ArrayList<EnvironmentExample> examples){
        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append("\n").append("==================================================").append("\n");
        stringBuilder.append(isSpecialized() ? "Specialized " : "Multitasking ").append("agent: ").append(this.getId()).append("\n");
        stringBuilder.append("==================================================").append("\n");
        stringBuilder.append("\n").append("Winning condition: ");
        stringBuilder.append(this.getWinningCondition().toString());
        stringBuilder.append(" task selection strategy: ");
        stringBuilder.append(this.getTaskSelectionStrategy().toString());
        stringBuilder.append(" unknown decision strategy: ");
        stringBuilder.append(this.getUnknownDecisionStrategy().toString());
        stringBuilder.append(" deficient sensors: ").append(deficientSensors.toString());
        if(experiment instanceof SpecializationLLExperiment) {
            stringBuilder.append(" average preferred tasks accuracy: ").append(this.getAccuracy(this.getPreferredTasks(), examples));
        }
        stringBuilder.append(" average accuracy: ").append(this.getAccuracy(tasks, examples));
        stringBuilder.append(" money: ").append(this.getMoney()).append("\n");

        //stringBuilder.append("Accuracy relative standard deviation: ").append(this.getAccuracyRelativeStandardDeviation()).append(" games relative standard deviation: ").append(this.getAttemptsRelativeStandardDeviation()).append(" success rate relative standard deviation: ").append(this.getSuccessRateRelativeStandardDeviation()).append("\n");
        for (int i = 0; i < tasks.size(); i++) {
            stringBuilder.append("\n");
            stringBuilder.append((favoriteTask!=null && favoriteTask==tasks.get(i)) ? "*" : " ").append(tasks.get(i).getName()).append((favoriteTask!=null) ? " rank: "+adaptingTasksRanking.get(tasks.get(i)) : "").append(" reward: ").append(tasks.get(i).getReward()).append(" reward coefficient: ").append(getTaskRewardCoefficient(tasks.get(i))).append(" penalty: ").append(tasks.get(i).getPenalty()).append(" penalty coefficient: ").append(getTaskPenaltyCoefficient(tasks.get(i))).append(" total games: ").append(getTaskTotalGames(tasks.get(i))).append(" losses: ").append(getTaskTotalLosses(tasks.get(i))).append(" adaptations: ").append(getTaskTotalAdaptations(tasks.get(i))).append(" task accuracy: ").append(this.getAccuracy(new ArrayList<>(Collections.singleton(tasks.get(i))), examples)).append(" task success rate: ").append(this.getTaskSuccessRate(tasks.get(i))).append("\n");
        }

        stringBuilder.append("\n");
        stringBuilder.append(this.getClassifier()).append("\n");
        stringBuilder.append("\n").append("\n");

        return stringBuilder.toString();
    }

    public void setKnowledgeLimit(double knowledgeLimit){
        this.knowledgeLimit = knowledgeLimit;
    }

    public double getKnowledgeLimit(){
        return this.knowledgeLimit;
    }

    public void clear(){
        if(taskRewardCoefficients!=null){
            taskRewardCoefficients.clear();
        }

        if(taskPenaltyCoefficients!=null){
            taskPenaltyCoefficients.clear();
        }

        if(taskScores!=null){
            taskScores.clear();
        }

        if(taskSpecialization!=null){
            taskSpecialization.clear();
        }

        if(acquaintances!=null){
            acquaintances.clear();
        }

        if(successful!=null){
            successful.clear();
        }

        if(tries!=null){
            tries.clear();
        }

        if(taskSuccessfulAttempts!=null){
            taskSuccessfulAttempts.clear();
        }

        if(taskTotalGames!=null){
            taskTotalGames.clear();
        }

        if(taskTotalAdaptations!=null){
            taskTotalAdaptations.clear();
        }

        if(taskTotalLessons!=null){
            taskTotalLessons.clear();
        }

        if(taskTotalLosses!=null){
            taskTotalLosses.clear();
        }

        this.setClassifier(new MultitaskTreeClassifierQuick(classesLimit));
    }

    HashMap<MultitaskingAgent, Double> getTotalInteractions(){
        if(totalInteractions==null)
            totalInteractions = new HashMap<MultitaskingAgent, Double>();
        return this.totalInteractions;
    }

    HashMap<MultitaskingAgent, Double> getSuccessfulInteractions(){
        if(successfulInteractions==null){
            successfulInteractions = new HashMap<MultitaskingAgent, Double>();
        }
        return this.successfulInteractions;
    }

    public double getCompensation(){
        return this.compensation;
    }

   public void setCompensation(double compensation){
        this.compensation = compensation;
   }

   public double getMaxAccuracy(ArrayList<Task> tasks){
        maxAccuracy = 0;
       for (int i = 0; i < tasks.size(); i++) {
           if (taskScores.containsKey(tasks.get(i))) {
               if (taskScores.get(tasks.get(i)) >= maxAccuracy) {
                   maxAccuracy = taskScores.get(tasks.get(i));
               }
           }
       }

       return maxAccuracy;
   }

   public boolean isAlive(){
        return alive;
   }

   public void setAlive(boolean alive){
        this.alive = alive;
   }

   public int getPeriodsAlive(){
        return periodsAlive;
   }

   public void setPeriodsAlive(int periodsAlive){
        this.periodsAlive = periodsAlive;
   }

   public ArrayList<MultitaskingAgent> getParents(){
        return parents;
   }

   public void addParent(MultitaskingAgent newParent){
        parents.add(newParent);
   }

   public ArrayList<TrainingExample> createExamples(ArrayList<EnvironmentExample> environmentExamples){

        HashMap<Task, ArrayList<TrainingExample>> examplesTable = new HashMap<>();

        Task[] tasks = environmentExamples.get(0).getMultiDecision().getTasks().toArray(new Task[environmentExamples.get(0).getMultiDecision().getTasks().size()]);

        for (int i = 0; i < environmentExamples.size(); i++) {

            BitSet currentInstance = environmentExamples.get(i).getInstance();

            for (int j = 0; j < tasks.length; j++) {

                BitSet taskSpecificInstance = (BitSet) currentInstance.clone();

                if(tasks[j].getIrrelevantFeatures().size()>0){

                    //TODO IS IT IMPORTANT TO KEEP THIS?
                    for (int k = 0; k < taskSpecificInstance.size(); k++) {
                        if(tasks[j].getIrrelevantFeatures().contains(k)){
                            taskSpecificInstance.set(k, false);
                        }
                    }

                }

                try {

                    int myDecision = (classifier.getClass(taskSpecificInstance).hasTask(tasks[j])) ? classifier.getClass(taskSpecificInstance).getDecision(tasks[j]) : classifier.getClass(taskSpecificInstance).getDecision(tasks[j], UnknownDecisionStrategy.RANDOM);

                    if(examplesTable.containsKey(tasks[j])){
                        examplesTable.get(tasks[j]).add(new TrainingExample(currentInstance, tasks[j], myDecision));
                    }else{
                        ArrayList<TrainingExample> list = new ArrayList<>();
                        list.add(new TrainingExample(currentInstance, tasks[j], myDecision));
                        examplesTable.put(tasks[j], list);
                    }

                } catch (NoClassException e) {
                    throw new RuntimeException(e);
                }

            }

        }

       HashMap<Task, Double> rawPercentages = new HashMap<>();

       double percentagesSum = 0;

       for (int i = 0; i < tasks.length; i++) {
           double percentage = taskScores.containsKey(tasks[i]) ? taskScores.get(tasks[i]) : 0;
           percentagesSum += percentage;
           rawPercentages.put(tasks[i], percentage);
       }

       HashMap<Task, Double> finalPercentages = new HashMap<>();

       for (int i = 0; i < tasks.length; i++) {
           finalPercentages.put(tasks[i], rawPercentages.get(tasks[i])/percentagesSum);
       }

        ArrayList<TrainingExample> finalExamples = new ArrayList<>();

       for (int i = 0; i < tasks.length; i++) {
           ArrayList<TrainingExample> taskExamples = examplesTable.get(tasks[i]);
           Collections.shuffle(taskExamples);
           int counter = (int) Math.round(environmentExamples.size()*tasks.length*finalPercentages.get(tasks[i])*experiment.getRatio()*0.5);
           finalExamples.addAll(taskExamples.subList(0,counter));
       }

        return finalExamples;
    }

    public Task getFavoriteTask(){
        return favoriteTask;
    }

    public void setFavoriteTask(Task favoriteTask){
        this.favoriteTask = favoriteTask;
    }

}
