/*
 * Copyright (C) INRIA, 2021-2024
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 */

package fr.inria.exmo.lazylavender.decisiontaking.multitask;

import fr.inria.exmo.lazylavender.decisiontaking.measurements.Recorder;
import fr.inria.exmo.lazylavender.expe.LLGame;

import java.util.List;
import java.util.Comparator;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Collections;
import java.util.Collection;
import java.util.LinkedList;

/**
 * Class containing multi-task experiment measure recorders.
 */
public class MultitaskRecorders {

    static abstract public class MultiDolaRecorder implements Recorder {

        protected List<String> names;

        public MultiDolaRecorder(String... names) {
            initNames(Arrays.asList(names));
        }

        void initNames(List<String> names) {
            this.names = new ArrayList<>();
            this.names.addAll(names);
        }

        @Override
        public List<String> getNames() {
            return names;
        }
    }

    /**
     * TOTAL POPULATION COMPENSATION
     */


    static public class TotalCompensation extends MultiDolaRecorder implements RecorderUpdater {

        protected Map<MultitaskingAgent, Double> lastCompensations = new HashMap<>();

        protected double sum;

        public TotalCompensation(List<MultitaskingAgent> multitaskingAgents, String name) {
            super(name);

            for (MultitaskingAgent agent : multitaskingAgents) {
                lastCompensations.put(agent, agent.getCompensation());
                sum += agent.getCompensation();
            }
        }

        @Override
        public void update(LLGame game) {
            MultitaskGame g = (MultitaskGame) game;

            MultitaskingAgent firstAgent = (MultitaskingAgent) g.getFirstAgent();
            MultitaskingAgent secondAgent = (MultitaskingAgent) g.getSecondAgent();

            if (lastCompensations.containsKey(firstAgent)) {
                sum -= lastCompensations.get(firstAgent);
                lastCompensations.put(firstAgent, firstAgent.getCompensation());
                sum += firstAgent.getCompensation();
            }

            if (lastCompensations.containsKey(secondAgent)) {
                sum -= lastCompensations.get(secondAgent);
                lastCompensations.put(secondAgent, secondAgent.getCompensation());
                sum += secondAgent.getCompensation();
            }

        }

        @Override
        public List<Double> record(LLGame game) {
            return Arrays.asList(sum);
        }

        @Override
        public void updateRecorder(ArrayList<MultitaskingAgent> births, ArrayList<MultitaskingAgent> deaths) {
            for (MultitaskingAgent newBorn : births) {
                lastCompensations.put(newBorn, newBorn.getCompensation());
                sum += newBorn.getCompensation();
            }
            /*
            for (MultitaskingAgent newDead : deaths){
                sum -= lastCompensations.get(newDead);
                lastCompensations.remove(newDead);
            }

             */
        }
    }

    /**
     * GINI
     */


    static public class DecileRatio extends MultiDolaRecorder implements RecorderUpdater{

        protected double richSum, poorSum;
        protected double richAvg, poorAvg;
        protected double dec;

        private int numberOfRich, numberOfPoor;

        private List<MultitaskingAgent> sortableAgents;

        private Comparator<MultitaskingAgent> comparator;

        private ArrayList<Task> environmentTasks;

        public DecileRatio(ArrayList<Task> environmentTasks, List<MultitaskingAgent> multitaskingAgents, double richPercentage, double poorPercentage, String name, Comparator<MultitaskingAgent> comparator) {
            super(name);

            this.environmentTasks = environmentTasks;

            sortableAgents = new ArrayList<>();
            sortableAgents.addAll(multitaskingAgents);

            this.comparator = comparator;

            Collections.sort(sortableAgents, comparator);

            numberOfRich = (int) Math.ceil(sortableAgents.size()*richPercentage);

            richSum = 0;
            richAvg = 0;
            for (int i = 0; i < numberOfRich; i++) {

                if(comparator instanceof CompensationComparator){
                    richSum += sortableAgents.get(i).getCompensation();
                }else if(comparator instanceof CarriedTasksAccuracyComparator){
                    richSum += sortableAgents.get(i).getAccuracy(sortableAgents.get(i).getPreferredTasks(), ((CarriedTasksAccuracyComparator) comparator).getExamples());
                }else if(comparator instanceof MaximumAccuracyComparator){
                    richSum += sortableAgents.get(i).getMaxAccuracy(environmentTasks);
                }

            }
            richAvg = richSum/(double) numberOfRich;

            numberOfPoor = (int)Math.ceil(sortableAgents.size()*poorPercentage);

            poorSum = 0;
            poorAvg = 0;
            for (int i = sortableAgents.size()-1; i > sortableAgents.size()-numberOfPoor; i--) {

                if(comparator instanceof CompensationComparator){
                    poorSum += sortableAgents.get(i).getCompensation();
                }else if(comparator instanceof CarriedTasksAccuracyComparator){
                    poorSum += sortableAgents.get(i).getAccuracy(sortableAgents.get(i).getPreferredTasks(), ((CarriedTasksAccuracyComparator) comparator).getExamples());
                }else if(comparator instanceof MaximumAccuracyComparator){
                    poorSum += sortableAgents.get(i).getMaxAccuracy(environmentTasks);
                }

            }
            poorAvg = poorSum/(double) numberOfPoor;

            if(poorAvg==0)
                poorAvg=0.01;
            if(richAvg==0)
                richAvg=0.01;

            dec = richAvg/poorAvg;

        }

        @Override
        public void update(LLGame game) {

            Collections.sort(sortableAgents, comparator);

            richSum = 0;
            richAvg = 0;
            for (int i = 0; i < numberOfRich; i++) {

                if(comparator instanceof CompensationComparator){
                    richSum += sortableAgents.get(i).getCompensation();
                }else if(comparator instanceof CarriedTasksAccuracyComparator){
                    richSum += sortableAgents.get(i).getAccuracy(sortableAgents.get(i).getPreferredTasks(), ((CarriedTasksAccuracyComparator) comparator).getExamples());
                }else if(comparator instanceof MaximumAccuracyComparator){
                    richSum += sortableAgents.get(i).getMaxAccuracy(environmentTasks);
                }

            }
            richAvg = richSum/(double) numberOfRich;


            poorSum = 0;
            poorAvg = 0;
            for (int i = sortableAgents.size()-1; i >= sortableAgents.size()-numberOfPoor; i--) {

                if(comparator instanceof CompensationComparator){
                    poorSum += sortableAgents.get(i).getCompensation();
                }else if(comparator instanceof CarriedTasksAccuracyComparator){
                    poorSum += sortableAgents.get(i).getAccuracy(sortableAgents.get(i).getPreferredTasks(), ((CarriedTasksAccuracyComparator) comparator).getExamples());
                }else if(comparator instanceof MaximumAccuracyComparator){
                    poorSum += sortableAgents.get(i).getMaxAccuracy(environmentTasks);
                }

            }
            poorAvg = poorSum/(double) numberOfPoor;

            if(poorAvg==0)
                poorAvg=0.01;
            if(richAvg==0)
                richAvg=0.01;

            dec = richAvg/poorAvg;

        }

        @Override
        public List<Double> record(LLGame game) {
            return Arrays.asList(dec);
        }

        @Override
        public void updateRecorder(ArrayList<MultitaskingAgent> births, ArrayList<MultitaskingAgent> deaths) {
            for(MultitaskingAgent newDead : deaths){
                for (MultitaskingAgent currentAgent : sortableAgents){
                    if(currentAgent.getId()== newDead.getId()){
                        sortableAgents.remove(currentAgent);
                        break;
                    }
                }
            }

            sortableAgents.addAll(births);
        }
    }

    /**
     * AVERAGE LAST ADAPTATION
     */


    static public class AverageLastAdaptation extends MultiDolaRecorder implements RecorderUpdater {

        protected Map<MultitaskingAgent, Double> lastAdaptations = new HashMap<>();

        protected double sum;
        protected double avg;

        public AverageLastAdaptation(List<MultitaskingAgent> multitaskingAgents, String name) {
            super(name);

            for (MultitaskingAgent agent : multitaskingAgents) {
                double agentLastAdaptation = agent.getActualAdaptations().isEmpty() ? 0 : agent.getActualAdaptations().get(agent.getActualAdaptations().size() - 1).getIteration();
                lastAdaptations.put(agent, agentLastAdaptation);
                sum += agentLastAdaptation;
            }
            avg = sum / multitaskingAgents.size();
        }

        @Override
        public void update(LLGame game) {
            MultitaskGame g = (MultitaskGame) game;

            MultitaskingAgent firstAgent = (MultitaskingAgent) g.getFirstAgent();
            MultitaskingAgent secondAgent = (MultitaskingAgent) g.getSecondAgent();

            if (lastAdaptations.containsKey(firstAgent)) {
                sum -= lastAdaptations.get(firstAgent);
                double firstAgentLastAdaptation = firstAgent.getActualAdaptations().isEmpty() ? 0 : firstAgent.getActualAdaptations().get(firstAgent.getActualAdaptations().size() - 1).getIteration();
                lastAdaptations.put(firstAgent, firstAgentLastAdaptation);
                sum += firstAgentLastAdaptation;
            }


            if (lastAdaptations.containsKey(secondAgent)) {
                sum -= lastAdaptations.get(secondAgent);
                double secondAgentAccuracy = secondAgent.getActualAdaptations().isEmpty() ? 0 : secondAgent.getActualAdaptations().get(secondAgent.getActualAdaptations().size() - 1).getIteration();
                lastAdaptations.put(secondAgent, secondAgentAccuracy);
                sum += secondAgentAccuracy;
            }

            avg = sum / lastAdaptations.size();
        }

        @Override
        public List<Double> record(LLGame game) {
            return Arrays.asList(avg);
        }

        @Override
        public void updateRecorder(ArrayList<MultitaskingAgent> births, ArrayList<MultitaskingAgent> deaths) {
            for (MultitaskingAgent newBorn : births) {
                lastAdaptations.put(newBorn, newBorn.getCompensation());
                sum += newBorn.getCompensation();
            }
            for (MultitaskingAgent newDead : deaths){
                sum -= lastAdaptations.get(newDead);
                lastAdaptations.remove(newDead);
            }
        }
    }

    /**
     * COLLISION RATE RECORDER
     */
    static public class CollisionRate extends MultiDolaRecorder {

        protected List<Double> collisionAt = new LinkedList<>();

        public CollisionRate(String name) {
            super(name);
        }

        @Override
        public void update(LLGame game) {
            MultitaskGame g = (MultitaskGame) game;
            double prevRate = 0;
            if (collisionAt.size() != 0)
                prevRate = collisionAt.get(collisionAt.size() - 1);
            collisionAt.add(prevRate + (g.getNumberOfCollisions()));
        }

        @Override
        public List<Double> record(LLGame game) {
            double aRate = collisionAt.get(collisionAt.size() - 1) / ((double)collisionAt.size()*2);
            return Arrays.asList(aRate);
        }

    }

    /**
     * DISTANCE RECORDER
     */

    static public class Distance extends MultiDolaRecorder implements RecorderUpdater{

        protected Map<MultitaskingAgent, Map<MultitaskingAgent, Double>> numEq = new HashMap<>();
        protected ArrayList<MultitaskingAgent> agents = new ArrayList<>();

        public Distance(ArrayList<MultitaskingAgent> agents, String name) {
            super(name);
            this.agents.addAll(agents);
        }

        @Override
        public void update(LLGame game) {
            updateAgentsEqs(agents, (MultitaskGame) game);
        }

        @Override
        public List<Double> record(LLGame game) {
            double avg = 0;
            for (MultitaskingAgent a : agents) {
                double sim = getSim(a, agents);
                avg += sim;
            }
            avg = avg / agents.size();
            return Arrays.asList(1 - avg);
        }


        static public boolean nodeExistsInTree(MultitaskTreeClassifier.MultiTaskNode n, MultitaskTreeClassifier.MultiTaskNode root) {
            MultitaskTreeClassifier.MultiCondition c1 = n.getRecursiveCondition(), c2 = root.getRecursiveCondition();
            if (!c2.subsumes(c1)) {
                c1 = null;
                c2 = null;
                return false;
            }
            else if (c1.subsumes(c2)) {
                c1 = null;
                c2 = null;
                return true;
            }
            for (MultitaskTreeClassifier.MultiTaskNode child : root.getChildren()) {
                if (nodeExistsInTree(n, child)) {
                    c1 = null;
                    c2 = null;
                    return true;
                }
            }
            return false;
        }

        static public double getTreeCommonNodesSize(MultitaskTreeClassifier.MultiTaskNode root1, MultitaskTreeClassifier.MultiTaskNode root2) {
            double existing = 0;

            if (nodeExistsInTree(root1, root2))
                existing += 1;
            for (MultitaskTreeClassifier.MultiTaskNode child : root1.getChildren())
                existing += getTreeCommonNodesSize(child, root2);
            return existing;
        }

        static public double getTreeClassesSize(MultitaskTreeClassifier.MultiTaskNode root) {
            int size = 1;
            for (MultitaskTreeClassifier.MultiTaskNode child : root.getChildren())
                size += getTreeClassesSize(child);
            return size;
        }

        public void updateAgentsEqs(ArrayList<MultitaskingAgent> agents, MultitaskGame game) {
            if (game == null || game.isSuccess())
                return;
            MultitaskingAgent loser = game.getLoser();
            MultitaskTreeClassifier.MultiChange change = game.getChange();
            if (change == null || (change.getAdded().isEmpty() && change.getDeleted().isEmpty()))
                return;
            for (MultitaskingAgent a : agents) {
                if (a.getId() == loser.getId())
                    continue;
                if (!numEqContainsKeys(loser, a)) {
                    getEq(loser, a); // will compute number of eq classes
                    continue;
                }
                double prevEq = getEq(loser, a);
                if (!change.getDeleted().isEmpty()) {
                    double newEq = getTreeCommonNodesSize(loser.getClassifier().getRoot(), a.getClassifier().getRoot());
                    setEq(loser, a, newEq);
                } else { // only added changed
                    for (MultitaskTreeClassifier.MultiTaskNode n : change.getAdded())
                        prevEq += (nodeExistsInTree(n, a.getClassifier().getRoot())) ? 1 : 0;
                    setEq(loser, a, prevEq);
                }
            }
        }

        public boolean numEqContainsKeys(MultitaskingAgent a1, MultitaskingAgent a2) {
            MultitaskingAgent min = (a1.getId() < a2.getId()) ? a1 : a2;
            MultitaskingAgent max = (a2.getId() < a1.getId()) ? a1 : a2;
            return numEq.containsKey(min) && numEq.get(min).containsKey(max);
        }

        public void setEq(MultitaskingAgent a1, MultitaskingAgent a2, double num) {
            MultitaskingAgent min = (a1.getId() < a2.getId()) ? a1 : a2;
            MultitaskingAgent max = (a2.getId() < a1.getId()) ? a1 : a2;
            if (!numEq.containsKey(min))
                numEq.put(min, new HashMap<>());
            numEq.get(min).put(max, num);
        }

        public double getEq(MultitaskingAgent a1, MultitaskingAgent a2) {
            MultitaskingAgent min = (a1.getId() < a2.getId()) ? a1 : a2;
            MultitaskingAgent max = (a2.getId() < a1.getId()) ? a1 : a2;
            if (!numEq.containsKey(min))
                setEq(a1, a2, getTreeCommonNodesSize(a1.getClassifier().getRoot(), a2.getClassifier().getRoot()));
            if (!numEq.get(min).containsKey(max))
                setEq(a1, a2, getTreeCommonNodesSize(a1.getClassifier().getRoot(), a2.getClassifier().getRoot()));
            return numEq.get(min).get(max);
        }

        public double getSim(MultitaskingAgent a1, MultitaskingAgent a2) {
            MultitaskTreeClassifier.MultiTaskNode r1 = a1.getClassifier().getRoot(), r2 = a2.getClassifier().getRoot();
            return getEq(a1, a2) / Math.max(getTreeClassesSize(r1), getTreeClassesSize(r2));
        }

        public double getSim(MultitaskingAgent a, ArrayList<MultitaskingAgent> agents) {
            double avg = 0;
            int skipped = 0;
            for (MultitaskingAgent agent : agents) {
                if (agent.getId() == a.getId()) {
                    skipped += 1;
                    continue;
                }
                avg += getSim(a, agent);
            }
            return avg / (agents.size() - skipped);
        }

        @Override
        public void updateRecorder(ArrayList<MultitaskingAgent> births, ArrayList<MultitaskingAgent> deaths) {

            //STARTING WITH DEATHS FOR LESS TRAVERSING
            for(MultitaskingAgent newDead : deaths){
                for(MultitaskingAgent currentAgent : agents){
                    if(currentAgent.getId() == newDead.getId()){
                        agents.remove(currentAgent);
                        break;
                    }
                }
                numEq.remove(newDead);
                for (Map.Entry<MultitaskingAgent, Map<MultitaskingAgent,Double>> numEqEntry : numEq.entrySet()) {
                    Map<MultitaskingAgent, Double> innerMap = numEqEntry.getValue();
                    if(innerMap.containsKey(newDead)){
                        innerMap.remove(newDead);
                    }
                }
            }

            agents.addAll(births);

        }
    }

    /**
     * RANDOM AGREEMENT RATE
     */
    static public class RandomAgreementRate extends MultiDolaRecorder implements RecorderUpdater {

        protected List<Double> randomAt = new LinkedList<>();

        protected HashSet<Task> tasks;

        public RandomAgreementRate(String name, HashSet<Task> tasks) {
            super(name);
            this.tasks = tasks;
        }

        @Override
        public void update(LLGame game) {
            MultitaskGame g = (MultitaskGame) game;
            if (tasks.contains(g.getTask())) {
                double prevRate = 0;
                if (randomAt.size() != 0) {
                    prevRate = randomAt.get(randomAt.size() - 1);
                }
                randomAt.add(prevRate + (g.isAgreementRandom() ? 1 : 0));
            }
        }

        @Override
        public List<Double> record(LLGame game) {
            double rRate;
            if (randomAt.size() != 0) {
                rRate = randomAt.get(randomAt.size() - 1) / (double) randomAt.size();
            }else{
                rRate = 0;
            }
            return Arrays.asList(rRate);
        }

        @Override
        public void updateRecorder(ArrayList<MultitaskingAgent> births, ArrayList<MultitaskingAgent> deaths) {
            randomAt.clear();
        }
    }


    /**
     * RANDOM ADAPTATION RATE
     */
    static public class RandomAdaptationRate extends MultiDolaRecorder implements RecorderUpdater {

        protected List<Double> randomAt = new LinkedList<>();

        protected HashSet<Task> tasks;

        public RandomAdaptationRate(String name, HashSet<Task> tasks) {
            super(name);
            this.tasks = tasks;
        }

        @Override
        public void update(LLGame game) {
            MultitaskGame g = (MultitaskGame) game;
            if (tasks.contains(g.getTask()) && g.isActualAdaptation()) {
                double prevRate = 0;
                if (randomAt.size() != 0) {
                    prevRate = randomAt.get(randomAt.size() - 1);
                }
                randomAt.add(prevRate + (g.isWinnerDecisionRandom() ? 1 : 0));
            }
        }

        @Override
        public List<Double> record(LLGame game) {
            double rRate;
            if (randomAt.size() != 0) {
                rRate = randomAt.get(randomAt.size() - 1) / (double) randomAt.size();
            }else{
                rRate = 0;
            }
            return Arrays.asList(rRate);
        }

        @Override
        public void updateRecorder(ArrayList<MultitaskingAgent> births, ArrayList<MultitaskingAgent> deaths) {
            randomAt.clear();
        }
    }


    /**
     * SUCCESS RATE RECORDER
     */
    static public class SuccessRate extends MultiDolaRecorder implements RecorderUpdater {

        protected List<Double> successAt = new LinkedList<>();

        protected HashSet<Task> tasks;

        public SuccessRate(String name, HashSet<Task> tasks) {
            super(name);
            this.tasks = tasks;
        }

        @Override
        public void update(LLGame game) {
            MultitaskGame g = (MultitaskGame) game;
            if (tasks.contains(g.getTask())) {
                double prevRate = 0;
                if (successAt.size() != 0) {
                    prevRate = successAt.get(successAt.size() - 1);
                }
                successAt.add(prevRate + (g.isSuccess() ? 1 : 0));
            }
        }

        @Override
        public List<Double> record(LLGame game) {
            double sRate = successAt.get(successAt.size() - 1) / (double) successAt.size();
            return Arrays.asList(sRate);
        }

        @Override
        public void updateRecorder(ArrayList<MultitaskingAgent> births, ArrayList<MultitaskingAgent> deaths) {
            successAt.clear();
        }

    }

    /**
     * SUCCESS RATE RECORDER UNDER UNCERTAINTY
     */
    static public class SuccessRateWithUncertainBits extends MultiDolaRecorder implements RecorderUpdater {

        protected List<Double> successWithUncertainBitsAt = new LinkedList<>();

        protected HashSet<Task> tasks;

        public SuccessRateWithUncertainBits(String name, HashSet<Task> tasks) {
            super(name);
            this.tasks = tasks;
        }

        @Override
        public void update(LLGame game) {
            MultitaskGame g = (MultitaskGame) game;
            if (tasks.contains(g.getTask())) {
                double prevRate = 0;
                if (successWithUncertainBitsAt.size() != 0)
                    prevRate = successWithUncertainBitsAt.get(successWithUncertainBitsAt.size() - 1);
                successWithUncertainBitsAt.add(prevRate + (g.isSuccessWithUncertainBits() ? 1 : 0));
            }
        }

        @Override
        public List<Double> record(LLGame game) {
            double sRate = successWithUncertainBitsAt.get(successWithUncertainBitsAt.size() - 1) / (double) successWithUncertainBitsAt.size();
            return Arrays.asList(sRate);
        }

        @Override
        public void updateRecorder(ArrayList<MultitaskingAgent> births, ArrayList<MultitaskingAgent> deaths) {
            successWithUncertainBitsAt.clear();
        }
    }

    /**
     * CORRECT DECISION RATE RECORDER
     */
    static public class CorrectDecisionRate extends MultiDolaRecorder implements RecorderUpdater{

        protected List<Double> correctDecisionAt = new LinkedList<>();

        protected HashSet<Task> tasks;

        public CorrectDecisionRate(String name, HashSet<Task> tasks) {
            super(name);
            this.tasks = tasks;
        }

        @Override
        public void update(LLGame game) {
            MultitaskGame g = (MultitaskGame) game;
            if (tasks.contains(g.getTask())) {
                double prevCorrectDecision = 0;
                if (correctDecisionAt.size() != 0)
                    prevCorrectDecision = correctDecisionAt.get(correctDecisionAt.size() - 1);
                correctDecisionAt.add(prevCorrectDecision + (g.isDecisionCorrect() ? 1 : 0));
            }
        }

        @Override
        public List<Double> record(LLGame game) {
            double correctDecisionRate = correctDecisionAt.get(correctDecisionAt.size() - 1) / (double) correctDecisionAt.size();
            return Arrays.asList(correctDecisionRate);
        }

        @Override
        public void updateRecorder(ArrayList<MultitaskingAgent> births, ArrayList<MultitaskingAgent> deaths) {
            correctDecisionAt.clear();
        }
    }

    /**
     * CORRECT DELEGATION RATE RECORDER
     */
    static public class DelegationRate extends MultiDolaRecorder implements RecorderUpdater{

        protected List<Double> delegatedAt = new LinkedList<>();

        protected HashSet<Task> tasks;

        public DelegationRate(String name, HashSet<Task> tasks) {
            super(name);
            this.tasks = tasks;
        }

        @Override
        public void update(LLGame game) {
            MultitaskGame g = (MultitaskGame) game;
            if (tasks.contains(g.getTask())) {
                double prevDel = 0;
                if (delegatedAt.size() != 0)
                    prevDel = delegatedAt.get(delegatedAt.size() - 1);
                delegatedAt.add(prevDel + (g.isDelegated() ? 1 : 0));
            }
        }

        @Override
        public List<Double> record(LLGame game) {
            double delegationRate = delegatedAt.get(delegatedAt.size() - 1) / (double) delegatedAt.size();
            return Arrays.asList(delegationRate);
        }

        @Override
        public void updateRecorder(ArrayList<MultitaskingAgent> births, ArrayList<MultitaskingAgent> deaths) {
            delegatedAt.clear();
        }
    }

    /**
     * MIN ACCURACY RECORDER
     */


    static public class WorstTaskAccuracy extends MultiDolaRecorder implements RecorderUpdater {

        protected Map<MultitaskingAgent, Double> minAccuracies = new HashMap<>();

        protected ArrayList<Task> tasks;
        protected HashMap<Task, ArrayList<Task>> taskMap;
        protected ArrayList<EnvironmentExample> examples;

        protected double sum;
        protected double avg;

        public WorstTaskAccuracy(MultitaskLLExperiment exp, String name) {
            super(name);
            this.tasks = new ArrayList<>(exp.getEnvironment().getEnvironmentTasks().values());
            this.taskMap = new HashMap<>();

            this.examples = new ArrayList<>(exp.getEnvironment().getEnvironmentExamples().values());

            sum = 0;
            avg = 0;

            for (int i = 0; i < tasks.size(); i++) {
                ArrayList<Task> tmpList = new ArrayList<>();
                tmpList.add(tasks.get(i));
                taskMap.put(tasks.get(i), tmpList);
            }

            ArrayList<MultitaskingAgent> multitaskingAgents = new ArrayList<>(exp.getAliveAgents().values());

            for (MultitaskingAgent agent : multitaskingAgents) {

                double minAccuracy = 1.0;

                for (int i = 0; i < tasks.size(); i++) {
                    double taskAccuracy = agent.getAccuracy(taskMap.get(tasks.get(i)), examples);
                    if (taskAccuracy < minAccuracy) {
                        minAccuracy = taskAccuracy;
                    }
                }

                minAccuracies.put(agent, minAccuracy);
                sum += minAccuracy;
            }
            avg = sum / multitaskingAgents.size();
        }

        @Override
        public void update(LLGame game) {
            MultitaskGame g = (MultitaskGame) game;

            if (taskMap.containsKey(g.getTask())) {

                MultitaskingAgent firstAgent = (MultitaskingAgent) g.getFirstAgent();
                MultitaskingAgent secondAgent = (MultitaskingAgent) g.getSecondAgent();

                if (minAccuracies.containsKey(firstAgent)) {
                    sum -= minAccuracies.get(firstAgent);
                    double firstAgentMinAccuracy = 1.0;
                    for (int i = 0; i < tasks.size(); i++) {
                        double taskAccuracy = firstAgent.getAccuracy(taskMap.get(tasks.get(i)), examples);
                        if (taskAccuracy < firstAgentMinAccuracy) {
                            firstAgentMinAccuracy = taskAccuracy;
                        }
                    }
                    minAccuracies.put(firstAgent, firstAgentMinAccuracy);
                    sum += firstAgentMinAccuracy;
                }


                if (minAccuracies.containsKey(secondAgent)) {
                    sum -= minAccuracies.get(secondAgent);
                    double secondAgentMinAccuracy = 1.0;
                    for (int i = 0; i < tasks.size(); i++) {
                        double taskAccuracy = secondAgent.getAccuracy(taskMap.get(tasks.get(i)), examples);
                        if (taskAccuracy < secondAgentMinAccuracy) {
                            secondAgentMinAccuracy = taskAccuracy;
                        }
                    }
                    minAccuracies.put(secondAgent, secondAgentMinAccuracy);
                    sum += secondAgentMinAccuracy;
                }
                avg = sum / minAccuracies.size();
            }
        }

        @Override
        public List<Double> record(LLGame game) {
            return Arrays.asList(avg);
        }

        @Override
        public void updateRecorder(ArrayList<MultitaskingAgent> births, ArrayList<MultitaskingAgent> deaths) {
            for (MultitaskingAgent newBorn : births) {
                double newBornMinimumAccuracy = 1.0;
                for (int i = 0; i < tasks.size(); i++) {
                    double taskAccuracy = newBorn.getAccuracy(taskMap.get(tasks.get(i)), examples);
                    if (taskAccuracy < newBornMinimumAccuracy) {
                        newBornMinimumAccuracy = taskAccuracy;
                    }
                }
                minAccuracies.put(newBorn, newBornMinimumAccuracy);
                sum += newBornMinimumAccuracy;

            }
            for (MultitaskingAgent newDead : deaths){
                sum -= minAccuracies.get(newDead);
                minAccuracies.remove(newDead);
            }
        }
    }

    /**
     * MAX ACCURACY RECORDER
     */


    static public class BestTaskAccuracy extends MultiDolaRecorder implements RecorderUpdater {

        protected Map<MultitaskingAgent, Double> maxAccuracies = new HashMap<>();

        protected ArrayList<Task> tasks;
        protected HashMap<Task, ArrayList<Task>> taskMap;
        protected ArrayList<EnvironmentExample> examples;

        protected double sum;
        protected double avg;

        public BestTaskAccuracy(List<MultitaskingAgent> multitaskingAgents, ArrayList<Task> tasks, ArrayList<EnvironmentExample> examples, String name) {
            super(name);
            this.tasks = new ArrayList<>();
            this.taskMap = new HashMap<>();
            this.examples = new ArrayList<>();

            this.tasks.addAll(tasks);
            this.examples.addAll(examples);

            sum = 0;
            avg = 0;

            for (int i = 0; i < tasks.size(); i++) {
                ArrayList<Task> tmpList = new ArrayList<>();
                tmpList.add(tasks.get(i));
                taskMap.put(tasks.get(i), tmpList);
            }

            for (MultitaskingAgent agent : multitaskingAgents) {

                double maxAccuracy = 0;

                for (int i = 0; i < tasks.size(); i++) {
                    double taskAccuracy = agent.getAccuracy(taskMap.get(tasks.get(i)), examples);
                    if (taskAccuracy > maxAccuracy) {
                        maxAccuracy = taskAccuracy;
                    }
                }

                maxAccuracies.put(agent, maxAccuracy);
                sum += maxAccuracy;
            }
            avg = sum / multitaskingAgents.size();
        }

        @Override
        public void update(LLGame game) {
            MultitaskGame g = (MultitaskGame) game;

            if (taskMap.containsKey(g.getTask())) {

                MultitaskingAgent firstAgent = (MultitaskingAgent) g.getFirstAgent();
                MultitaskingAgent secondAgent = (MultitaskingAgent) g.getSecondAgent();

                if (maxAccuracies.containsKey(firstAgent)) {
                    sum -= maxAccuracies.get(firstAgent);
                    double firstAgentMaxAccuracy = 0;
                    for (int i = 0; i < tasks.size(); i++) {
                        double taskAccuracy = firstAgent.getAccuracy(taskMap.get(tasks.get(i)), examples);
                        if (taskAccuracy > firstAgentMaxAccuracy) {
                            firstAgentMaxAccuracy = taskAccuracy;
                        }
                    }
                    maxAccuracies.put(firstAgent, firstAgentMaxAccuracy);
                    sum += firstAgentMaxAccuracy;
                }else{
                    //System.out.println("First agent not contained!");
                }


                if (maxAccuracies.containsKey(secondAgent)) {
                    sum -= maxAccuracies.get(secondAgent);
                    double secondAgentMaxAccuracy = 0;
                    for (int i = 0; i < tasks.size(); i++) {
                        double taskAccuracy = secondAgent.getAccuracy(taskMap.get(tasks.get(i)), examples);
                        if (taskAccuracy > secondAgentMaxAccuracy) {
                            secondAgentMaxAccuracy = taskAccuracy;
                        }
                    }
                    maxAccuracies.put(secondAgent, secondAgentMaxAccuracy);
                    sum += secondAgentMaxAccuracy;
                }else{
                    //System.out.println("Second agent not contained!");
                }
                avg = sum / maxAccuracies.size();
            }
        }

        @Override
        public List<Double> record(LLGame game) {
            return Arrays.asList(avg);
        }

        @Override
        public void updateRecorder(ArrayList<MultitaskingAgent> births, ArrayList<MultitaskingAgent> deaths) {
            for (MultitaskingAgent newBorn : births) {
                double newBornMaximumAccuracy = 0;
                for (int i = 0; i < tasks.size(); i++) {
                    double taskAccuracy = newBorn.getAccuracy(taskMap.get(tasks.get(i)), examples);
                    if (taskAccuracy > newBornMaximumAccuracy) {
                        newBornMaximumAccuracy = taskAccuracy;
                    }
                }
                maxAccuracies.put(newBorn, newBornMaximumAccuracy);
                sum += newBornMaximumAccuracy;

            }
            for (MultitaskingAgent newDead : deaths){
                sum -= maxAccuracies.get(newDead);
                maxAccuracies.remove(newDead);
            }
        }
    }


    /**
     * AVERAGE ACCURACY RECORDER
     */


    static public class AverageAccuracy extends MultiDolaRecorder implements RecorderUpdater {

        protected Map<MultitaskingAgent, Double> accuracies = new HashMap<>();
        protected ArrayList<Task> tasks;
        protected HashSet<Task> taskSet;
        protected ArrayList<EnvironmentExample> examples;

        private double sum;
        private double avg;

        public AverageAccuracy(List<MultitaskingAgent> multitaskingAgents, ArrayList<Task> tasks, ArrayList<EnvironmentExample> examples, String name) {
            super(name);
            this.tasks = new ArrayList<>();
            this.taskSet = new HashSet<>();
            this.examples = new ArrayList<>();

            this.tasks.addAll(tasks);
            this.taskSet.addAll(tasks);
            this.examples.addAll(examples);

            for (MultitaskingAgent agent : multitaskingAgents) {
                double agentAccuracy = agent.getAccuracy(tasks, examples);
                accuracies.put(agent, agentAccuracy);
                sum += agentAccuracy;
            }
            avg = sum / multitaskingAgents.size();
        }

        @Override
        public void update(LLGame game) {
            MultitaskGame g = (MultitaskGame) game;

            if (taskSet.contains(g.getTask())) {

                MultitaskingAgent firstAgent = (MultitaskingAgent) g.getFirstAgent();
                MultitaskingAgent secondAgent = (MultitaskingAgent) g.getSecondAgent();

                if (accuracies.containsKey(firstAgent)) {
                    sum -= accuracies.get(firstAgent);
                    double firstAgentAccuracy = firstAgent.getAccuracy(tasks, examples);
                    accuracies.put(firstAgent, firstAgentAccuracy);
                    sum += firstAgentAccuracy;
                }


                if (accuracies.containsKey(secondAgent)) {
                    sum -= accuracies.get(secondAgent);
                    double secondAgentAccuracy = secondAgent.getAccuracy(tasks, examples);
                    accuracies.put(secondAgent, secondAgentAccuracy);
                    sum += secondAgentAccuracy;
                }

                avg = sum / accuracies.size();

//                if(tasks.size()==1 && tasks.get(0).getName().equalsIgnoreCase("Task 0")){
//                    System.out.println("avg accuracy for task_0ZZ:"+avg);
//                }else if(tasks.size()==1 && tasks.get(0).getName().equalsIgnoreCase("Task 1")){
//                    System.out.println("avg accuracy for task_1OO:"+avg);
//                }else if(tasks.size()==1 && tasks.get(0).getName().equalsIgnoreCase("Task 2")){
//                    System.out.println("avg accuracy for task_2TT__:"+avg);
//                }

            }
        }

        @Override
        public List<Double> record(LLGame game) {
            return Arrays.asList(avg);
        }

        @Override
        public void updateRecorder(ArrayList<MultitaskingAgent> births, ArrayList<MultitaskingAgent> deaths) {
            for (MultitaskingAgent newBorn : births) {
                double agentAccuracy = newBorn.getAccuracy(tasks, examples);
                accuracies.put(newBorn, agentAccuracy);
                sum += agentAccuracy;
            }
            for (MultitaskingAgent newDead : deaths){
                sum -= accuracies.get(newDead);
                accuracies.remove(newDead);
            }
            //System.out.println("Accuracies size after births and deaths:"+accuracies.size());
        }
    }

    /**
     * PREFERRED TASKS ACCURACY RECORDER
     */


    static public class AveragePreferredTasksAccuracy extends MultiDolaRecorder implements RecorderUpdater {

        protected Map<MultitaskingAgent, Double> accuracies = new HashMap<>();

        protected ArrayList<EnvironmentExample> examples;

        protected double sum;
        protected double avg;

        public AveragePreferredTasksAccuracy(List<MultitaskingAgent> multitaskingAgents, ArrayList<EnvironmentExample> examples, String name) {
            super(name);
            this.examples = new ArrayList<>();

            this.examples.addAll(examples);

            for (MultitaskingAgent agent : multitaskingAgents) {
                double agentAccuracy = agent.getAccuracy(agent.getPreferredTasks(), examples);
                accuracies.put(agent, agentAccuracy);
                sum += agentAccuracy;
            }
            avg = sum / multitaskingAgents.size();
        }

        @Override
        public void update(LLGame game) {
            MultitaskGame g = (MultitaskGame) game;

            MultitaskingAgent firstAgent = (MultitaskingAgent) g.getFirstAgent();
            MultitaskingAgent secondAgent = (MultitaskingAgent) g.getSecondAgent();

            if (true) {

                if (accuracies.containsKey(firstAgent)) {
                    sum -= accuracies.get(firstAgent);
                    double firstAgentAccuracy = firstAgent.getAccuracy(firstAgent.getPreferredTasks(), examples);
                    accuracies.put(firstAgent, firstAgentAccuracy);
                    sum += firstAgentAccuracy;
                }


                if (accuracies.containsKey(secondAgent)) {
                    sum -= accuracies.get(secondAgent);
                    double secondAgentAccuracy = secondAgent.getAccuracy(secondAgent.getPreferredTasks(), examples);
                    accuracies.put(secondAgent, secondAgentAccuracy);
                    sum += secondAgentAccuracy;
                }

                avg = sum / accuracies.size();
            }
        }

        @Override
        public List<Double> record(LLGame game) {
            return Arrays.asList(avg);
        }

        @Override
        public void updateRecorder(ArrayList<MultitaskingAgent> births, ArrayList<MultitaskingAgent> deaths) {
            for (MultitaskingAgent newBorn : births) {
                double agentAccuracy = newBorn.getAccuracy(newBorn.getPreferredTasks(), examples);
                accuracies.put(newBorn, agentAccuracy);
                sum += agentAccuracy;
            }
            for (MultitaskingAgent newDead : deaths){
                sum -= accuracies.get(newDead);
                accuracies.remove(newDead);
            }
        }
    }


    /**
     * ATTEMPTS RELATIVE STANDARD DEVIATION RECORDER
     */

    static public class GamesStandardDeviation extends MultiDolaRecorder implements RecorderUpdater{
        protected List<Double> deviationAt = new LinkedList<>();
        protected MultitaskLLExperiment experiment;
        protected double stdev;

        public GamesStandardDeviation(String name, MultitaskLLExperiment experiment) {
            super(name);
            this.experiment = experiment;
        }

        @Override
        public void update(LLGame game) {

            double sum = 0;

            HashMap<Task, Integer> map = experiment.getGamesPlayed();

            for (Map.Entry<Task, Integer> entry : map.entrySet()) {
                sum+= entry.getValue();
            }

            double mean = sum/map.size();

            double stdevSum = 0;

            for (Map.Entry<Task, Integer> entry : map.entrySet()) {
                stdevSum += Math.pow(entry.getValue() - mean, 2);
            }

            stdev = (mean != 0) ? 100 * Math.sqrt(stdevSum / map.size()) / mean : 0;

            deviationAt.add(stdev);

        }

        @Override
        public List<Double> record(LLGame game) {
            return Arrays.asList(stdev);
        }

        @Override
        public void updateRecorder(ArrayList<MultitaskingAgent> births, ArrayList<MultitaskingAgent> deaths) {
            deviationAt.clear();
        }
    }

    /**
     * ACCURACY STANDARD DEVIATION RECORDER
     */


    static public class AccuracyStandardDeviation extends MultiDolaRecorder implements RecorderUpdater{
        protected List<Double> deviationAt = new LinkedList<>();

        protected MultitaskLLExperiment experiment;
        protected double accuracyStandardDeviation;

        protected ArrayList<EnvironmentExample> examples;
        protected HashMap<Task, ArrayList<Task>> taskMap;
        protected HashMap<Task, Double> taskAccuracyMap;

        protected ArrayList<Task> tasks;

        public AccuracyStandardDeviation(MultitaskLLExperiment experiment, ArrayList<Task> tasks, ArrayList<EnvironmentExample> examples, String name) {
            super(name);
            this.experiment = experiment;
            this.tasks = tasks;
            this.examples = new ArrayList<>();
            this.examples.addAll(examples);

            taskMap = new HashMap<Task, ArrayList<Task>>();

            for (int i = 0; i < tasks.size(); i++) {
                ArrayList<Task> indTask = new ArrayList<>();
                indTask.add(tasks.get(i));
                taskMap.put(tasks.get(i),indTask);
            }

            taskAccuracyMap = new HashMap<Task, Double>();

            deviationAt.add(this.getDeviation());

        }

        @Override
        public void update(LLGame game) {
            deviationAt.add(this.getDeviation());
        }

        @Override
        public List<Double> record(LLGame game) {
            return Arrays.asList(accuracyStandardDeviation);
        }

        @Override
        public void updateRecorder(ArrayList<MultitaskingAgent> births, ArrayList<MultitaskingAgent> deaths) {
            deviationAt.clear();
        }

        private double getDeviation(){

            Collection<MultitaskingAgent> collectionOfAgents = experiment.getAliveAgents().values();
            ArrayList<MultitaskingAgent> listOfAgentsAlive = new ArrayList<>(collectionOfAgents);

            //System.out.println("listOfAgents alive:"+listOfAgentsAlive.size());

            double stdevSum = 0;

            double stTaskSum = 0;
            double stTaskMean = 0;

            //System.out.println("taskMap size:"+ taskMap.size());

            for (Map.Entry<Task, ArrayList<Task>> entry : taskMap.entrySet()) {

                Task key = entry.getKey();
                ArrayList<Task> value = entry.getValue();

                double taskSum = 0;
                double taskAvg = 0;

                for (MultitaskingAgent agent : listOfAgentsAlive) {
                    double agentAccuracy = agent.getAccuracy(value, examples);
                    taskSum += agentAccuracy;
                }
                taskAvg = taskSum / listOfAgentsAlive.size();

                taskAccuracyMap.put(key, taskAvg);

                stTaskSum += taskAvg;

            }

            stTaskMean = stTaskSum/tasks.size();

            for(Map.Entry<Task, Double> entry: taskAccuracyMap.entrySet()){
                stdevSum += Math.pow(entry.getValue() - stTaskMean, 2);
            }

            accuracyStandardDeviation = (stTaskMean != 0) ? 100 * Math.sqrt(stdevSum / taskAccuracyMap.size()) / stTaskMean : 0;

//            if(accuracyStandardDeviation==0) {
//
//                System.out.println("stdevSum:"+stdevSum+" "+"stTaskMean:"+stTaskMean+" "+"taskAccuracyMap size:"+taskAccuracyMap.size());
//
//                System.out.println("returned standard deviation:" + accuracyStandardDeviation);
//            }

            return accuracyStandardDeviation;

        }
    }


}
