package fr.inria.exmo.lazylavender.decisiontaking.multitask;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;

public class AccuracyComparator implements Comparator<MultitaskingAgent> {

    private ArrayList<EnvironmentExample> examples;
    private ArrayList<Task> tasks;

    public AccuracyComparator(ArrayList<Task> tasks, ArrayList<EnvironmentExample> examples){
        this.tasks = tasks;
        this.examples = examples;
    }

    @Override
    public int compare(MultitaskingAgent agent1, MultitaskingAgent agent2) {
        if (agent1.getAccuracy(tasks, examples) != agent2.getAccuracy(tasks, examples)) {
            return agent1.getAccuracy(tasks, examples)<agent2.getAccuracy(tasks, examples) ? -1 : 1;
        }
        return 0;
    }

    public ArrayList<EnvironmentExample> getExamples(){
        return examples;
    }

}
