package fr.inria.exmo.lazylavender.decisiontaking;

import fr.inria.exmo.lazylavender.decisiontaking.treeclassifier.Decision;
import fr.inria.exmo.lazylavender.logger.ActionLogger;
import fr.inria.exmo.lazylavender.model.LLException;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

public class CrossValidationExperiment extends Experiment {

    int testSet = 0;
    double testSetProp = 0.1;


    @Override
    protected void loadParams(Properties p) {
        super.loadParams(p);
        if (p.containsKey("testSet"))
            testSet = Integer.parseInt(p.getProperty("testSet"));
        if (p.containsKey("testSetProp"))
            testSetProp = Double.parseDouble(p.getProperty("testSetProp"));
        p.setProperty("testSet", String.valueOf(testSet));
        p.setProperty("testSetProp", String.valueOf(testSetProp));
    }

    @Override
    public ActionLogger init(Properties param) throws LLException {
        loadParams(param);

        if (runDir != null)
            new File(runDir).mkdirs();

        env = new Environment(numberOfFeatures, numberOfClasses);
        env.init(param);

        for (int i = 0; i < numberOfAgents; i++) {
            agents.add(new Agent(i, env));
            agents.get(i).init(param);
        }
        loadTrainingSets();
        trainAgents();
        if (runDir != null)
            saveTrainingSets();
        if (loadRunDir == null)
            gameIt = new GameIterators.UniformRandomGameIterator(this);
        else {
            try {
                gameIt = Files.lines(Paths.get(loadRunDir, "games.tsv")).iterator();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

//        alogger = new ActionLogger(false, measures);
        alogger = rManager.init(new DolaRecorders.Srate("ssrate"),
                new DolaRecorders.Accuracy(agents, env.getInputs(), env.getTargets(), "accuracy"),
                new DolaRecorders.Distance(agents, "distance"),
                new DolaRecorders.PrecisionRecall(agents, env.getInputs(), env.getTargets(), "precision",
                        "recall", numberOfClasses),
                new DolaRecorders.Accuracy(agents, env.getTestInputs(), env.getTestTargets(), "taccuracy"),
                new DolaRecorders.PrecisionRecall(agents, env.getTestInputs(), env.getTestTargets(),
                        "tprecision", "trecall", numberOfClasses));

        return alogger;
    }

}
