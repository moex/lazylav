package fr.inria.exmo.lazylavender.decisiontaking;

import fr.inria.exmo.lazylavender.decisiontaking.generations.GenerationExperiment;
import fr.inria.exmo.lazylavender.decisiontaking.intrinsicmotivation.IntrinsicMotivationExperiment;
import fr.inria.exmo.lazylavender.decisiontaking.intrinsicmotivation.MotivatedAgent;
import fr.inria.exmo.lazylavender.decisiontaking.intrinsicmotivation.Motivation;
import fr.inria.exmo.lazylavender.decisiontaking.treeclassifier.NoClassException;

import java.util.*;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toList;

public class GameIterators {

    static public class Item {
        int id, relativeProb;

        public Item(int id, int relativeProb) {
            this.id = id;
            this.relativeProb = relativeProb;
        }
    }

    static public class RandomSelector {
        List<Item> items = new ArrayList<>();
        Random rand = new Random();
        int totalSum = 0;
        int initialProb;

        RandomSelector(List<Integer> ids, int initialProb) {
            for (Integer id : ids)
                items.add(new Item(id, initialProb));

            for (Item item : items)
                totalSum = totalSum + item.relativeProb;
            this.initialProb = initialProb;
        }

        private void reInitProbs() {
            for (Item item : items)
                item.relativeProb = initialProb;
            totalSum = initialProb * items.size();
        }

        private boolean allZeros() {
            for (Item item : items)
                if (item.relativeProb > 0)
                    return false;
            return true;
        }

        public Item getRandom(Item selected) {
            if (selected == null)
                selected = new Item(-1, 0);
            int totalSum = this.totalSum - selected.relativeProb;
            try {
                if (totalSum <= 0) {
                    int id = rand.nextInt(items.size());
                    while (id == selected.id)
                        id = rand.nextInt(items.size());
                    return new Item(id, 1);
                }
                int index = rand.nextInt(totalSum);
                int sum = 0;
                int i = 0;

                while (sum <= index) {
                    if (items.get(i).id != selected.id)
                        sum = sum + items.get(i).relativeProb;
                    i++;
                }
                return items.get(Math.max(0, i - 1));
            } catch (Exception e) {
                System.out.println(totalSum);
                for (Item item : items)
                    System.out.println(item.id + " relativeProb: " + item.relativeProb);
                throw e;
            }
        }

        public Common.Pair<Integer, Integer> getRandomIDs() {
            if (allZeros())
                reInitProbs();
            Item item1 = getRandom(null);
            Item item2 = getRandom(item1);
            item1.relativeProb--;
            item2.relativeProb--;
            this.totalSum -= 2;
            if (item1.relativeProb < 0)
                System.out.println("ERROR: 0 Proba selected");
            if (item2.relativeProb < 0)
                System.out.println("ERROR: 0 Proba selected");
            return new Common.Pair<>(item1.id, item2.id);
        }
    }

    static public class UniformRandomGameIterator implements Iterator<String> {

        int maxIter, currentIter;
        Experiment exp;
        Random rand;
        RandomSelector r;
        List<Integer> agentsIDs = new ArrayList<>();

        public UniformRandomGameIterator(Experiment exp) {
            this.exp = exp;
            this.maxIter = exp.getNumberOfIterations();
            this.currentIter = 0;
            fillGen(agentsIDs);
            rand = new Random();
            r = new RandomSelector(agentsIDs, maxIter / agentsIDs.size());
        }

        public UniformRandomGameIterator(Experiment exp, int numberOfIterations) {
            this(exp);
            this.maxIter = numberOfIterations;
        }

        @Override
        public boolean hasNext() {
            return currentIter < maxIter;
        }

        private void fillGen(List<Integer> list) {
            for (int i = 0; i < exp.numberOfAgents; i++) {
                list.add(i);
            }
        }

        @Override
        public String next() {
//            Common.Pair<Integer, Integer> items = r.getRandomIDs();
//            int a1 = items.getKey();
//            int a2 = items.getValue();
            int a1 = rand.nextInt(exp.getAgents().size());
            int a2 = rand.nextInt(exp.getAgents().size());
            while (a2 == a1)
                a2 = rand.nextInt(exp.getAgents().size());
            BitSet instance = exp.env.generateInstance();
            currentIter += 1;
            return "" + a1 + "\t" + a2 + "\t" + instance;
        }

        public String next(List<Agent> agents) {
            int a1 = rand.nextInt(agents.size());
            int a2 = rand.nextInt(agents.size());
            while (a2 == a1)
                a2 = rand.nextInt(agents.size());
            currentIter += 1;
            return "" + a1 + "\t" + a2;
        }
    }

    static public class ParentBiasedGameIterator implements Iterator<String> {

        int maxIter, currentIter;
        GenerationExperiment exp;
        Random rand;
        double epsilon;
        double annealing;
        double verticalThreshold = 0;
        double horizontal=0;


        Map<Agent, Double> proba = new HashMap<>();
        Map<Integer, ArrayList<Agent>> generation = new HashMap<>();
        Map<Agent, Integer> agentGen = new HashMap<>();
        int countGen = 0;

        public ParentBiasedGameIterator(GenerationExperiment exp) {
            this.exp = exp;
            this.maxIter = exp.getNumberOfIterations();
            this.currentIter = 0;
            rand = new Random();
            epsilon = exp.getEpsilon();
            annealing = exp.getAnnealing();

        }

        public ParentBiasedGameIterator(GenerationExperiment exp, double horizontal) {
            this(exp);
            this.horizontal = horizontal;

        }

        @Override
        public boolean hasNext() {
            return currentIter < maxIter;
        }

        @Override
        public String next() {
            ArrayList<Agent> newGen = new ArrayList<>();
            for(Agent a: exp.getAgents())
                if(!agentGen.containsKey(a)) {
                    newGen.add(a);
                    agentGen.put(a, countGen);
                }
            if(!newGen.isEmpty())
                generation.put(countGen++, newGen);


            currentIter++;

            // select random a1 and a2
            int a1 = rand.nextInt(exp.getAgents().size());
            int a2 = rand.nextInt(exp.getAgents().size());

            if(Math.random() < horizontal) // horizontal
            {
                newGen = generation.get(agentGen.get(exp.getAgents().get(a1)));

                a2 = exp.getAgents().indexOf(newGen.get(rand.nextInt(newGen.size())));
                while(a2 == a1 || a2 == -1)
                    a2 = exp.getAgents().indexOf(newGen.get(rand.nextInt(newGen.size())));
            }
            else
                while (a2 == a1)
                    a2 = rand.nextInt(exp.getAgents().size()); // oblique

            // override a2 if agent has parents, and restricted communication is activated
            Agent a = exp.getAgents().get(a1);
            if(exp.getParentsMap().containsKey(a) && !exp.getParentsMap().get(a).isEmpty()) { // a has parents
                if(!proba.containsKey(a)) { // newly added agents that was not drawn for interactions before
                    proba.put(a, epsilon);
                }
                double r = Math.random();
                if(r < proba.get(a)) { // choose one of parents
                    List<Agent> parents = exp.getParentsMap().get(a);
                    int index = rand.nextInt(parents.size());

                    a2 = exp.getAgents().indexOf(parents.get(index));
                }
                if(proba.get(a) > verticalThreshold)
                    proba.put(a, proba.get(a) - annealing);
                else
                    proba.put(a, verticalThreshold);
            }

            BitSet instance = exp.env.generateInstance();

            return a1 + "\t" + a2 + "\t" + instance;
        }
    }


    static public class IntrinsicMotivationGameIterator implements Iterator<String> {

        int maxIter, currentIter;
        IntrinsicMotivationExperiment exp;
        Random rand;

        /**
         * An intrinsic motivation game iterator.
         *
         * @param exp the experiment the iterator is used for
         */
        public IntrinsicMotivationGameIterator(IntrinsicMotivationExperiment exp) {
            this.exp = exp;
            this.maxIter = exp.getNumberOfIterations();
            this.currentIter = 0;
            rand = new Random();
        }

        /**
         * Returns whether all iterations have been performed.
         *
         * @return boolean whether there still is an iteration open
         */
        @Override
        public boolean hasNext() {
            return currentIter < maxIter;
        }

        /**
         * Returns the next game with an agent, one or multiple interaction partners and an interaction object.
         * Splitted by a string: a1 \t an1,an2,...,anm \t instance
         *
         * @return string the next game
         */
        @Override
        public String next() {
            int a1 = rand.nextInt(exp.getAgents().size());
            MotivatedAgent agent1 = (MotivatedAgent) exp.getAgents().get(a1);
            Motivation motivation = agent1.getCurrentMotivation();

            // interaction partner choice
            ArrayList<Integer> a2 = new ArrayList<>();
            a2.add(rand.nextInt(exp.getAgents().size()));
            while (a2.get(0) == a1)
                a2.set(0, rand.nextInt(exp.getAgents().size()));
            // get multiple partners, if social
            if (agent1.isSocial()) {
                List<MotivatedAgent> an = getChosenInteractionPartners(agent1, motivation, exp.getAgents(), exp.getMaxGroupSize());

                // prepare agents for return
                if (an.size() == 1) a2.set(0, an.get(0).getId());
                else
                    a2 = (ArrayList<Integer>) an.stream().map(Agent::getId).collect(toList());
            }

            // object choice
            BitSet instance = exp.env.generateInstance();
            if (agent1.isMotivated()) {
                try {
                    instance = agent1.chooseInteractionObject(motivation, exp.getNumberOfFeatures());
                } catch (NoClassException e) {
                    // do nothing and leave randomly chosen object
                }
            }

            currentIter += 1;
            String interactionPartners = a2.stream().map(Object::toString).collect(Collectors.joining(","));
            return "" + a1 + "\t" + interactionPartners + "\t" + instance;
        }

        /**
         * Returns the next partner / group, for learning games in reinforcement learning.
         *
         * @param agents the list of agents used
         * @param a1 the agent choosing
         * @return the next partner(s)
         */
        public String nextPartner(List<Agent> agents, MotivatedAgent a1, int maxGroupSize) {
            Motivation motivation = a1.getCurrentMotivation();

            // interaction partner choice
            ArrayList<Integer> a2 = new ArrayList<>();
            a2.add(rand.nextInt(agents.size()));
            while (a2.get(0) == a1.getId())
                a2.set(0, rand.nextInt(agents.size()));
            if (a1.isSocial()) {
                List<MotivatedAgent> an = getChosenInteractionPartners(a1, motivation, agents, maxGroupSize);

                // prepare agents for return
                if (an.size() == 1) a2.set(0, an.get(0).getId());
                else
                    a2 = (ArrayList<Integer>) an.stream().map(Agent::getId).collect(toList());
            }

            currentIter += 1;
            return a2.stream().map(Object::toString).collect(Collectors.joining(","));
        }
    }

    /**
     * Returns the next partner(s) in case of social motivation.
     *
     * @param agent1 the choosing agents
     * @param motivation the agent's motivation
     * @param allAgents all agents of the game
     * @param maxGroupSize the size of the group to be created
     * @return a list with the partners
     */
    private static List<MotivatedAgent> getChosenInteractionPartners(MotivatedAgent agent1, Motivation motivation, List<Agent> allAgents, int maxGroupSize) {
        Random rand = new Random();
        int amountInteractionPartners = rand.nextInt(maxGroupSize - 1) + 1; // minus one because a1 is also part of the group
        List<MotivatedAgent> an = agent1.chooseInteractionPartners(motivation, amountInteractionPartners, allAgents.size());

        // check if the group is big enough
        if (an.size() < amountInteractionPartners) { // add random agents
            List<Agent> agents = new ArrayList<>(allAgents);
            MotivatedAgent randAgent;
            while (an.size() < amountInteractionPartners) {
                // add only agents that are not part of the group already, agent1 or the agent has already
                // interacted with
                randAgent = (MotivatedAgent) agents.get(rand.nextInt(agents.size()));
                if (randAgent.getId() != agent1.getId()
                        && !an.contains(randAgent)
                        && (!agent1.interactedWith(randAgent) || motivation == Motivation.NONE)) {
                    an.add(randAgent);
                    agents.remove(randAgent);
                } else agents.remove(randAgent);
            }
        }
        return an;
    }
}
