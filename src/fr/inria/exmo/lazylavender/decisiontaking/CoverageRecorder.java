/*
 *
 * Copyright (C) INRIA, 2024
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 */

package fr.inria.exmo.lazylavender.decisiontaking;

import fr.inria.exmo.lazylavender.expe.LLGame;

// JE 2024: This depends on intrinsicmotivation only because of
// (1) groupgame -> that could have been used more generally
// (2) fullCoverage implemented in MotivatedAgent -> is more general as well!
// Hence all this should be implemented as a more general measure
import fr.inria.exmo.lazylavender.decisiontaking.intrinsicmotivation.GroupGame;
import fr.inria.exmo.lazylavender.decisiontaking.intrinsicmotivation.MotivatedAgent;
import fr.inria.exmo.lazylavender.decisiontaking.measurements.Recorder;
import fr.inria.exmo.lazylavender.decisiontaking.Agent;

import java.util.List;
import java.util.HashMap;
import java.util.Map;

/**
 *  Coverage RECORDER
 *  Currently records full coverage, but may be adapted (actually through an argument at instanciation time)
 */
public class CoverageRecorder extends DolaRecorders.DolaRec {

	protected Map<Agent, Double> coverage;
	protected double nbObjects = 0;
	protected double nbAgents = 0;
	protected double nbPos = 0;

	/**
	 * Records the coverage of the agents' ontologies.
	 *
	 * @param name the recorder's name
	 * @param agents the set of agents
	 * @param env the environment
	 */
	public CoverageRecorder( String name, List<Agent> agents, Environment env ) {
		super(name);
		nbObjects = Math.pow(2,env.getNumberOfFeatures());
		nbAgents = agents.size();
		nbPos = (nbAgents-1)*nbObjects;
		coverage = new HashMap<Agent, Double>();
		for ( Agent agent : agents ) coverage.put( agent, Double.valueOf( 0. ) );
	}
	
	/**
	 * Updates the score keeper for those agents which have played
	 *
	 * @param game the game that was just played
	 */
	@Override
	public void update( LLGame game ) {
		// Just compute the new value for the involved agents
		// and add it to the map
		// get agent or agents depending on game
		if ( game instanceof GroupGame ) {
			MotivatedAgent ag = (MotivatedAgent)((GroupGame)game).getFirstAgent();
			coverage.put( ag, Double.valueOf(ag.fullCoverage()) );
			for ( Agent agg : ((GroupGame)game).getOtherAgents() ) {
				coverage.put( agg, Double.valueOf(((MotivatedAgent)agg).fullCoverage()) );
			}
		} else {
			MotivatedAgent ag = (MotivatedAgent)((Game)game).getFirstAgent();
			coverage.put( ag, Double.valueOf(ag.fullCoverage()) );
			ag = (MotivatedAgent)((Game)game).getSecondAgent();
			coverage.put( ag, Double.valueOf(ag.fullCoverage()) );
		}
	}
	
	/**
	 * Returns a list with the average coverage over all agents.
	 *
	 * @param game the game that was just played
	 */
	@Override
	public List<Double> record( LLGame game ) {
		return List.of(coverage.values().stream().reduce(0., Double::sum) / (nbPos*nbAgents) );
	}
}
