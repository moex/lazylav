package fr.inria.exmo.lazylavender.decisiontaking.culturalvalues;

import fr.inria.exmo.lazylavender.decisiontaking.*;
import fr.inria.exmo.lazylavender.logger.ActionLogger;
import fr.inria.exmo.lazylavender.model.LLException;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

public class CulturalValuesExperiment extends Experiment {
//    coefficients for Independence, Novelty, Success, Authority
    protected double coeffSucc = 0;
    protected double coeffAuth = 0;
    protected double coeffIndep = 0;
    protected double coeffNov = 0;
//    discounts for Independence, Success, Authority (we don't need to discount Novelty)
    protected double discountIndep = 0.9;
    protected double discountSucc = 0.9;
    protected double discountAuth = 0.9;

    protected void loadParams(Properties p) {
        super.loadParams(p);
//        get coefficients and discounts for indices
        if (p.containsKey("coeffIndep"))
            coeffIndep = Double.parseDouble(p.getProperty("coeffIndep"));
        if (p.containsKey("coeffNov"))
            coeffNov = Double.parseDouble(p.getProperty("coeffNov"));
        if (p.containsKey("coeffSucc"))
            coeffSucc = Double.parseDouble(p.getProperty("coeffSucc"));
        if (p.containsKey("coeffAuth"))
            coeffAuth = Double.parseDouble(p.getProperty("coeffAuth"));

        if (p.containsKey("discountIndep"))
            discountIndep = Double.parseDouble(p.getProperty("discountIndep"));
        if (p.containsKey("discountSucc"))
            discountSucc = Double.parseDouble(p.getProperty("discountSucc"));
        if (p.containsKey("discountAuth"))
            discountAuth = Double.parseDouble(p.getProperty("discountAuth"));

//        set adaptation operator
//        double sum = coeffIndep + coeffNov + coeffAuth + coeffSucc;
//        if (sum < 0) {
//            p.setProperty("op", "noCom");
//        } else if (sum == 0) {
//            p.setProperty("op", "oneCom");
//        } else {
//            p.setProperty("op", "allCom");
//        }
    }

    @Override
    public ActionLogger init(Properties param) throws LLException {
        loadParams(param);

        if (runDir != null)
            new File(runDir).mkdirs();

        env = new Environment(numberOfFeatures, numberOfClasses);
        env.init(param);

        for (int i = 0; i < numberOfAgents; i++) {
            agents.add(new Agent(i, env));
            agents.get(i).init(param);
        }
        loadTrainingSets();
        trainAgents();
        if (runDir != null)
            saveTrainingSets();
        if (loadRunDir == null)
            gameIt = new GameIterators.UniformRandomGameIterator(this);
        else {
            try {
                gameIt = Files.lines(Paths.get(loadRunDir, "games.tsv")).iterator();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

//        Independence (InvSocBias)
        TransmissionBias bias_indep = new TransmissionBiases.InvSocBias(this, discountIndep, 1-(1./numberOfClasses));
//        Novelty (not discounted)
        TransmissionBias bias_nov = new TransmissionBiases.NovBias(this);
//        Success (EnvBias)
        TransmissionBias bias_succ = new TransmissionBiases.EnvBias(this, discountSucc);
//        Authority
        TransmissionBias bias_auth = new TransmissionBiases.AuthBias(this, discountAuth);

        List<TransmissionBias> biases = new ArrayList<>();
        biases.add(bias_indep);
        biases.add(bias_nov);
        biases.add(bias_succ);
        biases.add(bias_auth);

        List<Double> coeffs = new ArrayList<>();
        coeffs.add(coeffIndep);
        coeffs.add(coeffNov);
        coeffs.add(coeffSucc);
        coeffs.add(coeffAuth);

        tBias = new TransmissionBiases.AggregatedBias(this, biases, coeffs);

        alogger = rManager.init(new DolaRecorders.Srate("ssrate"), new DolaRecorders.Accuracy(agents, env.getInputs()
                        ,env.getTargets(), "accuracy")
                , new DolaRecorders.AggregatedTransmissionBiasRecorder((TransmissionBiases.AggregatedBias) tBias,
//                        "envBias", "socBias", "invSocBias", "bias")
                        "indepBias", "novBias", "succBias", "authBias","bias")
//                        "authBias", "bias")
//                        "novBias", "bias")
                , new DolaRecorders.GameInstanceInfoRecorder(this)
                ,new DolaRecorders.Distance(agents, "distance")
                ,new DolaRecorders.GainLoss(agents, env, "ccorrectD", "gain", "loss")
        );

        return alogger;
    }
}
