package fr.inria.exmo.lazylavender.decisiontaking;

import fr.inria.exmo.lazylavender.decisiontaking.measurements.RecordingManager;
import fr.inria.exmo.lazylavender.decisiontaking.treeclassifier.Decision;
import fr.inria.exmo.lazylavender.decisiontaking.treeclassifier.IntDecision;
import fr.inria.exmo.lazylavender.expe.LLExperiment;
import fr.inria.exmo.lazylavender.decisiontaking.treeclassifier.NoClassException;
import fr.inria.exmo.lazylavender.logger.ActionLogger;
import fr.inria.exmo.lazylavender.model.LLException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Stream;

public class Experiment implements LLExperiment {
    final static Logger logger = LoggerFactory.getLogger(Experiment.class);
    protected Environment env;
    protected int numberOfFeatures = 4;
    protected int numberOfAgents = 40;
    protected int numberOfIterations = 40000;
    protected int numberOfClasses = 3;
    protected double ratio = 0.4;
    protected double sampleRatio = 0.2;
    protected boolean splitTrain = false;
    protected String runDir = null;
    protected String loadRunDir = null;
    protected int targetClass = -1;

    protected ArrayList<Agent> agents = new ArrayList<>();
    protected List<List<BitSet>> inputs = new ArrayList<>();
    protected List<List<Decision>> targets = new ArrayList<>();
    protected Iterator<String> gameIt;

    protected RecordingManager rManager = new RecordingManager();
    protected ActionLogger alogger;


    protected Properties params;
    protected TransmissionBias tBias;

    protected double coeffEnv = 1, coeffSoc = 0., coeffInvSoc = 0.;
    protected double discountEnv = 0, discountSoc = 0.9, discountInvSoc = 0.95;


    protected void loadParams(Properties p) {
        params = p;
        if (p.containsKey("numberOfFeatures"))
            numberOfFeatures = Integer.parseInt(p.getProperty("numberOfFeatures"));
        if (p.containsKey("numberOfAgents"))
            numberOfAgents = Integer.parseInt(p.getProperty("numberOfAgents"));
        if (p.containsKey("numberOfIterations"))
            numberOfIterations = Integer.parseInt(p.getProperty("numberOfIterations"));
        if (p.containsKey("nbIterations"))
            numberOfIterations = Integer.parseInt(p.getProperty("nbIterations"));
        if (p.containsKey("numberOfClasses"))
            numberOfClasses = Integer.parseInt(p.getProperty("numberOfClasses"));
        if (p.containsKey("ratio"))
            ratio = Double.parseDouble(p.getProperty("ratio"));
        if (p.containsKey("sampleRatio"))
            sampleRatio = Double.parseDouble(p.getProperty("sampleRatio"));
        if (p.containsKey("runDir"))
            runDir = p.getProperty("runDir");
        if (p.containsKey("loadRunDir"))
            loadRunDir = p.getProperty("loadRunDir");
        if (p.containsKey("splitTrain"))
            splitTrain = true;
        if (p.containsKey("targetClass"))
            targetClass = Integer.parseInt(p.getProperty("targetClass"));
        if (!p.containsKey("loadEnvDir") && p.containsKey("loadRunDir"))
            p.setProperty("loadEnvDir", loadRunDir);
        if (p.containsKey("coeffEnv"))
            coeffEnv = Double.parseDouble(p.getProperty("coeffEnv"));
        if (p.containsKey("coeffSoc"))
            coeffSoc = Double.parseDouble(p.getProperty("coeffSoc"));
        if (p.containsKey("coeffInvSoc"))
            coeffInvSoc = Double.parseDouble(p.getProperty("coeffInvSoc"));
        if (p.containsKey("discountEnv"))
            discountEnv = Double.parseDouble(p.getProperty("discountEnv"));
        if (p.containsKey("discountSoc"))
            discountSoc = Double.parseDouble(p.getProperty("discountSoc"));
        if (p.containsKey("discountInvSoc"))
            discountInvSoc = Double.parseDouble(p.getProperty("discountInvSoc"));

    }


    @Override
    public ActionLogger init(Properties param) throws LLException {
        loadParams(param);

        if (runDir != null)
            new File(runDir).mkdirs();

        env = new Environment(numberOfFeatures, numberOfClasses);
        env.init(param);

        for (int i = 0; i < numberOfAgents; i++) {
            agents.add(new Agent(i, env));
            agents.get(i).init(param);
        }
        loadTrainingSets();
        trainAgents();
        if (runDir != null)
            saveTrainingSets();
        if (loadRunDir == null)
            gameIt = new GameIterators.UniformRandomGameIterator(this);
        else {
            try {
                gameIt = Files.lines(Paths.get(loadRunDir, "games.tsv")).iterator();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        TransmissionBias tb1 = new TransmissionBiases.EnvBias(this,discountEnv);
        TransmissionBias tb2 = new TransmissionBiases.SocBias(this, discountSoc);
        TransmissionBias tb3 = new TransmissionBiases.InvSocBias(this, discountInvSoc, 1-(1./numberOfClasses));

        List<TransmissionBias> biases = new ArrayList<>();
        biases.add(tb1); biases.add(tb2); biases.add(tb3);

        List<Double> coeffs = new ArrayList<>();
        coeffs.add(coeffEnv); coeffs.add(coeffSoc); coeffs.add(coeffInvSoc);

        tBias = new TransmissionBiases.AggregatedBias(this, biases, coeffs);

        alogger = rManager.init(new DolaRecorders.Srate("ssrate"), new DolaRecorders.Accuracy(agents, env.getInputs()
                        ,env.getTargets(), "accuracy")
                , new DolaRecorders.AggregatedTransmissionBiasRecorder((TransmissionBiases.AggregatedBias) tBias,
                "envBias", "socBias", "invSocBias", "bias")
                , new DolaRecorders.GameInstanceInfoRecorder(this)
                ,new DolaRecorders.Distance(agents, "distance")
                ,new DolaRecorders.GainLoss(agents, env, "ccorrectD", "gain", "loss")
        );

        return alogger;
    }

    @Override
    public void process() throws LLException {

        PrintWriter prGames = null;
        if (runDir != null) {
            try {
                prGames = new PrintWriter(new File(runDir, "games.tsv"));
            } catch (FileNotFoundException fnfex) {

            }
        }

        int remaining = numberOfIterations;
        performTasks(0, agents);

        int count = 0;
        while (gameIt.hasNext()) {
            Agent a1, a2;

            BitSet instance;
            String g = gameIt.next();
            String[] spec = g.split("\t");
            a1 = agents.get(Integer.parseInt(spec[0]));
            a2 = agents.get(Integer.parseInt(spec[1]));
            instance = env.parseInstance(spec[2]);

            performTasks(++count, agents);
//            performTasks(count, a1, a2);
            Game game = new Game(a1, a2, instance, this);

//            update novelty bias and environment (success) bias
            if(tBias instanceof TransmissionBiases.AggregatedBias) {
                List<TransmissionBias> transmissionBiases = ((TransmissionBiases.AggregatedBias) tBias).getTransmissionBiases();
                for(TransmissionBias tb: transmissionBiases) {
                    if(tb instanceof TransmissionBiases.NovBias) {
                        tb.update(game);
                    }
                    else if(tb instanceof TransmissionBiases.EnvBias) {
                        tb.update(game);
                    }
                }
            }

            a1.playGame(remaining--, game);

            tBias.update(game);
//            updateAgentSuccess(game);

            rManager.update(game);

            alogger.logGame(a1.getId(), a2.getId(), game.getInstance() + "");
            alogger.logResult(game.isSuccess() ? "SUCCESS" : "FAILURE", game.isSuccess(), game.getAdaptation() + "");

            rManager.log(game);

            if (prGames != null){
                game.save(prGames);
            }

//            if(count%3000==0) {
//                System.out.println("yes count: " + Game.counterYes);
//                System.out.println("no count: " + Game.counterNo);
//
//                System.out.println("yes avg: " + Game.sumCommonYes/Game.counterYes);
//                System.out.println("no avg: " + Game.sumCommonNo/Game.counterNo);
//
//                System.out.println("no conflict count: " + Game.countNoConflict);
//
//                System.out.println("yes count: " + Game.counterYes2);
//                System.out.println("no count: " + Game.counterNo2);
//
//                System.out.println("yes avg: " + Game.sumCommonYes2/Game.counterYes2);
//                System.out.println("no avg: " + Game.sumCommonNo2/Game.counterNo2);
//                Game.counterYes = 0;
//                Game.counterNo = 0;
//                Game.sumCommonYes = 0;
//                Game.sumCommonNo = 0;
//
//                Game.counterYes2 = 0;
//                Game.counterNo2 = 0;
//                Game.sumCommonYes2 = 0;
//                Game.sumCommonNo2 = 0;
//
//
//            }
//            count++;
        }

        if (prGames != null) prGames.close();
    }


    /////////////////////// Saving experiment ///////////////////////////

    protected void saveTrainingSets() {
        String saveDir = runDir;
        if (saveDir.charAt(saveDir.length() - 1) == '/')
            saveDir += "training";
        else
            saveDir += "/training";
        new File(saveDir).mkdirs();
        for (int i = 0; i < numberOfAgents; i++) {

            String suffix = i + ".tsv";
            try {
                PrintWriter instsWriter = new PrintWriter(new File(saveDir, "instances" + suffix));
                PrintWriter tarsWriter = new PrintWriter(new File(saveDir, "targets" + suffix));
                for (int j = 0; j < inputs.get(i).size(); j++) {
                    instsWriter.println(inputs.get(i).get(j));
                    tarsWriter.println(targets.get(i).get(j));
                }
                instsWriter.close();
                tarsWriter.close();

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    protected void loadTrainingSets() {
        loadTrainingSets(env);
    }

    protected void loadTrainingSets(Environment environment) {
        String loadDir = loadRunDir;
        if (loadRunDir != null)
            if (loadDir.charAt(loadDir.length() - 1) == '/')
                loadDir += "training";
            else
                loadDir += "/training";

        List<BitSet> inputCopy = new ArrayList<>(environment.getInputs());
        List<Decision> targetCopy = new ArrayList<>(environment.getTargets());
        ArrayList<Integer> indexes = new ArrayList<>();
        for (int j = 0; j < inputCopy.size(); j++) {
            indexes.add(j);
        }

        Collections.shuffle(indexes);
        int sizePerAgent = inputCopy.size() / numberOfAgents;
        int index = 0;
        for (int i = 0; i < numberOfAgents; i++) {
            ArrayList<BitSet> ins = new ArrayList<>();
            ArrayList<Decision> tars = new ArrayList<>();
            if (loadRunDir == null) {
                if (!splitTrain) {
                    Random rand = new Random();
                    for (int j = 0; j < inputCopy.size(); j++) {
                        if (rand.nextDouble() < ratio && targetCopy.get(j) != null) {
                            ins.add(inputCopy.get(j));
                            tars.add(targetCopy.get(j));
                        }
                    }

                    if (ins.size() == 0) {
                        int r;
                        do {
                            r = rand.nextInt(inputCopy.size());
                            ins.add(inputCopy.get(r));
                            tars.add(targetCopy.get(r));
                        } while (environment.getTargets().get(r) == null);
                    }
                } else {
                    for (int j = 0; j < sizePerAgent; j++) {
                        ins.add(inputCopy.get(indexes.get(j + index)));
                        tars.add(targetCopy.get(indexes.get(j + index)));
                    }
                    index += sizePerAgent;
                    if (inputCopy.size() - index < sizePerAgent) // the rest are added to the last agents
                        for (int j = index; j < inputCopy.size(); j++) {
                            ins.add(inputCopy.get(indexes.get(j)));
                            tars.add(targetCopy.get(indexes.get(j)));
                        }
                }
            } else {
                String suffix = i + ".tsv";
                try {
                    Stream<String> p1 = Files.lines(Paths.get(loadDir, "instances" + suffix));
                    Stream<String> p2 = Files.lines(Paths.get(loadDir, "targets" + suffix));
                    Iterator<String> instances = p1.iterator();
                    Iterator<String> targets = p2.iterator();
                    while (instances.hasNext()) {
                        ins.add(environment.parseInstance(instances.next()));
                        tars.add(new IntDecision(Integer.parseInt(targets.next())));
                    }
                    p1.close();
                    p2.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
            inputs.add(ins);
            targets.add(tars);
        }
    }

    /**
     * Train agent classifiers
     */

    protected void trainAgents() {
        for (int i = 0; i < agents.size(); i++) {
            Agent agent = agents.get(i);
            List<BitSet> input = inputs.get(i);
            List<Decision> target = targets.get(i);
            try {
                agent.classifier.train(input, target);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Generate samples for tasks (save them in disk if loadRunDir parameter is set to rerun the exact same experiment)
     *
     * @param sampleRatio
     * @param interaction
     * @return
     */

    Common.Pair<List<BitSet>, List<Decision>> sampleFromEnv(double sampleRatio, int interaction) {
        return sampleFromEnv(sampleRatio, interaction, env);
    }

    Common.Pair<List<BitSet>, List<Decision>> sampleFromEnv(double sampleRatio, int interaction, Environment environment) {
        List<BitSet> inputs = new ArrayList<>();
        List<Decision> targets = new ArrayList<>();
        String suffix = interaction + ".tsv";


        if (loadRunDir != null) {
            String loadDir = loadRunDir;
            if (loadDir.charAt(loadDir.length() - 1) != '/')
                loadDir += "/samples/";
            else
                loadDir += "samples/";

            try {
                Stream<String> p1 = Files.lines(Paths.get(loadDir, "sampleInstances" + suffix));
                Stream<String> p2 = Files.lines(Paths.get(loadDir, "sampleTars" + suffix));
                Iterator<String> ins = p1.iterator();
                Iterator<String> tars = p2.iterator();
                while (ins.hasNext()) {
                    inputs.add(environment.parseInstance(ins.next()));
                    targets.add(new IntDecision(Integer.parseInt(tars.next())));
                }
                p1.close();
                p2.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            Random r = new Random();
            for (int i = 0; i < environment.getTargets().size(); i++)
                if (r.nextDouble() <= sampleRatio) {
                    inputs.add(environment.getInputs().get(i));
                    targets.add(environment.getTargets().get(i));
                }

            if (inputs.isEmpty()) {
                int index = r.nextInt(environment.getInputs().size());
                inputs.add(environment.getInputs().get(index));
                targets.add(environment.getTargets().get(index));
            }
        }

        if (runDir != null) {
            String saveDir = runDir;
            if (saveDir.charAt(saveDir.length() - 1) != '/')
                saveDir += "/samples/";
            else
                saveDir += "samples/";
            new File(saveDir).mkdirs();
            try {
                PrintWriter instanceWriter = new PrintWriter(new File(saveDir, "sampleInstances" + suffix));
                PrintWriter targetsWriter = new PrintWriter(new File(saveDir, "sampleTars" + suffix));

                for (int i = 0; i < inputs.size(); i++) {
                    instanceWriter.println(inputs.get(i));
                    targetsWriter.println(targets.get(i));
                }

                instanceWriter.close();
                targetsWriter.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }

        return new Common.Pair<>(inputs, targets);
    }


    private void scoreAgentByAccuracy(Agent agent, List<BitSet> inputs, List<Decision> targets) {
        agent.setPayoff(getAgentAccuracy(agent, inputs, targets));
    }

    protected void performTasks(int interaction, Agent... agents) {
        performTasks(interaction, Arrays.asList(agents));
    }

    protected void performTasks(int interaction, List<Agent> agents) {
           Common.Pair<List<BitSet>, List<Decision>> p = sampleFromEnv(sampleRatio, interaction);
            for (Agent a : agents) {
                scoreAgentByAccuracy(a, p.getKey(), p.getValue());
            }
    }

    protected void performTasks(int interaction, List<Agent> agents, Environment environment) {
        Common.Pair<List<BitSet>, List<Decision>> p = sampleFromEnv(sampleRatio, interaction, environment);
        for (Agent a : agents) {
            scoreAgentByAccuracy(a, p.getKey(), p.getValue());
        }
    }

    static public double getAgentAccuracy(Agent agent, List<BitSet> inputs, List<Decision> targets) {
        double correct = 0;
        for (int j = 0; j < inputs.size(); j++) {
            BitSet in = inputs.get(j);
            Decision tar = targets.get(j);
            try {
                Decision predicted = agent.getClassifier().getClass(in);
                if (predicted.equals(tar))
                    correct++;
            } catch (NoClassException e) {
                e.printStackTrace();
            }
        }
        return correct / inputs.size();
    }

    //////////////////// GETTERS & SETTERS ////////////////////////

    public Environment getEnv() {
        return env;
    }

    public void setEnv(Environment env) {
        this.env = env;
    }

    public int getNumberOfFeatures() {
        return numberOfFeatures;
    }

    public void setNumberOfFeatures(int numberOfFeatures) {
        this.numberOfFeatures = numberOfFeatures;
    }

    public int getNumberOfAgents() {
        return numberOfAgents;
    }

    public void setNumberOfAgents(int numberOfAgents) {
        this.numberOfAgents = numberOfAgents;
    }

    public int getNumberOfIterations() {
        return numberOfIterations;
    }

    public void setNumberOfIterations(int numberOfIterations) {
        this.numberOfIterations = numberOfIterations;
    }

    public ArrayList<Agent> getAgents() {
        return agents;
    }

    public int getNumberOfClasses() {
        return numberOfClasses;
    }

    public void setNumberOfClasses(int numberOfClasses) {
        this.numberOfClasses = numberOfClasses;
    }

    public int getNumberOfAgentsWithDecisionForInstance(BitSet instance, Decision c) {
        int counter = 0;
        for(Agent agent: agents) {
            try {
                if(agent.getClassifier().getClass(instance).equals(c))
                    counter++;
            } catch (NoClassException e) {
                e.printStackTrace();
            }
        }
        return counter;
    }

    /////// TEST DEBUG ONE EXPERIMENT ///////////

    public static void main(String[] args) throws LLException {
        Experiment exp = new Experiment();
        Properties p = new Properties();
        p.setProperty("trainer", "ID3");
        p.setProperty("op", "oneCom");
        p.setProperty("numberOfClasses", ""+exp.numberOfClasses);
        p.setProperty("numberOfFeatures", "3");
        p.setProperty("numberOfAgents", "4");
        p.setProperty("numberOfIterations", "5000");
        p.setProperty("coeffEnv", "1");
        p.setProperty("coeffInvSoc", "0");
        p.setProperty("coeffSoc", "0");
        p.setProperty("adaptFreq", "1");
        exp.init(p);

        List<Agent> agents = exp.agents;
//        System.out.println(exp.env.classifier);
//        exp.scoreAgents(agents);
        double avg = 0;
        for (Agent a : agents) {
            double perf = getAgentAccuracy(a, exp.env.getInputs(), exp.env.getTargets());
            avg += perf;
            System.out.println("score " + a.getId() + ": " + perf + " trans bias: ");// + exp.tBias.getValue(a, null));
        }
        avg /= agents.size();
        System.out.println("average score: " + avg);
        System.out.println("============== START ==============");

        for(Agent a: agents)
            System.out.println(a.getClassifier());

        agents.get(0).classifier = agents.get(1).classifier.copy();

        System.out.println("after");

        for(Agent a: agents)
            System.out.println(a.getClassifier());

//        exp.log(null);

//        for(Agent a: agents)
//            for(Agent a2: agents)
//                if(a.getId() != a2.getId())
//                {
//                    System.out.println("similarity a"+a.getId()+" with a"+ a2.getId()+": "+exp.getAgentOntoSim(a, a2));
////                    TreeClassifier.Node r1 = a.getClassifier().getRoot(), r2 = a2.getClassifier().getRoot();
////                    Map<TreeClassifier.Node, TreeClassifier.Node> m = exp.getTreeCommonNodes(r1, r2);
////                    for(TreeClassifier.Node k: m.keySet())
////                        System.out.println("similar nodes: " + k.getRCondition() + " and " + m.get(k).getRCondition());
//                }

//        for(Agent a: agents)
//            System.out.println(a.getClassifier());

        exp.process();

        avg = 0;
        TransmissionBiases.AggregatedBias tb = (TransmissionBiases.AggregatedBias) exp.tBias;
        for (Agent a : agents) {
            double perf = getAgentAccuracy(a, exp.env.getInputs(), exp.env.getTargets());
            avg += perf;
            System.out.println("score " + a.getId() + ": " + perf + " trans bias: ");// + exp.tBias.getValue(a, null) +
//                    " inv soc: " + tb.getTransmissionBiases().get(2).getValue(a, null)+
//                    " env: " + tb.getTransmissionBiases().get(0).getValue(a, null));
        }
        avg /= agents.size();

        System.out.println("average score: " + avg);
//        logger.info("avg score: " + avg);

        System.out.println("==============  END  ==============");

//        System.out.println("yes count: " + Game.counterYes);
//        System.out.println("no count: " + Game.counterNo);
//
//        System.out.println("yes avg: " + Game.sumCommonYes/Game.counterYes);
//        System.out.println("no avg: " + Game.sumCommonNo/Game.counterNo);
//
//        System.out.println("no conflict count: " + Game.countNoConflict);
//
//        System.out.println("yes count: " + Game.counterYes2);
//        System.out.println("no count: " + Game.counterNo2);
//
//        System.out.println("yes avg: " + Game.sumCommonYes2/Game.counterYes2);
//        System.out.println("no avg: " + Game.sumCommonNo2/Game.counterNo2);

//        for(Agent a: agents)
//            for(Agent a2: agents)
//                if(a.getId() != a2.getId())
//                {
//                    System.out.println("similarity a"+a.getId()+" with a"+ a2.getId()+": "+exp.getAgentOntoSim(a, a2));
////                    TreeClassifier.Node r1 = a.getClassifier().getRoot(), r2 = a2.getClassifier().getRoot();
////                    Map<TreeClassifier.Node, TreeClassifier.Node> m = exp.getTreeCommonNodes(r1, r2);
////                    for(TreeClassifier.Node k: m.keySet())
////                        System.out.println("similar nodes: " + k.getRCondition() + " ***** " + m.get(k).getRCondition());
//                }
        for(Agent a: agents)
            System.out.println(a.getClassifier());

        agents.get(0).classifier = agents.get(1).classifier.copy();

        System.out.println("after");

        for(Agent a: agents)
            System.out.println(a.getClassifier());
//        for (double s: exp.successRates)
//            System.out.println(s);
    }

    public Double getSuccessScore(Agent a) {
        return tBias.getValue(a, null);
    }
}
