package fr.inria.exmo.lazylavender.decisiontaking.disruption;

import fr.inria.exmo.lazylavender.decisiontaking.Environment;
import fr.inria.exmo.lazylavender.decisiontaking.treeclassifier.Decision;
import fr.inria.exmo.lazylavender.env.LLEnvironment;
import fr.inria.exmo.lazylavender.model.LLException;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

public class EnvironmentDisruption extends Environment {

    double diruptionRatio = 0.5;



    protected List<BitSet> allInputs = new ArrayList<>();
    protected List<Decision> allTargets = new ArrayList<>();

    public EnvironmentDisruption(int numberOfFeatures, int numberOfClasses) {
        super(numberOfFeatures, numberOfClasses);
    }

    @Override
    public LLEnvironment init(Properties param) throws LLException {
        if(param.containsKey("loadEnvDir"))
            loadEnvDir = param.getProperty("loadEnvDir");
        if(param.containsKey("runDir"))
            runDir = param.getProperty("runDir");
        if(param.containsKey("targetClass"))
            targetClass = Integer.parseInt(param.getProperty("targetClass"));
        if(param.containsKey("testSet"))
            testSet = Integer.parseInt(param.getProperty("testSet"));
        if(param.containsKey("testSetProp"))
            testSetProp = Double.parseDouble(param.getProperty("testSetProp"));
        if(param.containsKey("envDiruptionRatio"))
            diruptionRatio = Double.parseDouble(param.getProperty("envDiruptionRatio"));

        if(loadEnvDir != null)
            try {
                savedClasses = Files.lines( Paths.get(loadEnvDir, "env.tsv") ).iterator();
            } catch (IOException e) {
                e.printStackTrace();
            }

        if(runDir != null) {
            try {
                writer = new PrintWriter(new File(runDir , "env.tsv"));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

        }

        fillInstances();

        hideObjects();

        initTargets(inputs, targets);

        if(writer != null)
            writer.close();

        return this;
    }

    protected void hideObjects() {
        Random r = new Random();
        allInputs.addAll(inputs);
        allTargets.addAll(targets);

        List<Integer> toRemove = new ArrayList<>();
        for (int i = 0; i < inputs.size(); i++) {
            if(r.nextDouble() < diruptionRatio) {
                toRemove.add(i);
            }
        }

        for (int i = toRemove.size()-1; i >= 0; i--) {
            inputs.remove((int)toRemove.get(i));
            targets.remove((int)toRemove.get(i));
        }
    }

    public void disrupt() {
        inputs.clear();
        targets.clear();
        inputs.addAll(allInputs);
        targets.addAll(allTargets);
    }

    public List<BitSet> getAllInputs() {
        return allInputs;
    }

    public List<Decision> getAllTargets() {
        return allTargets;
    }
}
