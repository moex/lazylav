package fr.inria.exmo.lazylavender.decisiontaking.disruption;

import fr.inria.exmo.lazylavender.decisiontaking.*;
import fr.inria.exmo.lazylavender.logger.ActionLogger;
import fr.inria.exmo.lazylavender.model.LLException;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

public class ExperimentDisruption extends Experiment {

    int disruptionIter = 10000;
    double divReductionRatio = 0.5;
    DolaRecorders.Distance disRecorder;

    @Override
    protected void loadParams(Properties p) {
        super.loadParams(p);
        // load other parameters

        if(p.containsKey("disruptionIter"))
            disruptionIter = Integer.parseInt(p.getProperty("disruptionIter"));

        if(p.containsKey("divReductionRatio"))
            divReductionRatio = 1-Double.parseDouble(p.getProperty("divReductionRatio"));

    }


    @Override
    public ActionLogger init(Properties param) throws LLException {
        loadParams(param);

        if (runDir != null)
            new File(runDir).mkdirs();

        // change environment
        env = new EnvironmentDisruption(numberOfFeatures, numberOfClasses);
        env.init(param);

        for (int i = 0; i < numberOfAgents; i++) {
            agents.add(new Agent(i, env));
            agents.get(i).init(param);
        }
        loadTrainingSets();
        trainAgents();
        if (runDir != null)
            saveTrainingSets();
        if (loadRunDir == null)
            gameIt = new GameIterators.UniformRandomGameIterator(this);
        else {
            try {
                gameIt = Files.lines(Paths.get(loadRunDir, "games.tsv")).iterator();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        TransmissionBias tb1 = new TransmissionBiases.EnvBias(this,discountEnv);
        TransmissionBias tb2 = new TransmissionBiases.SocBias(this, discountSoc);
        TransmissionBias tb3 = new TransmissionBiases.InvSocBias(this, discountInvSoc, 1-(1./numberOfClasses));

        List<TransmissionBias> biases = new ArrayList<>();
        biases.add(tb1); biases.add(tb2); biases.add(tb3);

        List<Double> coeffs = new ArrayList<>();
        coeffs.add(coeffEnv); coeffs.add(coeffSoc); coeffs.add(coeffInvSoc);

        tBias = new TransmissionBiases.AggregatedBias(this, biases, coeffs);

        disRecorder = new DolaRecorders.Distance(agents, "distance");
        alogger = rManager.init(new DolaRecorders.Srate("ssrate"),
                new DolaRecorders.Accuracy(agents, ((EnvironmentDisruption)env).getAllInputs()
                        ,((EnvironmentDisruption)env).getAllTargets(), "accuracy")
                , new DolaRecorders.AccuracyOnEnv(agents, env, "envAccuracy")
                , new DolaRecorders.AggregatedTransmissionBiasRecorder((TransmissionBiases.AggregatedBias) tBias,
                        "envBias", "socBias", "invSocBias", "bias")
                , new DolaRecorders.GameInstanceInfoRecorder(this)
                ,disRecorder
                ,new DolaRecorders.GainLoss(agents, env, "ccorrectD", "gain", "loss")
        );

        return alogger;
    }

    @Override
    public void process() throws LLException {

        PrintWriter prGames = null;
        if (runDir != null) {
            try {
                prGames = new PrintWriter(new File(runDir, "games.tsv"));
            } catch (FileNotFoundException fnfex) {

            }
        }

        int remaining = numberOfIterations;
        performTasks(0, agents);

        int count = 0;
        while (gameIt.hasNext()) {
            Agent a1, a2;

            BitSet instance;
            String g = gameIt.next();
            String[] spec = g.split("\t");
            a1 = agents.get(Integer.parseInt(spec[0]));
            a2 = agents.get(Integer.parseInt(spec[1]));
            instance = env.parseInstance(spec[2]);

            performTasks(++count, agents);
//            performTasks(count, a1, a2);
            Game game = new Game(a1, a2, instance, this);
            a1.playGame(remaining--, game);

            tBias.update(game);
//            updateAgentSuccess(game);

            rManager.update(game);

            alogger.logGame(a1.getId(), a2.getId(), game.getInstance() + "");
            alogger.logResult(game.isSuccess() ? "SUCCESS" : "FAILURE", game.isSuccess(), game.getAdaptation() + "");

            rManager.log(game);

            if (prGames != null){
                game.save(prGames);
            }

            // modified only here: added disruption
            if(disruptNow(count)) {
//                System.out.println("before disruption:");
//                for (Agent a : agents)
//                    System.out.println(a.getClassifier());
                disRecorder.reInit();


//                double avg = 0;
//                for (Agent a : agents) {
//
//                    double perf = getAgentAccuracy(a, env.getInputs(), env.getTargets());
//                    avg += perf;
//                    System.out.println("score " + a.getId() + ": " + perf);
//                }
//                avg /= agents.size();
//                System.out.println(avg);
//                System.out.println("after ******************");



                this.disrupt();




//                System.out.println("after disruption:");
//                for (Agent a : agents)
//                    System.out.println(a.getClassifier());
//
//                avg = 0;
//                for (Agent a : agents) {
//
//                    double perf = getAgentAccuracy(a, env.getInputs(), env.getTargets());
//                    avg += perf;
//                    System.out.println("score " + a.getId() + ": " + perf);
//                }
//                avg /= agents.size();
//                System.out.println(avg);
//                System.out.println("done ******************");
            }

        }

        if (prGames != null) prGames.close();
    }

    protected boolean disruptNow(int iteration) {
        return iteration == disruptionIter;
    }

    protected void disrupt() {
        ((EnvironmentDisruption)env).disrupt();
        reduceAgentDiversity();
    }

    protected void reduceAgentDiversity() {
        Agent a = agents.get(0);
        for (int i = 1; i < agents.size(); i++) {
            Random r = new Random();
            if(r.nextDouble() < divReductionRatio) {
                agents.get(i).setClassifier(a.getClassifier().copy());
            }
        }
    }


    public static void main(String[] args) throws LLException {
        ExperimentDisruption exp = new ExperimentDisruption();
        Properties p = new Properties();
        p.setProperty("trainer", "ID3");
        p.setProperty("op", "oneCom");
        p.setProperty("numberOfClasses", "3");
        p.setProperty("numberOfFeatures", "5");
        p.setProperty("numberOfAgents", "20");
        p.setProperty("numberOfIterations", "30000");
        p.setProperty("coeffEnv", "1");
        p.setProperty("coeffInvSoc", "0");
        p.setProperty("coeffSoc", "0");
        p.setProperty("adaptFreq", "1");
        p.setProperty("disruptionIter", "10000");
        p.setProperty("divReductionRatio", "0.5");
        p.setProperty("envDiruptionRatio", "0.5");
        exp.init(p);

        List<Agent> agents = exp.agents;
//        System.out.println(exp.env.classifier);
//        exp.scoreAgents(agents);
        double avg = 0;
        System.out.println(exp.env.getInputs().size());
        for (Agent a : agents) {

            double perf = getAgentAccuracy(a, exp.env.getInputs(), exp.env.getTargets());
            avg += perf;
            System.out.println("score " + a.getId() + ": " + perf);
        }
        avg /= agents.size();
        System.out.println("average score: " + avg);
        System.out.println("============== START ==============");

//        for(Agent a: agents)
//            System.out.println(a.getClassifier());

        exp.process();

        avg = 0;
        for (Agent a : agents) {
            double perf = getAgentAccuracy(a, exp.env.getInputs(), exp.env.getTargets());
            avg += perf;
            System.out.println("score " + a.getId() + ": " + perf);
        }
        avg /= agents.size();

        System.out.println("average score: " + avg);

        System.out.println("==============  END  ==============");

//        for(Agent a: agents)
//            System.out.println(a.getClassifier());


    }

}
