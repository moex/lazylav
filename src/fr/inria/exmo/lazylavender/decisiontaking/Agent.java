/*
 *
 * Copyright (C) INRIA, 2020-2023
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 */

package fr.inria.exmo.lazylavender.decisiontaking;

import fr.inria.exmo.lazylavender.agent.LLAgent;
import fr.inria.exmo.lazylavender.decisiontaking.intrinsicmotivation.GroupGame;
import fr.inria.exmo.lazylavender.decisiontaking.intrinsicmotivation.MotivatedAgent;
import fr.inria.exmo.lazylavender.decisiontaking.treeclassifier.Decision;
import fr.inria.exmo.lazylavender.expe.LLGame;
import fr.inria.exmo.lazylavender.decisiontaking.treeclassifier.NoClassException;
import fr.inria.exmo.lazylavender.decisiontaking.treeclassifier.TreeClassifier;
import fr.inria.exmo.lazylavender.decisiontaking.treeclassifier.TreeClassifierQuick;
import fr.inria.exmo.lazylavender.decisiontaking.treeclassifier.changers.NodeClassChanger;
import fr.inria.exmo.lazylavender.decisiontaking.treeclassifier.changers.SplitNodeChanger;
import fr.inria.exmo.lazylavender.model.LLException;

import java.util.*;

public class Agent implements LLAgent {

    TreeClassifier classifier;
    Environment env;
    int id;
    double payoff;

//    boolean useNodeConf = false;
    boolean adapt = true;

    Map<TreeClassifier.Node, Integer> successful = new HashMap<>();
    Map<TreeClassifier.Node, Integer> tries = new HashMap<>();
    String adaptOp = "allCom";
    double adaptFreq = 0.1;

    public Agent(int id, Environment env) {
        this.id = id;
        this.env = env;
    }

    @Override
    public LLAgent init(Properties param) {
        
//        if(param.containsKey("scoring"))
//            if(Integer.parseInt(param.getProperty("scoring")) == Experiment.SUCCESS_SCORE)
//                useNodeConf = true;

        if(param.containsKey("op"))
            adaptOp = param.getProperty("op");
        if(param.containsKey("adapt"))
            adapt = param.getProperty("adapt").equals("1");
        if (param.containsKey("adaptFreq"))
            adaptFreq = Double.parseDouble(param.getProperty("adaptFreq"));
//        classifier = new TreeClassifierOnto(
//                new TreeClassifierOnto.CommonKnowledgeOnto(OWLManager.createOWLOntologyManager(),env));
        classifier = new TreeClassifierQuick(new TreeClassifierQuick.CommonKnowledgeQuick(), param);
        return this;
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public void playGame(int remaining, LLGame game) throws LLException {
        if (game instanceof GroupGame) {
            this.playGame(remaining, (GroupGame) game);
            return;
        }

        Game g = (Game) game;
        Agent a2 = (Agent)g.getSecondAgent();
        BitSet instance = g.getInstance();
        Decision c1 = classifier.getClass(instance), c2 = a2.getClassifier().getClass(instance);

        if (g.getFirstAgent() instanceof MotivatedAgent)
            ((MotivatedAgent) g.getFirstAgent()).resetCreativityReward();
        if (g.getSecondAgent() instanceof MotivatedAgent)
            ((MotivatedAgent) g.getSecondAgent()).resetCreativityReward();

        if(c1.equals(c2))
        {
            g.setSuccess(true);
            a2.addSuccess(instance);
            this.addSuccess(instance);
        }
        else
        {
            g.setSuccess(false);

            if(adapt) {
                Common.Pair<Agent, Agent> p = g.getWinnerLoser();
                Agent winner = p.getKey();
                Agent adapter = p.getValue();

                g.setWinner(winner);
                g.setLoser(adapter);

                Random r = new Random();
                if(r.nextDouble() < adaptFreq)
                {
//                    int cb = g.exp.getNumberOfAgentsWithDecisionForInstance(instance, env.getClass(instance));
                    TreeClassifier.Node preAdaptationNode = adapter.getClassifier().getNode(instance);
                    TreeClassifier.Condition condition = adapter.getAdaptationCondition(instance, winner);
                    TreeClassifier.Changer changer = adapter.adaptKnowledge(condition, instance,
                            winner.getClassifier().getClass(instance), g);
                    g.setAdaptation(changer);

                    if (adapter instanceof MotivatedAgent && changer instanceof SplitNodeChanger) {
                        TreeClassifier.Node postAdaptationNode = adapter.getClassifier().getNode(instance);
                        TreeClassifier.Node postAdaptationNode1 = postAdaptationNode.getParent().getChildren().get(0);
                        TreeClassifier.Node postAdaptationNode2 = postAdaptationNode.getParent().getChildren().get(1);
                        ((MotivatedAgent) adapter).increaseCuriosityReward();
                        ((MotivatedAgent) adapter).updateCreativityReward(preAdaptationNode,
                                postAdaptationNode1, postAdaptationNode2, postAdaptationNode == postAdaptationNode1,
                                instance);
                    }
//                    int ca = g.exp.getNumberOfAgentsWithDecisionForInstance(instance, env.getClass(instance));
//                    if(cb != ca)
//                        System.out.println("number changed from " + cb + " to " + ca);
                }
                a2.addFailure(instance);
                this.addFailure(instance);
            }
        }
    }



        /*
    overwritten functions, adapted to group games -> play game
     */

    /**
     * Searches for the majority decision and lets all agents, that did not share that decision, adapt to
     * a random "winning" agent (who had the majority decision).
     *
     * @param remaining the number of remaining iterations (not needed, but for overwriting)
     * @param game the group game played
     * @throws LLException
     */
    public void playGame(int remaining, GroupGame game) throws LLException {
        BitSet instance = game.getInstance();
        HashMap<Decision, ArrayList<Agent>> opinionGroups = new HashMap<>();
        HashMap<Decision, Double> majorityCount = new HashMap<>();

        // count this agent and its decision
        Decision agentsDecision = game.getFirstAgent().getClassifier().getClass(instance);
        opinionGroups.put(agentsDecision, new ArrayList<>(List.of(game.getFirstAgent())));
        double success = game.getExp().getSuccessScore(game.getFirstAgent());
        majorityCount.put(agentsDecision, Double.isNaN(success) ? 1 : success); // if there is no successBias -> count 1

        if (game.getFirstAgent() instanceof MotivatedAgent)
            ((MotivatedAgent) game.getFirstAgent()).resetCreativityReward();

        // go through all interaction partners and count them and their decision
        for (Agent agent : game.getOtherAgents()) {
            if (agent instanceof MotivatedAgent)
                ((MotivatedAgent) agent).resetCreativityReward();
            // get their decision
            agentsDecision = agent.getClassifier().getClass(instance);
            // get the agents success score
            success = game.getExp().getSuccessScore(agent);
            // if the success bias is disabled, add 1 otherwise the successScore for
            // the decision
            majorityCount.merge(agentsDecision, Double.isNaN(success) ? 1 : success, Double::sum);
            ArrayList<Agent> tmp = new ArrayList<>();
            // add agents to the other agents with the same decision
            if (opinionGroups.containsKey(agentsDecision))
                tmp = opinionGroups.get(agentsDecision);
            tmp.add(agent);
            opinionGroups.put(agentsDecision, tmp);
        }

        // get the list of all majority decisions
        Random rand = new Random();
        List<Decision> currentMajorityDecisions = new ArrayList<>();
        currentMajorityDecisions.add(agentsDecision);
        // go through all decision
        for (Decision decision : majorityCount.keySet()) {
            // if the majority count is higher, then the current max
            if (majorityCount.get(decision) > majorityCount.get(currentMajorityDecisions.get(0))) {
                // remove old majority decisions and add current one
                currentMajorityDecisions.clear();
                currentMajorityDecisions.add(decision);
            // otherwise, if the majority count is the same, add the decision to the majority decision -> equal
            } else if(Objects.equals(majorityCount.get(decision), majorityCount.get(currentMajorityDecisions.get(0))))
                currentMajorityDecisions.add(decision);
        }

        // get a random majority decision
        Decision majorityDecision = currentMajorityDecisions.get(rand.nextInt(currentMajorityDecisions.size()));

        // add the winners and losers based on the winning decision (majority decision)
        for (Decision decision : majorityCount.keySet()) {
            if (decision.equals(majorityDecision))
                game.addWinners(opinionGroups.get(decision));
            else game.addLosers(opinionGroups.get(decision));
        }

        // game successfull = all agents agree
        if (opinionGroups.size() == 1) {
            game.setSuccess(true);
            this.addSuccess(instance);
            game.getOtherAgents().forEach( agent -> agent.addSuccess(instance));
        } else { // failure
            game.setSuccess(false);

            List<Agent> winners = game.getWinners();
            List<Agent> losers = game.getLosers();
            Random r = new Random();
            // all losers have to adapt to the majority decision
            if(r.nextDouble() < getAdaptFreq()) {
                for (Agent loser: losers){
                    Agent adapter = loser;
                    // always pick a random winner and adapt to him
                    Agent winner = winners.get(r.nextInt(winners.size()));
                    TreeClassifier.Node preAdaptationNode = adapter.getClassifier().getNode(instance);
                    TreeClassifier.Condition condition = adapter.getAdaptationCondition(instance, winner);
                    TreeClassifier.Changer changer = adapter.adaptKnowledge(condition, instance,
                            winner.getClassifier().getClass(instance), game, adapter);
                    game.setAdaptation(adapter, changer);

                    if (adapter instanceof MotivatedAgent && changer instanceof SplitNodeChanger) {
                        TreeClassifier.Node postAdaptationNode = adapter.getClassifier().getNode(instance);
                        TreeClassifier.Node postAdaptationNode1 = postAdaptationNode.getParent().getChildren().get(0);
                        TreeClassifier.Node postAdaptationNode2 = postAdaptationNode.getParent().getChildren().get(1);
                        ((MotivatedAgent) adapter).increaseCuriosityReward();
                        ((MotivatedAgent) adapter).updateCreativityReward(preAdaptationNode,
                                postAdaptationNode1, postAdaptationNode2, postAdaptationNode == postAdaptationNode1,
                                instance);
                    }
                }
            }
        }
    }

    public TreeClassifier.Changer adaptKnowledge(TreeClassifier.Condition condition, BitSet instance,
                                                 Decision c1, GroupGame game, Agent agent2) throws NoClassException {
        TreeClassifier.Condition myCondition = getClassifier().getCondition(instance);
        Decision c2 = getClassifier().getClass(instance);
        TreeClassifier.Changer changer;
        TreeClassifier.Change change;
        if (condition == null || condition.subsumes(myCondition)) {
            changer = new NodeClassChanger(instance, c1);
        } else {
            changer = new SplitNodeChanger(instance, condition, c1, c2);
        }
        change = getClassifier().applyModification(changer);
        game.setChange(agent2, change);
        return changer;

    }

    public TreeClassifier.Changer adaptKnowledge(TreeClassifier.Condition condition, BitSet instance,
                                                 Decision c1, Game game) throws NoClassException {
        TreeClassifier.Condition myCondition = getClassifier().getCondition(instance);
        Decision c2 = getClassifier().getClass(instance);
        TreeClassifier.Changer changer;
        TreeClassifier.Change change;
        if(condition == null || condition.subsumes(myCondition))
        {
//            System.out.println("got in first with " + condition + " and "+ myCondition);
            changer = new NodeClassChanger(instance, c1);
            change = classifier.applyModification(changer);
        }
        else
        {
//            System.out.println("got in second");
            changer = new SplitNodeChanger(instance, condition, c1, c2);
            change = classifier.applyModification(changer);
        }
        game.setChange(change);
        return changer;
    }

    protected void addSuccess(BitSet instance) {
        try {
            TreeClassifier.Node n = this.getClassifier().getNode(instance);
            int tried = 0;
            if(tries.containsKey(n))
                tried = tries.get(n);
            tries.put(n, tried + 1);
            int success = 0;
            if(successful.containsKey(n))
                success = successful.get(n);
            successful.put(n, success + 1);
        } catch (NoClassException e) {
            e.printStackTrace();
        }
    }

    protected void addFailure(BitSet instance) {
        try {
            TreeClassifier.Node n = this.getClassifier().getNode(instance);
            int tried = 0;
            if(tries.containsKey(n))
                tried = tries.get(n);
            tries.put(n, tried + 1);
        } catch (NoClassException e) {
            e.printStackTrace();
        }
    }

    public TreeClassifier getClassifier() {
        return classifier;
    }

    public void setClassifier(TreeClassifier c) {
        classifier = c;
    }

    public double getRelativePayoff(BitSet instance) {
        return payoff;
    }

    public TreeClassifier.Condition getAdaptationCondition(BitSet instance, Agent winner) throws NoClassException { // called by adapter agent
        if(adaptOp.equals("allCom")) {
            return winner.getClassifier().getCondition(instance);
        }
        else if(adaptOp.equals("oneCom")) {
            List<TreeClassifier.Condition> conditions = winner.getConditionsToInstance(instance);
            List<TreeClassifier.Condition> conds = new ArrayList<>();
            for(TreeClassifier.Condition c : conditions) {
                if(!c.subsumes(getClassifier().getCondition(instance)))
                    conds.add(c);
            }

            Random r = new Random();
            if(conds.isEmpty())
                return null;
            else
                return conditions.get(r.nextInt(conditions.size())); // one random condition from winner's conditions
//                return conds.get(conds.size()-1);
        }
        else { // adaptOp == noCom
            List<TreeClassifier.Condition> conditions = getConditionsFromInstance(instance);
            Random r = new Random();
            if(conditions.isEmpty())
                return null;
            else
                return conditions.get(r.nextInt(conditions.size())); // one random condition from instance that do not exist in this.classifier
//                return conditions.get(conditions.size()-1);
        }
    }

    public List<TreeClassifier.Condition> getConditionsToInstance(BitSet instance) throws NoClassException {
        TreeClassifier.Node n = getClassifier().getNode(instance);
        List<TreeClassifier.Condition> conditions = new ArrayList<>();
        while (n.getParent() != null) {
            conditions.add(n.getCondition());
            n = n.getParent();
        }
        return conditions;
    }

    public List<TreeClassifier.Condition> getConditionsFromInstance(BitSet instance) throws NoClassException {
        TreeClassifier.Condition myCondition = getClassifier().getCondition(instance);
        TreeClassifier.NodeCreator nodeCreator = getClassifier().getNodeCreator();
        List<TreeClassifier.Condition> conditions = new ArrayList<>();

        for (int i = 0; i < env.getNumberOfFeatures(); i++) {
            TreeClassifier.Condition cond = nodeCreator.node(null, i, instance.get(i), null).getCondition();
            conditions.add(cond);
        }
        return conditions;
    }

    public void setPayoff(double score) {
        this.payoff = score;
    }

    public double getAdaptFreq() { return this.adaptFreq; }

}
