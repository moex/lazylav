package fr.inria.exmo.lazylavender.decisiontaking;

import fr.inria.exmo.lazylavender.decisiontaking.treeclassifier.Decision;
import fr.inria.exmo.lazylavender.decisiontaking.treeclassifier.IntDecision;
import fr.inria.exmo.lazylavender.env.LLEnvironment;
import fr.inria.exmo.lazylavender.model.LLException;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Environment implements LLEnvironment {

    protected int numberOfFeatures;
    protected int numberOfClasses;
    protected int testSet = 0;
    protected double testSetProp = 0;
    protected int targetClass = -1;
    protected HashMap<BitSet, Decision> oracle = new HashMap<>();
    protected List<BitSet> inputs = new ArrayList<>();
    protected List<Decision> targets = new ArrayList<>();
    protected List<BitSet> testInputs = new ArrayList<>();
    protected List<Decision> testTargets = new ArrayList<>();
    protected List<BitSet> randomGenerator = new ArrayList<>();

    protected String loadEnvDir = null;
    protected String runDir = null;

    protected Iterator<String> savedClasses = null;
    protected PrintWriter writer = null;

    public Environment(int numberOfFeatures, int numberOfClasses) {
        this.numberOfFeatures = numberOfFeatures;
        this.numberOfClasses = numberOfClasses;
    }

    public Decision getClass(BitSet instance)
    {
        return oracle.get(instance);
    }

    protected void initTargets(List<BitSet> inputs, List<Decision> targets)
    {
        for (int i = 0; i < inputs.size() ; i++) {
            oracle.put(inputs.get(i), targets.get(i));
        }
    }

    protected void fillInstances() {
        if(savedClasses != null) {
            while (savedClasses.hasNext()) {
                String[] line = savedClasses.next().split("\t");
                inputs.add(parseInstance(line[0]));
                int target = Integer.parseInt(line[1]);
                if(targetClass > 0)
                    target = (target == targetClass)?1:0;
                else
                    if(target >= numberOfClasses)
                        target = numberOfClasses - 1;
                targets.add(new IntDecision(target));
                if(writer != null)
                    writer.println(line[0] + "\t" + line[1]);
            }
        }
        else
            fillInstances(0, new BitSet());

        splitTrainingTestSets();
    }

    protected void fillInstances(int level, BitSet parentBitset)
    {
        BitSet b1 = new BitSet(), b2 = new BitSet();
        b1.or(parentBitset);
        b2.or(parentBitset);
        b1.set(level);
        Random rand = new Random();
        if(level == numberOfFeatures - 1)
        {
            inputs.add(b1);
            inputs.add(b2);
//            if(savedClasses != null)
//            {
//                targets.add(Integer.parseInt(savedClasses.next()));
//                targets.add(Integer.parseInt(savedClasses.next()));
//            }
//            else {
                targets.add(new IntDecision(rand.nextInt(numberOfClasses)));
                targets.add(new IntDecision(rand.nextInt(numberOfClasses)));
//            }

            if(writer != null) {
                writer.println(inputs.get(inputs.size()-2) + "\t" + targets.get(targets.size() - 2));
                writer.println(inputs.get(inputs.size()-1) + "\t" + targets.get(targets.size() - 1));
            }
            return;
        }
        fillInstances(level+1, b1);
        fillInstances(level+1, b2);
    }

    @Override
    public LLEnvironment init() throws LLException {
        return init(new Properties());
    }

    @Override
    public LLEnvironment init(Properties param) throws LLException {
        if(param.containsKey("loadEnvDir"))
            loadEnvDir = param.getProperty("loadEnvDir");
        if(param.containsKey("runDir"))
            runDir = param.getProperty("runDir");
        if(param.containsKey("targetClass"))
            targetClass = Integer.parseInt(param.getProperty("targetClass"));
        if(param.containsKey("testSet"))
            testSet = Integer.parseInt(param.getProperty("testSet"));
        if(param.containsKey("testSetProp"))
            testSetProp = Double.parseDouble(param.getProperty("testSetProp"));

        if(loadEnvDir != null)
            try {
                savedClasses = Files.lines( Paths.get(loadEnvDir, "env.tsv") ).iterator();
            } catch (IOException e) {
                e.printStackTrace();
            }

        if(runDir != null) {
            try {
                writer = new PrintWriter(new File(runDir , "env.tsv"));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

        }

        fillInstances();

        initTargets(inputs, targets);

        if(writer != null)
            writer.close();

        return this;
    }

    @Override
    public void logInit() throws LLException {

    }

    @Override
    public void logReference() throws LLException {

    }

    @Override
    public void logFinal() throws LLException {

    }

    @Override
    public BitSet parseInstance(String text) {
        Pattern pattern = Pattern.compile("\\{([0-9,\\s]*)\\}");
        Matcher matcher = pattern.matcher( text );
        if ( matcher.find() ) {
            BitSet instance = new BitSet( numberOfFeatures );
            for ( String item : matcher.group(1).split("\\s*,\\s*") ) {
                if ( !("".equals(item)) )
                    instance.set( Integer.parseInt( item ) );
            }
            return instance;
        } else return null;
    }

    @Override
    public BitSet generateInstance() {
        if(randomGenerator.size() == 0)
            randomGenerator.addAll(inputs);
        Random rand = new Random();
        int r = rand.nextInt(randomGenerator.size());
        BitSet instance = randomGenerator.get(r);
        randomGenerator.remove(r);

//        BitSet instance = new BitSet( numberOfFeatures );
//        for ( int i = 0; i<numberOfFeatures ; i++ ) instance.set( i, rand.nextBoolean() );
        return instance;
    }

    protected void splitTrainingTestSets() {
        if(testSetProp > 0) {
            int testSetSize = (int)(testSetProp * targets.size());
            int startingIndex = testSet*testSetSize;
            int count = 0;
            for (int i = startingIndex; (i < startingIndex+ testSetSize || startingIndex+2*testSetSize > targets.size())
                    && i < targets.size(); i++) {
                testInputs.add(inputs.get(i));
                testTargets.add(targets.get(i));
                count ++;
            }
            for (int i = 0; i < count; i++) {
                inputs.remove(startingIndex);
                targets.remove(startingIndex);
            }
        }
    }


    public List<BitSet> getInputs() {
        return inputs;
    }

    public List<Decision> getTargets() {
        return targets;
    }

    public List<BitSet> getTestInputs() {
        return testInputs;
    }

    public List<Decision> getTestTargets() {
        return testTargets;
    }

    public int getNumberOfFeatures() {
        return numberOfFeatures;
    }

    public int getNumberOfClasses() {
        return numberOfClasses;
    }
}
