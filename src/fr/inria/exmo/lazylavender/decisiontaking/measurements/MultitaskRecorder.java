package fr.inria.exmo.lazylavender.decisiontaking.measurements;

import fr.inria.exmo.lazylavender.decisiontaking.multitask.Task;
import fr.inria.exmo.lazylavender.expe.LLGame;

import java.util.List;


/**
 * To be extended into custom recorders.
 * In the custom recorder, an inner state can be added which may contain other data
 * for example references to agent objects, to the environment, to the experiment, etc)
 */

public interface MultitaskRecorder extends Recorder {

    /**
     * Returns whether a recorder contains a task
     * @param task
     * @return A boolean value
     */
    boolean containsTask(Task task);

}
