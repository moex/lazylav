package fr.inria.exmo.lazylavender.decisiontaking.measurements;

import fr.inria.exmo.lazylavender.decisiontaking.multitask.MultitaskGame;
import fr.inria.exmo.lazylavender.expe.LLGame;
import fr.inria.exmo.lazylavender.logger.ActionLogger;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class RecordingManager {

    protected List<Recorder> recorders = new ArrayList<>();
    protected ActionLogger alogger;
    protected int size;


    public ActionLogger init(Recorder... recs) {
        return init(Arrays.asList(recs));
    }

    public ActionLogger init(List<Recorder> recs) {
        for(Recorder rec: recs)
            addRecorder(rec);

        ArrayList<String> measures = new ArrayList<>();
        for(Recorder rec: recorders) {
            measures.addAll(rec.getNames());
        }

        String[] arr = new String[measures.size()];
        int i = 0;
        for(String name: measures)
            arr[i++] = name;

        size = i;

        alogger = new ActionLogger(false, arr);

        return alogger;
    }

    /**
     * update must be called after each game to update the state of recorders.
     * @param game
     */

    public void update(LLGame game) {

        for(int i=0; i<recorders.size(); i++) {
            if(recorders.get(i) instanceof  MultitaskRecorder && game instanceof MultitaskGame){
                MultitaskRecorder mrecorder = (MultitaskRecorder) recorders.get(i);
                MultitaskGame mgame = (MultitaskGame) game;
                if(mrecorder.containsTask(mgame.getTask())){
                    mrecorder.update(game);
                }
            }else {
                recorders.get(i).update(game);
            }
        }

    }

    /**
     * logs the measures, this is called when a log is needed.
     * @param game
     */

    public void log(LLGame game) {
        double[] measures = new double[size];
        int i = 0;
        for(Recorder rec : recorders) {
            List<Double> values = rec.record(game);
            for (Double value: values) {
                measures[i++] = value;
            }
        }
        alogger.logMeasures2(measures);
    }

    protected void addRecorder(Recorder recorder) {
        recorders.add(recorder);
    }

    public ActionLogger getAlogger() {
        return alogger;
    }

    public List<Recorder> getRecorders() {
        return recorders;
    }
}
