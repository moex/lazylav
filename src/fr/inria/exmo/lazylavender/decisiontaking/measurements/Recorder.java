package fr.inria.exmo.lazylavender.decisiontaking.measurements;

import fr.inria.exmo.lazylavender.expe.LLGame;

import java.util.List;


/**
 * To be extended into custom recorders.
 * In the custom recorder, an inner state can be added which may contain other data
 * for example references to agent objects, to the environment, to the experiment, etc)
 */

public interface Recorder {

    /**
     * updates the state of the recorder, this is called after each game.
     * @param game
     * @return
     */
    void update(LLGame game);

    /**
     * takes the last game as parameter and returns measured values.
     * @param game
     * @return
     */
    List<Double> record(LLGame game);

    /**
     * @return list of measure names. Eg: "srate", "accuracy", "distance", etc.
     */
    List<String> getNames();
}
