package fr.inria.exmo.lazylavender.decisiontaking.treeclassifier;

import fr.inria.exmo.lazylavender.decisiontaking.treeclassifier.dttrainers.*;

import java.util.*;

public class TreeClassifierQuick extends TreeClassifierAbstract {

    public static class CommonKnowledgeQuick implements CommonKnowledge {

    }



    public TreeClassifierQuick(CommonKnowledge knowledge, Properties p) {
        super(knowledge, p);
    }

    @Override
    public TreeClassifier copy() {
        TreeClassifierQuick classifier = new TreeClassifierQuick(knowledge, properties);
        classifier.root = root.copy();
        return classifier;
    }

    @Override
    public void train(List<BitSet> input, List<Decision> target) throws Exception {
        super.train(input, target);
        int numAtt = 0;
        for(BitSet instance: input)
        {
            int lastSet = instance.previousSetBit(1000) + 1;
            if(lastSet > numAtt)
                numAtt = lastSet;
        }

        ArrayList<Integer> attributes = new ArrayList<>();
        for (int i = 0; i < numAtt; i++) {
            attributes.add(i);
        }
//        ID3Trainer trainer = new ID3Trainer(this, BitsetNode::new);
        root = trainer.train(input, target, attributes);
//        root = ID3(input, target, attributes, -1, false, null);
    }

    @Override
    public NodeCreator getNodeCreator() {
        return BitsetNode::new;
    }

    @Override
    public String toString()
    {
        String result = "tree:\n"+root;

        return result;
    }



    @Override
    public Decision getClass(BitSet input) throws NoClassException {
        BitsetNode root = (BitsetNode) this.root;
        return root.classify(input);
    }

    @Override
    public Node getNode(BitSet instance) throws NoClassException {
        BitsetNode root = (BitsetNode) this.root;
        return root.getNode(instance);
    }

    @Override
    public Condition getCondition(BitSet instance) {
        BitsetNode root = (BitsetNode) this.root;
        return root.getConditionInstance(instance);
    }



    @Override
    public Node createNode(Node parent, Condition condition, Decision c) {
        BitsetNode node = new BitsetNode(c, condition, parent);
        parent.addChild(node);
        return node;
    }

    @Override
    public Node getRoot() {
        return root;
    }


    public static class BitsetCondition implements Condition {
        /**
         * denotes the attributes on which there is a condition
         * a bit is set if there is a condition on it, unset if there is no condition on it
         */
        BitSet conditionedAttributes = new BitSet();
        /**
         * denotes the conditions on the attributes
         * each bit represent the value of that attribute
         */
        BitSet conditions = new BitSet();
        /**
         * returns not this condition if not is true
         */
        boolean not;
        /**
         * and conditions must be true for this condition to be true
         */
        ArrayList<Condition> andConditions = new ArrayList<>();
        /**
         * breaks a loop of and conditions
         */
        boolean loopBreaker = false;

        public BitsetCondition(int conditionedAttribute, boolean value, BitsetCondition parentCondition, boolean not) {
            this.conditionedAttributes.set(conditionedAttribute);
            if(value)
                this.conditions.set(conditionedAttribute);
            this.not = not;
            if(parentCondition != null)
                and(parentCondition);
//            this.parentCondition = parentCondition;
        }

        public BitsetCondition(BitSet conditionedAttributes, BitSet conditions, BitsetCondition parentCondition, boolean not) {
            this.conditionedAttributes.or(conditionedAttributes);
            this.conditions.or(conditions);
            this.conditions.and(conditionedAttributes);
            this.not = not;
            if(parentCondition != null)
                and(parentCondition);
//            this.parentCondition = parentCondition;
        }

        @Override
        public boolean evaluate(BitSet instance)
        {
            if(loopBreaker) // this condition called itself in a loop to evaluate itself
                return true;
            boolean value = true;
            if(!conditionedAttributes.isEmpty()) {
                BitSet toEvaluate = new BitSet();
                toEvaluate.clear();
                toEvaluate.or(instance);
                toEvaluate.and(conditionedAttributes);
                toEvaluate.xor(conditions);
                value = toEvaluate.isEmpty();
            }
            loopBreaker = true;
//            value = value && (parentCondition == null || parentCondition.evaluate(instance));
            for(Condition c: andConditions)
                if(!c.evaluate(instance)) {
                    value = false;
                    break;
                }
            loopBreaker = false;
            if(not)
                return !value;
            return value;
        }

        @Override
        public void and(Condition condition)
        {
            if(not) {
                Condition c = copy();
                // make this condition true
                andConditions.clear();
                conditionedAttributes.clear();
                conditions.clear();
                not = false;
                // add its copy to and conditions
                andConditions.add(c);
            }
            andConditions.add(condition);
        }

        @Override
        public void not()
        {
            this.not = ! this.not;
        }

        public boolean isNot() { return not; }

        public BitSet getConditions() { return this.conditions; }

        public BitSet getConditionedAttributes() {
            return this.conditionedAttributes;
        }

        @Override
        public String toString()
        {
            String neg = "\\neg ", land = "\\land ";
            int index = 0;
            ArrayList<Integer> atts = new ArrayList<>();
            while (conditionedAttributes.nextSetBit(index) != -1)
            {
                atts.add(conditionedAttributes.nextSetBit(index));
                index = conditionedAttributes.nextSetBit(index) + 1;
            }
            String result = "";
            if(not)
                result = neg+"(";
            for(Integer att: atts)
            {
                if(conditions.get(att))
                    result += (char)('a'+att) + land;
                else
                    result += neg+(char)('a'+att)+ land;
            }

            for (Condition cond: andConditions)
                if(!cond.toString().equals(""))
                    result += cond + land;
            if(result.length() == 0)
                result = "";
            else
                result = result.substring(0, result.length()-6);
            if(not)
                result += ")";
            return result;
        }

        @Override
        public Condition copy()
        {
            BitsetCondition condition = new BitsetCondition(conditionedAttributes, conditions, null, not);
            for(Condition c: andConditions)
                condition.andConditions.add(c.copy());
            return condition;
        }

        boolean lastBitLoopBreaker = false;

        public int getLastSetBit()
        {
            int lastSet = conditionedAttributes.previousSetBit(1000);
            if(lastBitLoopBreaker)
                return lastSet;
            lastBitLoopBreaker = true;
            for(Condition and: andConditions)
            {
//                BitsetCondition con = (BitsetCondition) and;
                int bit = ((BitsetCondition) and).getLastSetBit();
                if(bit > lastSet)
                    lastSet = bit;
            }
            lastBitLoopBreaker = false;
            return lastSet;
        }

        @Override
        public boolean subsumes(Condition condition) {
            int lastSet = getLastSetBit();
            BitsetCondition con = (BitsetCondition) condition;
            lastSet = Math.max(lastSet, con.getLastSetBit());
            for (int i = 0; i < (1<<(lastSet+1)); i++) {
                BitSet instance = BitSet.valueOf(new long[]{i});
                if(con.evaluate(instance) && !evaluate(instance))
                    return false;
            }
            return true;
        }

        @Override
        public boolean disjoint(Condition condition) {
            int lastSet = getLastSetBit();
            BitsetCondition con = (BitsetCondition) condition;
            lastSet = Math.max(lastSet, con.getLastSetBit());
            for (int i = 0; i < (1<<(lastSet+1)); i++) {
                BitSet instance = BitSet.valueOf(new long[]{i});
                if(con.evaluate(instance) && evaluate(instance))
                    return false;
            }
            return true;
        }

    }

    private static class BitsetNode implements Node {
        ArrayList<Node> children= new ArrayList<>();
        Decision c;
        Condition condition;
        Node parent;


        BitsetNode(Decision c, int attribute, boolean value, Node parent) {
            BitSet att = new BitSet();
            if(attribute >= 0)
                att.set(attribute);
            BitSet val = new BitSet();
            if(value) val.set(attribute);
            condition = new BitsetCondition(att, val, null, false);
            this.c = c;
            this.parent = parent;
        }

        BitsetNode(Decision c, Condition condition, Node parent) {
            this.condition = condition;
            this.c = c;
            this.parent = parent;
        }

        Decision classify(BitSet instance) throws NoClassException {
            BitsetNode node = (BitsetNode) getNode(instance);
            return node.getC();

        }

        Node getNode(BitSet instance) throws NoClassException {
            if(isLeaf())
                return this;
            for (Node c: children) {
                BitsetNode child = (BitsetNode) c;
                if (child.containsInstance(instance))
                    return child.getNode(instance);
            }
            throw new NoClassException("not leaf and instance satisfies no child");
        }

        @Override
        public Node getInstanceNode(BitSet instance)
        {
            for(Node c: children)
            {
                BitsetNode child = (BitsetNode) c;
                if(child.containsInstance(instance))
                    return child.getInstanceNode(instance); // root has no condition
            }
            return this;
        }


        public Condition getConditionInstance(BitSet instance)
        {
            for(Node c: children)
            {
                BitsetNode child = (BitsetNode) c;
                if(child.containsInstance(instance))
                    return child.getConditionInstance(instance, null); // root has no condition
            }
            return condition;
        }

        Condition getConditionInstance(BitSet instance, Condition parent)
        {
            Condition condition = getCondition().copy();
            if(parent != null)
                condition.and(parent);

            for(Node c: children)
            {
                BitsetNode child = (BitsetNode) c;
                if(child.containsInstance(instance))
                    return child.getConditionInstance(instance, condition);
            }
            return condition; // case this is leaf
        }

        boolean containsInstance(BitSet instance) {
            return condition.evaluate(instance);
        }

        @Override
        public void addChild(Node child) {
            children.add(child);
        }

        @Override
        public boolean isLeaf()
        {
            return children.isEmpty();
        }

        @Override
        public Condition getCondition() {
            return condition;
        }

        @Override
        public Condition getRCondition() {
            if(getParent() == null)
                return getCondition().copy();
            Condition c = getCondition().copy();
            c.and(parent.getRCondition());
            return c;
        }

        public void setCondition(BitsetCondition condition) {
            this.condition = condition;
        }

        @Override
        public Decision getC() {
            return c;
        }

        @Override
        public void setC(Decision c) {
            this.c = c;
        }

        @Override
        public Node copy() {
            return copy(null);
        }

        public Node copy(Node parent)
        {
            Node node = new BitsetNode(c, condition.copy(), parent);
            for(Node child: children)
                node.addChild(((BitsetNode)child).copy(node));
            return node;
        }

        @Override
        public String toString()
        {
            String tabs = "";
            for(Node p = parent; p != null; p = p.getParent())
                tabs += "----";
            String result = tabs+">("+condition+")";
            if(children.size() == 0)
                result += ": "+c;
            result += "\n";
            for (Node child: children)
                result += child;

            return result;
        }

        public ArrayList<Node> getChildren() {
            return children;
        }

        @Override
        public Node getParent() {
            return parent;
        }
    }
}
