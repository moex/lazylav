package fr.inria.exmo.lazylavender.decisiontaking.treeclassifier;

import fr.inria.exmo.lazylavender.decisiontaking.Environment;
import fr.inria.exmo.lazylavender.decisiontaking.treeclassifier.dttrainers.*;
import org.semanticweb.HermiT.Reasoner;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.*;
import org.semanticweb.owlapi.reasoner.OWLReasoner;
import org.semanticweb.owlapi.reasoner.OWLReasonerFactory;
import org.semanticweb.owlapi.reasoner.SimpleConfiguration;

import java.util.*;

public class TreeClassifierOnto extends TreeClassifierAbstract {

    public static class CommonKnowledgeOnto implements TreeClassifier.CommonKnowledge {
        public static String uriPrefix = "http://lazylav.exmo.inria.fr/godecisions/ontology/";
        public static String agentPrefix = "agent";
        public static String featurePrefix = "feature";

        OWLOntologyManager manager;
        OWLDataFactory factory;
        OWLReasoner reasoner;
        OWLOntology commonOntology;
        Environment env;
//        Map<String, OWLObjectProperty> properties = new HashMap<>();
        List<OWLObjectProperty> tproperties = new ArrayList<>();
        List<OWLObjectProperty> fproperties = new ArrayList<>();
        List<OWLClass> decisionClasses = new ArrayList<>();
        Map<String, OWLIndividual> individuals = new HashMap<>();
        Map<OWLObjectProperty, OWLIndividual> tvalues = new HashMap<>();

        public CommonKnowledgeOnto(OWLOntologyManager manager, Environment env) {
            this.manager = manager;
            this.env = env;
            this.factory = manager.getOWLDataFactory();

            try {
                commonOntology = manager.createOntology(IRI.create(uriPrefix + "common"));
            } catch (OWLOntologyCreationException e) {
                e.printStackTrace();
            }
            OWLReasonerFactory reasonerFactory = new Reasoner.ReasonerFactory();
            reasoner = reasonerFactory.createReasoner(commonOntology, new SimpleConfiguration());
            fillCommon();
        }

        protected void fillCommon() {
            for (int i = 0; i < env.getNumberOfFeatures(); i++) {
                String iri = uriPrefix+featurePrefix+"#"+(char)('a'+i);
                tproperties.add(factory.getOWLObjectProperty(IRI.create(iri)));
                String iri2 = uriPrefix+featurePrefix+"#n"+(char)('a'+i);
                fproperties.add(factory.getOWLObjectProperty(IRI.create(iri2)));
                manager.applyChange(new AddAxiom(commonOntology,
                        factory.getOWLDisjointObjectPropertiesAxiom(tproperties.get(i), fproperties.get(i))));
                tvalues.put(tproperties.get(i), factory.getOWLAnonymousIndividual("valueOf" + (char)('a'+i)));
                tvalues.put(fproperties.get(i), factory.getOWLAnonymousIndividual("valueOf" + (char)('a'+i)));
//                System.out.println("disjoint: " + tproperties.get(i) + " and " + fproperties.get(i));
            }

            for (int i = 0; i < env.getNumberOfClasses(); i++) {
                String iri = uriPrefix+"D"+i;
                decisionClasses.add(factory.getOWLClass(IRI.create(iri)));
            }

            for(BitSet individual: env.getInputs()) {
                addIndividual(individual);
            }
        }

        protected OWLIndividual addIndividual(BitSet b) {
            OWLIndividual individual = factory.getOWLNamedIndividual(IRI.create(b+""));
//            while (b.nextSetBit(index) != -1)
//            System.out.println(b);
            for (int i = 0; i < env.getNumberOfFeatures(); i++)
            {
                OWLObjectProperty p = getProperty(i);
                OWLObjectProperty np = getfProperty(i);
                OWLObjectPropertyAssertionAxiom a;
                if(b.get(i))
                    a = factory.getOWLObjectPropertyAssertionAxiom(p, individual, tvalues.get(p));
                else
                    a = factory.getOWLObjectPropertyAssertionAxiom(np, individual, tvalues.get(np));

//                System.out.println(a);
                manager.applyChange(new AddAxiom(commonOntology, a));
            }
            individuals.put(b+"", individual);
            return individual;
        }

        public boolean isInstanceOf(BitSet indiv, OWLClassExpression e) {
            OWLIndividual individual = individuals.get(indiv.toString());
            reasoner = new Reasoner.ReasonerFactory().createReasoner(commonOntology);
//            reasoner.flush();
            return reasoner.isEntailed(factory.getOWLClassAssertionAxiom(e,individual));
        }

        public boolean subsumes(OWLClassExpression c1, OWLClassExpression c2) {
            reasoner = new Reasoner.ReasonerFactory().createReasoner(commonOntology);
            return reasoner.isEntailed(factory.getOWLSubClassOfAxiom(c2, c1));
        }

        public boolean disjoint(OWLClassExpression c1, OWLClassExpression c2) {
            reasoner = new Reasoner.ReasonerFactory().createReasoner(commonOntology);
            return reasoner.isEntailed(factory.getOWLDisjointClassesAxiom(c1, c2));
        }

        public OWLObjectProperty getProperty(int index) {
            return tproperties.get(index);
        }

        public OWLObjectProperty getfProperty(int index) {
            return fproperties.get(index);
        }

        public OWLClassExpression hasTProperty(int index) {
            if(index == -1)
                return factory.getOWLThing();
            OWLObjectProperty p = getProperty(index);
            return factory.getOWLObjectHasValue(p, tvalues.get(p));
        }

        public OWLClassExpression hasFProperty(int index) {
            if(index == -1)
                return factory.getOWLThing();
            OWLObjectProperty np = getfProperty(index);
            return factory.getOWLObjectHasValue(np, tvalues.get(np));
        }
    }

    private static int count=0;
    private OWLOntology ontology;
    OWLOntologyManager manager;
    OWLDataFactory factory;
    OWLReasoner reasoner;
    CommonKnowledgeOnto common;
    NodeOnto r;




    public TreeClassifierOnto(TreeClassifier.CommonKnowledge knowledge, Properties p) {
        super(knowledge, p);
        this.manager = OWLManager.createOWLOntologyManager();
        common = (CommonKnowledgeOnto) knowledge;
        try {
            init(manager.createOntology(IRI.create(CommonKnowledgeOnto.uriPrefix+ CommonKnowledgeOnto.agentPrefix+(count++))));
        } catch (OWLOntologyCreationException e) {
            e.printStackTrace();
        }
    }

    public TreeClassifier copy() {
        TreeClassifierOnto classifier = new TreeClassifierOnto(knowledge, properties);
        classifier.root = root.copy();
        return classifier;
    }

    public void init(OWLOntology ontology) {
        this.ontology = ontology;
        this.manager = ontology.getOWLOntologyManager();
        this.factory = manager.getOWLDataFactory();
        OWLReasonerFactory reasonerFactory = new Reasoner.ReasonerFactory();
        this.reasoner = reasonerFactory.createReasoner(ontology, new SimpleConfiguration());
    }

    @Override
    public void train(List<BitSet> input, List<Decision> target) throws Exception {
//        final CommonKnowledgeOnto common = this

        ArrayList<Integer> attributes = new ArrayList<>();
        for (int i = 0; i < common.env.getNumberOfFeatures(); i++) {
            attributes.add(i);
        }
        root = trainer.train(input, target, attributes);
        r = (NodeOnto) root;
    }

    @Override
    public NodeCreator getNodeCreator() {
        return (c, attribute, value, parent) -> {
            NodeOnto n = new NodeOnto(knowledge, c, attribute, value, parent);
            addNodeToOnto(n);
            return n;
        };
    }

    public void addNodeToOnto(NodeOnto n) {
        OWLClass cl = n.cl;
        OWLClassExpression e = ((ConditionOnto)n.getRCondition()).expression;
        manager.applyChange(new AddAxiom(ontology, factory.getOWLEquivalentClassesAxiom(cl, e)));
    }

    @Override
    public Decision getClass(BitSet input) throws NoClassException {
        return r.classify(input);
    }

    @Override
    public TreeClassifier.Node getNode(BitSet instance) throws NoClassException {
        return r.getNode(instance);
    }

    @Override
    public TreeClassifier.Condition getCondition(BitSet instance) throws NoClassException {
        return r.getConditionInstance(instance);
    }

    @Override
    public TreeClassifier.Node createNode(TreeClassifier.Node parent, TreeClassifier.Condition condition, Decision c) {
        NodeOnto n = new NodeOnto(common, c, condition, parent);
        parent.addChild(n);
        addNodeToOnto(n);
        return n;
    }

    @Override
    public TreeClassifier.Node getRoot() {
        return root;
    }

    @Override
    public String toString()
    {
        String result = "tree:\n"+root;

        return result;
    }

    public static class ConditionOnto implements TreeClassifier.Condition {

        OWLClassExpression expression;
        CommonKnowledgeOnto common;

        public ConditionOnto(CommonKnowledgeOnto common, int attribute, boolean value) {
            this.common = common;
            if(value)
                expression = common.hasTProperty(attribute);
            else
                expression = common.hasFProperty(attribute);
        }

        public ConditionOnto(OWLClassExpression expression, CommonKnowledgeOnto common) {
            this.expression = expression;
            this.common = common;
        }

        @Override
        public boolean evaluate(BitSet instance) {
            return common.isInstanceOf(instance, expression);
        }

        @Override
        public void and(TreeClassifier.Condition condition) {
            ConditionOnto c = (ConditionOnto) condition;
            expression = common.factory.getOWLObjectIntersectionOf(expression, c.expression);
        }

        @Override
        public void not() {
            expression = expression.getObjectComplementOf();
        }

        @Override
        public TreeClassifier.Condition copy() {
            ConditionOnto o = new ConditionOnto(expression.getNNF(), common);
            return o;
        }

        @Override
        public boolean subsumes(TreeClassifier.Condition condition) {
            ConditionOnto c = (ConditionOnto) condition;
            return common.subsumes(expression, c.expression);
        }

        @Override
        public boolean disjoint(TreeClassifier.Condition condition) {
            ConditionOnto c = (ConditionOnto) condition;
            return common.disjoint(expression, c.expression);
        }

        @Override
        public String toString() {
            String result = expression.getNNF()+"";
            result = result.replace("ObjectIntersectionOf", "and");
            result = result.replace("ObjectUnionOf", "or");
            result = result.replace("ObjectComplementOf", "not");
            result = result.replace(CommonKnowledgeOnto.uriPrefix, "");
            result = result.replace("ObjectOneOf(_:valueOf", "");
//            result = result.replace("ObjectSomeValuesFrom", "");
            return result;
        }
    }

    public static class NodeOnto implements TreeClassifier.Node {
        ArrayList<TreeClassifier.Node> children= new ArrayList<>();
        OWLClass cl;
        Decision c;
        TreeClassifier.Condition condition;
        TreeClassifier.Node parent;
        CommonKnowledgeOnto common;

        static int count = 1;

        public NodeOnto(TreeClassifier.CommonKnowledge common, Decision c, int attribute, boolean value, TreeClassifier.Node parent) {
            this.common = (CommonKnowledgeOnto) common;
            condition = new ConditionOnto(this.common, attribute, value);
            this.c = c;
            this.parent = parent;
            this.cl = createClass();
        }

        public NodeOnto(TreeClassifier.CommonKnowledge common, Decision c, TreeClassifier.Condition condition, TreeClassifier.Node parent) {
            this.common = (CommonKnowledgeOnto) common;
            this.c = c;
            this.condition = condition;
            this.parent = parent;
            this.cl = createClass();
        }

        private OWLClass createClass() {
            String uriPrefix = CommonKnowledgeOnto.uriPrefix;
            String agentPrefix = CommonKnowledgeOnto.agentPrefix;
            return common.factory.getOWLClass(IRI.create(uriPrefix+agentPrefix+"#C"+count));
        }

        @Override
        public void addChild(TreeClassifier.Node child) {
            children.add(child);
        }

        @Override
        public boolean isLeaf() {
            return children.isEmpty();
        }

        @Override
        public List<TreeClassifier.Node> getChildren() {
            return children;
        }

        @Override
        public TreeClassifier.Node getInstanceNode(BitSet instance) {
            for(TreeClassifier.Node c: children)
            {
                NodeOnto child = (NodeOnto) c;
                if(child.containsInstance(instance))
                    return child.getInstanceNode(instance); // root has no condition
            }
            return this;
        }

        @Override
        public TreeClassifier.Condition getCondition() {
            return condition;
        }

        @Override
        public TreeClassifier.Condition getRCondition() {
            if(getParent() == null)
                return getCondition().copy();
            TreeClassifier.Condition c = getCondition().copy();
            c.and(parent.getRCondition());
            return c;
        }

        @Override
        public TreeClassifier.Node getParent() {
            return parent;
        }

        @Override
        public Decision getC() {
            return c;
        }

        @Override
        public void setC(Decision c) {
            this.c = c;
        }

        @Override
        public TreeClassifier.Node copy() {
            return copy(null);
        }

        public TreeClassifier.Node copy(TreeClassifier.Node parent)
        {
            TreeClassifier.Node node = new NodeOnto(common, c, condition.copy(), parent);
            for(TreeClassifier.Node child: children)
                node.addChild(((NodeOnto)child).copy(node));
            return node;
        }

        @Override
        public String toString()
        {
            String tabs = "";
            for(TreeClassifier.Node p = parent; p != null; p = p.getParent())
                tabs += "----";
            String result = tabs+">("+condition+")";
            if(children.size() == 0)
                result += ": "+c;
            result += "\n";
            for (TreeClassifier.Node child: children)
                result += child;

            return result;
        }

        boolean containsInstance(BitSet instance) {
            return condition.evaluate(instance);
        }

        Decision classify(BitSet instance) throws NoClassException {
            NodeOnto node = (NodeOnto) getNode(instance);
            return node.getC();

        }

        TreeClassifier.Node getNode(BitSet instance) throws NoClassException {
            if(isLeaf())
                return this;
            for (TreeClassifier.Node c: children) {
                NodeOnto child = (NodeOnto) c;
                if (child.containsInstance(instance))
                    return child.getNode(instance);
            }
            throw new NoClassException("not leaf and instance satisfies no child");
        }

        public TreeClassifier.Condition getConditionInstance(BitSet instance)
        {
            for(TreeClassifier.Node c: children)
            {
                NodeOnto child = (NodeOnto) c;
                if(child.containsInstance(instance))
                    return child.getConditionInstance(instance, null); // root has no condition
            }
            return condition;
        }

        TreeClassifier.Condition getConditionInstance(BitSet instance, TreeClassifier.Condition parent)
        {
            TreeClassifier.Condition condition = getCondition().copy();
            if(parent != null)
                condition.and(parent);

            for(TreeClassifier.Node c: children)
            {
                NodeOnto child = (NodeOnto) c;
                if(child.containsInstance(instance))
                    return child.getConditionInstance(instance, condition);
            }
            return condition; // case this is leaf
        }

    }

}
