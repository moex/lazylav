package fr.inria.exmo.lazylavender.decisiontaking.treeclassifier.dttrainers;

import fr.inria.exmo.lazylavender.decisiontaking.multitask.MultitaskTreeClassifier;

public abstract class MultitaskDTTrainer implements MultitaskTrainer {

    MultitaskTreeClassifier classifier;
    MultitaskTreeClassifier.MultiNodeCreator creator;

    public MultitaskDTTrainer(MultitaskTreeClassifier classifier, MultitaskTreeClassifier.MultiNodeCreator creator) {
        this.classifier = classifier;
        this.creator = creator;
    }
}
