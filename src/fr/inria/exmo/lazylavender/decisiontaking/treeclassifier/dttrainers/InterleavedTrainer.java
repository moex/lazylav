package fr.inria.exmo.lazylavender.decisiontaking.treeclassifier.dttrainers;

import fr.inria.exmo.lazylavender.decisiontaking.multitask.*;
//import jdk.swing.interop.SwingInterOpUtils;

import java.util.*;

public class InterleavedTrainer extends MultitaskDTTrainer {

    enum SplittingMethod {
        FIRST,
        RANDOM,
        BEST_SPLIT
    }

    private ArrayList<Integer> attributes;

    private MultitaskTreeClassifier.MultiTaskNode rootNode;

    private SplittingMethod splittingMethod = SplittingMethod.RANDOM;

    private int classesLimit = 2048;

    public InterleavedTrainer(MultitaskTreeClassifier multiTreeClassifier, MultitaskTreeClassifier.MultiNodeCreator multiNodeCreator, int classesLimit) {
        super(multiTreeClassifier, multiNodeCreator);
        this.classesLimit = classesLimit;
    }

    @Override
    public MultitaskTreeClassifier.MultiTaskNode train(ArrayList<TrainingExample> examples, ArrayList<Integer> attributes, MultitaskTreeClassifier.MultiTaskNode rootNode) {

        this.attributes = attributes;
        this.rootNode = rootNode;

        //System.out.println("\n\n\n Training agent");

        for (int i = 0; i < examples.size(); i++) {
            processExample(examples.get(i), this.rootNode, this.rootNode);
        }

        if(this.rootNode==null){
            System.out.println("Examples size: "+examples.size());
            System.out.println("Root node was null after training!");
        }

        return this.rootNode;
    }

    private void processExample(TrainingExample newInstance, MultitaskTreeClassifier.MultiTaskNode currentNode, MultitaskTreeClassifier.MultiTaskNode rootNode){

        //System.out.println("Current task to train: "+newInstance.getTask().getName());

        if(currentNode==null){

            MultitaskDecision rootMultitaskDecision = new MultitaskDecision();
            rootMultitaskDecision.putDecision(newInstance.getTask(), newInstance.getCorrectDecision());
            this.rootNode = creator.multitaskNode(rootMultitaskDecision, -1, false, null);
            this.rootNode.addAssociatedInstance(newInstance.getInstance());

            return;
        }else if(currentNode.isLeaf()){

            //TODO IF MUST SPLIT AND HAVE AVAILABLE NUMBER OF CLASSES BASED ON KNOWLEDGE LIMIT
            if(mustSplit(newInstance, currentNode)){

                double actualClasses = MultitaskRecorders.Distance.getTreeClassesSize(rootNode);

                //System.out.println("Actual classes: "+actualClasses+" Classes limit: "+classesLimit);
                boolean eligibleForSplit = actualClasses < classesLimit;

                if(eligibleForSplit) {
                    int splittingAttribute = getSplittingAttribute(newInstance, currentNode);

                    if (splittingAttribute != -1) {
                        splitNode(newInstance, currentNode, splittingAttribute);
                    }else{
                        //System.out.println("Splitting attribute == -1");
                        currentNode.addAssociatedInstance(newInstance.getInstance());
                        currentNode.getMultiDecision().putDecision(newInstance.getTask(), newInstance.getCorrectDecision());
                    }
                }else{
                    //NOT ELIGIBLE FOR SPLIT
                    currentNode.addAssociatedInstance(newInstance.getInstance());
                    currentNode.getMultiDecision().putDecision(newInstance.getTask(), newInstance.getCorrectDecision());
                }
            }else{
                currentNode.addAssociatedInstance(newInstance.getInstance());
                currentNode.getMultiDecision().putDecision(newInstance.getTask(), newInstance.getCorrectDecision());
            }
        }else{
            //TRAVERSE RECURSIVELY
            for (int i = 0; i < currentNode.getChildren().size(); i++) {
                MultitaskTreeClassifier.MultiTaskNode currentChild = currentNode.getChildren().get(i);
                if(currentChild.getCondition().evaluate(newInstance.getInstance())){
                    processExample(newInstance, currentChild, rootNode);
                    return;
                }
            }
        }
    }

    private void splitNode(TrainingExample newExample, MultitaskTreeClassifier.MultiTaskNode currentNode, int splittingAttribute){

        MultitaskDecision currentNodeMultitaskDecision = currentNode.getMultiDecision();
        HashSet<BitSet> currentNodeInstances = currentNode.getAssociatedInstances();

        ArrayList<BitSet> trueNodeInstances = new ArrayList<BitSet>();
        ArrayList<BitSet> falseNodeInstances = new ArrayList<BitSet>();

        MultitaskDecision newTrueMultitaskDecision = new MultitaskDecision();
        newTrueMultitaskDecision.setDecisions(currentNodeMultitaskDecision.copyDecisions());

        MultitaskDecision newFalseMultitaskDecision = new MultitaskDecision();
        newFalseMultitaskDecision.setDecisions(currentNodeMultitaskDecision.copyDecisions());

        if(newExample.getInstance().get(splittingAttribute)){
            trueNodeInstances.add(newExample.getInstance());
            newTrueMultitaskDecision.putDecision(newExample.getTask(), newExample.getCorrectDecision());
        }else{
            falseNodeInstances.add(newExample.getInstance());
            newFalseMultitaskDecision.putDecision(newExample.getTask(), newExample.getCorrectDecision());
        }

        for (BitSet currentBitSet : currentNodeInstances) {
            if(currentBitSet.get(splittingAttribute)){
                trueNodeInstances.add(currentBitSet);
            }else{
                falseNodeInstances.add(currentBitSet);
            }
        }

        currentNode.removeAllAssociatedInstances();

        MultitaskTreeClassifier.MultiTaskNode trueChild = creator.multitaskNode(newTrueMultitaskDecision, splittingAttribute, true, currentNode);
        for(BitSet trueBitSet : trueNodeInstances){
            trueChild.addAssociatedInstance(trueBitSet);
        }
        currentNode.addChild(trueChild);

        MultitaskTreeClassifier.MultiTaskNode falseChild = creator.multitaskNode(newFalseMultitaskDecision, splittingAttribute, false, currentNode);
        for(BitSet falseBitSet : falseNodeInstances){
            falseChild.addAssociatedInstance(falseBitSet);
        }
        currentNode.addChild(falseChild);

    }

    private boolean mustSplit(TrainingExample example, MultitaskTreeClassifier.MultiTaskNode node){
        if(node.getMultiDecision().hasTask(example.getTask()) && example.getCorrectDecision()!=node.getMultiDecision().getDecision(example.getTask())){
            return true;
        }else{
            return false;
        }
    }

    private int getSplittingAttribute(TrainingExample newExample, MultitaskTreeClassifier.MultiTaskNode node){

        BitSet newInstance = newExample.getInstance();

        HashSet<BitSet> existingInstances = node.getAssociatedInstances();

        ArrayList<BitSet> xorInstances = new ArrayList<BitSet>();

        for (BitSet existingInstance : existingInstances) {
            if(newInstance.equals(existingInstance)){
                continue;
            }else{
                BitSet cloneInstance = (BitSet) existingInstance.clone();
                cloneInstance.xor(newInstance);
                xorInstances.add(cloneInstance);
            }
        }

        if(xorInstances.isEmpty()){
            //System.out.println("Xor instances is empty!");
            return -1;
        }

        BitSet andResultBitSet = xorInstances.get(0);

        for (int i = 0; i < xorInstances.size(); i++) {
            andResultBitSet.and(xorInstances.get(i));
        }

        ArrayList<Integer> setBitsIndices = new ArrayList<Integer>();

        for (int i = andResultBitSet.nextSetBit(0); i != -1; i = andResultBitSet.nextSetBit(i + 1)) {
            setBitsIndices.add(i);
        }

        if(setBitsIndices.isEmpty()){
            //System.out.println("setBitsIndices is empty!");
            return -1;
        }else{
            //System.out.println("Possible splitting attributes : "+setBitsIndices.size());
        }

        //TODO FOR NOW WE HAVE THE OPTION TO RETURN EITHER A RANDOM OR THE FIRST POSSIBLE SPLITTING BIT. FIRST IS NOW PREFERABLE FOR REPRODUCIBLE RESULTS

        switch (splittingMethod) {
            case FIRST:
                return setBitsIndices.get(0);
                //return setBitsIndices.get(new Random().nextInt(setBitsIndices.size()));
            case RANDOM:
                return setBitsIndices.get(new Random().nextInt(setBitsIndices.size()));
            default:
                return setBitsIndices.get(new Random().nextInt(setBitsIndices.size()));
        }
    }

    public MultitaskTreeClassifier.MultiTaskNode getRootNode(){
        return this.rootNode;
    }



}
