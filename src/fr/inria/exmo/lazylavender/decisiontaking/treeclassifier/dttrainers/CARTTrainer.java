package fr.inria.exmo.lazylavender.decisiontaking.treeclassifier.dttrainers;

import fr.inria.exmo.lazylavender.decisiontaking.treeclassifier.Decision;
import fr.inria.exmo.lazylavender.decisiontaking.treeclassifier.TreeClassifier;
import fr.inria.exmo.lazylavender.decisiontaking.treeclassifier.TreeClassifierQuick;

import java.util.*;

public class CARTTrainer extends DTTrainer {

    public CARTTrainer(TreeClassifier classifier, TreeClassifier.NodeCreator creator) {
        super(classifier, creator);
    }


    @Override
    public TreeClassifier.Node train(List<BitSet> input, List<Decision> target, ArrayList<Integer> attributes) {
        return CART(input, target, attributes, -1, false, null);
    }

    private TreeClassifier.Node CART(List<BitSet> input, List<Decision> target, ArrayList<Integer> attributes,
                                    int attribute, boolean value, TreeClassifier.Node parent)
    {

        HashMap<Decision, List<BitSet>> map = new HashMap<>();
        for (int i = 0; i < input.size(); i++) {
            if(!map.containsKey(target.get(i)))
                map.put(target.get(i), new ArrayList<>());
            map.get(target.get(i)).add(input.get(i));
        }

        if(map.keySet().size() == 1)
            return creator.node(map.keySet().iterator().next(), attribute, value, parent);
//            return new TreeClassifierQuick.BitsetNode(map.keySet().iterator().next(), attribute, value, parent);

        Decision maxClass = null;
        int maxSize = 0;
        for (Map.Entry<Decision, List<BitSet>> entry: map.entrySet()) {
            if(maxSize < entry.getValue().size())
            {
                maxSize = entry.getValue().size();
                maxClass = entry.getKey();
            }
        }
        if(attributes.isEmpty())
            return creator.node(maxClass, attribute, value, parent);
//            return new TreeClassifierQuick.BitsetNode(maxClass, attribute, value, parent);


        int a = getBestAttributeSplit(map, attributes);
        TreeClassifier.Node r = creator.node(null, attribute, value, parent);
//        TreeClassifier.Node r = new TreeClassifierQuick.BitsetNode(-1, attribute, value, parent);
        ArrayList<BitSet> inT = new ArrayList<>(), inF = new ArrayList<>();
        ArrayList<Decision> tarT = new ArrayList<>(), tarF = new ArrayList<>();
        for (int i = 0; i < input.size(); i++) {
            if(input.get(i).get(a))
            {
                inT.add(input.get(i));
                tarT.add(target.get(i));
            }
            else
            {
                inF.add(input.get(i));
                tarF.add(target.get(i));
            }
        }

        ArrayList<Integer> newAtts = new ArrayList<>(attributes);
//        for(Integer v : newAtts)
//            System.out.print(v+ " ");
//        System.out.println(a);
        newAtts.remove(Integer.valueOf(a));

        if(inT.isEmpty()) {
//            classifier.createNode(r, new TreeClassifierQuick.BitsetCondition(a, true, null, false), maxClass);
            TreeClassifier.Node child = creator.node(maxClass, a, true, r);
            r.addChild(child);
        }
        else
            r.addChild(CART(inT, tarT, newAtts, a, true, r));

        if(inF.isEmpty()) {
//            classifier.createNode(r, new TreeClassifierQuick.BitsetCondition(a, false, null, false), maxClass);
            TreeClassifier.Node child = creator.node(maxClass, a, false, r);
            r.addChild(child);
        }
        else
            r.addChild(CART(inF, tarF, newAtts, a, false, r));

        return r;
    }

    private int getBestAttributeSplit(HashMap<Decision, List<BitSet>> map, ArrayList<Integer> possibleAttributes)
    {
        HashMap<Integer, Double> AGain = new HashMap<>();

        for(Integer att: possibleAttributes) {
            HashMap<Decision, Integer> classMapT = new HashMap<>(), classMapF = new HashMap<>();
            double totalT = 0, totalF = 0;
            for (Map.Entry<Decision, List<BitSet>> entry: map.entrySet())
            {
                Decision c = entry.getKey();
                List<BitSet> instances = entry.getValue();
                int sumT = 0, sumF= 0;

                for(BitSet instance: instances)
                    if(instance.get(att))
                        sumT++;
                    else
                        sumF++;

                totalT += sumT;
                totalF += sumF;
                classMapT.put(c, sumT);
                classMapF.put(c, sumF);
            }
            double h1 = 0, h2 = 0;
            for(Decision c: map.keySet())
            {
                double x = (totalT!=0)?(double)classMapT.get(c)/totalT:0;
                h1 += x*x;
                x = (totalF!=0)?(double)classMapF.get(c)/totalF:0;
                h2 += x*x;
            }

            h1 = 1 - h1;
            h2 = 1 - h2;

            double total = totalT+totalF;
            double p1 = totalT/total, p2 = totalF/total;
            double gini =  p1*h1 + p2*h2;
            AGain.put(att, gini);
        }

        double maxGain = -10000000;
        int bestAtt = -1;
        for (Map.Entry<Integer, Double> entry: AGain.entrySet()) {
            if(maxGain <= entry.getValue())
            {
                maxGain = entry.getValue();
                bestAtt = entry.getKey();
            }
        }
        if(bestAtt == -1)
        {
            System.out.println("max gain: " + maxGain + " size of AGain: "+ AGain.size());
            for (Map.Entry<Integer, Double> entry: AGain.entrySet())
                System.out.println(entry.getKey() + ": " + entry.getValue());
            System.out.println(map.size());
            System.out.println();
            System.out.println();
        }
        return bestAtt;
    }
}
