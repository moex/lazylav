package fr.inria.exmo.lazylavender.decisiontaking.treeclassifier.dttrainers;

import fr.inria.exmo.lazylavender.decisiontaking.treeclassifier.Decision;
import fr.inria.exmo.lazylavender.decisiontaking.treeclassifier.TreeClassifier;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.List;

public interface Trainer {

    TreeClassifier.Node train(List<BitSet> input, List<Decision> target, ArrayList<Integer> attributes);

}
