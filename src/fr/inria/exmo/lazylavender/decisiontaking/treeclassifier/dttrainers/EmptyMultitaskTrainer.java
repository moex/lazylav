package fr.inria.exmo.lazylavender.decisiontaking.treeclassifier.dttrainers;

import fr.inria.exmo.lazylavender.decisiontaking.multitask.MultitaskTreeClassifier;
import fr.inria.exmo.lazylavender.decisiontaking.multitask.TrainingExample;
import fr.inria.exmo.lazylavender.decisiontaking.treeclassifier.Decision;
import fr.inria.exmo.lazylavender.decisiontaking.treeclassifier.IntDecision;
import fr.inria.exmo.lazylavender.decisiontaking.treeclassifier.TreeClassifier;

import java.util.*;

public class EmptyMultitaskTrainer extends MultitaskDTTrainer {


    public EmptyMultitaskTrainer(MultitaskTreeClassifier classifier, MultitaskTreeClassifier.MultiNodeCreator creator) {
        super(classifier, creator);
    }


    @Override
    public MultitaskTreeClassifier.MultiTaskNode train(ArrayList<TrainingExample> trainingExamples, ArrayList<Integer> attributes, MultitaskTreeClassifier.MultiTaskNode currentNode) {
        return null;
    }
}
