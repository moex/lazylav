package fr.inria.exmo.lazylavender.decisiontaking.treeclassifier.dttrainers;

import fr.inria.exmo.lazylavender.decisiontaking.treeclassifier.TreeClassifier;

import java.util.Properties;

public abstract class DTTrainer implements Trainer {



    TreeClassifier classifier;
    TreeClassifier.NodeCreator creator;

    public DTTrainer(TreeClassifier classifier, TreeClassifier.NodeCreator creator) {
        this.classifier = classifier;
        this.creator = creator;
    }
}
