package fr.inria.exmo.lazylavender.decisiontaking.treeclassifier.dttrainers;

import fr.inria.exmo.lazylavender.decisiontaking.multitask.MultitaskTreeClassifier;
import fr.inria.exmo.lazylavender.decisiontaking.multitask.TrainingExample;

import java.util.ArrayList;

public interface MultitaskTrainer {

    MultitaskTreeClassifier.MultiTaskNode train(ArrayList<TrainingExample> trainingExamples, ArrayList<Integer> attributes, MultitaskTreeClassifier.MultiTaskNode currentNode);

}
