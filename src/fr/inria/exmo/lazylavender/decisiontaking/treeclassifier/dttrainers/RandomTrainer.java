package fr.inria.exmo.lazylavender.decisiontaking.treeclassifier.dttrainers;

import fr.inria.exmo.lazylavender.decisiontaking.treeclassifier.Decision;
import fr.inria.exmo.lazylavender.decisiontaking.treeclassifier.IntDecision;
import fr.inria.exmo.lazylavender.decisiontaking.treeclassifier.TreeClassifier;

import java.util.*;

public class RandomTrainer extends DTTrainer {

    double probaLeaf = 0;
    int targets = 0;

    public RandomTrainer(TreeClassifier classifier, TreeClassifier.NodeCreator creator, Properties p) {
        super(classifier, creator);
        if (p.containsKey("tr.probaLeaf"))
            probaLeaf = Double.parseDouble(p.getProperty("tr.probaLeaf"));
        if (p.containsKey("numberOfClasses"))
            targets = Integer.parseInt(p.getProperty("numberOfClasses"));
    }

    public RandomTrainer(TreeClassifier classifier, TreeClassifier.NodeCreator creator,
                         double probaLeaf, int targets) {
        super(classifier, creator);
        this.probaLeaf = probaLeaf;
        this.targets = targets;
    }


    @Override
    public TreeClassifier.Node train(List<BitSet> input, List<Decision> target, ArrayList<Integer> attributes) {

        if(targets == 0) {
            targets = 1;
            Map<Decision, Integer> decisions = new HashMap<>();
            for(Decision t: target)
                decisions.put(t, 1);

            targets = decisions.size();
        }

        return randomTree(attributes, -1 ,false, null);
    }

    private TreeClassifier.Node randomTree(ArrayList<Integer> attributes, int attribute,
                                    boolean value, TreeClassifier.Node parent) {
        if(probaLeaf == 0)
            probaLeaf = 1./(double) (attributes.size() + 1);


        Random r = new Random();
        if(r.nextDouble() < probaLeaf || attributes.isEmpty()) {
            Decision c = new IntDecision(r.nextInt(targets));
            return creator.node(c, attribute, value, parent);
        }
        else {
            int index = r.nextInt(attributes.size());
            int splitAtt = attributes.get(index);
            attributes.remove(index);
            TreeClassifier.Node splitNode = creator.node(null, attribute, value, parent);

            splitNode.addChild(randomTree(attributes, splitAtt, true, splitNode));
            splitNode.addChild(randomTree(attributes, splitAtt, false, splitNode));
            return splitNode;
        }
    }
}
