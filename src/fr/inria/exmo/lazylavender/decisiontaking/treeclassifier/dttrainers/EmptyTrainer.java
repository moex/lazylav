package fr.inria.exmo.lazylavender.decisiontaking.treeclassifier.dttrainers;

import fr.inria.exmo.lazylavender.decisiontaking.treeclassifier.Decision;
import fr.inria.exmo.lazylavender.decisiontaking.treeclassifier.IntDecision;
import fr.inria.exmo.lazylavender.decisiontaking.treeclassifier.TreeClassifier;

import java.util.*;

public class EmptyTrainer extends DTTrainer {

    int targets=2;

    public EmptyTrainer(TreeClassifier classifier, TreeClassifier.NodeCreator creator, Properties p) {
        super(classifier, creator);
        if (p.containsKey("numberOfClasses"))
            targets = Integer.parseInt(p.getProperty("numberOfClasses"));
    }

    public EmptyTrainer(TreeClassifier classifier, TreeClassifier.NodeCreator creator, int numberOfClasses) {
        super(classifier, creator);
        targets = numberOfClasses;
    }

    @Override
    public TreeClassifier.Node train(List<BitSet> input, List<Decision> target, ArrayList<Integer> attributes) {

        Random r = new Random();
        Decision c = new IntDecision(r.nextInt(targets));
        return creator.node(c, -1, false, null);
    }
}
