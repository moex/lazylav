package fr.inria.exmo.lazylavender.decisiontaking.treeclassifier.changers;

import fr.inria.exmo.lazylavender.decisiontaking.multitask.MultitaskDecision;
import fr.inria.exmo.lazylavender.decisiontaking.multitask.MultitaskTreeClassifier;
import fr.inria.exmo.lazylavender.decisiontaking.multitask.MultitaskingAgent;
import fr.inria.exmo.lazylavender.decisiontaking.multitask.Task;
import fr.inria.exmo.lazylavender.decisiontaking.treeclassifier.NoClassException;

import java.util.BitSet;

/**
 * Splits node of TreeClassifier according to a condition
 */
public class SplitMultiTaskNodeChanger implements MultitaskTreeClassifier.MultiChanger {

    public enum Adaptation {
        TASK,
        TASK_PLUS_UNKNOWN,
        ALL_TASKS
    }

    private Adaptation adaptation = Adaptation.TASK_PLUS_UNKNOWN;
    private BitSet instance;
    private MultitaskTreeClassifier.MultiCondition condition;
    private MultitaskDecision winnerMultitaskDecision, adapterMultitaskDecision;
    private Task task;

    public SplitMultiTaskNodeChanger(BitSet instance, MultitaskTreeClassifier.MultiCondition condition, MultitaskDecision winnerMultitaskDecision, MultitaskDecision adapterMultitaskDecision, Task task) {
        this.instance = instance;
        this.condition = condition.copy();
        this.winnerMultitaskDecision = winnerMultitaskDecision.copy();
        this.adapterMultitaskDecision = adapterMultitaskDecision.copy();
        this.task = task;
        //System.out.println("A split will take place!");
    }

    public SplitMultiTaskNodeChanger(Adaptation adaptation, BitSet instance, MultitaskTreeClassifier.MultiCondition condition, MultitaskDecision winnerMultitaskDecision, MultitaskDecision adapterMultitaskDecision, Task task) {
        this(instance, condition, winnerMultitaskDecision, adapterMultitaskDecision, task);
        this.adaptation = adaptation;
    }

    @Override
    public MultitaskTreeClassifier.MultiChange change(MultitaskTreeClassifier tree) {

        try {
            MultitaskTreeClassifier.MultiTaskNode parent = tree.getNode(instance);
            MultitaskTreeClassifier.MultiTaskNode n1 = tree.createNode(parent, condition, (adaptation == Adaptation.ALL_TASKS) ? winnerMultitaskDecision.copy() : adapterMultitaskDecision.copy());

            adapterMultitaskDecision.putDecision(task, winnerMultitaskDecision.getDecision(task));

            if (adaptation == Adaptation.TASK_PLUS_UNKNOWN) {

                for (Task t : winnerMultitaskDecision.getTasks()) {
                    if (t == task) {
                        continue;
                    } else {
                        if (!adapterMultitaskDecision.hasTask(t)) {
                            adapterMultitaskDecision.putDecision(t, winnerMultitaskDecision.getDecision(t));
                        }
                    }
                }
            }

            MultitaskTreeClassifier.MultiCondition reverse = condition.copy();
            reverse.not();
            MultitaskTreeClassifier.MultiTaskNode n2 = tree.createNode(parent, reverse, adapterMultitaskDecision);

            for (BitSet associatedInstance : parent.getAssociatedInstances()) {
                if (condition.evaluate(associatedInstance)) {
                    n1.addAssociatedInstance(associatedInstance);
                } else if (reverse.evaluate(associatedInstance)) {
                    n2.addAssociatedInstance(associatedInstance);
                }
            }

            if (condition.evaluate(instance)) {
                n1.addAssociatedInstance(instance);
            } else if (reverse.evaluate(instance)) {
                n2.addAssociatedInstance(instance);
            }

            parent.removeAllAssociatedInstances();

            MultitaskTreeClassifier.MultiChange change = new MultitaskTreeClassifier.MultiChange();
            change.addAdded(n1, n2);

            return change;
        } catch (NoClassException e) {
            e.printStackTrace();
        }
        return null;
    }


    @Override
    public String toString() {
        return "Split Node";
    }
}
