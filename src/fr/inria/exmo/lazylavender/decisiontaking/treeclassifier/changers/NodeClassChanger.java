package fr.inria.exmo.lazylavender.decisiontaking.treeclassifier.changers;

import fr.inria.exmo.lazylavender.decisiontaking.treeclassifier.Decision;
import fr.inria.exmo.lazylavender.decisiontaking.treeclassifier.NoClassException;
import fr.inria.exmo.lazylavender.decisiontaking.treeclassifier.TreeClassifier;

import java.util.BitSet;

public class NodeClassChanger implements TreeClassifier.Changer {

    BitSet instance;
    Decision c;

    public NodeClassChanger(BitSet instance, Decision c) {
        this.instance = instance;
        this.c = c;
    }

    @Override
    public TreeClassifier.Change change(TreeClassifier tree) {

        try {
            TreeClassifier.Node node = tree.getNode(instance);
            node.setC(c);
            TreeClassifier.Change change = new TreeClassifier.Change();
            change.addChanged(node);
            return change;
        } catch (NoClassException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public String toString() {
        return "Change class";
    }
}
