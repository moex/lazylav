package fr.inria.exmo.lazylavender.decisiontaking.treeclassifier.changers;

import fr.inria.exmo.lazylavender.decisiontaking.multitask.MultitaskDecision;
import fr.inria.exmo.lazylavender.decisiontaking.multitask.MultitaskTreeClassifier;
import fr.inria.exmo.lazylavender.decisiontaking.multitask.MultitaskingAgent;
import fr.inria.exmo.lazylavender.decisiontaking.multitask.Task;
import fr.inria.exmo.lazylavender.decisiontaking.treeclassifier.NoClassException;

import java.util.BitSet;

public class MultiTaskNodeClassChanger implements MultitaskTreeClassifier.MultiChanger {

    private BitSet instance;
    private MultitaskDecision winnerMultitaskDecision;
    private Task task;

    public MultiTaskNodeClassChanger(BitSet instance, MultitaskDecision winnerMultitaskDecision, Task task) {
        this.instance = instance;
        this.winnerMultitaskDecision = winnerMultitaskDecision;
        this.task = task;
        //System.out.println("A class change will take place!");
    }

    @Override
    public MultitaskTreeClassifier.MultiChange change(MultitaskTreeClassifier tree) {

        try {
            MultitaskTreeClassifier.MultiTaskNode node = tree.getNode(instance);

            MultitaskDecision adaptingMultitaskDecision = node.getMultiDecision();

            adaptingMultitaskDecision.putDecision(task, winnerMultitaskDecision.getDecision(task));

            /*
            for (Task t : winnerMultitaskDecision.getTasks()) {
                if (t == task) {
                    continue;
                } else {
                    if (!adaptingMultitaskDecision.hasTask(t)) {
                        adaptingMultitaskDecision.putDecision(t, winnerMultitaskDecision.getDecision(t));
                    }
                }
            }

             */


            MultitaskTreeClassifier.MultiChange change = new MultitaskTreeClassifier.MultiChange();
            change.addChanged(node);
            return change;
        } catch (NoClassException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public String toString() {
        return "Change class";
    }
}
