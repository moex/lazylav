package fr.inria.exmo.lazylavender.decisiontaking.treeclassifier.changers;

import fr.inria.exmo.lazylavender.decisiontaking.treeclassifier.Decision;
import fr.inria.exmo.lazylavender.decisiontaking.treeclassifier.NoClassException;
import fr.inria.exmo.lazylavender.decisiontaking.treeclassifier.TreeClassifier;

import java.util.BitSet;

/**
 * Splits node of TreeClassifier according to a condition
 */
public class SplitNodeChanger implements TreeClassifier.Changer
{
    BitSet instance;
    TreeClassifier.Condition condition;
    Decision c1, c2;

    public SplitNodeChanger(BitSet instance, TreeClassifier.Condition condition, Decision c1, Decision c2) {
        this.instance = instance;
        this.condition = condition;
        this.c1 = c1;
        this.c2 = c2;
    }

    @Override
    public TreeClassifier.Change change(TreeClassifier tree) {
        try {
            TreeClassifier.Node parent = tree.getNode(instance);
            TreeClassifier.Node n1 = tree.createNode(parent, condition, c1);
            TreeClassifier.Condition reverse = condition.copy();
            reverse.not();
            TreeClassifier.Node n2 = tree.createNode(parent, reverse, c2);


            TreeClassifier.Change change = new TreeClassifier.Change();
            change.addAdded(n1, n2);
            return change;
        } catch (NoClassException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public String toString() {
        return "Split Node";
    }
}
