package fr.inria.exmo.lazylavender.decisiontaking.treeclassifier.changers;

import fr.inria.exmo.lazylavender.decisiontaking.multitask.MultitaskDecision;
import fr.inria.exmo.lazylavender.decisiontaking.multitask.MultitaskTreeClassifier;
import fr.inria.exmo.lazylavender.decisiontaking.multitask.Task;
import fr.inria.exmo.lazylavender.decisiontaking.treeclassifier.NoClassException;
import org.codehaus.jackson.map.util.ISO8601Utils;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.HashSet;
import java.util.Stack;

/**
 * Splits node of TreeClassifier according to a condition
 */
public class MergeMultiTaskNodeChanger implements MultitaskTreeClassifier.MultiChanger {

    private ArrayList<MultitaskTreeClassifier.MultiTaskNode> visitedNodes;
    private MultitaskTreeClassifier.MultiTaskNode mergeRoot;
    private BitSet instance;
    private MultitaskDecision winnerMultitaskDecision;
    private Task task;

    public MergeMultiTaskNodeChanger(MultitaskTreeClassifier.MultiTaskNode mergeRoot, BitSet instance, MultitaskDecision winnerMultitaskDecision, Task task) {
        this.mergeRoot = mergeRoot;
        this.instance = instance;
        this.winnerMultitaskDecision = winnerMultitaskDecision;
        this.task = task;
        visitedNodes = new ArrayList<>();
        //System.out.println("A merge will take place!");
    }

    private void traversePostOrder(MultitaskTreeClassifier.MultiTaskNode node){
        if (node != null && !node.isLeaf()) {
            traversePostOrder(node.getChildren().get(0));
            traversePostOrder(node.getChildren().get(1));
            visitedNodes.add(node);
        }
    }

    @Override
    public MultitaskTreeClassifier.MultiChange change(MultitaskTreeClassifier tree) {
        MultitaskTreeClassifier.MultiChange change = new MultitaskTreeClassifier.MultiChange();

        //System.out.println("Entering MultiChange");

        traversePostOrder(mergeRoot);

        while (!visitedNodes.isEmpty()) {
            //System.out.println("Inside MultiChange, "+ visitedNodes.size() + " nodes remaining..");
                MultitaskTreeClassifier.MultiTaskNode currentNode = visitedNodes.get(0);
                if(currentNode.isLeaf()){
                    visitedNodes.remove(currentNode);
                    break;
                }else{
                    MultitaskTreeClassifier.MultiTaskNode firstChild = currentNode.getChildren().get(0);
                    MultitaskTreeClassifier.MultiTaskNode secondChild = currentNode.getChildren().get(1);

                    MultitaskDecision firstChildDecisions = firstChild.getMultiDecision();
                    MultitaskDecision secondChildDecisions = secondChild.getMultiDecision();

                    MultitaskDecision parentMultiDecision = new MultitaskDecision();

                    HashSet<Task> allTasks = new HashSet<>();
                    allTasks.addAll(firstChildDecisions.getTasks());
                    allTasks.addAll(secondChildDecisions.getTasks());

                    for (Task task : allTasks){
                        if(firstChildDecisions.hasTask(task) && secondChildDecisions.hasTask(task)){
                            parentMultiDecision.putDecision(task, (Math.random() < .5) ? firstChildDecisions.getDecision(task) : secondChildDecisions.getDecision(task));
                        }else if(firstChildDecisions.hasTask(task)){
                            parentMultiDecision.putDecision(task, firstChildDecisions.getDecision(task));
                        }else{
                            parentMultiDecision.putDecision(task, secondChildDecisions.getDecision(task));
                        }
                    }

                    HashSet<BitSet> firstChildAssociatedInstances = firstChild.getAssociatedInstances();
                    HashSet<BitSet> secondChildAssociatedInstances = secondChild.getAssociatedInstances();

                    HashSet<BitSet> parentAssociatedInstances = new HashSet<>();
                    parentAssociatedInstances.addAll(firstChildAssociatedInstances);
                    parentAssociatedInstances.addAll(secondChildAssociatedInstances);

                    currentNode.setMultiDecision(parentMultiDecision);
                    for(BitSet instance : parentAssociatedInstances){
                        currentNode.addAssociatedInstance(instance);
                    }

                    change.addDeleted(firstChild);
                    change.addDeleted(secondChild);

                    currentNode.getChildren().remove(firstChild);
                    currentNode.getChildren().remove(secondChild);

                    visitedNodes.remove(currentNode);
                    break;
                }
        }

        MultitaskTreeClassifier.MultiTaskNode node = null;
        try {
            node = tree.getNode(instance);
            MultitaskDecision adaptingMultitaskDecision = node.getMultiDecision();
//
//            if(adaptingMultitaskDecision.getDecision(task) != winnerMultitaskDecision.getDecision(task)){
//                adaptingMultitaskDecision.putDecision(task, winnerMultitaskDecision.getDecision(task));
//                change.addChanged(node);
//            }
        } catch (NoClassException e) {
            throw new RuntimeException(e);
        }

        return change;
    }

    @Override
    public String toString() {
        return "Merge nodes";
    }
}
