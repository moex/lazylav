package fr.inria.exmo.lazylavender.decisiontaking.treeclassifier;

public class IntDecision implements Decision{
    int c;

    public IntDecision(int c) {
        this.c = c;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + c;
        return result;
    }


    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        IntDecision other = (IntDecision) obj;
        if (c != other.c)
            return false;
        return true;
    }

    @Override
    public String toString() {
        return ""+c;
    }
}
