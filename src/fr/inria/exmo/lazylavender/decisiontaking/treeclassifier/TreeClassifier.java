package fr.inria.exmo.lazylavender.decisiontaking.treeclassifier;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.BitSet;
import java.util.List;

public interface TreeClassifier
{
    /**
     * Apply a change on the tree classifier
     */
    interface Changer {
        Change change(TreeClassifier tree);
    }

    /**
     * Creates a node with a condition on one attribute, a decision class and a parent node
     */

    interface NodeCreator {
        TreeClassifier.Node node(Decision c, int attribute, boolean value, TreeClassifier.Node parent);
    }


    class Change {
        List<Node> added = new ArrayList<>(), deleted = new ArrayList<>(), changed = new ArrayList<>();

        public void addAdded(Node... nodes) {
            added.addAll(Arrays.asList(nodes));
        }

        public void addDeleted(Node... nodes) {
            deleted.addAll(Arrays.asList(nodes));
        }

        public void addChanged(Node... nodes) {
            changed.addAll(Arrays.asList(nodes));
        }

        public List<Node> getAdded() {
            return added;
        }

        public void setAdded(List<Node> added) {
            this.added = added;
        }

        public List<Node> getDeleted() {
            return deleted;
        }

        public void setDeleted(List<Node> deleted) {
            this.deleted = deleted;
        }

        public List<Node> getChanged() {
            return changed;
        }

        public void setChanged(List<Node> changed) {
            this.changed = changed;
        }
    }

    interface CommonKnowledge {

    }

    interface Condition {
        boolean evaluate(BitSet instance);
        void and(Condition condition);
        void not();
        Condition copy();
        boolean subsumes(Condition condition);
        boolean disjoint(Condition condition);
    }

    interface Node {
        void addChild(Node child);
        boolean isLeaf();
        List<Node> getChildren();
        Node getInstanceNode(BitSet instance);

        /**
         * gets node's direct condition
         * @return
         */
        Condition getCondition();

        /**
         * gets recursive condition from root to node
         * @return
         */
        Condition getRCondition();
        Node getParent();
        Decision getC();
        void setC(Decision c);
        Node copy();
    }

    /**
     * train a tree classifier from input/target dataset
     * @param input a set of instances
     * @param target targets of the instances
     */
    void train(List<BitSet> input, List<Decision> target) throws Exception;

    /**
     * classifies an instance
     * @param input an instance
     * @return integer ID value of the class
     */
    Decision getClass(BitSet input) throws NoClassException;

    /**
     * gets the decision node of the input instance
     * @param instance
     * @return
     */
    Node getNode(BitSet instance) throws NoClassException;

    /**
     * gets the path condition of the input instance
     * @param instance
     * @return
     */
    Condition getCondition(BitSet instance) throws NoClassException;

    /**
     * applies a modification on the tree classifier
     * @param changer
     */

    Change applyModification(Changer changer);

    Node createNode(Node parent, Condition condition, Decision c);

    /**
     * returns the root of the tree
     * @return Node
     */
    Node getRoot();

    /**
     * returns true if the tree classifier has been trained
     * @return boolean
     */
    boolean isTrained();

    NodeCreator getNodeCreator();

    TreeClassifier copy();
}
