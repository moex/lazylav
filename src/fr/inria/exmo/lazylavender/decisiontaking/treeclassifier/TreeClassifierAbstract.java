package fr.inria.exmo.lazylavender.decisiontaking.treeclassifier;

import fr.inria.exmo.lazylavender.decisiontaking.treeclassifier.dttrainers.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.BitSet;
import java.util.List;
import java.util.Properties;

public abstract class TreeClassifierAbstract implements TreeClassifier {
    final static Logger logger = LoggerFactory.getLogger(TreeClassifierAbstract.class);
    private boolean trained = false;
    protected CommonKnowledge knowledge;

    Node root = null;
    String trainerName = "ID3";
    Trainer trainer;
    Properties properties;

    public TreeClassifierAbstract(CommonKnowledge knowledge, Properties p) {
        this.knowledge = knowledge;
        properties = p;
        if (p.containsKey("trainer")) {
            trainerName = p.getProperty("trainer");
        }

        trainer = trainerFromName();
    }

    protected Trainer trainerFromName() {
        return (trainerName.equals("CART"))? new CARTTrainer(this, getNodeCreator()) :
                (trainerName.equals("C45"))? new C45Trainer(this, getNodeCreator()) :
                        (trainerName.equals("RAND"))? new RandomTrainer(this, getNodeCreator(), properties) :
                        (trainerName.equals("EMPTY"))? new EmptyTrainer(this, getNodeCreator(), properties) :
                                new ID3Trainer(this, getNodeCreator());
    }

    public void train(List<BitSet> input, List<Decision> target) throws Exception {
        if(input.size() != target.size())
            throw new Exception("input and target sizes are different");
        trained = true;

    }

    public Change applyModification(Changer changer) {
        return changer.change(this);
    }

    public boolean isTrained()
    {
        return trained;
    }

    public String getTrainerName() {
        return trainerName;
    }

    public void setTrainerName(String trainerName) {
        this.trainerName = trainerName;
        this.trainer = trainerFromName();
    }

    public Trainer getTrainer() {
        return trainer;
    }

    public void setTrainer(Trainer trainer) {
        this.trainer = trainer;
    }

}
