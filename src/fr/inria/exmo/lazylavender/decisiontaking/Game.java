package fr.inria.exmo.lazylavender.decisiontaking;

import fr.inria.exmo.lazylavender.agent.LLAgent;
import fr.inria.exmo.lazylavender.decisiontaking.treeclassifier.Decision;
import fr.inria.exmo.lazylavender.decisiontaking.treeclassifier.NoClassException;
import fr.inria.exmo.lazylavender.expe.InstanceIdGame;
import fr.inria.exmo.lazylavender.decisiontaking.treeclassifier.TreeClassifier;

import java.util.BitSet;
import java.util.Random;

public class Game extends InstanceIdGame implements CEGame {
    boolean success;
    Agent winner=null, loser=null;
    TreeClassifier.Changer adaptation;
    Experiment exp;
    TreeClassifier.Change change;
    static int counterYes = 0;
    static int counterNo = 0;
    static double sumCommonYes = 0;
    static double sumCommonNo = 0;

    static int counterYes2 = 0;
    static int counterNo2 = 0;
    static double sumCommonYes2 = 0;
    static double sumCommonNo2 = 0;

    static int countNoConflict = 0;
    public Game(LLAgent firstAgent, LLAgent secondAgent, BitSet instance, Experiment exp) {
        super(firstAgent, secondAgent, instance);
        this.exp = exp;
    }

    Common.Pair<Agent, Agent> getWinnerLoser() {
        return getWinnerLoserMax();
    }

    Common.Pair<Agent, Agent> getWinnerLoserProba() {
        Agent a1 = (Agent) getFirstAgent(), a2 = (Agent) getSecondAgent();
        Random r = new Random();
        double norm1 = a1.getRelativePayoff(getInstance())/(a1.getRelativePayoff(getInstance())+a2.getRelativePayoff(getInstance()));
        if(r.nextDouble() <= norm1)
            return new Common.Pair<>(a1,a2);
        else
            return new Common.Pair<>(a2, a1);
    }

    Common.Pair<Agent, Agent> getWinnerLoserMax() {
        Agent a1 = (Agent) getFirstAgent(), a2 = (Agent) getSecondAgent();
//        TransmissionBiases.AggregatedBias agb = (TransmissionBiases.AggregatedBias) exp.tBias;
//        Decision d1 = null, c = exp.getEnv().getClass(getInstance());
//        try {
//            d1 = a1.getClassifier().getClass(getInstance());
//        } catch (NoClassException e) {
//            e.printStackTrace();
//        }
//        double v1 = exp.tBias.getValue(a1, this), v2 = exp.tBias.getValue(a2, this);
//        double inv1 = agb.getTransmissionBiases().get(0).getValue(a1, this),
//                inv2 = agb.getTransmissionBiases().get(0).getValue(a2, this);
//        String r1 = "[", r2 = "[";
//        for (TransmissionBias tb : agb.getTransmissionBiases()) {
//            r1 += " " + tb.getValue(a1, this);
//            r2 += " " + tb.getValue(a2, this);
//        }
//        r1 += "]";
//        r2 += "]";
//        if((v1-v2)*(inv1-inv2)<0) {
//
//            System.out.println(exp.tBias.getValue(a1, this) + " vs " + exp.tBias.getValue(a2, this));
//            System.out.println(a1.getRelativePayoff(null) + " vs " + a2.getRelativePayoff(null));
//
//            System.out.println("conflict: " + r1 + " vs " + r2);
//            try {
//                Agent winner = (v1 > v2)? a1: a2;
//                Agent loser = (v2 >= v1)? a1: a2;
//                Agent winner = (c.equals(d1))? a1: a2;
//                if(winner.getClassifier().getClass(getInstance()).equals(exp.getEnv().getClass(getInstance()))) {
//                    counterYes++;
//                    sumCommonYes += exp.getNumberOfAgentsWithDecisionForInstance(getInstance(), winner.getClassifier().getClass(getInstance()));
//                }
//                else {
//                    counterNo++;
//                    sumCommonNo += exp.getNumberOfAgentsWithDecisionForInstance(getInstance(), winner.getClassifier().getClass(getInstance()));
//                }
//                System.out.println(a1.getClassifier().getClass(getInstance()) + " vs " + a2.getClassifier().getClass(getInstance()));
//            } catch (NoClassException e) {
//                e.printStackTrace();
//            }
//            System.out.println(exp.getEnv().getClass(getInstance()));
//        }
//        else {
//            countNoConflict++;
//            Agent winner = (v1 > v2)? a1: a2;
//            Agent loser = (v2 >= v1)? a1: a2;
////            System.out.println("no conflict: " + r1 + " vs " + r2);
////            Agent winner = (c.equals(d1))? a1: a2;
//            try {
//                if(winner.getClassifier().getClass(getInstance()).equals(exp.getEnv().getClass(getInstance()))) {
//                    counterYes2++;
//                    sumCommonYes2 += exp.getNumberOfAgentsWithDecisionForInstance(getInstance(), winner.getClassifier().getClass(getInstance()));
//                }
//                else {
//                    counterNo2++;
//                    sumCommonNo2 += exp.getNumberOfAgentsWithDecisionForInstance(getInstance(), winner.getClassifier().getClass(getInstance()));
//                }
//            } catch (NoClassException e) {
//                e.printStackTrace();
//            }
//        }

//        if(exp.getEnv().getClass(getInstance()).equals(d1))
        if(exp.tBias.getValue(a1, this) > exp.tBias.getValue(a2,this))

            return new Common.Pair<>(a1, a2);
        else
            return new Common.Pair<>(a2, a1);
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public Agent getWinner() {
        return winner;
    }

    public void setWinner(Agent winner) {
        this.winner = winner;
    }

    public Agent getLoser() {
        return loser;
    }

    public void setLoser(Agent loser) {
        this.loser = loser;
    }

    public TreeClassifier.Changer getAdaptation() {
        return adaptation;
    }

    public void setAdaptation(TreeClassifier.Changer adaptation) {
        this.adaptation = adaptation;
    }

    public void setChange(TreeClassifier.Change change) {
        this.change = change;
    }

    public TreeClassifier.Change getChange() {
        return change;
    }


}
