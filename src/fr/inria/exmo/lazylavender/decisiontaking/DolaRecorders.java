/*
 *
 * Copyright (C) INRIA, 2021-2023
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 */

package fr.inria.exmo.lazylavender.decisiontaking;

import fr.inria.exmo.lazylavender.decisiontaking.intrinsicmotivation.GroupGame;
import fr.inria.exmo.lazylavender.decisiontaking.intrinsicmotivation.MotivatedAgent;
import fr.inria.exmo.lazylavender.decisiontaking.intrinsicmotivation.Motivation;
import fr.inria.exmo.lazylavender.decisiontaking.measurements.Recorder;
import fr.inria.exmo.lazylavender.decisiontaking.treeclassifier.Decision;
import fr.inria.exmo.lazylavender.decisiontaking.treeclassifier.NoClassException;
import fr.inria.exmo.lazylavender.decisiontaking.treeclassifier.TreeClassifier;
import fr.inria.exmo.lazylavender.expe.LLGame;

import java.util.*;


/**
 * Class containing DOLA experiment measure recorders.
 */
public class DolaRecorders {

    static abstract public class DolaRec implements Recorder {
        protected List<String> names;

        public DolaRec(String... names) {
            initNames(Arrays.asList(names));
        }

        public DolaRec(List<String> names) {
            initNames(names);
        }

        void initNames(List<String> names) {

            this.names = new ArrayList<>();
            this.names.addAll(names);
        }

        @Override
        public List<String> getNames() {
            return names;
        }
    }


    /**
     *  SUCCESS RATE RECORDER
     */
    static public class Srate extends DolaRec {

        protected List<Double> successAt = new LinkedList<>();

        public Srate(String name) {
            super(name);
        }

        @Override
        public void update(LLGame game) {
            CEGame g = (CEGame) game;
            double prevRate = 0;
            if (successAt.size() != 0)
                prevRate = successAt.get(successAt.size() - 1);
            successAt.add(prevRate + (g.isSuccess() ? 1 : 0));
        }

        @Override
        public List<Double> record(LLGame game) {
            double sRate = successAt.get(successAt.size() - 1) / (double) successAt.size();
            return Arrays.asList(sRate);
        }

    }

    /**
     * DISTANCE RECORDER
     */

    static public class Distance extends DolaRec {


        protected Map<Agent, Map<Agent, Double>> numEq = new HashMap<>();
        protected List<Agent> agents = new ArrayList<>();
        static int iter = 0;
        public Distance(List<Agent> agents, String name) {
            super(name);
            this.agents.addAll(agents);
        }

        public void reInit() {
            numEq = new HashMap<>();
        }

        @Override
        public void update(LLGame game) {
            if (game instanceof GroupGame) {
                updateAgentsEqs(agents, (GroupGame) game);
                return;
            }
            updateAgentsEqs(agents, (Game) game);
        }

        @Override
        public List<Double> record(LLGame game) {
            double avg = 0;
            for (Agent a : agents) {
                double sim = getSim(a, agents);
                avg += sim;
            }

            avg = avg / agents.size();
//            iter++;
//            if(iter%1000 == 0)
//                System.out.println(1-avg);
            return Arrays.asList(1-avg);
        }


        static public boolean nodeExistsInTree(TreeClassifier.Node n, TreeClassifier.Node root) {
            TreeClassifier.Condition c1 = n.getRCondition(), c2 = root.getRCondition();
            if(!c2.subsumes(c1))
                return false;
            else if(c1.subsumes(c2))
                return true;
            for(TreeClassifier.Node child: root.getChildren())
                if(nodeExistsInTree(n, child))
                    return true;
            return false;
        }

        static public double getTreeCommonNodesSize(TreeClassifier.Node root1, TreeClassifier.Node root2) {
            double existing = 0;
//        TreeClassifier.Condition c1 = root1.getRCondition(), c2 = root2.getRCondition();
//        if(c1.disjoint(c2)) // if they are disjoint, all children of root1 do not belong to root2
//            return 0;
            if(nodeExistsInTree(root1, root2))
                existing += 1;
            for(TreeClassifier.Node child: root1.getChildren())
                existing += getTreeCommonNodesSize(child, root2);
            return existing;
        }


        static public double getTreeClassesSize(TreeClassifier.Node root) {
            int size = 1;
            for(TreeClassifier.Node child: root.getChildren())
                size += getTreeClassesSize(child);
            return size;
        }


        public void updateAgentsEqs(List<Agent> agents, Game game) {
            if(game == null || game.isSuccess())
                return;
            Agent loser = game.getLoser();
            TreeClassifier.Change change = game.getChange();
            _updateAgentsEqs(change, loser);
        }

        public void updateAgentsEqs(List<Agent> agents, GroupGame game) {
            if(game == null || game.isSuccess())
                return;
            HashMap<Agent, TreeClassifier.Change> changes = game.getChanges();

            for (Agent loser : changes.keySet()) {
                TreeClassifier.Change change = changes.get(loser);
                _updateAgentsEqs(change, loser);
            }
        }

        private void _updateAgentsEqs(TreeClassifier.Change change, Agent loser) {
            if(change == null || (change.getAdded().isEmpty() && change.getDeleted().isEmpty()))
                return;
            for(Agent a: agents) {
                if(a.getId() == loser.getId())
                    continue;
                if(!numEqContainsKeys(loser, a)) {
                    getEq(loser, a); // will compute number of eq classes
                    continue;
                }
                double prevEq = getEq(loser, a);
                if(!change.getDeleted().isEmpty()) {
                    double newEq = getTreeCommonNodesSize(loser.getClassifier().getRoot(), a.getClassifier().getRoot());
                    setEq(loser, a, newEq);
                }
                else { // only added changed
                    for(TreeClassifier.Node n : change.getAdded())
                        prevEq += (nodeExistsInTree(n, a.getClassifier().getRoot()))?1:0;
                    setEq(loser, a, prevEq);
                }
            }
        }

        public boolean numEqContainsKeys(Agent a1, Agent a2) {
            Agent min = (a1.getId() < a2.getId())?a1:a2;
            Agent max = (a2.getId() < a1.getId())?a1:a2;
            return numEq.containsKey(min) && numEq.get(min).containsKey(max);
        }

        public void setEq(Agent a1, Agent a2, double num) {
            Agent min = (a1.getId() < a2.getId())?a1:a2;
            Agent max = (a2.getId() < a1.getId())?a1:a2;
            if(!numEq.containsKey(min))
                numEq.put(min, new HashMap<>());
            numEq.get(min).put(max, num);
        }

        public double getEq(Agent a1, Agent a2) {
            Agent min = (a1.getId() < a2.getId())?a1:a2;
            Agent max = (a2.getId() < a1.getId())?a1:a2;
            if(!numEq.containsKey(min))
                setEq(a1, a2, getTreeCommonNodesSize(a1.getClassifier().getRoot(), a2.getClassifier().getRoot()));
            if(!numEq.get(min).containsKey(max))
                setEq(a1, a2, getTreeCommonNodesSize(a1.getClassifier().getRoot(), a2.getClassifier().getRoot()));
            return numEq.get(min).get(max);
        }

        public double getSim(Agent a1, Agent a2) {
            TreeClassifier.Node r1 = a1.getClassifier().getRoot(), r2 = a2.getClassifier().getRoot();
            return getEq(a1, a2)/Math.max(getTreeClassesSize(r1),getTreeClassesSize(r2));
        }

        public double getSim(Agent a, List<Agent> agents) {
            double avg = 0;
            int skipped = 0;
            for(Agent agent: agents) {
                if(agent.getId() == a.getId()) {
                    skipped += 1;
                    continue;
                }
                avg += getSim(a, agent);
            }
            return avg / (agents.size() - skipped);
        }

    }



    /**
     * DISTANCE BY LEAVES RECORDER
     */


    static public class DistanceLeaves extends DolaRec {


        protected Map<Agent, Map<Agent, Double>> numEq = new HashMap<>();
        protected List<Agent> agents = new ArrayList<>();

        public DistanceLeaves(List<Agent> agents, String name) {
            super(name);
            this.agents.addAll(agents);
        }

        @Override
        public void update(LLGame game) {
            if (game instanceof GroupGame) {
                updateAgentsEqs((GroupGame) game);
                return;
            }
            updateAgentsEqs((Game) game);
        }

        @Override
        public List<Double> record(LLGame game) {
            double avg = 0;
            for (Agent a : agents) {
                double sim = getSim(a, agents);
                avg += sim;
            }
            avg = avg / agents.size();
            return Arrays.asList(1-avg);
        }

        public boolean nodeExistsInTreeLeaves(TreeClassifier.Node n, TreeClassifier.Node root) {
            TreeClassifier.Condition c1 = n.getRCondition(), c2 = root.getRCondition();
//        System.out.println("comparing: " + c1 + " and " + c2);
            if(!c2.subsumes(c1))
                return false;
            if(root.getChildren().isEmpty() && c1.subsumes(c2))
                return true;
//        System.out.println("outcome is false");
            for(TreeClassifier.Node child: root.getChildren())
                if(nodeExistsInTreeLeaves(n, child))
                    return true;
            return false;
        }

        public double getTreeCommonLeavesSize(TreeClassifier.Node root1, TreeClassifier.Node root2) {
            double existing = 0;
            if(root1.getChildren().isEmpty() && nodeExistsInTreeLeaves(root1, root2))
                existing += 1;
            for(TreeClassifier.Node child: root1.getChildren())
                existing += getTreeCommonLeavesSize(child, root2);
            return existing;
        }

        public double getTreeLeavesSize(TreeClassifier.Node root) {
            int size = root.getChildren().isEmpty()?1:0;
            for(TreeClassifier.Node child: root.getChildren())
                size += getTreeLeavesSize(child);
            return size;
        }


        public void updateAgentsEqs(Game game) {
            if(game == null || game.isSuccess())
                return;
            Agent loser = game.getLoser();
            TreeClassifier.Change change = game.getChange();
            _upadteAgentsEqs(change, loser);
        }

        public void updateAgentsEqs(GroupGame game) {
            if(game == null || game.isSuccess())
                return;
            HashMap<Agent, TreeClassifier.Change> changes = game.getChanges();

            for (Agent loser : changes.keySet())
                _upadteAgentsEqs(changes.get(loser), loser);
        }

        private void _upadteAgentsEqs(TreeClassifier.Change change, Agent loser) {
            if(change == null || (change.getAdded().isEmpty() && change.getDeleted().isEmpty()))
                return;
            for(Agent a: agents) {
                if(a.getId() == loser.getId())
                    continue;
                if(!numEqContainsKeys(loser, a)) {
                    getEq(loser, a); // will compute number of eq classes
                    continue;
                }
                double prevEq = getEq(loser, a);
                if(!change.getDeleted().isEmpty()) {
                    double newEq = getTreeCommonLeavesSize(loser.getClassifier().getRoot(), a.getClassifier().getRoot());
                    setEq(loser, a, newEq);
                }
                else { // only added changed
//                    for(TreeClassifier.Node n : change.getAdded())
//                        prevEq += (nodeExistsInTreeLeaves(n, a.getClassifier().getRoot()))?1:0;
//                    setEq(loser, a, prevEq);
                    double newEq = getTreeCommonLeavesSize(loser.getClassifier().getRoot(), a.getClassifier().getRoot());
                    setEq(loser, a, newEq);
                }
            }
        }

        public boolean numEqContainsKeys(Agent a1, Agent a2) {
            Agent min = (a1.getId() < a2.getId())?a1:a2;
            Agent max = (a2.getId() < a1.getId())?a1:a2;
            return numEq.containsKey(min) && numEq.get(min).containsKey(max);
        }

        public void setEq(Agent a1, Agent a2, double num) {
            Agent min = (a1.getId() < a2.getId())?a1:a2;
            Agent max = (a2.getId() < a1.getId())?a1:a2;
            if(!numEq.containsKey(min))
                numEq.put(min, new HashMap<>());
            numEq.get(min).put(max, num);
        }

        public double getEq(Agent a1, Agent a2) {
            Agent min = (a1.getId() < a2.getId())?a1:a2;
            Agent max = (a2.getId() < a1.getId())?a1:a2;
            if(!numEq.containsKey(min))
                setEq(a1, a2, getTreeCommonLeavesSize(a1.getClassifier().getRoot(), a2.getClassifier().getRoot()));
            if(!numEq.get(min).containsKey(max))
                setEq(a1, a2, getTreeCommonLeavesSize(a1.getClassifier().getRoot(), a2.getClassifier().getRoot()));
            return numEq.get(min).get(max);
        }

        public double getSim(Agent a1, Agent a2) {
            TreeClassifier.Node r1 = a1.getClassifier().getRoot(), r2 = a2.getClassifier().getRoot();
            return getEq(a1, a2)/Math.max(getTreeLeavesSize(r1),getTreeLeavesSize(r2));
        }

        public double getSim(Agent a, List<Agent> agents) {
            double avg = 0;
            int skipped = 0;
            for(Agent agent: agents) {
                if(agent.getId() == a.getId()) {
                    skipped += 1;
                    continue;
                }
                avg += getSim(a, agent);
//
//                if(getSim(a,agent) > 1) {
//                    double al1 = getTreeCommonLeavesSize(a.getClassifier().getRoot(), agent.getClassifier().getRoot());
//                    double al2 = getTreeCommonLeavesSize(agent.getClassifier().getRoot(), a.getClassifier().getRoot());
//                    System.out.println("sim is: " + getSim(a, agent));
//                    System.out.println("a1");
//                    System.out.println(getTreeLeavesSize(a.getClassifier().getRoot()));
//                    System.out.println(getEq(a, agent));
//                    System.out.println(al1);
//                    System.out.println(a.getClassifier());
//                    System.out.println("a2");
//                    System.out.println(getTreeLeavesSize(agent.getClassifier().getRoot()));
//                    System.out.println(getEq(agent, a));
//                    System.out.println(al2);
//                    System.out.println(agent.getClassifier());
//
//                }

            }
            return avg / (agents.size() - skipped);
        }

    }


    /**
     * ACCURACY RECORDER
     */


    static public class Accuracy extends DolaRec {

        protected Map<Agent, Double> accuracies = new HashMap<>();
        protected List<BitSet> inputs = new ArrayList<>();
        protected List<Decision> targets = new ArrayList<>();
        protected double sum;
        protected double avg;
        public Accuracy(List<Agent> agents, List<BitSet> inputs, List<Decision> targets, String name) {
            super(name);
            this.inputs.addAll(inputs);
            this.targets.addAll(targets);
            for(Agent a: agents) {
                double acc = getAgentAccuracy(a, this.inputs, this.targets);
                accuracies.put(a, acc);
                sum += acc;
            }
            avg = sum/agents.size();
        }

        @Override
        public void update(LLGame game) {
            if (game instanceof GroupGame) {
                GroupGame g = (GroupGame) game;
                Agent a1 = g.getFirstAgent();
                List<Agent> an = g.getOtherAgents();
                for (Agent a2 : an)
                    _update(a1, a2);
                return;
            }

            Game g = (Game) game;
            Agent a1 = (Agent) g.getFirstAgent();
            Agent a2 = (Agent) g.getSecondAgent();
            _update(a1, a2);

        }

        private void _update(Agent a1, Agent a2) {
            if(accuracies.containsKey(a1)) {
                sum -= accuracies.get(a1);
                double acc1 = getAgentAccuracy(a1, inputs, targets);
                accuracies.put(a1, acc1);
                sum += acc1;
            }

            if(accuracies.containsKey(a2)) {
                sum -= accuracies.get(a2);
                double acc2 = getAgentAccuracy(a2, inputs, targets);
                accuracies.put(a2, acc2);
                sum += acc2;
            }

            avg = sum/accuracies.size();
        }

        @Override
        public List<Double> record(LLGame game) {
            return Arrays.asList(avg);
        }

        static public double getAgentAccuracy(Agent agent, List<BitSet> inputs, List<Decision> targets)
        {
            double correct=0;
            for (int j = 0; j < inputs.size(); j++) {
                BitSet in = inputs.get(j);
                Decision tar = targets.get(j);
                try {
                    Decision predicted = agent.getClassifier().getClass(in);
                    if(predicted.equals(tar))
                        correct++;
                } catch (NoClassException e) {
                    e.printStackTrace();
                }
            }
            return correct/inputs.size();
        }

    }


    static public class AccuracyOnEnv extends DolaRec {

        protected Map<Agent, Double> accuracies = new HashMap<>();
        Environment env;
        protected double sum;
        protected double avg;
        public AccuracyOnEnv(List<Agent> agents, Environment env, String name) {
            super(name);
            this.env = env;
            for(Agent a: agents) {
                double acc = getAgentAccuracy(a, env.getInputs(), env.getTargets());
                accuracies.put(a, acc);
                sum += acc;
            }
            avg = sum/agents.size();
        }

        @Override
        public void update(LLGame game) {
            Game g = (Game) game;
            Agent a1 = (Agent) g.getFirstAgent();
            Agent a2 = (Agent) g.getSecondAgent();

            if(accuracies.containsKey(a1)) {
                sum -= accuracies.get(a1);
                double acc1 = getAgentAccuracy(a1, env.getInputs(), env.getTargets());
                accuracies.put(a1, acc1);
                sum += acc1;
            }


            if(accuracies.containsKey(a2)) {
                sum -= accuracies.get(a2);
                double acc2 = getAgentAccuracy(a2, env.getInputs(), env.getTargets());
                accuracies.put(a2, acc2);
                sum += acc2;
            }


            avg = sum/accuracies.size();
        }

        @Override
        public List<Double> record(LLGame game) {
            return Arrays.asList(avg);
        }

        static public double getAgentAccuracy(Agent agent, List<BitSet> inputs, List<Decision> targets)
        {
            double correct=0;
            for (int j = 0; j < inputs.size(); j++) {
                BitSet in = inputs.get(j);
                Decision tar = targets.get(j);
                try {
                    Decision predicted = agent.getClassifier().getClass(in);
                    if(predicted.equals(tar))
                        correct++;
                } catch (NoClassException e) {
                    e.printStackTrace();
                }
            }
            return correct/inputs.size();
        }

    }


    static public class PrecisionRecall extends DolaRec {

        protected Map<Agent, Double> precisions = new HashMap<>();
        protected Map<Agent, Double> recalls = new HashMap<>();
        protected List<Agent> agents = new ArrayList<>();
        protected List<BitSet> inputs = new ArrayList<>();
        protected List<Decision> targets = new ArrayList<>();
        protected double psum, rsum;
        protected double pavg, ravg;
        protected int numberOfClasses;

        public PrecisionRecall(List<Agent> agents, List<BitSet> inputs, List<Decision> targets,
                               String pname, String rname, int numberOfClasses) {
            super(pname, rname);
            this.inputs.addAll(inputs);
            this.targets.addAll(targets);
            this.agents.addAll(agents);
            this.numberOfClasses = numberOfClasses;
            initAgents();

            for(Agent a: agents) {
                psum += precisions.get(a);
                rsum += recalls.get(a);
            }
            pavg = psum/precisions.size();
            ravg = rsum/recalls.size();
        }

        @Override
        public void update(LLGame game) {
            if (game instanceof GroupGame)  {
                GroupGame g = (GroupGame) game;
                Agent a1 = g.getFirstAgent();
                List<Agent> an = g.getOtherAgents();

                for (Agent a2 : an)
                    _update(a1, a2);
                return;
            }

            Game g = (Game) game;
            Agent a1 = (Agent) g.getFirstAgent(), a2 = (Agent) g.getSecondAgent();
            _update(a1, a2);
        }

        private void _update(Agent a1, Agent a2) {
            psum -= precisions.get(a1);
            psum -= precisions.get(a2);

            rsum -= recalls.get(a1);
            rsum -= recalls.get(a2);

            updateAgent(a1, inputs, targets);
            updateAgent(a2, inputs, targets);

            psum += precisions.get(a1);
            psum += precisions.get(a2);

            rsum += recalls.get(a1);
            rsum += recalls.get(a2);

            pavg = psum/precisions.size();
            ravg = rsum/recalls.size();
        }

        @Override
        public List<Double> record(LLGame game) {
            return Arrays.asList(pavg, ravg);
        }

        protected void updateAgent(Agent agent, List<BitSet> inputs, List<Decision> targets) {
            Common.Pair<Double, Double> pr = getAgentPR(agent, inputs, targets, numberOfClasses);
            precisions.put(agent, pr.getKey());
            recalls.put(agent, pr.getValue());
        }

        protected void initAgents() {
            for (int i = 0; i < agents.size(); i++) {
                Agent agent = agents.get(i);
                updateAgent(agent, inputs, targets);
            }
        }

        static public Common.Pair<Double, Double> getAgentPR(Agent agent, List<BitSet> inputs,
                                                             List<Decision> targets, int numberOfClasses)
        {
            double p,r;
            Decision i = targets.get(0);
            double tp = 0, fp = 0, fn = 0;
            for (int j = 0; j < inputs.size(); j++) {
                BitSet in = inputs.get(j);
                Decision tar = targets.get(j);
                try {
                    Decision predicted = agent.getClassifier().getClass(in);
                    if (tar.equals(i))
                        if (tar == predicted)
                            tp++;
                        else
                            fn++;
                    else
                    if(tar != predicted)
                        fp++;
                } catch (NoClassException e) {
                    e.printStackTrace();
                }
            }
            p = (tp+fp != 0)?tp/(tp+fp):1;
            r = (tp+fn != 0)?tp/(tp+fn):1;
            return new Common.Pair<>(p,r);
        }
    }


    static public class GainLoss extends DolaRec {

        protected Map<String, Integer> initialCorrect = new HashMap<>();
        protected Map<String, List<Agent>> currentCorrect = new HashMap<>();
        protected List<Agent> agents;
        protected Environment env;

        double gainTrack = 0, lossTrack = 0;

        public GainLoss(List<Agent> agents, Environment env,String cname, String gname, String lname) {
            super(cname, gname, lname);
            this.agents = agents;
            this.env = env;
            try {
                for (Agent a : agents) {
                    for (BitSet b : env.getInputs()) {
                        if (a.getClassifier().getClass(b).equals(env.getClass(b))) {
                            if(!currentCorrect.containsKey(b.toString())) {
                                initialCorrect.put(b.toString(), 1);
                                currentCorrect.put(b.toString(), new ArrayList<>());
                            }
                            currentCorrect.get(b.toString()).add(a);
                        }
                    }
                }
            }
            catch(NoClassException e){
                e.printStackTrace();

            }
        }

        @Override
        public void update(LLGame game) {
            if (game instanceof GroupGame) {
                GroupGame g = (GroupGame) game;
                Agent a1 = g.getFirstAgent();
                List<Agent> an = g.getOtherAgents();

                for (Agent a2 : an)
                    _update(a1, a2, env.getInputs());
                return;
            }

            Game g = (Game) game;
            Agent a1 = (Agent) g.getFirstAgent(), a2 = (Agent) g.getSecondAgent();
            _update(a1, a2, env.getInputs());

        }

        private void _update(Agent a1, Agent a2, List<BitSet> inputs) {
            for(BitSet b: env.getInputs()) {
                for(Agent a: Arrays.asList(a1, a2)) {
                    check(a, b);
                }
            }
        }

        int t = 0;
        @Override
        public List<Double> record(LLGame game) {
//            if((t++)%300 == 0)
//                System.out.println(t + ": " + currentCorrect.size() + " " + gainTrack + " " + lossTrack);
            return Arrays.asList((double)currentCorrect.size(), gainTrack, lossTrack);
        }

        protected void check(Agent a, BitSet b) {
            try {
                if (a.getClassifier().getClass(b).equals(env.getClass(b))) {
                    correct(a, b);
                }
                else {
                    wrong(a, b);
                }
            } catch (NoClassException e) {
                e.printStackTrace();
            }
        }

        protected void correct(Agent a, BitSet b) {
            if (!currentCorrect.containsKey(b.toString())) {
                currentCorrect.put(b.toString(), new ArrayList<>());
                currentCorrect.get(b.toString()).add(a);
                if (initialCorrect.containsKey(b.toString())) {
                    lossTrack--;
                } else
                    gainTrack++;
            } else {
                if (!currentCorrect.get(b.toString()).contains(a))
                    currentCorrect.get(b.toString()).add(a);
            }
        }


        protected void wrong(Agent a, BitSet b) {
            if(currentCorrect.containsKey(b.toString())) {
                currentCorrect.get(b.toString()).remove(a);
                if(currentCorrect.get(b.toString()).isEmpty()) {
                    currentCorrect.remove(b.toString());
                    if(initialCorrect.containsKey(b.toString()))
                        lossTrack++;
                    else
                        gainTrack--;

                }
            }
        }

    }

    /**
     * Winner's Transmission Biases
     */

    static public class AggregatedTransmissionBiasRecorder extends DolaRec {

        TransmissionBiases.AggregatedBias aggBias;

        public AggregatedTransmissionBiasRecorder(TransmissionBiases.AggregatedBias aggBias, String... names) {
            super(names);
            this.aggBias = aggBias;
        }

        @Override
        public void update(LLGame game) {
            aggBias.update((Game) game);
        }

        @Override
        public List<Double> record(LLGame game) {
            Game g = (Game) game;
            List<Double> records = new ArrayList<>();
            for(TransmissionBias bias: aggBias.getTransmissionBiases()) {
                if(!g.isSuccess())
                    records.add(bias.getValue(g.getWinner(),g));
                else
                    records.add(-1.0);
            }
            if(!g.isSuccess())
                records.add(aggBias.getValue(g.getWinner(),g));
            else
                records.add(-1.0);

//            for (int i = 0; i < names.size(); i++) {
//                System.out.println(names.get(i) + ": " + records.get(i));
//            }
            return records;
        }
    }


    /**
     * records info about game's instance:
     * how many agents have correct decision for instance
     * how many agents have winner's decision
     * is winner's decision correct
     */

    static public class GameInstanceInfoRecorder extends DolaRec {

        Experiment exp;
        double correctD, winnerD, isCorrect;

        public GameInstanceInfoRecorder(Experiment exp) {
            super(Arrays.asList("correctD", "winnerD", "isCorrect"));
            this.exp = exp;
        }

        @Override
        public void update(LLGame game) {
            Game g = (Game) game;
            Agent winner = g.getWinner();
            isCorrect = -1;
            correctD = -1;
            winnerD = -1;

            if(winner == null)
                return;
            try {
                Decision wD = winner.getClassifier().getClass(g.getInstance());
                Decision cD = exp.getEnv().getClass(g.getInstance());
                isCorrect = 0;
                if(wD.equals(cD))
                    isCorrect = 1;
                correctD = exp.getNumberOfAgentsWithDecisionForInstance(g.getInstance(), cD);
                winnerD = exp.getNumberOfAgentsWithDecisionForInstance(g.getInstance(), wD);
            } catch (NoClassException e) {
                e.printStackTrace();
            }

        }

        @Override
        public List<Double> record(LLGame game) {
//            System.out.println(names.get(0) + ": " + correctD);
//            System.out.println(names.get(1) + ": " + winnerD);
//            System.out.println(names.get(2) + ": " + isCorrect);
            return Arrays.asList(correctD, winnerD, isCorrect);
        }
    }

    /**
     *  Exploration SCORE RECORDER
     */
    static public class Exploration extends DolaRec {

        protected Map<Agent, Double> explorationScores;

        protected Map<Agent, Map<BitSet, Integer>> agentsSeenObjects;
        protected Queue<Object[]> lastObjects;

        protected Map<Agent, Map<Agent, Integer>> agentsSeenAgents;

        private int amtProperties;
        private boolean objectsExploration;

        /**
         * Creates the exploration score recorder.
         *
         * @param name the recorder's name
         * @param amtProperties the amount of properties used in the experiments
         * @param objects whether the object exploration (true) or the partner exploration should be measured (false)
         */
        public Exploration(String name, int amtProperties, boolean objects) {
            super(name);
            this.explorationScores = new HashMap<>();
            this.amtProperties = amtProperties;
            this.agentsSeenObjects = new HashMap<>();
            this.agentsSeenAgents = new HashMap<>();
            this.lastObjects = new LinkedList<>();
            this.objectsExploration = objects;
        }

        /**
         * Updates the score keeper. It saves the chosen object and interaction partners of the first agent of the
         * game. For the partner exploration, the ontology distance is used, for the object exploration, the
         * difference in terms of how many properties have a different form (negated and not negated).
         *
         * @param game the game that was just played
         */
        @Override
        public void update(LLGame game) {
            CEGame g;
            Agent choosingAgent;
            List<Agent> interactionPartners;

            // get game
            if (game instanceof GroupGame) {
                g = (GroupGame) game;
                choosingAgent = ((GroupGame) game).getFirstAgent();
                interactionPartners = ((GroupGame) game).getOtherAgents();
            } else { // instance of Game
                g = (Game) game;
                choosingAgent = (Agent) ((Game) game).getFirstAgent();
                interactionPartners = new ArrayList<>();
                interactionPartners.add((Agent) ((Game) game).getSecondAgent());
            }

            // the object exploration is saved
            if (objectsExploration) {
                // get the current object
                BitSet currInstance = g.getInstance();
                Map<BitSet, Integer> seenObjectTypes = new HashMap<>();
                // if it is the first object the agent saw, add object with score of 1
                if (agentsSeenObjects.get(choosingAgent) == null){
                    seenObjectTypes.put(currInstance, 1);
                    agentsSeenObjects.put(choosingAgent, seenObjectTypes);
                    Object[] currObject = new Object[2];
                    currObject[0] = choosingAgent;
                    currObject[1] = currInstance;
                    lastObjects.add(currObject);
                    explorationScores.put(choosingAgent, 0.);
                    return;
                }

                // otherwise go through all object types the agent saw
                seenObjectTypes = agentsSeenObjects.get(choosingAgent);
                double differenceCounter = 0., objectTypeDifferenceCounter, objectTypeDifferenceScore, amtObjects = 0.;
                for (BitSet objectType : seenObjectTypes.keySet()) {
                    objectTypeDifferenceCounter = 0.;
                    // count how many properties have a different form
                    for (int i = 0; i < amtProperties; i++) {
                        if (currInstance.get(i) != objectType.get(i))
                            objectTypeDifferenceCounter++;
                    }
                    // get difference for object type as percentage
                    objectTypeDifferenceScore = objectTypeDifferenceCounter / amtProperties;
                    // weighten score by amount of objects of this type seen
                    differenceCounter += objectTypeDifferenceScore * seenObjectTypes.get(objectType);
                    amtObjects += seenObjectTypes.get(objectType);
                }
                // increase counter for seeing this object type
                seenObjectTypes.put(currInstance, seenObjectTypes.get(currInstance) == null ? 1 : seenObjectTypes.get(currInstance) + 1);
                agentsSeenObjects.put(choosingAgent, seenObjectTypes);
                Object[] currObject = new Object[2];
                currObject[0] = choosingAgent;
                currObject[1] = currInstance;
                lastObjects.add(currObject);
                // only compare with a window of the last seen objects, size = 25% of all possible object types
                // therefore delete the oldest object from last objects, if more objects than allowed
                if (lastObjects.size() > Math.pow(2, this.amtProperties) / 4) {
                    Object[] removedInstance = lastObjects.poll();
                    assert removedInstance != null;
                    MotivatedAgent agent = (MotivatedAgent) removedInstance[0];
                    BitSet instance = (BitSet) removedInstance[1];
                    seenObjectTypes = agentsSeenObjects.get(agent);
                    seenObjectTypes.put(instance, seenObjectTypes.get(instance) - 1);
                    agentsSeenObjects.put(agent, seenObjectTypes);
                }
                double explorationScore = differenceCounter / amtObjects;
                explorationScores.put(choosingAgent, Double.isNaN(explorationScore) ? 0 : explorationScore);

            } else { // interaction partner exploration
                Map<Agent, Integer> seenInteractionPartners = new HashMap<>();
                double explorationScore = 0.;
                // go through all interaction partners
                for (Agent partner : interactionPartners) {
                    // add them to the counter
                    if (agentsSeenAgents.get(choosingAgent) == null){
                            seenInteractionPartners.put(partner, 1);
                    } else {
                        seenInteractionPartners = agentsSeenAgents.get(choosingAgent);
                        seenInteractionPartners.put(choosingAgent, seenInteractionPartners.get(partner) == null ? 1 : seenInteractionPartners.get(partner) + 1);
                    }
                    agentsSeenAgents.put(choosingAgent, seenInteractionPartners);

                    // use distance measure from distance recorder above
                    TreeClassifier.Node r1 = choosingAgent.getClassifier().getRoot(), r2 = partner.getClassifier().getRoot();
                    explorationScore += 1 - (getTreeCommonLeavesSize(r1, r2) / Math.max(getTreeLeavesSize(r1), getTreeLeavesSize(r2))); // 1 - the similarity = difference
                }
                explorationScores.put(choosingAgent, explorationScore / interactionPartners.size());
            }

        }

        /**
         * Returns a list with the average exploration score over all agents over a window of the past interaction objects
         * or all interaction partners.
         *
         * @param game the game currently played
         * @return List<Double> with the current exploration value
         */
        @Override
        public List<Double> record(LLGame game) {
            double explorationScore = 0.;
            for (Agent agent : explorationScores.keySet())
                explorationScore += explorationScores.get(agent);
            return Arrays.asList(explorationScore / explorationScores.keySet().size());
        }

        // copied from Yasser: Distance measure //

        public double getTreeLeavesSize(TreeClassifier.Node root) {
            int size = root.getChildren().isEmpty()?1:0;
            for(TreeClassifier.Node child: root.getChildren())
                size += getTreeLeavesSize(child);
            return size;
        }

        public double getTreeCommonLeavesSize(TreeClassifier.Node root1, TreeClassifier.Node root2) {
            double existing = 0;
            if(root1.getChildren().isEmpty() && nodeExistsInTreeLeaves(root1, root2))
                existing += 1;
            for(TreeClassifier.Node child: root1.getChildren())
                existing += getTreeCommonLeavesSize(child, root2);
            return existing;
        }

        public boolean nodeExistsInTreeLeaves(TreeClassifier.Node n, TreeClassifier.Node root) {
            TreeClassifier.Condition c1 = n.getRCondition(), c2 = root.getRCondition();
            if(!c2.subsumes(c1))
                return false;
            if(root.getChildren().isEmpty() && c1.subsumes(c2))
                return true;
            for(TreeClassifier.Node child: root.getChildren())
                if(nodeExistsInTreeLeaves(n, child))
                    return true;
            return false;
        }

        // end of copy //

    }

    /**
     *  Reward RECORDER
     */
    static public class ExplorationMotivationReward extends DolaRec {

        private Map<Agent, List<Double>> rewards;
        private Motivation motivation;

        /**
         * Records the reward of the agent, uses the last reward of the agent, if there is one, otherwise 0.
         *
         * @param name the recorder's name
         */
        public ExplorationMotivationReward(String name) {
            super(name);
            this.rewards = new HashMap<>();
        }
        /**
         * Records the reward of the agent for a given motivation, even if the agent is not motivated.
         *
         * @param name the recorder's name
         * @param motivation the motivation to be recorded
         */
        public ExplorationMotivationReward(String name, Motivation motivation) {
            super(name);
            this.rewards = new HashMap<>();
            this.motivation = motivation;
        }

        /**
         * Updates the score keeper. It gets the current reward the (motivated) agent has or the motivation that the
         * agent would get, if it had the motivation **motivation**.
         *
         * @param game the game that was just played
         */
        @Override
        public void update(LLGame game) {
            Agent agent;

            // get the game
            if (game instanceof GroupGame)
                agent = ((GroupGame) game).getFirstAgent();
            else
                agent = (Agent) ((Game) game).getFirstAgent();

            // create empty lists for each agent if not there already
            rewards.computeIfAbsent(agent, k -> new ArrayList<>());
            // get the past rewards
            List<Double> pastRewards = rewards.get(agent);
            // add the last reward the agent received (or reward the agent would have received with motivation
            // **motivation**
            if (agent instanceof MotivatedAgent) {
                MotivatedAgent motivatedAgent = (MotivatedAgent) agent;
                if (motivation != null) {
                    switch (motivation) {
                        case CURIOUS:
                            pastRewards.add(motivatedAgent.curiosityReward());
                            break;
                        case CREATIVE:
                            pastRewards.add(motivatedAgent.creativityReward());
                            break;
                        case NONEXPLORATORY:
                            pastRewards.add(motivatedAgent.nonExploratoryReward());
                            break;
                        default:
                            pastRewards.add(0.);
                    }
                } else
                    pastRewards.add(motivatedAgent.getLastReward());
            } else // if agent is no motivated agent, no reward can be retrieved -> 0
                pastRewards.add(0.);
            rewards.put(agent, pastRewards);
        }

        /**
         * Returns a list with the average reward over all agents over all past rewards.
         *
         * @param game the game that was just played
         */
        @Override
        public List<Double> record(LLGame game) {
            List<Double> avgRewards = new ArrayList<>();
            for (Agent agent : rewards.keySet()) {
                avgRewards.add(rewards.get(agent).stream().reduce(0., Double::sum) / rewards.get(agent).size());
            }
            return Arrays.asList(avgRewards.stream().reduce(0., Double::sum) / avgRewards.size());
        }
    }

    /**
     *  Completeness RECORDER
     */
    static public class Completeness extends DolaRec {

        protected Map<Agent, Double> completeness;
        protected List<BitSet> allObjects;
        protected List<Decision> allDecisions;

        /**
         * Records the completness of the agents' ontologies.
         *
         * @param name the recorder's name
         */
        public Completeness(String name, List<Agent> agents, List<BitSet> objects, List<Decision> decisions) {
            super(name);
            this.completeness = new HashMap<>();
            for (Agent agent : agents)
                this.completeness.put(agent, 0.);
            this.allObjects = objects;
            this.allDecisions = decisions;
            this._update(agents);
        }

        /**
         * Updates the score keeper.
         *
         * @param game the game that was just played
         */
        @Override
        public void update(LLGame game) {
            // get agent or agents depending on game
            List<Agent> agents = new ArrayList<>();
            if (game instanceof GroupGame)
                agents.addAll(((GroupGame) game).getLosers());
            else
                agents.add(((Game) game).getLoser());

            // filter successful games
            if (agents.isEmpty() || agents.get(0) == null)
                return;

            _update(agents);
        }

        private void _update(List<Agent> agents) {
            // go through all objects of the agent(s) that lost and adapted to update its completeness-score
            TreeClassifier.Node currLeaf;
            BitSet object;
            Decision correctDecision;
            Map<TreeClassifier.Node, Map<Decision, Integer>> leaves;
            double completenessScore;
            // go through loser-agents
            for (Agent agent : agents) {
                completenessScore = 0;
                leaves = new HashMap<>();
                // go through all objects
                for (int i = 0; i < allObjects.size(); i++) {
                    // get the object and the correct decision from the environment
                    // ignore agent's decision, since we only want to measure, whether the objects are
                    // in correct classes and do not care, if the class is correct, but the agent has a wrong decision
                    // for the class
                    object = allObjects.get(i);
                    correctDecision = allDecisions.get(i);
                    try {
                        // get correct decisions for the objects classified by the same leaf
                        currLeaf = agent.getClassifier().getNode(object);
                        leaves.putIfAbsent(currLeaf, new HashMap<>());
                        Map<Decision, Integer> updated = leaves.get(currLeaf);
                        updated.putIfAbsent(correctDecision, 0);
                        updated.put(correctDecision, updated.get(correctDecision) + 1);
                        leaves.put(currLeaf, updated);
                    } catch (NoClassException e) {
                        e.printStackTrace();
                    }
                }

                // go through all leaves and see how many objects share the same correct decision -> actually
                // belong to the same class
                for (TreeClassifier.Node leaf : leaves.keySet()) {
                    double sameDecisionsCounter = 0, decisionsCounter = 0;
                    Map<Decision, Integer> counter = leaves.get(leaf);
                    for (Decision decision : counter.keySet()) {
                        double thisDecisionCounter = counter.get(decision);
                        decisionsCounter += thisDecisionCounter;
                        if (thisDecisionCounter > sameDecisionsCounter)
                            sameDecisionsCounter = thisDecisionCounter;
                    }
                    completenessScore += (sameDecisionsCounter / decisionsCounter);
                }

                completeness.put(agent, completenessScore / leaves.keySet().size());
            }
        }

        /**
         * Returns a list with the average completeness over all agents.
         *
         * @param game the game that was just played
         */
        @Override
        public List<Double> record(LLGame game) {
            return List.of(completeness.values().stream().reduce(0., Double::sum)
                    / completeness.keySet().size());
        }
    }

}
