package fr.inria.exmo.lazylavender.decisiontaking.generations;

import fr.inria.exmo.lazylavender.decisiontaking.Agent;
import fr.inria.exmo.lazylavender.decisiontaking.Experiment;
import fr.inria.exmo.lazylavender.decisiontaking.TransmissionBias;
import fr.inria.exmo.lazylavender.decisiontaking.TransmissionBiases;

import java.util.ArrayList;

public class GenTransmissionBiases {

    interface GenBias extends TransmissionBias {
        void update(ArrayList<Agent> addedAgents, ArrayList<Agent> removedAgents);
    }

    public static class GenEnvBias extends TransmissionBiases.EnvBias implements GenBias {

        public GenEnvBias(Experiment exp) {
            super(exp);
        }

        public GenEnvBias(Experiment exp, double discount) {
            super(exp, discount);
        }

        public GenEnvBias(Experiment exp, double discount, double initVal) {
            super(exp, discount, initVal);
        }

        @Override
        public void update(ArrayList<Agent> addedAgents, ArrayList<Agent> removedAgents) {
            for(Agent agent: addedAgents) {
                idToValue.put(agent.getId(), initVal);
                idToNormValue.put(agent.getId(), 1.);
            }
            for(Agent agent: removedAgents) {
                idToValue.remove(agent.getId());
                idToNormValue.remove(agent.getId());
            }
        }
    }
}
