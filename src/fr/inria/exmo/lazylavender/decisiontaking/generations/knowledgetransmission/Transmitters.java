package fr.inria.exmo.lazylavender.decisiontaking.generations.knowledgetransmission;

import fr.inria.exmo.lazylavender.decisiontaking.Agent;
import fr.inria.exmo.lazylavender.decisiontaking.Environment;
import fr.inria.exmo.lazylavender.decisiontaking.treeclassifier.Decision;
import fr.inria.exmo.lazylavender.decisiontaking.treeclassifier.IntDecision;
import fr.inria.exmo.lazylavender.decisiontaking.treeclassifier.NoClassException;
import fr.inria.exmo.lazylavender.decisiontaking.treeclassifier.TreeClassifierAbstract;
import fr.inria.exmo.lazylavender.decisiontaking.treeclassifier.dttrainers.DTTrainer;
import fr.inria.exmo.lazylavender.decisiontaking.treeclassifier.dttrainers.EmptyTrainer;
import fr.inria.exmo.lazylavender.decisiontaking.treeclassifier.dttrainers.ID3Trainer;
import fr.inria.exmo.lazylavender.decisiontaking.treeclassifier.dttrainers.RandomTrainer;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.List;
import java.util.Random;

public class Transmitters {

    static public class MergeTransmitter implements KnowledgeTransmitter {

        @Override
        public void initializeKnowledge(List<Agent> parents, Agent child) {

        }
    }


    static public class RandomTransmitter implements KnowledgeTransmitter {

        int numberOfClasses;
        int numberOfFeatures;
        public RandomTransmitter(int numberOfClasses, int numberOfFeatures) {
            this.numberOfClasses = numberOfClasses;
            this.numberOfFeatures = numberOfFeatures;
        }

        @Override
        public void initializeKnowledge(List<Agent> parents, Agent child) {
            TreeClassifierAbstract treeClassifier = (TreeClassifierAbstract) child.getClassifier();
            DTTrainer trainer = new RandomTrainer(treeClassifier, treeClassifier.getNodeCreator(),
                    1./(numberOfFeatures + 1), numberOfClasses);
            treeClassifier.setTrainer(trainer);
            try {
                ArrayList<BitSet> input = new ArrayList<>();
                BitSet in = new BitSet();
                in.set(numberOfFeatures-1);
                input.add(in);

                ArrayList<Decision> target = new ArrayList<>();
                target.add(null);
                treeClassifier.train(input, target); // added one input/target so that the training algorithm knows how many features there are
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    static public class EmptyTransmitter implements KnowledgeTransmitter {

        int numberOfClasses;


        public EmptyTransmitter(int numberOfClasses) {
            this.numberOfClasses = numberOfClasses;

        }

        @Override
        public void initializeKnowledge(List<Agent> parents, Agent child) {
            TreeClassifierAbstract treeClassifier = (TreeClassifierAbstract) child.getClassifier();
            DTTrainer trainer = new EmptyTrainer(treeClassifier, treeClassifier.getNodeCreator(), numberOfClasses);
            treeClassifier.setTrainer(trainer);
            try {
                treeClassifier.train(new ArrayList<>(), new ArrayList<>());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    static public class TeachingTransmitter implements KnowledgeTransmitter {

        double ttratio;
        Environment env;
        double errorRate;
        int numberOfClasses;

        public TeachingTransmitter(double ttratio, Environment env, double errorRate, int numberOfClasses) {
            this.ttratio = ttratio;
            this.env = env;
            this.errorRate = errorRate;
            this.numberOfClasses = numberOfClasses;
        }

        @Override
        public void initializeKnowledge(List<Agent> parents, Agent child) {
            TreeClassifierAbstract treeClassifier = (TreeClassifierAbstract) child.getClassifier();
            DTTrainer trainer = new ID3Trainer(treeClassifier, treeClassifier.getNodeCreator());
            treeClassifier.setTrainer(trainer);

            ArrayList<BitSet> inputs = new ArrayList<>();
            ArrayList<Decision> targets = new ArrayList<>();

            Random r = new Random();

            for(BitSet input: env.getInputs()) {
                if(r.nextDouble() < ttratio) {
                    try {
                        inputs.add(input);
                        if(r.nextDouble() < errorRate)
                            targets.add(new IntDecision(r.nextInt(numberOfClasses)));
                        else
                            targets.add(parents.get(r.nextInt(parents.size())).getClassifier().getClass(input));

                    } catch (NoClassException e) {
                        e.printStackTrace();
                    }
                }
            }

            //TODO make trainer find random Decision alone instead of this:
            if(inputs.isEmpty()) {
                EmptyTransmitter tr = new EmptyTransmitter(numberOfClasses);
                tr.initializeKnowledge(parents, child);

//                BitSet input = env.getInputs().get(r.nextInt(env.getInputs().size()));
//                inputs.add(input);
//                try {
//                    targets.add(parents.get(r.nextInt(parents.size())).getClassifier().getClass(input));
//                } catch (NoClassException e) {
//                    e.printStackTrace();
//                }
            }
            else {
                try {
                    child.getClassifier().train(inputs, targets);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }


}
