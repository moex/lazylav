package fr.inria.exmo.lazylavender.decisiontaking.generations.knowledgetransmission;

import fr.inria.exmo.lazylavender.decisiontaking.Agent;

import java.util.List;

public interface KnowledgeTransmitter {

    void initializeKnowledge(List<Agent> parents, Agent child);

}
