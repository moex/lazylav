package fr.inria.exmo.lazylavender.decisiontaking.generations;

import fr.inria.exmo.lazylavender.decisiontaking.*;
import fr.inria.exmo.lazylavender.decisiontaking.treeclassifier.Decision;
import fr.inria.exmo.lazylavender.expe.LLGame;

import java.util.*;

public class DolaGenRecorders {

    interface DolaGenUpdater {
        void update(ArrayList<Agent> addedAgents, ArrayList<Agent> removedAgents);
    }

    /**
     *  SUCCESS RATE RECORDER
     */
    static public class Srate extends DolaRecorders.Srate implements DolaGenUpdater{

        boolean newGenOnly = false;
        public Srate(String name) {
            super(name);
        }

        public Srate(String name, boolean newGenOnly) {
            super(name);
            this.newGenOnly = newGenOnly;
        }

        @Override
        public void update(ArrayList<Agent> addedAgents, ArrayList<Agent> removedAgents) {
            if(newGenOnly)
            {
                successAt.clear();
            }
        }
    }

    /**
     *  Adaptation between Generations count
     */

    static public class AdaptationCounter extends DolaRecorders.DolaRec implements DolaGenUpdater {

        protected double h=0, h1=0, v=0, vr=0;
        protected double ha=0,h1a=0,va=0;

        protected Map<Agent, Integer> agentGen = new HashMap<>();
        protected int genCount = 0;

        public AdaptationCounter(String ha, String h1a, String va, String h, String h1, String v, String vr, List<Agent> agents) {
            super(ha,h1a,va,h,h1,v,vr); //all horizontal n, all horizontal n+1, all vertical, horizontal n, horizontal n+1, vertical, vertical reversed
            for(Agent a: agents)
                agentGen.put(a, genCount);
        }

        @Override
        public void update(LLGame game) {
            Game g = (Game) game;
            int l = agentGen.get((Agent) g.getFirstAgent()), w = agentGen.get((Agent) g.getSecondAgent());
            if(w == l)
                if(w == genCount)
                    h1a++;
                else
                    ha++;
            else
                va++;
            if(!g.isSuccess() && g.getLoser() != null && g.getWinner() != null) { //adaptation happened
                l = agentGen.get(g.getLoser());
                w = agentGen.get(g.getWinner());
//                System.out.println(g.getWinner().getId() + " of gen " + w + " vs " + g.getLoser().getId() + " of gen " + l);
//                System.out.println(genCount);
                if(w == l)
                    if(genCount == 0) {
                        h1++;
                        h++;
                    }
                    else if(w == genCount)
                        h1++;
                    else
                        h++;
                else if(w < l) // child adapted knowledge
                    v++;
                else
                    vr++;
//                System.out.println(names.get(3) + h + "/" + ha + names.get(4) + h1 + "/" + h1a +
//                        names.get(5) + v + "/" + va + names.get(6) + vr + "/" + va);

            }
//            System.out.println("interactions:" + (ha+h1a+va));
        }

        @Override
        public List<Double> record(LLGame game) {

            return Arrays.asList(ha,h1a,va,h,h1,v,vr);
        }

        @Override
        public void update(ArrayList<Agent> addedAgents, ArrayList<Agent> removedAgents) {
            genCount++;
            for(Agent a: addedAgents)
                agentGen.put(a, genCount);

        }
    }

    /**
     * DISTANCE RECORDER
     */

    static public class Distance extends DolaRecorders.Distance implements DolaGenUpdater{

        boolean newPrevGen = false;
        ArrayList<Agent> prevGenArr = new ArrayList<>();
        ArrayList<Agent> newGenArr = new ArrayList<>();
        public Distance(List<Agent> agents, String name) {
            super(agents,name);
        }

        public Distance(List<Agent> agents, String name, String newGen, String prevGen) {
            super(agents, name);
            names.add(newGen);
            names.add(prevGen);
            this.newPrevGen = true;
            prevGenArr.addAll(agents);
            newGenArr.addAll(agents);
        }

        @Override
        public void update(ArrayList<Agent> addedAgents, ArrayList<Agent> removedAgents) {
            newGenArr.clear();
            prevGenArr.clear();

            newGenArr.addAll(addedAgents);

            this.agents.removeAll(removedAgents);

            prevGenArr.addAll(this.agents);

            this.agents.addAll(addedAgents);
            for (Agent a : removedAgents) {
                numEq.remove(a);
            }
        }


        protected double getAvg(List<Agent> ags) {
            double avg = 0;
            for (Agent a : ags) {
                double sim = getSim(a, ags);
                avg += sim;
            }
            avg = avg / ags.size();
            return avg;
        }


        @Override
        public List<Double> record(LLGame game) {
            if(newPrevGen) {
                double avg1 = getAvg(agents), avg2 = getAvg(newGenArr), avg3 = getAvg(prevGenArr);
                return Arrays.asList(1 - avg1, 1 - avg2, 1 - avg3);
            }
            return Arrays.asList(1-getAvg(agents));
        }
    }


    /**
     * DISTANCE RECORDER BY LEAVES
     */

    static public class DistanceLeaves extends DolaRecorders.DistanceLeaves implements DolaGenUpdater{

        boolean newGenOnly = false;
        public DistanceLeaves(List<Agent> agents, String name) {
            super(agents,name);
        }

        public DistanceLeaves(List<Agent> agents, String name, boolean newGenOnly) {
            super(agents,name);
            this.newGenOnly = newGenOnly;
        }

        @Override
        public void update(ArrayList<Agent> addedAgents, ArrayList<Agent> removedAgents) {
            if(newGenOnly) {
                this.agents.clear();
                this.agents.addAll(addedAgents);
                this.numEq.clear();
            }
            else {
                this.agents.removeAll(removedAgents);
                this.agents.addAll(addedAgents);
                for (Agent a : removedAgents) {
                    numEq.remove(a);
                }
            }
        }
    }


    /**
     * ACCURACY RECORDER
     */


    static public class Accuracy extends DolaRecorders.Accuracy implements DolaGenUpdater {

        boolean newGenOnly = false;
        public Accuracy(List<Agent> agents, List<BitSet> inputs, List<Decision> targets, String name) {
            super(agents, inputs, targets, name);
        }

        public Accuracy(List<Agent> agents, List<BitSet> inputs, List<Decision> targets, String name, boolean newGenOnly) {
            super(agents, inputs, targets, name);
            this.newGenOnly = newGenOnly;
        }


        @Override
        public void update(ArrayList<Agent> addedAgents, ArrayList<Agent> removedAgents) {
            if(newGenOnly) {
                accuracies.clear();
                sum = 0;
                avg = 0;
                for(Agent a: addedAgents) {
                    double acc = getAgentAccuracy(a, this.inputs, this.targets);
                    accuracies.put(a, acc);
                    sum += acc;
                }
                avg = sum/addedAgents.size();
            }
            else {
                for (Agent a : removedAgents) {
                    sum -= accuracies.get(a);
                    accuracies.remove(a);

                }
                for (Agent a : addedAgents) {
                    double acc = getAgentAccuracy(a, this.inputs, this.targets);
                    accuracies.put(a, acc);
                    sum += acc;
                }

                avg = sum / accuracies.size();
            }
        }
    }


    static public class AgentsIncome extends DolaRecorders.DolaRec implements DolaGenUpdater {

        ArrayList<Double> distribution = new ArrayList<>();
        GenerationExperiment exp;
        public AgentsIncome(GenerationExperiment exp) {
            List<String> names = new ArrayList<>();
            int numParents = exp.getNumberOfAgents() - exp.getNumberOfBirthDeath();
            for (int i = 1; i <= numParents; i++) {
                names.add("a"+i);
                distribution.add(1./exp.getAgents().size());
            }
            this.names = names;
            this.exp = exp;
        }



        @Override
        public void update(ArrayList<Agent> addedAgents, ArrayList<Agent> removedAgents) {
            distribution.clear();
            double sum = 0;
            for(Agent a: exp.getAgents()) {
                if(addedAgents.contains(a))
                    continue;
                sum += a.getRelativePayoff(null);
                distribution.add(a.getRelativePayoff(null));
            }
            for (int i = 0; i < distribution.size(); i++) {
                distribution.set(i, distribution.get(i)/sum);
            }

            Collections.sort(distribution);
//            for (int i = 0; i < distribution.size(); i++) {
//                System.out.print(distribution.get(i) + " ");
//            }
//            System.out.println();
        }

        @Override
        public void update(LLGame game) {

        }

        @Override
        public List<Double> record(LLGame game) {
            return distribution;
        }
    }

    static public class PrecisionRecall extends DolaRecorders.PrecisionRecall implements DolaGenUpdater {

        Map<Agent, Double> precisions = new HashMap<>();
        Map<Agent, Double> recalls = new HashMap<>();
        List<Agent> agents = new ArrayList<>();
        List<BitSet> inputs = new ArrayList<>();
        List<Integer> targets = new ArrayList<>();
        double psum, rsum;
        double pavg, ravg;
        int numberOfClasses;

        public PrecisionRecall(List<Agent> agents, List<BitSet> inputs, List<Decision> targets,
                               String pname, String rname, int numberOfClasses) {
            super(agents, inputs, targets, pname, rname, numberOfClasses);
        }

        @Override
        public void update(ArrayList<Agent> addedAgents, ArrayList<Agent> removedAgents) {
            this.agents.removeAll(removedAgents);
            this.agents.addAll(addedAgents);

            initAgents();

            psum = 0;
            rsum = 0;
            for(Agent a: agents) {
                psum += precisions.get(a);
                rsum += recalls.get(a);
            }
            pavg = psum/precisions.size();
            ravg = rsum/recalls.size();
        }
    }

    static public class GainLoss extends DolaRecorders.GainLoss implements DolaGenUpdater {

        public GainLoss(List<Agent> agents, Environment env, String cname, String gname, String lname) {
            super(agents, env, cname, gname, lname);

        }

        @Override
        public void update(ArrayList<Agent> addedAgents, ArrayList<Agent> removedAgents) {
            for (Agent a : removedAgents) {
                for (BitSet b : env.getInputs()) {
                    wrong(a, b);
                }
            }

            for(Agent a: addedAgents) {
                for(BitSet b: env.getInputs()) {
                    check(a, b);
                }
            }

        }
    }
}
