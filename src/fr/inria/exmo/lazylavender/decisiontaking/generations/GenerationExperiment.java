package fr.inria.exmo.lazylavender.decisiontaking.generations;

import fr.inria.exmo.lazylavender.decisiontaking.*;
import fr.inria.exmo.lazylavender.decisiontaking.generations.knowledgetransmission.KnowledgeTransmitter;
import fr.inria.exmo.lazylavender.decisiontaking.generations.knowledgetransmission.Transmitters;
import fr.inria.exmo.lazylavender.decisiontaking.measurements.Recorder;
import fr.inria.exmo.lazylavender.logger.ActionLogger;
import fr.inria.exmo.lazylavender.model.LLException;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

public class GenerationExperiment extends fr.inria.exmo.lazylavender.decisiontaking.Experiment {


    protected int numberOfParents = 2;
    protected String parentSelection = "best"; // can be: "income", "srate", "distance", "best"
    protected String survivalSelection = "random";

    protected String initOnto = "learn"; // can be: "learn", "empty", "random", "merge"
    protected double trainingTransmissionRatio = 1; //number of objects selected by parents as training set
    protected double errorRate = 0; //number of objects labelled incorrectly by parents

    protected double epsilon = 1; // probability of a child agent interacting with one of its parents
    protected double annealing = 0.01; // meaning after (epsilon/annealing) interactions, the agent becomes independent of its parents

    protected int period = 1; // after period iterations, a birth/death event occurs
    protected int numberOfBirthDeath = 2; // number of agents that die/are born at a birth/death event


    protected Map<Agent, List<Agent>> parentsMap = new HashMap<>();

    Map<Agent, Integer> agentLives = new HashMap<>();


    KnowledgeTransmitter transmitter;
    DolaGenRecorders.Distance disReco;
    DolaGenRecorders.DistanceLeaves ldisReco;

    int agentIDs = 0;

    @Override
    protected void loadParams(Properties p) {
        super.loadParams(p);

        if (p.containsKey("numberOfParents"))
            numberOfParents = Integer.parseInt(p.getProperty("numberOfParents"));

        if (p.containsKey("parentSelection"))
            parentSelection = p.getProperty("parentSelection");

        if (p.containsKey("survivalSelection"))
            survivalSelection = p.getProperty("survivalSelection");

        if (p.containsKey("initOnto"))
            initOnto = p.getProperty("initOnto");

        if (p.containsKey("ttratio"))
            trainingTransmissionRatio = Double.parseDouble(p.getProperty("ttratio"));

        if (p.containsKey("errorRate"))
            errorRate = Double.parseDouble(p.getProperty("errorRate"));

        if (p.containsKey("epsilon"))
            epsilon = Double.parseDouble(p.getProperty("epsilon"));

        if (p.containsKey("annealing"))
            annealing = Double.parseDouble(p.getProperty("annealing"));


        if (p.containsKey("period"))
            period = Integer.parseInt(p.getProperty("period"));

        if (p.containsKey("numberOfBirthDeath"))
            numberOfBirthDeath = Integer.parseInt(p.getProperty("numberOfBirthDeath"));

    }

    public int getNumberOfParents() {
        return numberOfParents;
    }

    public String getParentSelection() {
        return parentSelection;
    }

    public String getSurvivalSelection() {
        return survivalSelection;
    }

    public String getInitOnto() {
        return initOnto;
    }

    public double getTrainingTransmissionRatio() {
        return trainingTransmissionRatio;
    }

    public double getErrorRate() {
        return errorRate;
    }

    public int getPeriod() {
        return period;
    }

    public int getNumberOfBirthDeath() {
        return numberOfBirthDeath;
    }

    @Override
    public ActionLogger init(Properties param) throws LLException {
        loadParams(param);

        if (runDir != null)
            new File(runDir).mkdirs();

        env = new Environment(numberOfFeatures, numberOfClasses);
        env.init(param);

        transmitter = (initOnto.equals("learn"))? new Transmitters.TeachingTransmitter(trainingTransmissionRatio, env,
                errorRate, numberOfClasses):
                (initOnto.equals("merge"))? new Transmitters.MergeTransmitter():
                (initOnto.equals("random"))? new Transmitters.RandomTransmitter(numberOfClasses, numberOfFeatures):
                                new Transmitters.EmptyTransmitter(numberOfClasses);

        for (int i = 0; i < numberOfAgents; i++) {
            agents.add(new Agent(agentIDs++, env));
            agents.get(i).init(param);
        }
        loadTrainingSets();
        trainAgents();
        if (runDir != null)
            saveTrainingSets();
        if (loadRunDir == null)
            gameIt = new GameIterators.ParentBiasedGameIterator(this);
        else {
            try {
                gameIt = Files.lines(Paths.get(loadRunDir, "games.tsv")).iterator();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        disReco = new DolaGenRecorders.Distance(agents, "distance", "newGenDis", "prevGenDis");
        ldisReco = new DolaGenRecorders.DistanceLeaves(agents, "lDistance");
        alogger = rManager.init(new DolaGenRecorders.Srate("ssrate"), new DolaGenRecorders.Accuracy(agents,
                        env.getInputs(), env.getTargets(), "accuracy"),
                disReco,
//                ldisReco,
                new DolaGenRecorders.AdaptationCounter("ha","h1a","va","aToA", "cToC", "aToC",
                        "cToA", agents)
//                new DolaGenRecorders.Srate("genSrate", true),
//                new DolaGenRecorders.AgentsIncome(this),
//                new DolaGenRecorders.Distance(agents, "genDistance",true),
//                new DolaGenRecorders.AverageAccuracy(agents, env.getInputs(), env.getTargets(), "genAccuracy", true)
        );

        tBias = new GenTransmissionBiases.GenEnvBias(this);
        return alogger;
    }

    @Override
    public void process() throws LLException {
        PrintWriter prGames = null;

        if (runDir != null) {
            try {
                prGames = new PrintWriter(new File(runDir, "games.tsv"));
            } catch (FileNotFoundException fnfex) {

            }
        }

        int remaining = numberOfIterations;
        performTasks(0, agents);

        int count = 0;
        while (gameIt.hasNext()) {
            Agent a1, a2;

            BitSet instance;
            String g = gameIt.next();
            String[] spec = g.split("\t");
            a1 = agents.get(Integer.parseInt(spec[0]));
            a2 = agents.get(Integer.parseInt(spec[1]));
            instance = env.parseInstance(spec[2]);
            count++;
            performTasks(count, a1, a2);
            Game game = new Game(a1, a2, instance, this);
            a1.playGame(remaining--, game);

            tBias.update(game);
//            updateAgentSuccess(game);

            rManager.update(game);

            alogger.logGame(a1.getId(), a2.getId(), game.getInstance() + "");
            alogger.logResult(game.isSuccess() ? "SUCCESS" : "FAILURE", game.isSuccess(), game.getAdaptation() + "");

            rManager.log(game);
            if (prGames != null) game.save(prGames);

            if(isBirthDeathEvent(count)) {

                updateAgentLives();
                Common.Pair<ArrayList<Agent>, ArrayList<Agent>> p = birthDeathEvent();
                performTasks(count, p.getKey());
                for(Recorder rec: rManager.getRecorders()) {
                    ((DolaGenRecorders.DolaGenUpdater)rec).update(p.getKey(), p.getValue());
                }

                ((GenTransmissionBiases.GenBias)tBias).update(p.getKey(), p.getValue());

            }


        }

        if (prGames != null) prGames.close();

    }


    protected boolean isBirthDeathEvent(int iteration) {
        return (iteration % period) == 0;
    }

    protected void updateAgentLives() {
        for(Agent a: getAgents()) {
            int life = 0;
            if(agentLives.containsKey(a))
                life = agentLives.get(a);

            agentLives.put(a, life+1);
        }
        List<Agent> toRemove = new ArrayList<>();
        for(Agent a: agentLives.keySet()) {
            if(!agents.contains(a))
                toRemove.add(a);
        }
        for(Agent a: toRemove)
            agentLives.remove(a);
    }

    protected Double getParentSelectionFitness(Agent a) {
        // TODO fix this numberOfBirthDeath
        int lifeCoeff = (agentLives.get(a) == 1 && a.getId() >= numberOfBirthDeath)?1:0; // select only from agents that just finished their first period
        if(parentSelection.equals("income") || parentSelection.equals("best"))
            return a.getRelativePayoff(null) * lifeCoeff;
        else if(parentSelection.equals("distance"))
            return 1. * lifeCoeff; // TODO
        return 1. * lifeCoeff; // equi-probable, they all have the same fitness = 1
    }

    protected Double getParentGroupSelectionFitness(Agent... agents) {
        double score = 1;
        if (parentSelection.equals("income") || parentSelection.equals("best") || parentSelection.equals("random"))
            for(Agent a: agents) {
                int lifeCoeff = (agentLives.get(a) == 1) ? 1 : 0; // select only from agents that just finished their first period
                if(parentSelection.equals("random"))
                    score *= 1. * lifeCoeff; // equi-probable, they all have the same fitness = 1
                else
                    score *= a.getRelativePayoff(null) * lifeCoeff;
            }
        else if (parentSelection.equals("distance")) {
            List<Agent> lagents = new ArrayList<>();
            for(Agent a: agents)
                lagents.add(a);
            for (Agent a : agents) {
                int lifeCoeff = (agentLives.get(a) == 1) ? 1 : 0; // select only from agents that just finished their first period


                score *= disReco.getSim(a, lagents) * lifeCoeff;
            }
        }
        return score;
    }

    protected int selectParent(ArrayList<Double> parentsfitness) {
        int index = 0;
        double max = 0;
        if(parentSelection.equals("best")) {
            for (int i = 0; i < parentsfitness.size(); i++) {
                if (parentsfitness.get(i) > max) {
                    max = parentsfitness.get(i);
                    index = i;
                }
            }
        }
        else {
            double sum = 0;
            for(double d : parentsfitness)
                sum += d;
            double random = Math.random() * sum;
            for (; index < parentsfitness.size(); index++) {
                random -= parentsfitness.get(index);
                if(random <= 0)
                    break;
            }
        }
        return index;
    }

    protected ArrayList<Agent> selectParentGroup() {
        ArrayList<Agent> parents = new ArrayList<>();
        ArrayList<Agent> candidates = new ArrayList<>(agents);

//        for(Agent p: candidates) {
//            System.out.println("parent " + p.getId());
//            System.out.println(getAgentAccuracy(p, env.getInputs(), env.getTargets()));
//        }

        if(parentSelection.equals("distance")) {
            int[] indexes = new int[numberOfParents];
            for (int i = 0; i < numberOfParents; i++) {
                indexes[i] = 0;
            }
            indexes[numberOfParents-1] = -1;
            while (true) {
                int l = numberOfParents-1;
                indexes[l]++;
                while (l > 0 && indexes[l] == numberOfParents) {
                    indexes[l--] = 0;
                    indexes[l] ++;
                }
                if(l == 0 && indexes[l] == numberOfParents)
                    break;

                
            }
        }
        else {
            while (parents.size() < numberOfParents) {
                ArrayList<Double> fitnesses = new ArrayList<>();
                for (Agent candidate : candidates)
                    fitnesses.add(getParentSelectionFitness(candidate));
                int index = selectParent(fitnesses);
                parents.add(candidates.get(index));
                candidates.remove(index);
            }
        }
//        System.out.println("--end of selection--");
//
//        for(Agent p: parents)
//            System.out.println("selected " + p.getId());

        return parents;
    }

    protected ArrayList<Agent> createNewAgents(int size) {
        ArrayList<Agent> newAgents = new ArrayList<>();
        parentsMap = new HashMap<>();
        for (int i = 0; i < size; i++) {
            Agent a = new Agent(agentIDs++, env);
            a.init(params);
            newAgents.add(a);
            ArrayList<Agent> parents = selectParentGroup();
            parentsMap.put(a, parents);
        }
        return newAgents;
    }

    protected double getSurvivalFitness(Agent a) {
//        if(survivalSelection.equals("income"))
//            return 1.0-a.getScore(null);
//        return 1.; // random
        return agentLives.get(a);
    }

    protected void knowledgeTransmission(ArrayList<Agent> newAgents) {
        for(Agent a: newAgents) {
            transmitter.initializeKnowledge(parentsMap.get(a), a);
        }
    }





    protected ArrayList<Agent> removeUnfitAgents() {
        int removed = 0;
        ArrayList<Agent> removedAgents = new ArrayList<>();

        while (removed < numberOfBirthDeath) {

//            ArrayList<Double> fitnesses = new ArrayList<>();
//            double sum = 0;
//            for(Agent a: agents) {
//                double f = getSurvivalFitness(a);
//                fitnesses.add(f);
//                sum += f;
//            }
//
//            double r = Math.random() * sum;
//            int index = 0;
//
//            for(;index < fitnesses.size();index++) {
//                r -= fitnesses.get(index);
//                if(r <= 0)
//                    break;
//            }

            int index = 0;
            double max = 0;
            for (int i = 0; i < agents.size(); i++) {
                double fitness = getSurvivalFitness(agents.get(i));
                if(fitness > max) {
                    max = fitness;
                    index = i;
                }
            }

            removedAgents.add(removeAgent(index));

            removed++;
        }

        return removedAgents;
    }

    protected Agent removeAgent(int index) {
        Agent a = agents.remove(index);

        for(Agent child: parentsMap.keySet()) {
            parentsMap.get(child).remove(a); // remove automatically checks if it is contained or not
        }
        parentsMap.remove(a);

        return a;
    }


    protected Common.Pair<ArrayList<Agent>, ArrayList<Agent>> birthDeathEvent() {

        ArrayList<Agent> newAgents = createNewAgents(numberOfBirthDeath);
        knowledgeTransmission(newAgents);
        ArrayList<Agent> removedAgents = removeUnfitAgents();

        agents.addAll(newAgents);


        return new Common.Pair<>(newAgents, removedAgents);
    }


    public double getEpsilon() {
        return epsilon;
    }

    public double getAnnealing() {
        return annealing;
    }

    public Map<Agent, List<Agent>> getParentsMap() {
        return parentsMap;
    }


    public static void main(String[] args) throws LLException {
        GenerationExperiment exp = new GenerationExperiment();
        Properties p = new Properties();
        p.setProperty("trainer", "RAND");
        p.setProperty("op", "oneCom");
        p.setProperty("numberOfClasses", "4");
        p.setProperty("numberOfFeatures", "4");
        p.setProperty("numberOfAgents", "20");
        p.setProperty("adapt", "1");
        p.setProperty("sampleRatio", "0.2");
        p.setProperty("ratio", "0.4");
        p.setProperty("parentSelection", "random");
        p.setProperty("numberOfIterations", "80000");
        p.setProperty("numberOfBirthDeath", "10");
        p.setProperty("period", "10001");
        p.setProperty("epsilon", "1");
        p.setProperty("annealing", "0.001");
        p.setProperty("numberOfParents", "2");
        p.setProperty("ttratio", "0.3");
        p.setProperty("initOnto", "learn");

        exp.init(p);

        List<Agent> agents = exp.agents;
//        System.out.println(exp.env.classifier);
//        exp.scoreAgents(agents);
        double avg = 0;
        for (Agent a : agents) {
            double perf = getAgentAccuracy(a, exp.env.getInputs(), exp.env.getTargets());
            avg += perf;
            System.out.println("score " + a.getId() + ": " + perf);
        }
        avg /= agents.size();
        System.out.println("average score: " + avg);
        System.out.println("============== START ==============");
//        exp.log(null);

//        for(Agent a: agents)
//            for(Agent a2: agents)
//                if(a.getId() != a2.getId())
//                {
//                    System.out.println("similarity a"+a.getId()+" with a"+ a2.getId()+": "+exp.getAgentOntoSim(a, a2));
////                    TreeClassifier.Node r1 = a.getClassifier().getRoot(), r2 = a2.getClassifier().getRoot();
////                    Map<TreeClassifier.Node, TreeClassifier.Node> m = exp.getTreeCommonNodes(r1, r2);
////                    for(TreeClassifier.Node k: m.keySet())
////                        System.out.println("similar nodes: " + k.getRCondition() + " and " + m.get(k).getRCondition());
//                }

        for(Agent a: agents) {
            System.out.println(a.getId()+": \n"+a.getClassifier());
        }

        exp.process();

        avg = 0;
        for (Agent a : agents) {
            double perf = getAgentAccuracy(a, exp.env.getInputs(), exp.env.getTargets());
            avg += perf;
            System.out.println("score " + a.getId() + ": " + perf);
        }
        avg /= agents.size();

        System.out.println("average score: " + avg);

//        for(Agent a: agents) {
//            System.out.println(a.getId()+": \n"+a.getClassifier());
//        }

//        System.out.println("environment");
//        for (BitSet input: exp.env.getInputs()) {
//            System.out.println(input + ": " + exp.env.getClass(input));
//        }
//
//        System.out.println("agents");
//        for (BitSet input: exp.env.getInputs()) {
//            System.out.println(input + ": " + agents.get(0).getClassifier().getClass(input));
//        }
//        System.out.println(agents.get(0).getClassifier());
//        logger.info("avg score: " + avg);

        System.out.println("==============  END  ==============");

//        for(Agent a: agents)
//            for(Agent a2: agents)
//                if(a.getId() != a2.getId())
//                {
//                    System.out.println("similarity a"+a.getId()+" with a"+ a2.getId()+": "+exp.getAgentOntoSim(a, a2));
////                    TreeClassifier.Node r1 = a.getClassifier().getRoot(), r2 = a2.getClassifier().getRoot();
////                    Map<TreeClassifier.Node, TreeClassifier.Node> m = exp.getTreeCommonNodes(r1, r2);
////                    for(TreeClassifier.Node k: m.keySet())
////                        System.out.println("similar nodes: " + k.getRCondition() + " ***** " + m.get(k).getRCondition());
//                }
//        for(Agent a: agents)
//            System.out.println(a.getClassifier());

//        for (double s: exp.successRates)
//            System.out.println(s);
    }


}
