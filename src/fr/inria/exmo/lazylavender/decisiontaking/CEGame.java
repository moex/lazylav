package fr.inria.exmo.lazylavender.decisiontaking;

import fr.inria.exmo.lazylavender.expe.LLGame;

import java.util.BitSet;

public interface CEGame extends LLGame {

    public boolean isSuccess();

    public void setSuccess(boolean success);

    public BitSet getInstance();

}
