package fr.inria.exmo.lazylavender.decisiontaking;

import java.util.List;

public interface TransmissionBias {
    void update(CEGame game);
    double getValue(Agent agent, Game game); // this could return an object of a new class for more modularity
}
