package fr.inria.exmo.lazylavender.decisiontaking;

public class Common {
    public static class Pair<T,K> {
        T key;
        K value;

        public Pair(T key, K value) {
            this.key = key;
            this.value = value;
        }

        public T getKey() {
            return key;
        }

        public K getValue() {
            return value;
        }
    }
}
