/*
 * Copyright (C) INRIA, 2023-2024
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 */

package fr.inria.exmo.lazylavender.decisiontaking.intrinsicmotivation;

import fr.inria.exmo.lazylavender.decisiontaking.treeclassifier.NoClassException;
import fr.inria.exmo.lazylavender.decisiontaking.treeclassifier.TreeClassifier;
import fr.inria.exmo.lazylavender.decisiontaking.treeclassifier.TreeClassifierQuick;

import java.util.*;
import java.util.List;
import java.util.BitSet;
import java.util.ArrayList;
import java.util.*;
import java.util.*;
import java.util.*;

public class Util {

    /**
     * Returns all possible permutations (object types) with binary properties of a given size.
     *
     * @param currentObject because recursive, the current object to be changed
     * @param i the current position
     * @param size the size of the object
     * @return the list with all permutations
     */
    public static List<BitSet> getAllPermutations(BitSet currentObject, int i, int size) {
        if (i == size) {
            List<BitSet> result = new ArrayList<>();
            result.add(currentObject);
            return result;
        }
        BitSet positive = (BitSet) currentObject.clone();
        positive.set(i, true);
        List<BitSet> result = new ArrayList<BitSet>(getAllPermutations(positive, i + 1, size));
        BitSet negative = (BitSet) currentObject.clone();
        negative.set(i, false);
        result.addAll(getAllPermutations(negative, i + 1, size));
        return result;
    }

    /**
     * Returns a HashMap with all leafs (keys) and their paths to the root (values) of an ontology.
     *
     * @param classifier the agent's ontology
     * @return HashMap with all leafs (keys) and their paths to the root (values) of an ontology
     */
	//@SuppressWarnings("unchecked")
	public static HashMap<TreeClassifier.Node, List<TreeClassifier.Node>> getAllLeafPaths(TreeClassifier classifier) {
        HashMap<TreeClassifierQuick.Node, List<TreeClassifierQuick.Node>> paths = new HashMap<>();
        TreeClassifierQuick.Node currNode = classifier.getRoot();
        ArrayList<TreeClassifierQuick.Node> currPath = new ArrayList<>();
        paths.put(currNode, currPath);

        // go through whole tree and retrieve paths & at the same time find longest path
        Stack<TreeClassifierQuick.Node> openNodes = new Stack<>();
        ArrayList<TreeClassifierQuick.Node> longestPath = currPath;
        openNodes.push(currNode);
        while (! openNodes.isEmpty()) {
            currNode = openNodes.pop();
            // go to every child and save its current path
            for (TreeClassifierQuick.Node child : currNode.getChildren()) {
                currPath = (ArrayList<TreeClassifierQuick.Node>) ((ArrayList<TreeClassifierQuick.Node>) paths.get(currNode)).clone();
                currPath.add(child);
                paths.put(child, currPath);
                openNodes.push(child);

                if (currPath.size() > longestPath.size())
                    longestPath = currPath;
            }
            if (! currNode.getChildren().isEmpty())
                paths.remove(currNode);
        }

        longestPath.remove(classifier.getRoot());
        return paths;
    }

    /**
     * Returns the path from a given leaf to the root.
     *
     * @param classifier the agent's classifier
     * @param leaf the leaf to be traced to the root
     * @return the path of the leaf to the root
     */
    public static List<Property> getPath(TreeClassifier classifier, TreeClassifierQuick.Node leaf) {
        List<Property> path = new ArrayList<>();
        while (! leaf.equals(classifier.getRoot())) {
            TreeClassifierQuick.BitsetCondition bitsetCondition = (TreeClassifierQuick.BitsetCondition) leaf.getCondition();
            int property = bitsetCondition.getConditionedAttributes().length() - 1;
            path.add(new Property(property, ! bitsetCondition.getConditions().get(property) || bitsetCondition.isNot()));
            leaf = leaf.getParent();
        }
        return path;
    }

    /**
     * Searches wether the property *propertyName* is already in the collection *properties*.
     *
     * @param propertyName the searched property
     * @param properties the collection to be searched
     * @return whether the property exists
     */
    public static boolean propertyExists(int propertyName, Collection<Property> properties) {
        boolean exists = false;
        for (Property prop : properties) {
            if (prop.propertyName == propertyName){
                exists = true;
                break;
            }
        }
        return exists;
    }

    /**
     * Returns the type / form (negated or not negated) of the property least or most used.
     *
     * @param seenObjects a list of all seen objects
     * @param property the property that is checked
     * @param minimum whether the minimum or maximum is searched
     * @return the property
     */
    public static Property getPropertyType(List<BitSet> seenObjects, int property, boolean minimum) {
        // calculate how often the unused property was used
        int missingProperty = 0;
        int nMissingProperty = 0;
        for (BitSet seenObject : seenObjects) {
            if (seenObject.get(property)) {
                missingProperty++;
            } else {
                nMissingProperty++;
            }
        }

        // save the least or most (depends on minimum) used option of the property (true or false)
        double rand = new Random().nextDouble();
        Property newProperty = new Property(property, rand <= 0.5);
        if (minimum && (missingProperty < nMissingProperty || missingProperty > nMissingProperty))
            newProperty = new Property(property,
                    missingProperty > nMissingProperty,
                    Math.min(missingProperty, nMissingProperty));
        else if (! minimum && (missingProperty < nMissingProperty || missingProperty > nMissingProperty))
            newProperty = new Property(property,
                    missingProperty < nMissingProperty,
                    Math.max(missingProperty, nMissingProperty));

        return newProperty;
    }

    /**
     * Calculates the novelty score for every leaf in the agent's ontology.
     *
     * @param classifier the agent's classifier
     * @param seenObjects all seen objects
     * @param amtProperties the amount of properties the objects have
     * @param mostNovel whether the most or least novel node is searched
     * @return TreeClassifier.Node the most or least novel node
     * @throws NoClassException
     */
    public static TreeClassifier.Node noveltyBasedChosenNode(TreeClassifier classifier, List<BitSet> seenObjects,
                                                             int amtProperties, boolean mostNovel) throws NoClassException {
        // count classified objects and object types for each leaf
        Map<TreeClassifier.Node, Integer[]> classifiedObjects = new HashMap<>();
        Map<TreeClassifier.Node, List<BitSet>> classifiedObjectTypes = new HashMap<>();

        Map<TreeClassifier.Node, List<TreeClassifier.Node>> leaves = getAllLeafPaths(classifier);
        for (TreeClassifier.Node leaf : leaves.keySet()) {

            // add the objectType list if not already there
            List<BitSet> tmp = new ArrayList<>();
            classifiedObjectTypes.putIfAbsent(leaf, tmp);

            // add the classifiedObjects if not already there
            // calculate the specificity of the leaf
            int sp = 0;
            TreeClassifierQuick.Node _currLeaf = leaf;
            while (_currLeaf.getParent() != null && ! _currLeaf.getParent().equals(classifier.getRoot())) {
                sp++;
                _currLeaf = _currLeaf.getParent();
            }
            // and add the counter for one object seen
            classifiedObjects.putIfAbsent(leaf, new Integer[]{sp, 0});

            for (BitSet seenObject : seenObjects){
                TreeClassifierQuick.Node classificationLeaf = classifier.getNode(seenObject);

                if (classificationLeaf.equals(leaf)) {
                    // an object has already been classified by the current leaf (so the agent saw the object e.g. a second time)
                    if (classifiedObjects.get(leaf) != null) {
                        // update the counter for the classifiedObjects in this leaf
                        Integer[] spAndCounter = classifiedObjects.get(leaf);
                        spAndCounter[1] = spAndCounter[1] + 1;
                        classifiedObjects.put(leaf, spAndCounter);
                    } else if (leaf == classifier.getRoot()) {
                        classifiedObjects.put(leaf, new Integer[]{0, 1});
                    }

                    // only add object type, if not there already
                    if (! classifiedObjectTypes.get(leaf).contains(seenObject)) {
                        tmp = classifiedObjectTypes.get(leaf);
                        tmp.add(seenObject);
                        classifiedObjectTypes.put(leaf, tmp);
                    }
                }
            }
        }

        // get the most or least novel leaf node (depends on bool mostNovel)
        Map<Double, List<TreeClassifier.Node>> scoresNodes = new HashMap<>();
        int amtSeenObjects = seenObjects.size();
        double maxScore = mostNovel ? 0 : 1000000, iSeen, sp, cClassified, nscore;
        for (TreeClassifierQuick.Node leaf : classifiedObjects.keySet()) {
            // calculate the novelty score
            iSeen = classifiedObjectTypes.get(leaf).size();
            sp = classifiedObjects.get(leaf)[0];
            cClassified = classifiedObjects.get(leaf)[1];
            nscore = 1.0 - (( iSeen / Math.pow(2, amtProperties - sp) + (cClassified / (double) amtSeenObjects)) * (1/2.0));
            // add to most novel
            if (nscore > maxScore && mostNovel)
                maxScore = nscore;
            else if (nscore < maxScore && ! mostNovel)
                maxScore = nscore;
            // save in list of specific nscore, to determine the most novel leaf randomly later
            scoresNodes.putIfAbsent(nscore, new ArrayList<>());
            List<TreeClassifier.Node> tmp = scoresNodes.get(nscore);
            tmp.add(leaf);
            scoresNodes.put(nscore, tmp);
        }

        // randomly get most novel leaf of equally novel leaves
        Random rand = new Random();
        List<TreeClassifier.Node> bestNodes = scoresNodes.get(maxScore);
        return bestNodes.get(rand.nextInt(bestNodes.size()));
    }
}
