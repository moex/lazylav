package fr.inria.exmo.lazylavender.decisiontaking.intrinsicmotivation;

import fr.inria.exmo.lazylavender.agent.LLAgent;

import java.util.BitSet;

class InteractionPartner {

    private BitSet coreBelief;
    private int disagreements;
    private int interactions;

    private LLAgent agent;

    public InteractionPartner(BitSet coreBelief, int disagreements, int interactions, LLAgent agent) {
        this.coreBelief = coreBelief;
        this.disagreements = disagreements;
        this.interactions = interactions;
        this.agent = agent;
    }

    public double getDifferenceScore(BitSet belief) {
        double dScore = 0;
        if ((belief == null && coreBelief != null) || (belief != null && coreBelief == null) ||
                (belief != null && coreBelief != null && !belief.equals(coreBelief))) dScore = 0.5;
        return dScore + this.getDisagreementRate();
    }

    public void addInteraction() {
        this.interactions++;
    }

    public void addDisagreement() {
        this.disagreements++;
    }

    public double getDisagreementRate() {
        return (double) disagreements / (double) interactions;
    }

    public LLAgent getAgent() {
        return agent;
    }

}
