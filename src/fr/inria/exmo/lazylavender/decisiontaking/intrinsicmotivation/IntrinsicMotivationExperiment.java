/*
 *
 * Copyright (C) INRIA, 2023-2024
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 */

package fr.inria.exmo.lazylavender.decisiontaking.intrinsicmotivation;

import fr.inria.exmo.lazylavender.decisiontaking.intrinsicmotivation.reinforcement.GameState;
import fr.inria.exmo.lazylavender.decisiontaking.intrinsicmotivation.reinforcement.LearningGame;
import fr.inria.exmo.lazylavender.decisiontaking.intrinsicmotivation.reinforcement.NetworkUtil;
import fr.inria.exmo.lazylavender.decisiontaking.intrinsicmotivation.Motivation;

// JE: there is too much copy here instead of reuse.
import fr.inria.exmo.lazylavender.decisiontaking.Experiment;
import fr.inria.exmo.lazylavender.decisiontaking.Environment;
import fr.inria.exmo.lazylavender.decisiontaking.Agent;
import fr.inria.exmo.lazylavender.decisiontaking.GameIterators;
import fr.inria.exmo.lazylavender.decisiontaking.DolaRecorders;
import fr.inria.exmo.lazylavender.decisiontaking.CEGame;
import fr.inria.exmo.lazylavender.decisiontaking.TransmissionBiases;
import fr.inria.exmo.lazylavender.decisiontaking.TransmissionBias;
import fr.inria.exmo.lazylavender.decisiontaking.CoverageRecorder;
import fr.inria.exmo.lazylavender.decisiontaking.Game;

import fr.inria.exmo.lazylavender.logger.ActionLogger;
import fr.inria.exmo.lazylavender.model.LLException;

import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.rl4j.learning.sync.qlearning.discrete.QLearningDiscreteDense;

import java.util.BitSet;
import java.util.List;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.Properties;
import java.util.stream.Collectors;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;


/**
 * The Intrinsic Motivation Experiment for exploration with curiosity, creativity and non-exploratory (curious in
 * familiar knowledge and trying to keep a stable state).
 */
public class IntrinsicMotivationExperiment extends Experiment {

    // ratio of curious / creative / non-exploratory agents
    protected double curiousRatio = 1, creativeRatio = 0, nonExploratoryRatio = 0; // ratio of nonExploratory agents

    protected boolean direct = true; // using the direct or indirect (reinforcement learning) method
    protected boolean social = false; // does the agent only use objects (default) or also interaction partners
    protected int maxGroupSize = 2; // default max two agents

    // successBias
    protected double discountEnv = 0.9;
    protected double coeffEnv = 1;

    // reinforcement learning
    protected String reinforcementModelFolder = "models/";
    protected int numberOfReinforcementLearningIterations = 30000; // default
    protected int reinforcementLearningSteps = 100000; // default
    protected int reinforcementLearningEpochs = 10000; // default
    protected int reinforcementLearningExpReplay = 100000; // default
    protected int reinforcementLearningBatchSize = 64; // default

    public static int NUMBER_OF_INPUTS; // number of neural network inputs == length of game state vector (for reinforcement learning)
    public static double LOW_VALUE; // lowest value of the observation (game state for reinforcement learning)
    public static double HIGH_VALUE; // highest value of the observation (game state for reinforcement learning)

    /**
     * Load the parameter for the experiment.
     */
    @Override
    protected void loadParams(Properties p) {
        super.loadParams(p);

        if (runDir != null)
            new File(runDir).mkdirs();

        if (p.containsKey("explorationMotivation")) {
            String[] explorationMotivations = p.getProperty("explorationMotivation")
                    .replace("[", "").replace("]","").split(":");
            curiousRatio = Double.parseDouble(explorationMotivations[0]);
            creativeRatio = Double.parseDouble(explorationMotivations[1]);
            nonExploratoryRatio = Double.parseDouble(explorationMotivations[2]);
            double sum = curiousRatio + creativeRatio + nonExploratoryRatio;
            curiousRatio = curiousRatio / sum;
            creativeRatio = creativeRatio / sum;
            nonExploratoryRatio = nonExploratoryRatio / sum;
        }

        if(p.containsKey("direct"))
            direct = Boolean.parseBoolean(p.getProperty("direct"));
        if(p.containsKey("social"))
            social = Boolean.parseBoolean(p.getProperty("social"));

        if(p.containsKey("maxGroupSize"))
            maxGroupSize = Integer.parseInt(p.getProperty("maxGroupSize"));
        if (maxGroupSize > numberOfAgents)
            maxGroupSize = numberOfAgents;

        if(p.containsKey("numberOfReinforcementLearningIterations"))
            numberOfReinforcementLearningIterations = Integer.parseInt(p.getProperty("numberOfReinforcementLearningIterations"));
        if(p.containsKey("reinforcementLearningSteps"))
            reinforcementLearningSteps = Integer.parseInt(p.getProperty("reinforcementLearningSteps"));
        if(p.containsKey("reinforcementLearningEpochs"))
            reinforcementLearningEpochs = Integer.parseInt(p.getProperty("reinforcementLearningEpochs"));
        if(p.containsKey("reinforcementLearningExpReplay"))
            reinforcementLearningExpReplay = Integer.parseInt(p.getProperty("reinforcementLearningExpReplay"));
        if(p.containsKey("reinforcementLearningBatchSize"))
            reinforcementLearningBatchSize = Integer.parseInt(p.getProperty("reinforcementLearningBatchSize"));

        if (p.containsKey("coeffEnv"))
            coeffEnv = Double.parseDouble(p.getProperty("coeffEnv"));
        if (p.containsKey("discountEnv"))
            discountEnv = Double.parseDouble(p.getProperty("discountEnv"));
    }

    /**
     * Initialise the experiment.
     */
    @Override
    public ActionLogger init(Properties param) throws LLException {
        loadParams(param);

        // create the environment
        env = new Environment(numberOfFeatures, numberOfClasses);
        env.init(param);

        // reinforcement learning
        if (! direct)
            doReinforcementLearning(param);

        // create the agents for the experiment
        createAgents(param);

        // train the agents for the experiment
        loadTrainingSets();
        trainAgents();
        if (runDir != null) // save the trainingsset if wanted
            saveTrainingSets();
        for (Agent agent : agents)
            ((MotivatedAgent) agent).initRewards();

        // create the game
        gameIt = new GameIterators.IntrinsicMotivationGameIterator(this);

        // adding the transmission biases ( mainly success bias)
        TransmissionBias tb1 = new TransmissionBiases.EnvBias(this,discountEnv);
        List<TransmissionBias> biases = new ArrayList<>();
        biases.add(tb1);
        List<Double> coeffs = new ArrayList<>();
        coeffs.add(coeffEnv);
        tBias = new TransmissionBiases.AggregatedBias(this, biases, coeffs);

        // adding the logger
        alogger = rManager.init(
                new DolaRecorders.Srate("successRate"),
                new DolaRecorders.Accuracy(agents, env.getInputs(), env.getTargets(), "accuracy"),
				// JE (2024): likely this one is to change (see my TestExperiment)
                //new DolaRecorders.Completeness("completeness", agents, env.getInputs(), env.getTargets()),
                new DolaRecorders.Distance(agents, "distance"),
                new DolaRecorders.Exploration("objectExplorScore", env.getNumberOfFeatures(), true),
                new DolaRecorders.Exploration("partnerExplorScore", env.getNumberOfFeatures(), false),
                new DolaRecorders.ExplorationMotivationReward("curiosityReward", Motivation.CURIOUS),
                new DolaRecorders.ExplorationMotivationReward("creativityReward", Motivation.CREATIVE),
                new DolaRecorders.ExplorationMotivationReward("nonExploratoryReward", Motivation.NONEXPLORATORY),
				new CoverageRecorder( "coverage", agents, env ));
        return alogger;
    }

    /**
     * Performs all games and logs results.
     *
     * @throws LLException
     */
    @Override
    public void process() throws LLException {
        PrintWriter prGames = null;
        if (runDir != null) {
            try {
                prGames = new PrintWriter(new File(runDir, "games.tsv"));
            } catch (FileNotFoundException fnfex) {

            }
        }

        int remaining = numberOfIterations;
        performTasks(0, agents);

        int count = 0;
        while (gameIt.hasNext()) {
            MotivatedAgent a1;
            ArrayList<Agent> an;

            // get first game
            BitSet instance;
            String g = gameIt.next();
            String[] spec = g.split("\t");
            String[] interactionPartners = spec[1].split(",");

            // get agents of the game
            a1 = (MotivatedAgent) agents.get(Integer.parseInt(spec[0]));
            an = (ArrayList<Agent>) Arrays.stream(interactionPartners)
                    .map(partner -> {return agents.get(Integer.parseInt(partner));})
                    .collect(Collectors.toList());
            instance = env.parseInstance(spec[2]);

            // an egoistic game
            CEGame game;
            if (!social) {
                performTasks(++count, agents);
                game = new Game(a1, an.get(0), instance, this);

                // update successBias
                if(tBias instanceof TransmissionBiases.AggregatedBias) {
                    List<TransmissionBias> transmissionBiases = ((TransmissionBiases.AggregatedBias) tBias).getTransmissionBiases();
                    for(TransmissionBias tb: transmissionBiases) {
                        tb.update((Game) game);
                    }
                }

                // play the game
                a1.playGame(remaining--, game);

                // update the two agents
                MotivatedAgent a2 = (MotivatedAgent) an.get(0);
				a1.notifyGame( (Game)game );
				a2.notifyGame( (Game)game );

                // log and save the information
                rManager.update(game);
                alogger.logGame(a1.getId(), a2.getId(), ((Game) game).getInstance() + "");
                alogger.logResult(game.isSuccess() ? "SUCCESS" : "FAILURE", game.isSuccess(), ((Game) game).getAdaptation() + "");

            // a social game
            } else {
                game = new GroupGame(a1, an, instance, this);

                // update successBias
                if(tBias instanceof TransmissionBiases.AggregatedBias) {
                    List<TransmissionBias> transmissionBiases = ((TransmissionBiases.AggregatedBias) tBias).getTransmissionBiases();
                    for(TransmissionBias tb: transmissionBiases) {
                        tb.update(game);
                    }
                }

                // play the game
                a1.playGame(remaining--, game);

                // update the group of agents
                //a1.addSeenObject(instance);
                //an.forEach(agent -> {
                //    MotivatedAgent ai = (MotivatedAgent) agent;
                //    ai.addSeenObject(instance);
                //    a1.addInteractionPartner(ai, game.isSuccess());
                //    ai.addInteractionPartner(a1, game.isSuccess());
                //});
				// JE: extension
				a1.notifyGame( (GroupGame)game );
                an.forEach(agent -> { ((MotivatedAgent)agent).notifyGame( (GroupGame)game ); });

                // log and save the information
                rManager.update(game);
                int[] partners = new int[an.size()];
                for (int i = 0; i < an.size(); i++) {
                    partners[i] = an.get(i).getId();
                }
                alogger.logGame(a1.getId(), partners,((GroupGame) game).getInstance() + "");
                alogger.logResult(game.isSuccess() ? "SUCCESS" : "FAILURE", game.isSuccess(), ((GroupGame) game).getAdaptation() + "");
            }

            rManager.log(game);

            if (prGames != null)
                game.save(prGames);

            if (remaining % 1000 == 0)
                System.out.println("Progress: " + (((double )(numberOfIterations - remaining) / numberOfIterations) * 100.0) + "%");

        }
        if (prGames != null) prGames.close();
    }

    /**
     * Performs the reinforcement learning, if there is not already a model.
     *
     * @param param the properties needed to create the agents
     */
    private void doReinforcementLearning(Properties param) {
        List<Motivation> motivations = new ArrayList<>();
        if (curiousRatio > 0)
            motivations.add(Motivation.CURIOUS);
        if (creativeRatio > 0)
            motivations.add(Motivation.CREATIVE);
        if (nonExploratoryRatio > 0)
            motivations.add(Motivation.NONEXPLORATORY);

        // learn the models
        for (Motivation motivation : motivations) {
            // Give a name to the network we are about to train
            final String networkName = getReinforcementNetworkFileName(motivation);
            File f = new File(networkName);
            if(f.exists() && !f.isDirectory())
                continue;

            // get the min and max values of the game state for the observation
            NUMBER_OF_INPUTS = ((int) Math.pow(2, numberOfFeatures) - 1) * numberOfFeatures + (int) Math.pow(2, numberOfFeatures);
            switch (motivation) {
                case CURIOUS:
                    LOW_VALUE = 0;
                    HIGH_VALUE = Math.pow(2, numberOfFeatures);
                    break;
                case CREATIVE:
                case NONEXPLORATORY:
                    LOW_VALUE = 0;
                    HIGH_VALUE = Math.pow(2, numberOfFeatures) / 2;
                    break;
                default:
                    LOW_VALUE = 0;
                    HIGH_VALUE= 10;
            }

            // create the learning game for the reinforcement learning
            LearningGame game = new LearningGame(numberOfAgents, motivation, env, social,
                    param, numberOfReinforcementLearningIterations, discountEnv, coeffEnv, this.maxGroupSize);
            // create the training environment
            final fr.inria.exmo.lazylavender.decisiontaking.intrinsicmotivation.reinforcement.Environment mdp;
            mdp = new fr.inria.exmo.lazylavender.decisiontaking.intrinsicmotivation.reinforcement.Environment(game);
            final QLearningDiscreteDense<GameState> dql = new QLearningDiscreteDense<>(
                    mdp,
                    NetworkUtil.buildDQNFactory(),
                    NetworkUtil.buildConfig(reinforcementLearningEpochs, reinforcementLearningSteps,
                            reinforcementLearningExpReplay, reinforcementLearningBatchSize)
            );

            // Start the training
            dql.train();
            mdp.close();

            // Save network
            try {
                dql.getNeuralNet().save(networkName);
            } catch (IOException e) {
                System.out.println(e.getMessage());
            }
        }
    }

    /**
     * Prepares the agent list -> creates the motivated and unmotivated agents in their respective ratio.
     *
     * @param param the properties needed to create the agents
     */
    private void createAgents(Properties param) {
        // calculate ratios of motivation
        int numberOfCuriousAgents = (int) Math.round(numberOfAgents * curiousRatio);
        int numberOfCreativeAgents = (int) Math.round(numberOfAgents * creativeRatio);
        int numberOfNonExploratoryAgents = nonExploratoryRatio > 0.0 ? numberOfAgents - numberOfCuriousAgents - numberOfCreativeAgents : 0;
        int numberOfNonMotivatedAgents = numberOfAgents - numberOfCuriousAgents - numberOfCreativeAgents - numberOfNonExploratoryAgents;

        // create the CURIOUS agents
        for (int i = 0; i < numberOfCuriousAgents; i++) {
            if (! direct) { // load reinforcement network, if indirect action is preferred
                String networkName = getReinforcementNetworkFileName(Motivation.CURIOUS);
                final MultiLayerNetwork multiLayerNetwork = NetworkUtil.loadNetwork(networkName);
                agents.add(new MotivatedAgent(i, env, multiLayerNetwork, 1, 0, 0, social, direct));

            } else
                agents.add(new MotivatedAgent(i, env, 1, 0, 0, social));
            agents.get(i).init(param);
        }

        // create the CREATIVE agents
        for (int i = numberOfCuriousAgents;
             i < numberOfCuriousAgents + numberOfCreativeAgents
                     && agents.size() < numberOfAgents;
             i++) {
            if (! direct) { // load reinforcement network, if indirect action is preferred
                String networkName = getReinforcementNetworkFileName(Motivation.CREATIVE);
                final MultiLayerNetwork multiLayerNetwork = NetworkUtil.loadNetwork(networkName);
                agents.add(new MotivatedAgent(i, env, multiLayerNetwork, 1, 0, 0, social, direct));
            } else
                agents.add(new MotivatedAgent(i, env, 0, 1, 0, social));
            agents.get(i).init(param);
        }

        // create the NON-EXPLORATORY agents
        for (int i = numberOfCuriousAgents + numberOfCreativeAgents;
             i < numberOfCuriousAgents + numberOfCreativeAgents + numberOfNonExploratoryAgents
                     && agents.size() < numberOfAgents;
             i++) {
            if (! direct) { // load reinforcement network, if indirect action is preferred
                String networkName = getReinforcementNetworkFileName(Motivation.NONEXPLORATORY);
                final MultiLayerNetwork multiLayerNetwork = NetworkUtil.loadNetwork(networkName);
                agents.add(new MotivatedAgent(i, env, multiLayerNetwork, 1, 0, 0, social, direct));
            } else
                agents.add(new MotivatedAgent(i, env, 0, 0, 1, social));
            agents.get(i).init(param);
        }

        // create the NOT MOTIVATED agents
        for (int i = numberOfCuriousAgents + numberOfCreativeAgents + numberOfNonExploratoryAgents;
             i < numberOfCuriousAgents + numberOfCreativeAgents + numberOfNonExploratoryAgents + numberOfNonMotivatedAgents; i++) {
            agents.add(new MotivatedAgent(i, env, 0, 0, 0, social));
            agents.get(i).init(param);
        }
    }

    /**
     * Returns the full path for the reinforcement network file depending on the motivation.
     *
     * @param motivation the motivation the network is trained for
     * @return String the full path of the network
     */
    public String getReinforcementNetworkFileName(Motivation motivation) {
        // get folder for reinforcement learning
        String saveDir = runDir;
        if (saveDir == null) {
            saveDir = System.getProperty("user.dir");
            String[] folders = saveDir.split("/");
            String lazylav = folders[folders.length-1];
            saveDir = saveDir.replace("/" + lazylav, "");
        }
        if (saveDir.charAt(saveDir.length() - 1) == '/')
            saveDir += reinforcementModelFolder;
        else
            saveDir += "/" + reinforcementModelFolder;
        new File(saveDir).mkdirs(); // create folders if they do not exist
        return saveDir + "network-" + motivation.name() + "-noProp" + numberOfFeatures
                + "-noAg" + numberOfAgents + "-soc" + (social ? "1" : "0")
                + "-rl_st" + reinforcementLearningSteps + "_ep" + reinforcementLearningEpochs
                + "_rep" + reinforcementLearningExpReplay + "_bt" + reinforcementLearningBatchSize + ".zip";
    }

    /**
     * Returns the maximal group size.
     *
     * @return int
     */
    public int getMaxGroupSize() { return this.maxGroupSize; }


}
