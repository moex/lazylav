package fr.inria.exmo.lazylavender.decisiontaking.intrinsicmotivation;

import fr.inria.exmo.lazylavender.decisiontaking.Agent;
import fr.inria.exmo.lazylavender.decisiontaking.CEGame;
import fr.inria.exmo.lazylavender.decisiontaking.Experiment;
import fr.inria.exmo.lazylavender.decisiontaking.treeclassifier.TreeClassifier;
import fr.inria.exmo.lazylavender.model.LLException;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

/**
 * A Group Game -> same as the normal game with the exception that there is no second agent, but a list of
 * other agents.
 */
public class GroupGame implements CEGame {

    final Agent agent1;
    final List<Agent> otherAgents;
    final BitSet indiv;
    Experiment exp;
    boolean success;

    HashMap<Agent, TreeClassifier.Change> changes;
    HashMap<Agent, TreeClassifier.Changer> adaptations;

    ArrayList<Agent> winners;
    ArrayList<Agent> losers;


    public GroupGame(Agent firstAgent, List<Agent> partnerAgents, BitSet instance, Experiment exp) {
        this.agent1 = firstAgent;
        this.otherAgents = partnerAgents;
        this.indiv = instance;
        this.exp = exp;
        this.winners = new ArrayList<>();
        this.losers = new ArrayList<>();
        this.changes = new HashMap<>();
        this.adaptations = new HashMap<>();
    }

    /**
     * Returns the list of winners.
     *
     * @return the list with winners of the game
     */
    public List<Agent> getWinners() { return this.winners; }

    /**
     * Adds one winner to the list of winners.
     */
    public void addWinners(List<Agent> winners) { this.winners.addAll(winners);}

    /**
     * Returns the list of losers.
     *
     * @return the list with losers of the game
     */
    public List<Agent> getLosers() { return this.losers; }

    /**
     * Adds one loser to the list of losers.
     */
    public void addLosers(List<Agent> losers) { this.losers.addAll(losers); }

    /**
     * Returns the list of other agents (interaction partners during this game).
     *
     * @return the list with other agents of the game
     */
    public List<Agent> getOtherAgents() { return this.otherAgents; }

    /**
     * Returns whether the game was successful.
     *
     * @return boolean whether the game was successful
     */
    @Override
    public boolean isSuccess() {
        return success;
    }

    /**
     * Sets whether the game was successful.
     *
     * @param success was successful
     */
    @Override
    public void setSuccess(boolean success) { this.success = success; }

    /**
     * Returns all adaptations as string.
     *
     * @return the adaptations as string
     */
    public String getAdaptation() {
        return this.adaptations.keySet().stream().map(agent -> {
            return agent.toString() + " -> " + this.adaptations.get(agent).toString() + "}";
        }).collect(Collectors.joining(", "));
    }

    /**
     * Sets an adaptation for an agent.
     *
     * @param agent the agent that adapted
     * @param adaptation the adaptation performed
     */
    public void setAdaptation(Agent agent, TreeClassifier.Changer adaptation) { this.adaptations.put(agent, adaptation); }

    /**
     * Sets a change for an agent.
     *
     * @param agent the agent that adapted
     * @param change the change performed
     */
    public void setChange(Agent agent, TreeClassifier.Change change) { this.changes.put(agent, change); }

    /**
     * Returns all changes.
     *
     * @return the changes as a HashMap
     */
    public HashMap<Agent, TreeClassifier.Change> getChanges() { return this.changes; }

    /**
     * Returns the object of the game.
     *
     * @return BitSet the instance
     */
    public BitSet getInstance() {
        return this.indiv;
    }

    /**
     * Returns the first agent of the game.
     *
     * @return agent
     */
    public Agent getFirstAgent() { return this.agent1; }

    /**
     * Returns the experiment in which the game is situated.
     *
     * @return experiment
     */
    public Experiment getExp() { return exp; }

    @Override
    public void save(PrintWriter file) throws LLException {
        file.printf( "%d\t%s\t%s\n", agent1.getId(),
                otherAgents.stream().map(agent -> agent.getId() + "").collect(Collectors.joining(",")),
                indiv );
    }
}
