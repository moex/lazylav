package fr.inria.exmo.lazylavender.decisiontaking.intrinsicmotivation;

import fr.inria.exmo.lazylavender.decisiontaking.*;
import fr.inria.exmo.lazylavender.logger.ActionLogger;
import fr.inria.exmo.lazylavender.model.LLException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

public class BaselineExperiment extends Experiment {

    boolean social = true;

    @Override
    protected void loadParams(Properties p) {
        super.loadParams(p);

        if (runDir != null)
            new File(runDir).mkdirs();

        if(p.containsKey("social"))
            social = Boolean.parseBoolean(p.getProperty("social"));

        // is the successBias used
        if (p.containsKey("successBias"))
            if (!Boolean.parseBoolean(p.getProperty("successBias"))) {
                discountEnv = 0;
                coeffEnv = 0;
            }
    }

    @Override
    public ActionLogger init(Properties param) throws LLException {
        loadParams(param);

        if (runDir != null)
            new File(runDir).mkdirs();

        env = new Environment(numberOfFeatures, numberOfClasses);
        env.init(param);

        for (int i = 0; i < numberOfAgents; i++) {
            agents.add(new Agent(i, env));
            agents.get(i).init(param);
        }
        loadTrainingSets();
        trainAgents();
        if (runDir != null)
            saveTrainingSets();
        if (loadRunDir == null)
            gameIt = new GameIterators.UniformRandomGameIterator(this);
        else {
            try {
                gameIt = Files.lines(Paths.get(loadRunDir, "games.tsv")).iterator();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        TransmissionBias tb1 = new TransmissionBiases.EnvBias(this,discountEnv);
        TransmissionBias tb2 = new TransmissionBiases.SocBias(this, discountSoc);
        TransmissionBias tb3 = new TransmissionBiases.InvSocBias(this, discountInvSoc, 1-(1./numberOfClasses));

        List<TransmissionBias> biases = new ArrayList<>();
        biases.add(tb1); biases.add(tb2); biases.add(tb3);

        List<Double> coeffs = new ArrayList<>();
        coeffs.add(coeffEnv); coeffs.add(coeffSoc); coeffs.add(coeffInvSoc);

        tBias = new TransmissionBiases.AggregatedBias(this, biases, coeffs);

        alogger = rManager.init(
                new DolaRecorders.Srate("ssrate"),
                new DolaRecorders.Accuracy(agents, env.getInputs(), env.getTargets(), "accuracy"),
                new DolaRecorders.Distance(agents, "distance"),
                new DolaRecorders.PrecisionRecall(agents, env.getInputs(), env.getTargets(),
                        "tprecision", "trecall", numberOfClasses),
                new DolaRecorders.GainLoss(agents, env, "ccorrectD", "gain", "loss"));
        return alogger;
    }

    @Override
    public void process() throws LLException {

        PrintWriter prGames = null;
        if (runDir != null) {
            try {
                prGames = new PrintWriter(new File(runDir, "games.tsv"));
            } catch (FileNotFoundException fnfex) {

            }
        }

        int remaining = numberOfIterations;
        performTasks(0, agents);

        int count = 0;
        while (gameIt.hasNext()) {
            Agent a1;
            ArrayList<Agent> an;

            BitSet instance;
            String g = gameIt.next();
            String[] spec = g.split("\t");
            String[] interactionPartners = spec[1].split(",");

            a1 = agents.get(Integer.parseInt(spec[0]));
            an = (ArrayList<Agent>) Arrays.stream(interactionPartners)
                    .map(partner -> {return agents.get(Integer.parseInt(partner));})
                    .collect(Collectors.toList());
            instance = env.parseInstance(spec[2]);

            // an egoistic game
            CEGame game;
            if (!social) {
                performTasks(++count, agents);
                game = new Game(a1, an.get(0), instance, this);

                // update successBias
                if(tBias instanceof TransmissionBiases.AggregatedBias) {
                    List<TransmissionBias> transmissionBiases = ((TransmissionBiases.AggregatedBias) tBias).getTransmissionBiases();
                    for(TransmissionBias tb: transmissionBiases) {
                        tb.update((Game) game);
                    }
                }

                // play the game
                a1.playGame(remaining--, game);

                // log and save the information
                Agent a2 = an.get(0);
                tBias.update((Game) game);
                rManager.update(game);
                alogger.logGame(a1.getId(), a2.getId(), ((Game) game).getInstance() + "");
                alogger.logResult(game.isSuccess() ? "SUCCESS" : "FAILURE", game.isSuccess(), ((Game) game).getAdaptation() + "");

                // a social game
            } else {
                game = new GroupGame(a1, an, instance, this);

                // update successBias
                if(tBias instanceof TransmissionBiases.AggregatedBias) {
                    List<TransmissionBias> transmissionBiases = ((TransmissionBiases.AggregatedBias) tBias).getTransmissionBiases();
                    for(TransmissionBias tb: transmissionBiases) {
                        tb.update(game);
                    }
                }

                // play the game
                a1.playGame(remaining--, game);

                // log and save the information
                rManager.update(game);
                int[] partners = new int[an.size()];
                for (int i = 0; i < an.size(); i++) {
                    partners[i] = an.get(i).getId();
                }
                alogger.logGame(a1.getId(), partners,((GroupGame) game).getInstance() + "");
                alogger.logResult(game.isSuccess() ? "SUCCESS" : "FAILURE", game.isSuccess(), ((GroupGame) game).getAdaptation() + "");
            }

            rManager.log(game);

            if (prGames != null)
                game.save(prGames);

        }
        if (prGames != null) prGames.close();
    }

}
