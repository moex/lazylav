/*
 * Copyright (C) INRIA, 2023-2024
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 */

package fr.inria.exmo.lazylavender.decisiontaking.intrinsicmotivation.reinforcement;

import fr.inria.exmo.lazylavender.decisiontaking.*;
import fr.inria.exmo.lazylavender.decisiontaking.Environment;
import fr.inria.exmo.lazylavender.decisiontaking.intrinsicmotivation.GroupGame;
import fr.inria.exmo.lazylavender.decisiontaking.intrinsicmotivation.IntrinsicMotivationExperiment;
import fr.inria.exmo.lazylavender.decisiontaking.intrinsicmotivation.MotivatedAgent;
import fr.inria.exmo.lazylavender.decisiontaking.intrinsicmotivation.Motivation;
import fr.inria.exmo.lazylavender.decisiontaking.treeclassifier.Decision;
import fr.inria.exmo.lazylavender.model.LLException;

import java.util.*;
import java.util.stream.Collectors;


/**
 * The game used to learn during the reinforcement learning.
 */
public class LearningGame extends IntrinsicMotivationExperiment {

    private List<Agent> learningAgents;
    private GameIterators.IntrinsicMotivationGameIterator learningIterator;
    protected int remaining;
    protected int count;
    protected Motivation motivation;

    protected int amtProperties;
    protected CEGame currentGame;
    protected Environment env;
    protected Properties param;

    /**
     * Creates the learning game, sets the parameters for the learning game.
     *
     * @param numberOfAgents the number of agents in the game – same as the game the learning is used for
     * @param motivation the motivation the agent learns for
     * @param env the environment
     * @param social whether the agent plays in groups or not
     * @param param the parameters of the experiment – same as the experiment which uses reinforcement learning
     * @param numberOfReinforcementLearningIterations the amount of iterations until one learning game is over
     * @param discountEnv the environment bias used in the game – same as the game the learning is used for
     * @param coeffEnv the environment bias used in the game – same as the game the learning is used for
     */
    public LearningGame(int numberOfAgents, Motivation motivation, Environment env,
                        boolean social, Properties param, int numberOfReinforcementLearningIterations,
                        double discountEnv, double coeffEnv, int maxGroupSize) {
        this.motivation = motivation;
        this.amtProperties = env.getNumberOfFeatures();
        this.currentGame = null;
        this.env = env;
        this.param = param;
        this.numberOfReinforcementLearningIterations = numberOfReinforcementLearningIterations;
        this.social = social;
        this.maxGroupSize = maxGroupSize;

        init(numberOfAgents, env, social, param, discountEnv, coeffEnv);
    }

    /**
     * Initialises the parameters used.
     *
     * @param numberOfAgents the number of agents in the game – same as the game the learning is used for
     * @param env the environment
     * @param social whether the agent plays in groups or not
     * @param param the parameters of the experiment – same as the experiment which uses reinforcement learning
     * @param discountEnv the environment bias used in the game – same as the game the learning is used for
     * @param coeffEnv the environment bias used in the game – same as the game the learning is used for
     */
    private void init(int numberOfAgents, Environment env, boolean social, Properties param, double discountEnv,
                      double coeffEnv) {
        this.learningAgents = new ArrayList<>();
        this.learningIterator = new GameIterators.IntrinsicMotivationGameIterator(this);
        this.remaining = numberOfReinforcementLearningIterations;
        this.count = 0;
        // create the learning agents
        for (int i = 0; i < numberOfAgents; i++) {
            switch (motivation) {
                case CURIOUS:
                    learningAgents.add(new MotivatedAgent(i, env,1, 0, 0, social));
                    break;
                case CREATIVE:
                    learningAgents.add(new MotivatedAgent(i, env, 0, 1, 0, social));
                    break;
                case NONEXPLORATORY:
                    learningAgents.add(new MotivatedAgent(i, env, 0, 0, 1, social));
                    break;
                default:
                    learningAgents.add(new MotivatedAgent(i, env, 0, 0, 0, social));

            }
            learningAgents.get(i).init(param);
        }

        // train agents
        loadTrainingSets(env);
        trainAgents(this.learningAgents); // train the agents
        for (Agent agent : agents)
            ((MotivatedAgent) agent).initRewards();

        // create the games
        performTasks(0, learningAgents, env);

        // add the transmission biases
        TransmissionBias tb1 = new TransmissionBiases.EnvBias(this, discountEnv, learningAgents);
        List<TransmissionBias> biases = new ArrayList<>();
        biases.add(tb1);
        List<Double> coeffs = new ArrayList<>();
        coeffs.add(coeffEnv);
        tBias = new TransmissionBiases.AggregatedBias(this, biases, coeffs);
    }

    /**
     * Initialises a new game.
     * @return the game state of the initial game -> the internal state of agent 1
     */
    public GameState initializeGame() {
        init(learningAgents.size(), this.env, social, this.param, discountEnv, coeffEnv);

        this.count = 0;
        this.currentGame = getNewGame(true, -1);
        MotivatedAgent a1 = (MotivatedAgent) ((GroupGame) this.currentGame).getFirstAgent();

        return new GameState(this.amtProperties, a1.getClassifier(),
                a1.getSeenObjects(), a1.getAllObjects());
    }

    /**
     * Returns whether the game is still ongoing.
     *
     * @return whether the game has ended (game iterator has no games anymore)
     */
    public boolean isOngoing() { return this.learningIterator.hasNext(); }

    /**
     * Performs one game with agent a1 and then a few random games (half of population) with the other agents.
     *
     * @param actionId the id from the reinforcement learning
     */
    public void perform(Integer actionId) {
        // set the game -> retrieve object
        MotivatedAgent a1 = (MotivatedAgent) ((GroupGame) this.currentGame).getFirstAgent();
        List<Agent> an = ((GroupGame) this.currentGame).getOtherAgents();
        BitSet chosenInstance = a1.getAllObjects().get(actionId);

        for (int i = 0; i < learningAgents.size() && isOngoing(); i++) {

            // if a group game
            if (social) {
                performTasks(++this.count, learningAgents, env);
                this.currentGame = new GroupGame(a1, an, chosenInstance, this);

                // update successBias
                if(tBias instanceof TransmissionBiases.AggregatedBias) {
                    List<TransmissionBias> transmissionBiases =  ((TransmissionBiases.AggregatedBias) tBias).getTransmissionBiases();
                    for(TransmissionBias tb: transmissionBiases) {
                        tb.update(this.currentGame);
                    }
                }

                // play the game
                try {
                    a1.playGame(this.remaining--, this.currentGame);
                } catch (LLException e) {
                    throw new RuntimeException(e);
                }

                // update the two agents
                a1.addSeenObject(chosenInstance);
                MotivatedAgent finalA = a1;
                an.forEach(agent -> {
                    MotivatedAgent ai = (MotivatedAgent) agent;
                    ai.addSeenObject(chosenInstance);
                    finalA.addInteractionPartner(ai, this.currentGame.isSuccess());
                    ai.addInteractionPartner(finalA, this.currentGame.isSuccess());
                });

            // if not a group game
            } else {
                performTasks(++this.count, learningAgents, env);
                this.currentGame = new Game(a1, an.get(0), chosenInstance, this);

                // update successBias
                if(tBias instanceof TransmissionBiases.AggregatedBias) {
                    List<TransmissionBias> transmissionBiases = ((TransmissionBiases.AggregatedBias) tBias).getTransmissionBiases();
                    for(TransmissionBias tb: transmissionBiases) {
                        tb.update((Game) this.currentGame);
                    }
                }

                // play the game
                try {
                    a1.playGame(remaining--, (Game) this.currentGame);
                } catch (Exception ignored) { }

                // update the two agents
                MotivatedAgent a2 = (MotivatedAgent) an.get(0);
                a1.addSeenObject(chosenInstance);
                a2.addSeenObject(chosenInstance);
                a1.addInteractionPartner(a2, this.currentGame.isSuccess());
                a2.addInteractionPartner(a1, this.currentGame.isSuccess());
            }

            // prepare random games
            this.currentGame = getNewGame(false, actionId);
        }

        // set next game
        this.currentGame = getNewGame(true, actionId);
    }

    /**
     * Returns the reward of the learning agent, considering the motivation of the agent.
     *
     * @return the reward
     */
    public double getReward() {
        MotivatedAgent a1 = (MotivatedAgent) this.learningAgents.get(0);
        switch (this.motivation) {
            case CREATIVE:
                return a1.creativityReward();
            case NONEXPLORATORY:
                return a1.nonExploratoryReward();
            default: // curious
                return a1.curiosityReward();
        }
    }

    /**
     * Returns the state of the learning agent.
     *
     * @return GameState -> a vector with the learning agent's ontology and objects
     */
    public GameState getState() {
        MotivatedAgent a1 = (MotivatedAgent) this.learningAgents.get(0);
        return new GameState(this.amtProperties, a1.getClassifier(),
                a1.getSeenObjects(), a1.getAllObjects());
    }

    /**
     * Trains the agents during initialisation, so that each agent has a starting ontology.
     *
     * @param agentList the agents which should be trained
     * @return the trained agents
     */
    protected List<Agent> trainAgents(List<Agent> agentList) {
        for (int i = 0; i < agentList.size(); i++) {
            Agent agent = agentList.get(i);
            List<BitSet> input = inputs.get(i);
            List<Decision> target = targets.get(i);
            try {
                agent.getClassifier().train(input, target);
                agentList.set(i, agent);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return agentList;
    }


    /**
     * Returns a new game with agent 1 = the first agent of the learning agent -> *the* learning agent
     *
     * @return a game with agent 1 as the first agent of the learning agents
     */
    private CEGame getNewGame(boolean agent1Fixed, int actionId) {
        MotivatedAgent a1;
        BitSet instance;

        if(agent1Fixed) {
            a1 = (MotivatedAgent) learningAgents.get(0); // first agent always has to be the same for learning
            instance = new BitSet(this.amtProperties);
        } else {
            Random rand = new Random();
            a1 = (MotivatedAgent) learningAgents.get(rand.nextInt(learningAgents.size() - 1) + 1); // get a first agent that is not agent 1, so at position 0
            if (actionId == -1 || rand.nextBoolean())
                instance = this.env.generateInstance();
            else instance = a1.getAllObjects().get(actionId);
        }

        String g = learningIterator.nextPartner(learningAgents, a1, this.maxGroupSize);
        String[] interactionPartners = g.split(",");
        List<Agent> an = (ArrayList<Agent>) Arrays.stream(interactionPartners)
                .map(partner -> {return learningAgents.get(Integer.parseInt(partner));})
                .collect(Collectors.toList());
        return new GroupGame(a1, an, instance, this);
    }

}
