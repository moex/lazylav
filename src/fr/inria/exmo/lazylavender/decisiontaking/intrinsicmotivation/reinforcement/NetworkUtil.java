package fr.inria.exmo.lazylavender.decisiontaking.intrinsicmotivation.reinforcement;

import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.rl4j.learning.configuration.QLearningConfiguration;
import org.deeplearning4j.rl4j.network.configuration.DQNDenseNetworkConfiguration;
import org.deeplearning4j.rl4j.network.dqn.DQNFactoryStdDense;
import org.deeplearning4j.util.ModelSerializer;
import org.nd4j.linalg.learning.config.RmsProp;

import java.io.File;
import java.io.IOException;

import static org.deeplearning4j.nn.multilayer.MultiLayerNetwork.*;

/**
 * Util class containing methods to build the neural network and its configuration.
 */
public final class NetworkUtil {

    /**
     * Build the configuration for QLearning.
     *
     * @param epochs the amount of maximum steps by epoch
     * @param steps the amount of maximum steps
     * @param expReplay the maximum size of experience replay
     * @param batchSize the batch size
     * @return the QLearning configuration
     */
    public static QLearningConfiguration buildConfig(int epochs, int steps, int expReplay, int batchSize) {
        return QLearningConfiguration.builder()
                .seed(1L)                   //Random seed (for reproducability)
                .maxEpochStep(epochs)       // Max step By epoch
                .maxStep(steps)             // Max step
                .expRepMaxSize(expReplay)   // Max size of experience replay
                .batchSize(batchSize)       // size of batches
                .targetDqnUpdateFreq(2000)  // target update (hard)
                .updateStart(0)             // num step noop warmup
                .rewardFactor(0.01)         // reward scaling
                .gamma(0.95)                // gamma
                .errorClamp(1.0)            // /td-error clipping
                .minEpsilon(0.1f)           // min epsilon
                .epsilonNbStep(1000)        // num step for eps greedy anneal
                .doubleDQN(true)            // double DQN
                .build();
    }

    /**
     * Builds the factory for a dense Deep-Q-Learning network.
     *
     * @return DQNFactoryStdDense
     */
    public static DQNFactoryStdDense buildDQNFactory() {
        final DQNDenseNetworkConfiguration build = DQNDenseNetworkConfiguration.builder()
                .l2(0.001)
                .updater(new RmsProp(0.000025))
                .numHiddenNodes(300)
                .numLayers(3)
                .build();

        return new DQNFactoryStdDense(build);
    }

    /**
     * Loads a network from a filepath.
     *
     * @param networkName the filepath to the network
     * @return MultiLayerNetwork (the network)
     */
    /*
    public static MultiLayerNetwork loadNetwork(final String networkName) {
        try {
            return MultiLayerNetwork.load(new File(networkName), true);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

     */

    public static MultiLayerNetwork loadNetwork(final String networkName) {
        try {
            return ModelSerializer.restoreMultiLayerNetwork(networkName);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }
}
