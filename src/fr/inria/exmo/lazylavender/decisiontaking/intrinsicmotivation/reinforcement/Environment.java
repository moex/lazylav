package fr.inria.exmo.lazylavender.decisiontaking.intrinsicmotivation.reinforcement;

import org.deeplearning4j.gym.StepReply;
import org.deeplearning4j.rl4j.mdp.MDP;
import org.deeplearning4j.rl4j.space.DiscreteSpace;
import org.deeplearning4j.rl4j.space.ObservationSpace;

/**
 * The environment for learning. Contains the game, game-state and action-space.
 */
public class Environment implements MDP<GameState, Integer, DiscreteSpace> {

    private final DiscreteSpace actionSpace;
    private final LearningGame game;

    public Environment(final LearningGame game) {
        this.game = game;
        // action space is as big as the amount of object types
        this.actionSpace = new DiscreteSpace((int) Math.round(Math.pow(2, game.amtProperties)));
    }

    @Override
    public ObservationSpace<GameState> getObservationSpace() {
        return new GameObservationSpace();
    }

    @Override
    public DiscreteSpace getActionSpace() {
        return actionSpace;
    }

    @Override
    public GameState reset() {
        return game.initializeGame();
    }

    @Override
    public void close() {}

    @Override
    public StepReply<GameState> step(final Integer actionIndex) {
        // perform action
        game.perform(actionIndex);

        // Get reward
        double reward = game.getReward();

        // Get current state
        final GameState observation = game.getState();

        return new StepReply<>(
                observation,
                reward,
                isDone(),
                "IntrinsicMotivationGame"
        );
    }

    @Override
    public boolean isDone() {
        return !game.isOngoing();
    }

    @Override
    public MDP<GameState, Integer, DiscreteSpace> newInstance() {
        game.initializeGame();
        return new Environment(game);
    }
}
