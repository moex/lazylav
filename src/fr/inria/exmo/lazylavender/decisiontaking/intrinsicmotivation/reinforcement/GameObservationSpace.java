package fr.inria.exmo.lazylavender.decisiontaking.intrinsicmotivation.reinforcement;

import fr.inria.exmo.lazylavender.decisiontaking.intrinsicmotivation.IntrinsicMotivationExperiment;
import org.deeplearning4j.rl4j.space.ObservationSpace;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.factory.Nd4j;

/**
 * The game-observation space, contains the meta-data of the game-state.
 */
public class GameObservationSpace implements ObservationSpace<GameState> {

    private static final double[] LOWS =
            GameObservationSpace.createValueArray(IntrinsicMotivationExperiment.LOW_VALUE); // lowest possible value in the game state
    private static final double[] HIGHS =
            GameObservationSpace.createValueArray(IntrinsicMotivationExperiment.HIGH_VALUE); // highest possible value in the game state

    @Override
    public String getName() {
        return "GameObservationSpace";
    }

    // returns game-state shape
    @Override
    public int[] getShape() {
        return new int[] {
                1, IntrinsicMotivationExperiment.NUMBER_OF_INPUTS
        };
    }

    @Override
    public INDArray getLow() {
        return Nd4j.create(LOWS);
    }

    @Override
    public INDArray getHigh() {
        return Nd4j.create(HIGHS);
    }

    private static double[] createValueArray(final double value) {
        final double[] values = new double[IntrinsicMotivationExperiment.NUMBER_OF_INPUTS];
        for (int i = 0; i < IntrinsicMotivationExperiment.NUMBER_OF_INPUTS; i++) {
            values[i] = value;
        }

        return values;
    }
}
