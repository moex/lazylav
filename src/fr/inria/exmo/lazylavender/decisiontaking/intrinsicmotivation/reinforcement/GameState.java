/*
 *
 * Copyright (C) INRIA, 2023-2024
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 */

package fr.inria.exmo.lazylavender.decisiontaking.intrinsicmotivation.reinforcement;

import fr.inria.exmo.lazylavender.decisiontaking.treeclassifier.TreeClassifier;
import fr.inria.exmo.lazylavender.decisiontaking.treeclassifier.TreeClassifierQuick;

import org.deeplearning4j.rl4j.space.Encodable;

import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.factory.Nd4j;

import java.util.List;
import java.util.Stack;
import java.util.Arrays;
import java.util.Collections;
import java.util.BitSet;

/**
 * The game-state, contains the agent's ontology and seen objects, encoded as a state for reinforcement learning.
 */
public class GameState implements Encodable {
    private final double[] inputs;

    /**
     * Returns the agent's state (ontology + seen objects) as a double vector. First the ontology is transformed,
     * next, for each object type, the amount of seen objects is added.
     *
     * @param amtProperties the amount of properties or features
     * @param classifier the classifier of the agent from which the state is created
     * @param seenObjects the objects that the agent already saw
     * @param allObjects all possible object types
     */
    public GameState(int amtProperties, TreeClassifier classifier,
                     List<BitSet> seenObjects, List<BitSet> allObjects) {
        Stack<Double> tmpInputs = new Stack<>();

        // go through all positive leafs (if 3 properties: c1, c11, c111, c121, c21, c211, c221)
        for (int i = 1; i < Math.pow(2, amtProperties); i++) {
            // initialise array -> one hot encoding
            int[] tmp = new int[amtProperties];
            Arrays.fill(tmp, 0);

            // initialise pointer and counters for search during tree traversal
            TreeClassifier.Node cursor = classifier.getRoot(), pointer = null, sp_cursor = null;
            Stack<TreeClassifier.Node> searcher = new Stack<>();
            TreeClassifierQuick.BitsetCondition bitsetCondition;
            int property = 0, sp, counter = 0;

            // if it is only the root, no children are added and the empty array (with zeros) is added
            if (! classifier.getRoot().isLeaf()) {
                searcher.push(classifier.getRoot().getChildren().get(1));
                searcher.push(classifier.getRoot().getChildren().get(0));
            }

            // go through all classes
            while (! searcher.isEmpty()) {
                // the class is the searched index
                if (counter == i) {
                    pointer = cursor;
                    break;
                }

                // get next class
                cursor = searcher.pop();

                // add the children for search
                if (cursor != null && cursor.getChildren().size() != 0) {
                    searcher.push(cursor.getChildren().get(1));
                    searcher.push(cursor.getChildren().get(0));
                } else if (cursor != null) {
                    // if there are no children -> get the specificity
                    sp = 1;
                    sp_cursor = cursor;
                    while (sp_cursor.getParent() != classifier.getRoot()){
                        sp++;
                        sp_cursor = sp_cursor.getParent();
                    }
                    // and check if the class is a leaf class
                    if (sp < amtProperties) {
                        // if not, add empty classes for every positive class that would have come
                        for (int _i = 0; _i < (int) Math.round(Math.pow(2, amtProperties - sp)) - 1; _i++) {
                            searcher.push(null);
                        }
                    }
                }

                // if it is an empty class -> so the tree was incomplete
                // or if it is a positive class, increase counter by one
                if(cursor == null)
                    counter++;
                else {
                    bitsetCondition = (TreeClassifierQuick.BitsetCondition) cursor.getCondition();
                    property = bitsetCondition.getConditionedAttributes().length() - 1;
                    if(bitsetCondition.getConditions().get(property) || bitsetCondition.isNot())
                        counter++;
                }
            }

            // set
            if (pointer != null) {
                tmp[property] = 1;
            }

            for (int j = 0; j < tmp.length; j++) {
                tmpInputs.push((double) tmp[j]);
            }
        }

        // add the seen objects
        for (BitSet currObject : allObjects) {
            tmpInputs.push((double) Collections.frequency(seenObjects, currObject));
        }

        this.inputs = new double[tmpInputs.size()];
        for (int i = 0; i < this.inputs.length; i++) {
            inputs[i] = tmpInputs.pop();
        }
    }

	// JE: This is not directly used.
	// It has been deprecated but cannot be removed without error
    @Override
	@SuppressWarnings("deprecation")
    public double[] toArray() {
        return inputs;
    }

    @Override
    public boolean isSkipped() {
        return false;
    }

    @Override
    public INDArray getData() {
        return Nd4j.create(inputs);
    }

    public INDArray getMatrix() {
        return Nd4j.create(new double[][] {
                inputs
        });
    }

    @Override
    public Encodable dup() {
        return null;
    }

    @Override
    public String toString() {
        return Arrays.toString(inputs);
    }
}
