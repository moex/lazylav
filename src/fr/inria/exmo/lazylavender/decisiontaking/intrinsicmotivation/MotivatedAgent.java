/*
 *
 * Copyright (C) INRIA, 2023-2024
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 */

package fr.inria.exmo.lazylavender.decisiontaking.intrinsicmotivation;

import fr.inria.exmo.lazylavender.agent.LLAgent;

import fr.inria.exmo.lazylavender.decisiontaking.Agent;
import fr.inria.exmo.lazylavender.decisiontaking.Game;
import fr.inria.exmo.lazylavender.decisiontaking.CEGame;
import fr.inria.exmo.lazylavender.decisiontaking.Environment;
import fr.inria.exmo.lazylavender.decisiontaking.intrinsicmotivation.reinforcement.GameState;
import fr.inria.exmo.lazylavender.decisiontaking.treeclassifier.NoClassException;
import fr.inria.exmo.lazylavender.decisiontaking.treeclassifier.TreeClassifier;
import fr.inria.exmo.lazylavender.decisiontaking.treeclassifier.TreeClassifierQuick;

import java.util.Random;
import java.util.Hashtable;
import java.util.Map;
import java.util.List;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.BitSet;
import java.util.Map.Entry;

import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.nd4j.linalg.api.ndarray.INDArray;

import static fr.inria.exmo.lazylavender.decisiontaking.intrinsicmotivation.Motivation.*;

/**
 * A motivated agent.
 */
public class MotivatedAgent extends fr.inria.exmo.lazylavender.decisiontaking.Agent {

    private boolean social = false; // default
    private boolean directModel = true; // default

    // default -> if all are 0, the agent has no motivation and acts as a normal agent
    private double curious = 0;
    private double creative = 0;
    private double nonExploratory = 0;

    protected final List<BitSet> seenObjects = new ArrayList<>();
    protected final Map<Integer, InteractionPartner> pastInteractionPartners = new HashMap<>();
	// JE: Temptative generalisation of what is above
	// seenObjects: hastable: BitSet -> List<Game>
	protected int nbUniqueGames = 0;
	protected final Hashtable<BitSet,List<CEGame>> playedObjects = new Hashtable<BitSet,List<CEGame>>();
	// pastInteractionPartner: hashtable: InteractionPartner -> List<Game>
	protected final Hashtable<Agent,List<CEGame>> playedPartners = new Hashtable<Agent,List<CEGame>>();

	// JE It does not seem good that each agent has a List of all objects.
	// If this is necessary this could be given to the environment
	// But it is likely not as it can be computed from the number of properties
	// i.e., env.getNumberOfFeatures()
    protected List<BitSet> allObjects;
	
    // for reinforcement learning
    protected TreeClassifier pastClassifier;
    protected MultiLayerNetwork network;
    protected Map<TreeClassifier.Node, List<BitSet>> leaves;

    protected double curiosityReward = 0.;
    protected double creativityReward = 0.;

    /*
    Constructors
     */

    /**
     * A simple unmotivated motivated agent.
     *
     * @param id the agent's id
     * @param env the environment of the agent
     */
    public MotivatedAgent(int id, Environment env) {
        super(id, env);

        BitSet initObjectType = new BitSet(env.getNumberOfFeatures());
        allObjects = Util.getAllPermutations(initObjectType, 0, env.getNumberOfFeatures());
        pastClassifier = null;
        network = null;
        leaves = new HashMap<>();
    }

    /**
     * A simple motivated agent.
     *
     * @param id the agent's id
     * @param env the environment of the agent
     * @param curious the degree of curiosity (only works for direct models -> untested)
     * @param creative the degree of creativity (only works for direct models -> untested)
     * @param nonExploratory the degree of nonExploratoryness (only works for direct models -> untested)
     * @throws Exception
     */
    public MotivatedAgent(int id, Environment env, double curious, double creative, double nonExploratory) {
        this(id, env);
        double sum = curious + creative + nonExploratory;
        this.curious = curious / sum;
        this.creative = creative / sum;
        this.nonExploratory = nonExploratory / sum;
    }

    /**
     * A simple social motivated agent, that uses a direct action policy to choose objects
     * (and chooses its partners -> always directly).
     *
     * @param id the agent's id
     * @param env the environment of the agent
     * @param curious the degree of curiosity (only works for direct models -> untested)
     * @param creative the degree of creativity (only works for direct models -> untested)
     * @param nonExploratory the degree of nonExploratoryness (only works for direct models -> untested)
     * @param social if the agent plays with groups and chooses the interaction partners
     * @throws Exception
     */
    public MotivatedAgent(int id, Environment env, double curious, double creative, double nonExploratory,
                          boolean social) {
        this(id, env, curious, creative, nonExploratory);
        this.social = social;
    }

    /**
     * A simple social motivated agent, that uses a direct or indirect action policy to choose objects
     * (and chooses its partners -> always directly).
     *
     * @param id the agent's id
     * @param env the environment of the agent
     * @param network the reinforcement learning model for indirect action choice
     * @param curious the degree of curiosity (only works for direct models -> untested)
     * @param creative the degree of creativity (only works for direct models -> untested)
     * @param nonExploratory the degree of nonExploratoryness (only works for direct models -> untested)
     * @param social if the agent plays with groups and chooses the interaction partners
     * @param directModel if the agent uses reinforcement learning to choose the object
     */
    public MotivatedAgent(int id, Environment env, MultiLayerNetwork network, double curious, double creative, double nonExploratory,
                          boolean social, boolean directModel) {
        this(id, env, curious, creative, nonExploratory, social);
        this.directModel = directModel;
        this.network = network;
    }

    /*
    choose interaction object
     */

    /**
     * Returns a chosen object.
     *
     * @param motivation the motivation of the agent, which decides what kind of motivation is used (only works for
     *                   direct model -> untested)
     * @param amtProperties the amount of features / properties objects have
     * @return the chosen object
     */
    public BitSet chooseInteractionObject(Motivation motivation, int amtProperties) throws NoClassException {
        // indirect (reinforcement) choice
        if (! directModel){
            final GameState state = new GameState(amtProperties, getClassifier(), getSeenObjects(), getAllObjects());
            final INDArray output = this.network.output(state.getMatrix(), false);
            double[] data = output.data().asDouble();
            int maxAt = 0;
            for (int i = 0; i < data.length; i++) {
                maxAt = data[i] > data[maxAt] ? i : maxAt;
            }
            return this.allObjects.get(maxAt);
        }
        // direct choice
        if (motivation == CURIOUS)
            return curiouslyDirectlyChooseObject(amtProperties);
        if (motivation == CREATIVE)
            return creativelyDirectlyChooseObject();
        return nonExploratorylyDirectlyChooseObject(amtProperties);
    }

    /**
     * Returns an object, that a curious agent chooses based on his knowledge (ontology + seen objects).
     * If the agent did not see an object so far, the returned object is randomly chosen.
     * The agent first searches for the most novel class in its ontology, and then picks an object with the
     * properties from the class and the properties, which did not appear in the path of the class. It
     * always takes the least known form (negated or not negated) of the property (ideal complexity).
     *
     * @return BitSet the object that the agent choose if it has motivation in form of curiosity
     * @throws NoClassException
     */
    private BitSet curiouslyDirectlyChooseObject(int amtProperties) throws NoClassException {

        if (seenObjects.isEmpty())
            throw new NoClassException("no objects seen so far");

        // get most novel leaf and the used properties for this leaf
        TreeClassifier.Node mostNovelLeaf = Util.noveltyBasedChosenNode(getClassifier(), seenObjects,
                amtProperties, true);
        List<Property> usedProperties = getPath(mostNovelLeaf);

        // get all unused properties
        List<Property> unusedPropertiesList = new ArrayList<>();
        HashMap<Integer, List<Property>> unusedProperties = new HashMap<>();
        int min = 1000000000;

        // find out if property is in usedProperties
        for (int property = 0; property < amtProperties; property++) {
            if (Util.propertyExists(property, unusedPropertiesList)) continue;
            if (Util.propertyExists(property, usedProperties)) continue;

            // save the least used property and exchange, if there is a less used property
            Property newProperty = Util.getPropertyType(seenObjects, property, true);
            unusedPropertiesList.add(newProperty);
        }

        BitSet chosenObject = new BitSet(amtProperties);
        // set values from most novel path
        for (Property used : usedProperties)
            if ( ! used.not )
                chosenObject.set(used.propertyName);

        // turn around value, because we want the more used value for the other unused properties -> ideal
        // not to overwhelm the agent
        for (Property unused : unusedPropertiesList) {
                if (!unused.not)
                    chosenObject.set(unused.propertyName);
        }
        return chosenObject;
    }

    /**
     * Returns an object, that a creative agent chooses based on his knowledge (ontology + seen
     * objects). If the agent did not see an object so far, the returned object is randomly chosen.
     * The choice is based on a similarity heuristic: the agent tests all object types that can be
     * deduced from its ontology and tests whether there is a path more similar to the object than
     * the current classification path of the object.
     *
     * @return BitSet the object that the agent choose if it has motivation in form of creativity
     */
    private BitSet creativelyDirectlyChooseObject() throws NoClassException {
        HashMap<TreeClassifier.Node, List<TreeClassifier.Node>> paths = Util.getAllLeafPaths(getClassifier());

        // go through all permutations and see where they are classified and if there is a more specific path
        for (TreeClassifierQuick.Node testedLeafNode : paths.keySet()) {

           // go through all permutations of the used properties
            List<Property> objectTypePath = getPath(testedLeafNode);
           for (BitSet permutation : this.allObjects) {
               List<Property> actualLeafPath = getPath(getClassifier().getNode(permutation));

               int objectTypePathCounter = 0, actualLeafPathCounter = 0;
               for (int i = 0; i < Math.max(objectTypePath.size(), actualLeafPath.size()); i++) {
                   try {
                       if (! objectTypePath.get(i).isNot() == permutation.get(objectTypePath.get(i).getPropertyName())) {
                           objectTypePathCounter++;
                       }
                   } catch (IndexOutOfBoundsException e) {
                       // do nothing
                   }
                   try {
                       if (! actualLeafPath.get(i).isNot() == permutation.get(actualLeafPath.get(i).getPropertyName())) {
                           actualLeafPathCounter++;
                       }
                   } catch (IndexOutOfBoundsException e) {
                       // do nothing
                   }
               }

               // check whether there is a path which shares more properties with the current
               // permuted object than the classification of this object
               // if so -> agent assumes that it will be surprised during interaction (be wrong),
               // because the object according to a similarity heuristic is likely to be wrongly classified
               if (objectTypePathCounter > actualLeafPathCounter){
                   try {
                       BitSet chosenObject = new BitSet(seenObjects.get(0).size());
                       chosenObject.or(permutation);
                       return chosenObject;
                   } catch (IndexOutOfBoundsException e) {
                       throw new NoClassException("the agent has not seen an object so far");
                   }
               }
           }
       }

        throw new NoClassException("no probably surprising object could be found");
    }

    /**
     * Returns an object, that a non-exploratory agent chooses based on his knowledge (ontology + seen
     * objects). If the agent did not see an object so far, the returned object is randomly chosen.
     * The choice is similar to the choice of a curious agent, but the non-exploratory agent chooses
     * the least novel object and always the most known properties for those, which are not in the
     * class path.
     *
     * @return BitSet the object that the agent choose if it has motivation in form of nonExploration
     * @throws NoClassException
     */
    private BitSet nonExploratorylyDirectlyChooseObject(int amtProperties) throws NoClassException {

        if (seenObjects.isEmpty())
            throw new NoClassException("no objects seen so far");

        TreeClassifier.Node leastNovelLeaf = Util.noveltyBasedChosenNode(getClassifier(), seenObjects,
                amtProperties, false);
        List<Property> usedProperties = getPath(leastNovelLeaf);

        // get all unused properties
        List<Property> unusedProperties = new ArrayList<>();

        // find out if property is in usedProperties
        for (int property = 0; property < amtProperties; property++) {
            if (Util.propertyExists(property, unusedProperties)) continue;

            // save the least used property and exchange, if there is a less used property
            Property newProperty = Util.getPropertyType(seenObjects, property, false);
            unusedProperties.add(newProperty);
        }

        BitSet chosenObject = new BitSet(amtProperties);
        // set values from most novel path
        for (Property used : usedProperties) {
            if ( ! used.not )
                chosenObject.set(used.propertyName);
        }
        // set the most unknown properties
        for (Property unused : unusedProperties) {
            if (! unused.not)
                chosenObject.set(unused.propertyName);
        }

        return chosenObject;
    }

    /*
    indirect (reinforcement learning) rewards
     */

    /**
     * Initialises the rewards.
     */
    public void initRewards() {
        // curiosity
        this.curiosityReward = Util.getAllLeafPaths(getClassifier()).keySet().size();
    }

    /**
     * Returns the curiosity reward -> amount of leaves.
     *
     * @return double the amount of leaves
     */
    public double curiosityReward () { return this.curiosityReward; }

    /**
     * Increases the curiosity reward -> amount of leaves.
     */
    public void increaseCuriosityReward() { this.curiosityReward++; }

    /**
     * Returns the creativity reward -> the amount of reclassified objects.
     *
     * @return double the reward
     */
    public double creativityReward () { return this.creativityReward;  }

    /**
     * Update the creativity reward -> amount of reclassified objects.
     */
    public void updateCreativityReward(TreeClassifier.Node preAdaptationNode, TreeClassifier.Node postAdaptationNode1,
                                       TreeClassifier.Node postAdaptationNode2, boolean firstNodeIsNew, BitSet instance) {
        leaves.putIfAbsent(preAdaptationNode, new ArrayList<>());
        List<BitSet> preClassifiedObjects = leaves.get(preAdaptationNode);
        this.creativityReward = preClassifiedObjects.size();
        leaves.put(postAdaptationNode1, new ArrayList<>());
        leaves.put(postAdaptationNode2, new ArrayList<>());
        List<BitSet> objects1, objects2;
        if (firstNodeIsNew) {
            objects1 = leaves.get(postAdaptationNode1);
            objects1.add(instance);
            leaves.put(postAdaptationNode1, objects1);
            objects2 = leaves.get(postAdaptationNode2);
            objects2.addAll(preClassifiedObjects);
            leaves.put(postAdaptationNode2, objects2);
        } else {
            objects2 = leaves.get(postAdaptationNode2);
            objects2.add(instance);
            leaves.put(postAdaptationNode2, objects2);
            objects1 = leaves.get(postAdaptationNode1);
            objects1.addAll(preClassifiedObjects);
            leaves.put(postAdaptationNode1, objects1);
        }
        leaves.remove(preAdaptationNode);
    }

    /**
     * Resets the creativity reward to 0.
     */
    public void resetCreativityReward() {
        this.creativityReward = 0;
    }

    /**
     * Returns the non-exploration reward -> punishment -> the amount of changed objects in negative.
     *
     * @return double the reward
     */
    public double nonExploratoryReward () {
        return (allObjects.size() / 2) - this.creativityReward;
    }

    /**
     * Returns the reward of the current intrinsic motivation (does not work for different ratios).
     *
     * @return double the reward
     */
    public double getLastReward () {
        if (! isMotivated())
            return 0.;

        if (this.curious > 0)
            return this.curiosityReward();
        if (this.creative > 0)
            return this.creativityReward();
        if (this.nonExploratory > 0)
            return this.nonExploratoryReward();

        return 0.;
    }

    /*
    choose interaction partner
     */


    /**
     * Returns a chosen interaction partner.
     *
     * @param motivation the motivation of the agent, which decides what kind of motivation is used (only works for
     *                   direct model -> untested)
     * @param groupSize the size of the group to be created
     * @return List<MotivatedAgent> returns a list with interaction partners (other agents)
     */
    public List<MotivatedAgent> chooseInteractionPartners(Motivation motivation, int groupSize, int amtAgents) {
        if (motivation == CURIOUS)
            return curiouslyDirectlyChoosePartners(groupSize, amtAgents);
        if (motivation == CREATIVE)
            return creativelyDirectlyChoosePartners(groupSize, amtAgents);
        if (motivation == NONEXPLORATORY)
            return nonExploratorylyDirectlyChoosePartners(groupSize);
        return new ArrayList<>();
    }

    /**
     * Returns a group of agents that a curious agent would choose. A curious agent always chooses the agents it
     * interacted least with so far. If it did not interact with enough agents to create a group, it will create a group
     * with as many agents, so that all unknown agents can still be added to the group.
     *
     * @param groupSize the amount of interaction partners to choose
     * @param amtAgents the amount of agents there are
     * @return List<MotivatedAgent> a list with chosen interaction partners
     */
    private List<MotivatedAgent> curiouslyDirectlyChoosePartners(int groupSize, int amtAgents) {
        List<MotivatedAgent> partners = new ArrayList<>();

        // if not all agents have been seen, get the unseen partners and fill with the least known past interaction
        // partners -> unseen partners are added in the game iterator
        if (this.pastInteractionPartners.size() < amtAgents) {
            int[] interactionPartners = getInteractionOrderedPartners(this.pastInteractionPartners.size(), false);
            for (int partnerIndex = 0; partnerIndex < groupSize - (amtAgents - this.pastInteractionPartners.size() - 1)
                    && partnerIndex < interactionPartners.length; partnerIndex++)
                partners.add((MotivatedAgent) this.pastInteractionPartners.get(interactionPartners[partnerIndex]).getAgent());
            return partners;
        }

        // otherwise get sorted list of agents (least interacted to most interacted)
        int[] interactionPartners = getInteractionOrderedPartners(groupSize, false);
        for (int partner : interactionPartners)
            partners.add((MotivatedAgent) this.pastInteractionPartners.get(partner).getAgent());

        return partners;
    }

    /**
     * Returns a group of agents that a creative agent would choose. A creative agent always chooses the agents that
     * are maximally dissimilar (different core belief & most disagreements). If it did not interact with enough agents
     * to create a group, it will create a group with as many agents, so that all unknown agents can still be added to
     * the group.
     *
     * @param groupSize the amount of interaction partners to choose
     * @param amtAgents the amount of agents there are
     * @return List<MotivatedAgent> a list with chosen interaction partners
     */
    private List<MotivatedAgent> creativelyDirectlyChoosePartners(int groupSize, int amtAgents) {
        List<MotivatedAgent> partners = new ArrayList<>();

        // if not all agents have been seen, get the unseen partners and fill the rest with the most dissimilar
        // past interaction partners -> unseen partners are added in the game iterator
        if (this.pastInteractionPartners.size() < amtAgents){
            int[] interactionPartners = getDifferenceOrderedPartners(this.pastInteractionPartners.size());
            for (int partnerIndex = 0; partnerIndex < groupSize - (amtAgents - this.pastInteractionPartners.size() - 1)
                    && partnerIndex < interactionPartners.length; partnerIndex++)
                partners.add((MotivatedAgent) this.pastInteractionPartners.get(interactionPartners[partnerIndex]).getAgent());
            return partners;
        }

        // otherwise get sorted list of agents (most different to least different)
        int[] interactionPartners = getDifferenceOrderedPartners(groupSize);
        for (int partner : interactionPartners)
            partners.add((MotivatedAgent) this.pastInteractionPartners.get(partner).getAgent());

        return partners;
    }

    /**
     * Returns a group of agents that a non-exploratory agent would choose. A non-exploratory agent always chooses the
     * agents it interacted most with so far. If it did not interact with enough agents to create a group, all
     * known agents are picked.
     *
     * @param groupSize the amount of interaction partners to choose
     * @return List<MotivatedAgent> a list with chosen interaction partners
     */
    private List<MotivatedAgent> nonExploratorylyDirectlyChoosePartners(int groupSize) {
        List<MotivatedAgent> partners = new ArrayList<>();

        // add as many agents as possible
        if (this.pastInteractionPartners.size() < groupSize) {
            int[] interactionPartners = getInteractionOrderedPartners(this.pastInteractionPartners.size(), true);
            for (int partnerIndex = 0; partnerIndex < Math.round(interactionPartners.length / 2.0); partnerIndex++)
                partners.add((MotivatedAgent) this.pastInteractionPartners.get(interactionPartners[partnerIndex]).getAgent());
            return partners;
        }

        // otherwise get sorted list of agents (most interacted to least interacted)
        int[] interactionPartners = getInteractionOrderedPartners(groupSize, true);
        for (int partner : interactionPartners)
            partners.add((MotivatedAgent) this.pastInteractionPartners.get(partner).getAgent());

        return partners;
    }

    /**
     * Adds an object to the seen objects.
     *
     * @param object the object to be added
     */
    public void addSeenObject(BitSet object) {
        seenObjects.add(object);
        try {
            TreeClassifier.Node node = getClassifier().getNode(object);
            leaves.putIfAbsent(node, new ArrayList<>());
            List<BitSet> objects = leaves.get(getClassifier().getNode(object));
            objects.add(object);
            leaves.put(node, objects);
        } catch (NoClassException ignored) {
        }
    }
	// JE: generalisation of both addSeenObject and addInteractionPartner
	public void notifyGame( Game game ) {
		// Redo the old behaviour
		addSeenObject( game.getInstance() );
		if ( this == game.getFirstAgent() ) {
			addInteractionPartner( (MotivatedAgent)game.getSecondAgent(), game.isSuccess() );
		} else {
			addInteractionPartner( (MotivatedAgent)game.getFirstAgent(), game.isSuccess() );
		}
		// Use the alternative behaviour
		List<CEGame> games = playedObjects.get( game.getInstance() );
		if ( games == null ) {
			games = new ArrayList<CEGame>();
			playedObjects.put( game.getInstance(), games );
		}
		games.add( game );
		// playedPartners
		Agent other = (Agent)game.getFirstAgent();
		if ( this == game.getFirstAgent() ) other = (Agent)game.getSecondAgent();
		addToPlayedAgents( other, game );
	}

	public void notifyGame( GroupGame game ) {
		// Redo the old behaviour
		addSeenObject( game.getInstance() );
		if ( this == game.getFirstAgent() ) {
			for ( Agent other : game.getOtherAgents() ) {
				addInteractionPartner( (MotivatedAgent)other, game.isSuccess() );
			}
		} else {
			addInteractionPartner( (MotivatedAgent)game.getFirstAgent(), game.isSuccess() );
			for ( Agent other : game.getOtherAgents() ) {
				if ( this != other )
					addInteractionPartner( (MotivatedAgent)other, game.isSuccess() );
			}
		}
		// Use the alternative behaviour
		List<CEGame> games = playedObjects.get( game.getInstance() );
		if ( games == null ) {
			games = new ArrayList<CEGame>();
			playedObjects.put( game.getInstance(), games );
		}
		games.add( game );
		// playedPartners
		addToPlayedAgents( game.getFirstAgent(), game );
		for ( Agent other : game.getOtherAgents() ) {
			addToPlayedAgents( other, game );
		}
	}

	protected void addToPlayedAgents( Agent agent, CEGame game ) {
		if ( this == agent ) return;
		List<CEGame> games = playedPartners.get( agent );
		boolean newGame = true;
		if ( games == null ) {
			games = new ArrayList<CEGame>();
			playedPartners.put( agent, games );
		} else {
			BitSet obj = game.getInstance();
			for ( CEGame g : games ) {
				if ( obj.equals( g.getInstance() ) ) {
					newGame = false;
					break;
				}
			}
		}
		games.add( game );
		if ( newGame ) nbUniqueGames++;
	}

	// JE These are embryonic temptative measures
	// It may be enough that the agents return the sizes and the experiment performs the division
	// It may also be worth having counters that, at insertion time, checks if objects have already been played with this agents and does not count it any more...
	// This would have the exact number of objects*agents
	
	// 2^^env.getNumberOfFeatures()
	public double domainCoverage() {
		return playedObjects.size();
		// env is not visible!
		//Math.pow(2,env.getNumberOfFeatures());
	}
	
	public double populationCoverage() {
		return playedPartners.size();
	}

	// This does not give a good idea, because the same partner may have played different games with the same object...
	public double fullCoverage() {
		//int size = 0;
		// Done with partners because the same game/object may concern several partners...
		//playedPartners.forEach( (k,v) -> { size += v.size(); } );
		//for ( Entry<Agent,List<CEGame>> lg : playedPartners.entrySet() ) {
		//	size += lg.getValue().size();
		//}
		//return size;
		return nbUniqueGames;
	}
	
    /**
     * Adds an interaction partner and counts the amount of successful interactions.
     *
     * @param partner the partner of the interaction
     * @param justSuccessfullyInteracted whether the game was successful
     */
    public void addInteractionPartner(MotivatedAgent partner, boolean justSuccessfullyInteracted) {
        InteractionPartner currPartner = partner.pastInteractionPartners.get(partner.getId());
        if (currPartner == null)
            currPartner = new InteractionPartner(partner.getCoreBelief(), justSuccessfullyInteracted ? 0 : 1, 1, partner);
        else {
            currPartner.addInteraction();
            if (! justSuccessfullyInteracted) currPartner.addDisagreement();
        }

        pastInteractionPartners.put(partner.getId(), currPartner);
    }


    /**
     * Returns wether the agent already interacted with the other agent in the past.
     *
     * @param a2 the agent to be checked
     * @return boolean wether the agent already interacted with a2 in the past
     */
    public boolean interactedWith(Agent a2) {
        for (InteractionPartner partner : this.pastInteractionPartners.values()) {
            if (a2.getId() == partner.getAgent().getId())
                return true;
        }
        return false;
    }

    /*
    getter and setter
     */

    /**
     * Returns t
     *
     * @return the path of a leaf to the root
     */
    private List<Property> getPath(TreeClassifierQuick.Node leaf) {
        return Util.getPath(getClassifier(), leaf);
    }

    /**
     * Returns a list with interaction partners, ordered by amount of interactions.
     *
     * @param groupSize the amount of partners searched
     * @param desc whether the interaction partner should be ascending or descending
     * @return array with the ids of the partners
     */
    private int[] getInteractionOrderedPartners(int groupSize, boolean desc) {
        int[] interactionPartners = new int[groupSize];
        int counter = 0;
        // go through all past interaction partners
        for (int amtInteractions : this.pastInteractionPartners.keySet()){
            // first add all first interaction partners to the result
            if (counter < interactionPartners.length) {
                interactionPartners[counter] = amtInteractions;
                counter++;
                continue;
            }

            // then compare the next partners with the partners in the list
            int minIndex = 0;
            for (int i = 0; i < interactionPartners.length; i++) {
                if (desc && interactionPartners[i] < interactionPartners[minIndex]) minIndex = i;
                if (!desc && interactionPartners[i] > interactionPartners[minIndex]) minIndex = i;
            }
            // and exchange if it is smaller (or larger)
            if (desc && interactionPartners[minIndex] < amtInteractions)
                interactionPartners[minIndex] = amtInteractions;
            if (!desc && interactionPartners[minIndex] > amtInteractions)
                interactionPartners[minIndex] = amtInteractions;
        }

        return interactionPartners;
    }

    /**
     * Returns a list with interaction partners, ordered by dissimilarity.
     *
     * @param groupSize the amount of partners searched
     * @return array with the ids of the partners
     */
    private int[] getDifferenceOrderedPartners(int groupSize) {
        int[] interactionPartners = new int[groupSize];
        int counter = 0;
        // go through all interaction partners
        for (int currPartnerIndex : this.pastInteractionPartners.keySet()){
            if (counter < interactionPartners.length) {
                interactionPartners[counter] = currPartnerIndex;
                counter++;
                continue;
            }

            int minIndex = 0;
            InteractionPartner ref, curr, min;
            for (int i = 0; i < interactionPartners.length; i++) {
                ref = this.pastInteractionPartners.get(interactionPartners[i]);
                min = this.pastInteractionPartners.get(interactionPartners[minIndex]);
                if (ref.getDifferenceScore(this.getCoreBelief()) < min.getDifferenceScore(this.getCoreBelief()) )
                    minIndex = i;
            }
            min = this.pastInteractionPartners.get(interactionPartners[minIndex]);
            curr = this.pastInteractionPartners.get(currPartnerIndex);
            if (curr.getDifferenceScore(this.getCoreBelief()) > min.getDifferenceScore(this.getCoreBelief()))
                interactionPartners[minIndex] = currPartnerIndex;
        }
        return interactionPartners;
    }


    /**
     * Returns the current motivation of the agent (randomly, if the agent has different ratios of different
     * motivations).
     *
     * @return the agent's current motivation
     */
    public Motivation getCurrentMotivation() {
        if (isMotivated()) {
            double rand = new Random().nextDouble();
            if (rand <= curious)
                // agent acts curiously
                return CURIOUS;

            if (rand <= curious + creative)
                // agent acts creatively
                return CREATIVE;

            // agent acts non-exploratory
            return NONEXPLORATORY;
        }
        return NONE;
    }

    /**
     * Returns all seen objects.
     *
     * @return list with all seen objects
     */
    public List<BitSet> getSeenObjects() {
        return seenObjects;
    }

    /**
     * Returns all possible object types in the environment.
     *
     * @return list with all object types.
     */
    public List<BitSet> getAllObjects() {
        return allObjects;
    }

    /**
     * Returns whether the agent has any motivation.
     *
     * @return boolean whether the agent is motivated
     */
    public boolean isMotivated() { return curious > 0 || creative > 0 || nonExploratory > 0; }

    /**
     * Returns if the agent is social (plays in groups and chooses interaction partners).
     *
     * @return boolean if the agent is social
     */
    public boolean isSocial() { return social; }

    /**
     * Returns the agent's core belief (first class distinction).
     *
     * @return bitset with the property of the first class distinction
     */
    public BitSet getCoreBelief() {
        if (getClassifier().getRoot().getChildren().isEmpty()) return null;
        return ((TreeClassifierQuick.BitsetCondition) getClassifier().getRoot().getChildren().get(0).getCondition()).getConditionedAttributes();
    }

}


