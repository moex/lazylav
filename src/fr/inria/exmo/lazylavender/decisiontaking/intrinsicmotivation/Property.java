/*
 * Copyright (C) INRIA, 2023-2024
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 */

package fr.inria.exmo.lazylavender.decisiontaking.intrinsicmotivation;

import java.util.Objects;

public class Property {
    final public int propertyName;
    final public boolean not;
    private int occurance = -1;

    public Property(int propertyName, boolean not) {
        this.propertyName = propertyName;
        this.not = not;
    }

    public Property(int propertyName, boolean not, int occurance) {
        this(propertyName, not);
        this.occurance = occurance;
    }

    public int getPropertyName() {
        return propertyName;
    }

    public boolean isNot() {
        return not;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Property))
            return false;
        Property prop2 = (Property) obj;
        return propertyName == prop2.propertyName && not == prop2.not;
    }

	@Override
	public int hashCode() {
		return Objects.hash( propertyName, not );
	}
}
