package fr.inria.exmo.lazylavender.decisiontaking.intrinsicmotivation;

public enum Motivation {
    CURIOUS,
    CREATIVE,
    NONEXPLORATORY,
    NONE
}