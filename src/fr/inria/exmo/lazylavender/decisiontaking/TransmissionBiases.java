package fr.inria.exmo.lazylavender.decisiontaking;

import fr.inria.exmo.lazylavender.decisiontaking.intrinsicmotivation.GroupGame;
import fr.inria.exmo.lazylavender.decisiontaking.treeclassifier.NoClassException;
import fr.inria.exmo.lazylavender.decisiontaking.treeclassifier.TreeClassifier.Condition;

import java.util.*;

public class TransmissionBiases {


    public static abstract class DiscountedBias implements TransmissionBias {

        protected Experiment exp;
        protected Map<Integer, Double> idToValue = new HashMap<>();
        protected Map<Integer, Double> idToNormValue = new HashMap<>();
        protected double discount = 0, initVal = 0;

        public DiscountedBias(Experiment exp) {
            this.exp = exp;
            initVal = 1./exp.getNumberOfClasses(); // default
            for(Agent agent: exp.getAgents()) {
                idToValue.put(agent.getId(), initVal);
                idToNormValue.put(agent.getId(), 1.);
            }
        }

        public DiscountedBias(Experiment exp, double discount) {
            this.exp = exp;
            this.discount = discount;
            initVal = 1./exp.getNumberOfClasses(); // default
            for(Agent agent: exp.getAgents()) {
                idToValue.put(agent.getId(), initVal);
                idToNormValue.put(agent.getId(), 1.);
            }
        }

        public DiscountedBias(Experiment exp, double discount, List<Agent> agents) {
            this.exp = exp;
            this.discount = discount;
            initVal = 1./exp.getNumberOfClasses(); // default
            for(Agent agent: agents) {
                idToValue.put(agent.getId(), initVal);
                idToNormValue.put(agent.getId(), 1.);
            }
        }

        public DiscountedBias(Experiment exp, double discount, double initVal) {
            this.exp = exp;
            this.discount = discount;
            this.initVal = initVal;
            for(Agent agent: exp.getAgents()) {
                idToValue.put(agent.getId(), initVal);
                idToNormValue.put(agent.getId(), 1.);
            }
        }



        public abstract void update(CEGame game);

        protected void updateValue(int agentID, double newValue) {
            idToValue.put(agentID, idToValue.get(agentID)*discount + newValue);
            idToNormValue.put(agentID, idToNormValue.get(agentID)*discount + 1);
        }

        @Override
        public double getValue(Agent agent, Game game) {
            return idToValue.get(agent.getId())/idToNormValue.get(agent.getId());
        }
    }

    /**
     * Environment based bias (success in tasks)
     */
    public static class EnvBias extends DiscountedBias {


        public EnvBias(Experiment exp) {
            super(exp);
        }

        public EnvBias(Experiment exp, double discount) {
            super(exp, discount);
        }

        public EnvBias(Experiment exp, double discount, List<Agent> agents) {
            super(exp, discount, agents);
        }

        public EnvBias(Experiment exp, double discount, double initVal) {
            super(exp, discount, initVal);
        }

        @Override
        public void update(CEGame g) {
            for(Agent agent:exp.agents) {
                int id = agent.getId();
                updateValue(id, agent.getRelativePayoff(g.getInstance()));
            }
        }
    }

    /**
     * Society based bias (success in interactions)
     */

    public static class SocBias extends DiscountedBias {


        public SocBias(Experiment exp) {
            super(exp);
        }

        public SocBias(Experiment exp, double discount) {
            super(exp, discount);
        }

        public SocBias(Experiment exp, double discount, double initVal) {
            super(exp, discount, initVal);
        }

        @Override
        public void update(CEGame g) {
            if (g instanceof GroupGame) {
                return; // not implemented since not needed yet
            }

            Game game = (Game) g;
            Agent ag1 = (Agent) game.getFirstAgent(), ag2 = (Agent) game.getSecondAgent();
            updateValue(ag1.getId(), game.isSuccess()?1:0);
            updateValue(ag2.getId(), game.isSuccess()?1:0);
        }

        public void update(GroupGame game) {
            Agent ag1 = game.getFirstAgent();
            List<Agent> agn = game.getOtherAgents();
            for (Agent ag2 : agn) {
                updateValue(ag1.getId(), game.isSuccess()?1:0);
                updateValue(ag2.getId(), game.isSuccess()?1:0);
            }
        }
    }

    /**
     * Inverse of Society Bias
     */

    public static class InvSocBias extends DiscountedBias {


        public InvSocBias(Experiment exp) {
            super(exp);
        }

        public InvSocBias(Experiment exp, double discount) {
            super(exp, discount);
        }

        public InvSocBias(Experiment exp, double discount, double initVal) {
            super(exp, discount, initVal);
        }

        @Override
        public void update(CEGame g) {
            if (g instanceof GroupGame) {
                return; // not implemented since not needed yet
            }

            Game game = (Game) g;
            Agent ag1 = (Agent) game.getFirstAgent(), ag2 = (Agent) game.getSecondAgent();
            updateValue(ag1.getId(), game.isSuccess()?0:1);
            updateValue(ag2.getId(), game.isSuccess()?0:1);
        }

//        static int adpt = 0;
//        @Override
//        public double getValue(Agent agent, Game game) {
//            double value = 0;
//            try {
//                double sum = 0;
//                int c = 0;
//                for(BitSet instance: game.exp.getEnv().getInputs()) {
////                    BitSet instance = game.getInstance();
//                    double ss = game.exp.getNumberOfAgentsWithDecisionForInstance(instance,
//                            agent.getClassifier().getClass(instance));
////                    if(ss!= exp.getNumberOfAgents())
//                    {
//                        sum += ss;
//                        c += 1;
//                    }
//                }
//                sum /= c;
//                value = 1.-(sum/(double)game.exp.getNumberOfAgents());
//            } catch (NoClassException e) {
//                e.printStackTrace();
//            }
//            double value2 = super.getValue(agent, game);
//
////            System.out.println(value + " vs " + value2 + " difference: " + Math.abs(value2 - value));
//
//            return value;
//        }
    }

//    New biases for CulturalValuesExperiment (Authority and Novelty)
    public static class AuthBias extends DiscountedBias {
        public AuthBias(Experiment exp) {
            super(exp);
        }

        public AuthBias(Experiment exp, double discount) {
            super(exp, discount);
        }

        public AuthBias(Experiment exp, double discount, double initVal) {
            super(exp, discount, initVal);
        }

        @Override
        public void update(CEGame g) {
            if (g instanceof GroupGame) {
                return; // not implemented since not needed yet
            }

            Game game = (Game) g;
//            If the agents agree, they both get 0.5
//            If the agents disagree, the winner gets 1 and the loser gets 0
            int id1 = game.getFirstAgent().getId();
            int id2 = game.getSecondAgent().getId();
            double val1, val2;

            if(game.isSuccess()) {
                val1 = 0.5;
            } else {
                val1 = (game.getWinner().getId() == id1) ? 1 : 0;
            }
            val2 = 1 - val1;
            updateValue(id1, val1);
            updateValue(id2, val2);
        }
    }

//    Novelty is not discounted (it doesn't care about past interactions)
    public static class NovBias implements TransmissionBias {
        protected Experiment exp;
        protected Map<Integer, Double> idToValue = new HashMap<>();
        protected double initVal = 0;

        public NovBias(Experiment exp) {
            this.exp = exp;
//            initVal = 1./exp.getNumberOfClasses(); // default
            for(Agent agent: exp.getAgents()) {
                idToValue.put(agent.getId(), initVal);
            }
        }

        @Override
        public void update(CEGame g) {
            /*
            Value is the % of the reclassified instances if agent loses

            Explanation for agent1's value (same logic applies for agent2):
                cond1 = agent1's classification of the current instance
                cond2 = agent2's classification of the current instance
                value1 = (# of instances in env classified with cond1 AND cond2) /
                        (# of instances in env classified with cond1)
             */

            if (g instanceof GroupGame) {
                return; // not implemented since not needed yet
            }

//            get current agents & instance
            Game game = (Game) g;
            Agent ag1 = (Agent) game.getFirstAgent();
            Agent ag2 = (Agent) game.getSecondAgent();
            BitSet currInstance = game.getInstance();

//            if interaction is successful -> no one adapts -> novelty = 0
            if(game.isSuccess()) {
                idToValue.put(ag1.getId(), 0.0);
                idToValue.put(ag2.getId(), 0.0);
                return;
            }
//            otherwise (agents disagree):
            try {
//                get agents' classification of the current instance
                Condition cond1 = ag1.getClassifier().getCondition(currInstance);
                Condition cond2 = ag2.getClassifier().getCondition(currInstance);

//                get all instances in the environment
                List<BitSet> inputs = exp.getEnv().getInputs();
//                initialize counters
                int cnt1 = 0, cnt2 = 0, reclassCnt = 0;

//                compute the novelty index
                for(BitSet instance: inputs) {
//                    Decision d1 = ag1.getClassifier().getClass(instance);
//                    Decision d2 = ag2.getClassifier().getClass(instance);

                    boolean eval1 = cond1.evaluate(instance);
                    boolean eval2 = cond2.evaluate(instance);

                    if(eval1) { cnt1++; }
                    if(eval2) { cnt2++; }
                    if(eval1 && eval2) { reclassCnt++; }
                }

//                update values
                idToValue.put(ag1.getId(), 1.0*reclassCnt/cnt1);
                idToValue.put(ag2.getId(), 1.0*reclassCnt/cnt2);
            } catch (NoClassException e) {
                e.printStackTrace();
            }
        }

        @Override
        public double getValue(Agent agent, Game game) {
            return idToValue.get(agent.getId());
        }
    }

    public static class AggregatedBias implements TransmissionBias {

        Experiment exp;
        List<TransmissionBias> transmissionBiases;
        List<Double> coeffs;
        protected double sum;

        public AggregatedBias(Experiment exp, List<TransmissionBias> transmissionBiases) {
            this.exp = exp;
            this.transmissionBiases = transmissionBiases;
            coeffs = new ArrayList<>();
            for(TransmissionBias tb: transmissionBiases)
                coeffs.add(1.);
            sum = coeffs.size();
        }

        public AggregatedBias(Experiment exp, List<TransmissionBias> transmissionBiases, List<Double> coeffs) {
            this.exp = exp;
            this.transmissionBiases = transmissionBiases;
            this.coeffs = coeffs;
            for(double coeff: coeffs)
                sum += Math.abs(coeff);
        }



        @Override
        public void update(CEGame game) {
            for(TransmissionBias tb: transmissionBiases) {
//                don't update Novelty and Env (success) (they get updated before the game is played)
                if(!(tb instanceof NovBias) && !(tb instanceof EnvBias)) {
                    tb.update(game);
                }
            }
        }

        @Override
        public double getValue(Agent agent, Game game) {
            double value = 0;
            for (int i = 0; i < transmissionBiases.size(); i++) {
                value += transmissionBiases.get(i).getValue(agent, game)*coeffs.get(i);
            }
            return value/sum;
        }

        public List<TransmissionBias> getTransmissionBiases() {
            return transmissionBiases;
        }
    }


}
