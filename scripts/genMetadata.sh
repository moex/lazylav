#!/bin/bash
# generate metadata.json from params.sh
# It may be passed the directory as argument or launched from the directory.
# The switch --dataset is used for publishing a dataset instead of an experiment

DS=0
DIR=""

# Analyse arguments
for var in "$@"
do
    if [[ "${var}" == "--dataset" ]]
    then
	DS=1
    else
	DIR="${var}/"
    fi
done

#echo Analysing params.sh

. "${DIR}params.sh"

DATE="${LABEL:0:4}-${LABEL:4:2}-${LABEL:6:2}"

CREATOR="Euzenat, Jérôme"
AFFILIATION="INRIA"

CREATORS="[
";

#     && ! "${PERFORMER}" =~ "${CREATORS}"
     
if [ -n "${PERFORMER}" ]
then
    all=("$PERFORMER" "$DESIGNER" "$EXPERIMENTER" "$ANALYST")
    for NAME in "${all[@]}"
    do 
        # "Firstname Lastname (Affiliation)"
        FIRST="${NAME%% *}"
        REST="${NAME#* }"
        LAST="${REST%% *}"
        REST="${REST#*\(}"
        #AFFILIATION="${REST%%\)*}" # May be empty
        CREATOR="{
        \"name\": \"${LAST}, ${FIRST}\""
        if [ -n "${AFFILIATION}" ]
        then
            CREATOR="${CREATOR},
            \"affiliation\": \"${AFFILIATION}\""
        fi
        CREATOR="${CREATOR}
            }"
        if [[ "$CREATORS" != *"$CREATOR"* ]]
        then
        CREATORS="${CREATORS}
            ${CREATOR},"
        fi
    done
fi
CREATORS=${CREATORS::-1}
CREATORS="${CREATORS}
   ]";


if [[ "${DS}" != "1" ]]
then
DESC="This archive contains the results of a multi-agent simulation experiment [1] carried out with Lazy lavender [2] environment.

Experiment Label: ${LABEL}

Experiment design: ${EXPE}

Experiment setting: ${SETTING}

Hypotheses: ${HYPOTHESIS}

Detailed information can be found in index.html or notebook.ipynb.

[1] https://sake.re/${LABEL}
[2] https://gitlab.inria.fr/moex/lazylav/
"
   else
DESC="This archive contains a data set for being used as input of experiment using the Lazy lavender [1] environment.

It has been generated in experiment ${LABEL} [2] and used in further experiments.

Although, Lazy lavender is made for generating full random data sets, using a standardised test set is useful for non regression and precise comparison purposes.

Reproducing an existing experiments using this input is usually achieved by uncompressing this archive into the input directory:
$ mkdir input
$ unzip ${LABEL}.zip input/

[1] https://gitlab.inria.fr/moex/lazylav/
[2] https://sake.re/${LABEL}

This data set is alternatively published at https://files.inria.fr/sakere/input/${LABEL}.zip
"
fi

#echo Creating metadata

METADATA="{\"metadata\":
  {\"title\": \"${LABEL}: ${EXPE}\",
   \"description\" : \"$(echo "$DESC"| sed 's/$/<br \/>/g' | sed 's;\(http[s]*:[^ <>]*\);<a href=\\\"\1\\\">\1</a>;g' )\",
    \"keywords\": [
        \"Multi-agent Simulation\",
        \"Cultural Evolution\"
    ],
   \"language\": \"eng\",
   \"license\": \"cc-by-4.0\",
   \"access_right\": \"open\",
   \"upload_type\": \"dataset\",
   \"related_identifiers\": [
         {
        \"identifier\": \"https://sake.re/${LABEL}\",
        \"relation\": \"isAlternateIdentifier\"
         }
   ],
   \"publication_date\": \"${DATE}\",
   \"creators\": ${CREATORS}
  }
}
"

#generating files

echo "${DESC}" > ${DIR}README.TXT
echo "${METADATA}" > ${DIR}metadata.json
