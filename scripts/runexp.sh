#!/bin/bash
# ssh <server> 'cd /scratch/LazyLav/lazylav; bash scripts/runexp.sh -d <dir> <command> &'
# ssh seals-4.inrialpes.fr 'cd /scratch/LazyLav/lazylav; bash scripts/runexp.sh -d newdit2 ls -al &'
# The really quiet way on server is:
# nohup bash scripts/runexp.sh -d <dir> <command> < /dev/null 2> /dev/null > /dev/null &

dir=llout

# get flags
while getopts "p:d:h" option
do
    case "${option}" in
	p) prefix=${OPTARG}/;;
        d) filename=${OPTARG};;
	h) echo "usage: bash runexp.sh [OPT] COMMAND"
	   echo "OPT:"
	   echo "   -p DIRPREFIX: the prefix of the directory where results are stored"
	   echo "   -d FILENAME: the directory where to store output"
	   echo "   -h: this output"
	   echo "COMMAND: experiment to execute"
	    exit;;
    esac
done

shift $((OPTIND-1))

[ "$1" = "--" ] && shift

command=$@

DIR=${prefix}${filename}
mkdir -p ${DIR}
LOGFILE=${DIR}/${filename}.txt

echo `date` > ${LOGFILE}
echo >> ${LOGFILE}
echo `uname -a` >> ${LOGFILE}
grep "model name" /proc/cpuinfo >> ${LOGFILE}
grep "MemTotal" /proc/meminfo >> ${LOGFILE}
# hwinfo --short --cpu > ${LOGFILE}

nbcores=`grep "model name" /proc/cpuinfo | wc -l`
cpu=`grep "model name" /proc/cpuinfo | uniq | sed "s/model name[^:]*: //"`
mem=`grep "MemTotal" /proc/meminfo | sed "s/MemTotal:       //" | sed "s/...... kB/GB/"`

echo "CC: ${nbcores} * ${cpu} with ${mem} RAM" >> ${LOGFILE}

osvers=`uname -a | sed "s/^\([^ ]*\) [^ ]* \([^ ]*\) .*/\1 \2/"`

echo "OS: ${osvers}" >> ${LOGFILE}

echo >> ${LOGFILE}
java -version 2>> ${LOGFILE}
java -XshowSettings 2>&1 >/dev/null | grep "Heap Size"  >> ${LOGFILE}

heapsize=`java -XshowSettings 2>&1 >/dev/null | grep "Heap Size" | sed "s/[^:]*: //"`
javaprod=`java -version 2>&1 >/dev/null | head -2 |tail -1 | sed 's/ (buil.*//'`
javavers=`java -version 2>&1 >/dev/null | head -1 | sed 's/[^"]*"\([^"]*\)"/\1/'`

echo "JJ: ${javaprod} ${javavers} with ${heapsize} max heap size" >> ${LOGFILE}

if [ -z "${LLHASH}" ]
then
    git pull >> ${LOGFILE}
else
    git checkout ${LLHASH} >> ${LOGFILE}
fi

echo >> ${LOGFILE}
git rev-parse --verify HEAD >> ${LOGFILE}

ant compileall >> ${LOGFILE}

echo >> ${LOGFILE}
echo $command >> ${LOGFILE}

echo >> ${LOGFILE}
nohup $command -o ${DIR}/${filename}.tsv >> ${LOGFILE} 2>&1

echo >> ${LOGFILE}
echo `date` >> ${LOGFILE}

if [ -f nohup.out ]; then mv nohup.out ${DIR}/output.txt; fi
#CDIR=$(pwd)
#cd ${DIR}; zip -q -r ${DIR}.zip ./*; cd ${CDIR}
#rm -rf ${DIR}

#echo "Finished job: " $command | at -m NOW

# notif done

