#!/bin/bash
# This file is part of Lazy lavender (https://gitlab.inria.fr/moex/lazylav/)
# It is used to switch between process and analyse in docker containers.

if [[ "$1" = 'process' || "${1}" = 'full' ]]; then
    echo "Processing experiment (${1}) for ${LABEL}"
    bash ./script.sh
fi

if [[ "$1" = 'analyse' || "$1" = 'analyze' || "${1}" = 'full' ]]; then
    echo "Preparing analysis (${1}) for ${LABEL}"
    # Check that results are here...
    if [ -d "results/${LABEL}" ]
    then
      jupyter trust notebook.ipynb
      jupyter notebook --port=7777 --no-browser --ip=0.0.0.0 --allow-root
    else
      echo "Results must be available under results/${LABEL}"
    fi
fi
