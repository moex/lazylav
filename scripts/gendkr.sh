#!/bin/bash
# Usage: bash gendkr.sh params.sh script.sh

# Load parameters
. $1

# Convert params and scripts to Dokerfile format
paramsToEnv=$(cat $1 | sed -e '/^[[:space:]]*#/d' -e '/^[[:space:]]*$/d' |  sed 's/^/ENV /g')

script=$(cat $2 | sed -e '/^[[:space:]]*#/d' -e '/^[[:space:]]*$/d' \
                      | sed -e "/^.*$1.*\$/d" \
                      | sed 's/"/\\"/g ;
                             s/$/ \; \\/g ;
                             s/do[[:space:]]*\;/do/' \
                      | sed 's/\$ //g ; 1{s/^[[:space:]]*/"/} ; s/^[ \t]*// ; s/^/            /g')

copy=$(for i in ${TOCOPY}; do echo "COPY ./$i ./$i"; done)

# Generates Dockerfile
echo -e 'ARG version="'"${OSVERS}"'"
FROM lazylav:${version}
ARG version="'"${OSVERS}"'"

'"${paramsToEnv}"'

ENV OUTPUT=/workdir/'"${OUTPUT}"'

RUN if ! [ "$version" = "'"${OSVERS}"'" ]; then \
        git --git-dir="lazylav/.git" pull; \
    else \
        git --git-dir="lazylav/.git" checkout ${LLHASH} \
        && wget -q https://repo1.maven.org/maven2/ch/qos/logback/logback-classic/${LOGBACK_VERSION}/logback-classic-${LOGBACK_VERSION}.jar -P lazylav/lib/slf4j/ \
        && wget -q https://repo1.maven.org/maven2/ch/qos/logback/logback-core/${LOGBACK_VERSION}/logback-core-${LOGBACK_VERSION}.jar -P lazylav/lib/slf4j/ ; \
    fi

RUN cd lazylav && ant compileall && cd ..

# ----- ideally this should be system
# ideally would use requirements.txt
RUN jupyter contrib nbextension install --sys-prefix

RUN jupyter nbextensions_configurator enable --sys-prefix

RUN jupyter nbextension enable python-markdown/main
RUN jupyter nbextension enable nbextensions_configurator/config_menu/main
RUN jupyter nbextension enable hide_input_all/main
RUN jupyter nbextension enable collapsible_headings/main
RUN jupyter nbextension enable livemdpreview/livemdpreview

# Not necessary
# EXPOSE 7777

# ----- ideally all this should be user...

'"${copy}"'

ENTRYPOINT ["./lazylav/scripts/startcont.sh"]

CMD ["full"]' > ${LABEL}.dkr

