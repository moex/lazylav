#!/bin/bash
# ./genmd.sh
# Generates a MarkDown file from experiment results
# Ready to fill the analysis

. params.sh

echo "[[Image:icon8-docker-50.png|right|50px|link=Media:"${OSVERS}".dkr.txt]] [[Image:icon8-blue-docker-50.png|right|50px|link=Media:"${LABEL}".dkr.txt]]"
# Should be the date of the experiment
echo "'''Date''': "`date -Idate --date=${DATE}`
echo
echo "${EXPE} (${NBAGENTS} agents; ${NBRUNS} runs; ${NBITERATIONS} games; "`echo ${OPS} | sed "s: :/:g"`")"
echo
echo "'''Hash''': "${LLHASH}
echo
# That would be good to also have: input data
echo "'''Hypotheses''':"
echo ${HYPOTHESIS}
echo
echo "'''Experimental setting''': "
echo ${SETTING}
echo
echo "'''Command line''':"
echo "<pre  style=\"overflow: auto;\">"
cat script.sh  | sed -e '/^[[:space:]]*#/d'
echo "</pre>"
echo
echo "'''Class used''': "
echo ${CLASSES}
echo
echo "'''Results''':"
echo
for filename in `ls *.png`
do
    echo "[[File:"${filename}"|center|800px]]"
    echo
done
echo "{| style=\"vertical-align:top; width: 100%; border-collapse: collapse; border-spacing: 0;\""
echo "! style=\"border: 1px solid #CCC;\" | test"
echo "! style=\"border: 1px solid #CCC;\" | success<br />rate"
echo "! style=\"border: 1px solid #CCC;\" | network<br />size"
echo "! style=\"border: 1px solid #CCC;\" | incoherence<br />degree"
echo "! style=\"border: 1px solid #CCC;\" | semantic<br />precision"
echo "! style=\"border: 1px solid #CCC;\" | semantic<br />F-measure"
echo "! style=\"border: 1px solid #CCC;\" | semantic<br />recall"
echo "! style=\"border: 1px solid #CCC;\" | maximum<br />convergence"
for filename in `ls *.txt|sed "s:.txt::"`
do
        inc=`grep Final ${filename}.txt | sed "s:^.*incoherence = \(.*\)$:\1:" | awk '{ total += $1 } END {printf "%.2f\n", total/NR }'`
	conv=`cat ${filename}.tsv | awk '!seen[$9]++' | tail -1 | cut -d' ' -f1`
	tail -1 str-${filename}.tsv | cut -d' ' -f3,6,9,12 | awk -v filename=${filename} -v inc=$inc -v conv=$conv '{printf("|-\n| style=\"border: 1px solid #CCC; font-weight: bold;\" | %s\n| style=\"border: 1px solid #CCC;\" | %.2f\n| style=\"border: 1px solid #CCC;\" | %d\n| style=\"border: 1px solid #CCC;\" | %.2f\n| style=\"border: 1px solid #CCC;\" | %.2f\n| style=\"border: 1px solid #CCC;\" | %.2f\n| style=\"border: 1px solid #CCC;\" | %.2f\n| style=\"border: 1px solid #CCC;\" | %d\n", filename, $1, $2, inc, $3, 2*$3*$4/($3+$4), $4, conv)}'
done
echo "|}"
echo
echo "'''Observations''':"
echo "* "
echo
echo "'''Analysis''':"
echo "* "
echo 
echo "'''Conclusion''':"
echo "* "
echo
echo "'''Further experiments''':"
echo "* "
echo
echo "'''Full log''': [[file:${NAME}.zip]]"
echo
echo "'''Execution environment''':"
grep "CC:" *.txt | sed "s/^[^:]*:CC: //" | sort | uniq
#echo "/ Linux ProxMox 2 "
echo "/ " `grep "OS:" *.txt | sed "s/^[^:]*:OS: //" | sort | uniq`
echo "/ " `grep "JJ:" *.txt | sed "s/^[^:]*:JJ: //" | sort | uniq`
echo
echo "'''Designer''': "${DESIGNER}" ("`date -Idate -d${DESIGNDATE}`")"
echo
echo "'''Experimenter''': "${EXPERIMENTER}" ("`date -Idate -d${DATE}`")"
echo
echo "'''Analyst''': "${ANALYST}" ("`date -Idate`")"
echo
echo "Back to [[Experiments]]."
echo

