#!/bin/bash

# or take it from params.sh?
LABEL="$1"

if [[ $# > 1 ]]
then
	FILE="$2"
else
	FILE="notebook.ipynb"
fi

if [[ -f "index.html" ]]
then
    echo "index.html exists, aborting"
    exit
else
    echo "Generating index.html for ${LABEL} from ${FILE}"
fi

touch "index.html"
ed "index.html" <<EOF
a
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="" xml:lang="">
<head>
  <meta charset="utf-8" />
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta name="generator" content="nbconvert (sakere genHTML)" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
  <title>${LABEL}</title>
  <link rel="stylesheet" href="https://sake.re/styles/expstyle.css" />
  <style type="text/css">
      code{white-space: pre-wrap;}
      span.smallcaps{font-variant: small-caps;}
      span.underline{text-decoration: underline;}
      div.column{display: inline-block; vertical-align: top; width: 50%;}
      hr { border-bottom: 1px solid black; }
  </style>
  <script type="text/x-mathjax-config">
    MathJax.Hub.Config({
      messageStyle: "none",
      jax: [ "input/TeX", "output/HTML-CSS" ],
      extensions: [ "tex2jax.js", "jsMath2jax.js" ],
      tex2jax: { inlineMath: [ ['$','$'], ["\\\\(","\\\\)"] ],
		 displayMath: [ ['\$\$','\$\$'], ['\\\\[','\\\\]'] ],
                 preview: "none",
                 processEnvironments: false },
      TeX: { extensions: ["AMSmath.js", "AMSsymbols.js"],
             equationNumbers: { autoNumber: "AMS" },
          }
        });
  </script>
  <script type="text/javascript" src="https://cdn.mathjax.org/mathjax/latest/MathJax.js"></script>
</head>
<body>

.
w
q
EOF

jupyter nbconvert "${FILE}" --to html --template basic --ExecutePreprocessor.timeout=-1 --TemplateExporter.exclude_input=True --output /tmp/"${LABEL}.html"

cat /tmp/"${LABEL}.html" >> "index.html"

ed "index.html" <<EOF
$
a

</body>
</html>
.
w
q
EOF
