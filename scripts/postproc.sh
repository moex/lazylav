#!/bin/bash
# ./postproc.sh DIR
# DIR is the directory where the row result zip files are
# Could use params.sh as parameter (and script.sh as well)

. params.sh

CWD=.

if [ $# -eq 0 ]; then
    echo "Expand files in current directory"
    DIR=.
else
    DIR=$1
fi
SCRIPTDIR=`dirname $0`

cd ${DIR}
# - unzip results
for i in `ls *.zip`; do unzip -j $i; rm $i; done

# - prepare results for exploiting (txt to be able to redo it)
for filename in `ls *.txt|sed "s:.txt::"`
do
	sed -n '2p;1~100p' $filename.tsv > str-$filename.tsv
done
cd ${CWD}

# - generate figures
if [[ -f plot-${LABEL}.tex ]]
then
    pdflatex --shell-escape plot-${LABEL}.tex
fi

# - generate Dockerfile
if [[ ! -f ${LABEL}.dkr ]]
then
    ${SCRIPTDIR}/gendkr.sh params.sh script.sh
fi

# Generate markdown
${SCRIPTDIR}/genreport.sh params.sh script.sh > ${LABEL}.md

# Of course, should be done last
cd ..; zip -q -r ${NAME}.zip ${NAME}; cd ${NAME}
