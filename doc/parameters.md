
# _Lazy lavender_ list of parameters

Parameters are usually passed to classes through a `init( Properties p )`.

They are passed in command line through `-Dparamname=paramvalue`

Parameters are listed below per classes. The first value is usually the default, please do not put a parameter for running the default:

## engine

### Monitor

<dt>experiment = NOORevision|OntologyEvolution|classname
<dd>The type of experiment to perform as implemented in an Experiment class.
This may be a fully qualified Java class name.
</dd>

<dt>nbRuns = int (5)
<dd>Number of experiments to run
</dd>

### ExperimentalPlan
Runs Monitor as many times as there are variations of parameters.

A parameter can be varied by separating its values with comma (,).
Example: `-DnbAgents=4,8` runs monitor with 4 and 8 agents combined with all other parameter variations.

<dt>saveXPlanDir = String(null)</dt>
<dd>If specified, save experimental plan in that directory.</dd>
saved as:

~~~~
    <saveXPlanDir>
    |-- monitorRun1
    |   |-- 0 (save experiment run inside)
    |   |-- 1
    |   |-- ...
    |   |-- ...
    |   `-- nbRuns
    |-- monitorRun2
    |-- ...
    |-- ...
    |-- monitorRunK (K number of combinations)
    `-- params.xml (parameters of experimental plan)
~~~~

<dt>loadXPlanDir = String(null)</dt>
<dd>If specified, rerun experimental plan saved in that directory.</dd>

<dt>organiser = console|godecisions|classname</dt>
<dd>Results organiser: it controls how results are organised in files.
Can be a fully qualified Java class name that extends engine.ExperimentalPlan.PlanResultsOrganiser.
</dd>

### ExperimentalPlan.PlanResultsOrganiser

<dt>resultDir = String("planResults")</dt>
<dd>Directory where results are stored</dd>

<dt>fnamingPattern = String(null)</dt>
<dd>Specifies how to name directories of each parameter combination.
@(parameterName)@ To use value of a parameter.
default, use all varied parameters seperated by "-".</dd>

Example:`-DnbAgents=4,8 -DnbIterations=10,100 -DfnamingPattern=nba@nbAgents@-nbi@nbIterations@`  
would create directories:
nba4nbi10, nba8nbi10, nba4nbi100 and nba8nbi100 where results are stored.

### _Obsoleted_ test.TestOntologyEvolution

    TestOntologyEvolution -DontologyFilesInputLocation=dir
is to be replaced by:

    Monitor -Dexperiment=fr.inria.exmo.lazylav.expe.OntologyEvolutionExperiment -DloadDir=dir -DloadAgents
With the agent data as specified above

## expe

<dt>nbAgents = int (3)
<dd>Number of agents

<dt>nbIterations = int (200)
<dd>Number of games to be played by the agents

<dt>verboseExpeLoggers = boolean (false)
<dd>If loggers describe extensively actions undertaken

The following primitives are used for saving and reloading experiments.

<dt> saveDir / loadDir = directory
<dd> Directory where the data concerning the experiments will be stored / loaded

<dt>saveParams / loadParams = boolean (false)
<dd>Save / load the parameters from saveDir / loadDir (overridden by local parameters)

<dt>saveInit = boolean (false)
<dd>Save initial situation (agent+environment state) in saveDir if specified

<dt>loadAgents = boolean (false)
<dd>Load the initial agents state from loadDir

<dt>loadEnv = boolean (false)
<dd>Load the initial environment state from loadDir

<dt>saveRef = boolean (false)
<dd>Save reference, i.e., best, situation (agent+environment state) in saveDir if specified

<dt>saveGames / replayGames = boolean (false)
<dd>Save / replay the sequence of games played in saveDir / loadDir if specified

<dt>saveEachRun = boolean (false)
<dd>Save the results of each runs independently in saveDir if specified

<dt>saveFinal = boolean (false)
<dd>Save final situation (agent+environment state) in saveDir if specified

The way information is saved depends on the experiment.
Below is the layout for AlignmentRevisionExperiment.
This can also be used to understand globally what is in the agents.

~~~~
    <dir>
    |-- run
    |   |-- reference
    |   |   |-- agent
    |   |   |   `-- private knowledge (ontology)
    |   |   `-- environment
    |   |       `-- public knowledge (alignments)
    |   |-- init
    |   |   |-- agent
    |   |   |   `-- private knowledge (ontology)
    |   |   `-- environment
    |   |       `-- public knowledge (alignments)
    |   |-- games.tsv
    |   `-- final
    |       |-- agent
    |       |   `-- private knowledge (ontology)
    |       `-- environment
    |           `-- public knowledge (alignments)
    `-- params.xml
~~~~


### PopulatedARExperiment

<dt>nbPopulation = int (4)
<dd>the number of population to be interacting. Each populations will contain nbAgents agents

<dt>synchronisationRate = int (0)
<dd>the number of games after which the agents of the same population synchronise

<dt>generationRate = int (0)
<dd>the number of games after which a new generation of agents take the place of the previous one
</dd>

### AlignmentRevisionExperiment

<dt>repairers = {logmap|alcomo|class}+ (comma separated list of repairers)
<dd>repair the network of ontologies before playing games for comparison purposes (default: inactive)

<dt>rawRepair = logmap|alcomo|class
<dd>repetitively repair the actually used network of ontologies with repairer(default: inactive)

<dt>repeatRepairs = 100|int (0)
<dd>number of iterations at which repair is repeated (default: inactive)

<dt>tRevision = 100|int (0)
<dd>repetitively apply theory revision, set the size of batch examples (default: inactive)
This will only work if a native Prolog library is available on the machine
</dd>

### OntologyEvolutionExperiment

<dt>features = [ { name&#58; featurename, values&#58; [ value* ] }* ] (JSON feature description)
<dd> features on which ontologies are defined
Example:

~~~
"[{\"name\":\"color\",\"values\":[\"black\",\"white\"]},{\"name\":\"shape\",\"values\":[\"triangle\",\"square\"]},{\"name\":\"size\",\"values\":[\"s\",\"m\",\"l\"]},{\"name\":\"letter\",\"values\":[\"a\",\"b\",\"c\"]}]"
~~~

## env

### NOOEnvironment

<dt>nbOntologies = int (nbAgents)
<dd>Number of ontology in the network of alignment

<dt>nbFeatures = int (nbAgents)
<dd>Number of features for the objects in the environment

<dt>disjointRatio = 0..100 (100)
<dd>Percentage of disjointness statement between classes in each ontology

<dt>nbCorresp = int (default: 2**nbAgents-1)
<dd>Number of correspondances to be included in initial random alignments

<dt>realistic = boolean (false)
<dd>Generate an inital network of ontologies with the exact number of (>) correspondences. When set to true, nbCorresp is ignored

<dt>noo = circperm|random
<dd>use a circular permutation of features as ontologies or random ordering of features (default: circperm)

<dt>startempty = boolean (false)
<dd>Starts with an 'empty' network of ontologies (default: false).
<dd>If set to true, nbCorresp and realistic are ignored.

<dt>computeLogMapScore = boolean (false)
<dd>Run LogMap alignment repair on the network

<dt>computeAlcomoScore = boolean (false)
<dd>Run Alcomo alignment repair on the network

<dt>outputNetworks = boolean (false)
<dd>Prints the ontology networks in a compact form (for OWL/RDF use Monitor/saveInit,saveRef,saveFinal)

<dt>reportPrecRec = boolean (false)
<dd>Reports precision and recall instead of the standard F-measure

<dt>syntactic = boolean (false)
<dd>Type of network quality measurement (syntactic|semantic|weighted|... to be improved)
</dd>

### feature.FeatureEnvironment

## pop

### Population

<dt>synchronisationMode = majority|general|specific
<dd>Mode used for synchonising the knowledge of a population
</dd>


## agent

### AlignmentAdjustmentAgent

<dt>revisionModality = delete|replace|add|addjoin|refine|refadd|nothing
<dd>Adaptation operator to recover from communication failure

<dt>expandAlignments = none|random|protected|clever
<dd>Alignments are expanded when adaptation delete a correspondence

* NONE: no expansion is performed
* RANDOM: a fully random correspondence is added to replace a deleted one
* PROTECTED: it is checked that the correspondence is not the deleted correspondence that has just been proved incorrect
* CLEVER: each removed correspondence is cached so that the protection may be applied to all these discarded correspondences
</dd>

<dt>nonRedundancy = boolean (false)
<dd>Only add (add, addjoin, expand) correspondences which are not entailed by the current alignment

<dt>immediateRatio = 0..100 (100)
<dd>Probability of returning the most specific known answer

<dt>complete = boolean (false)
<dd>When checking for correspondence entailment use the ontologies of both agents (not advised)

<dt>generative = boolean (false)
<dd>When no correspondence applies to the current situation (game), generate a new one which applies to the case (default: false).

<dt>expansionpolicy = general|unique
<dd>Type of expansion performed (preserve one-to-one alignment?)

<dt>strengthen = none|mostspecific|mostgeneral
<dd>When interaction succeed, explore stronger (more specific on the target) correspondences for correctness

<dt>reportIncoherence = boolean (false)
<dd>Reports the incoherence degree (average). This requires long computation times and memory. Use -Xms500M -Xmx1G

<dt>reportPrecRec = boolean (false)
<dd>Reports precision and recall instead of the standard F-measure
</dd>

### PopulationAlignmentAdjustingAgent
<dt>integrationMode = consprior|consonly|locprior|loconly|random|proba|aligndist|meminter
<dd>Mode used for agents to use the population knowledge produced by a syncronisation mode.

<dt>convertProbability = 0..1 (0)
<dd>Probability of agents reject population consensus in proba integration mode

<dt>maxAlignmentDistance = 0..1 (0)
<dd>Maximum alignemend distance an agents' aligenemnt and the population consensus in a population, used only in aligndist integration mode



### OntologyEvolvingAgent


## godecisions (evolving ontologies with a game of decisions)

### decisiontaking.Experiment implements LLExperiment

<dt>numberOfFeatures = int(5)</dt>
<dd>the number of features of environment objects</dd>

<dt>numberOfClasses = int(2)</dt>
<dd>the number of possible decision classes</dd>

<dt>assessment = 1</dt>
<dd>Agent assessment method can be:

* 1: assessment from the environment.
* 2: assessment from society.
* 3: assessment from both environment and society.
</dd>

<dt>assessOnce = boolean(false)</dt>
<dd>If true, assess agents once at the start of the experiment</dd>

<dt>ratio = float(0.3)</dt>
<dd>The training ratio is the ratio of objects that each agent receive for initial training</dd>

<dt>splitTrain = boolean(false)</dt>
<dd>If true, ignore training ratio and split environment object between agents equally</dd>

<dt>sampleRatio = float(0.5)</dt>
<dd>The ratio of objects that agents receive when doing a task</dd>

run save:
~~~~
    <dir>
    |-- run
    |   |-- samples
    |   |   |-- sampleInstance0
    |   |   |-- sampleTars0
    |   |   |-- ...
    |   |   |-- ...
    |   |   |-- sampleInstance$(nbIterations-1)
    |   |   `-- sampleTars$(nbIterations-1)
    |   |-- training
    |   |   |-- instances0
    |   |   |-- targets0
    |   |   |-- ...
    |   |   |-- ...
    |   |   |-- instances$(nbAgents-1)
    |   |   `-- targets$(nbAgents-1)
    |   |-- games.tsv
    |   `-- env.tsv

~~~~


### decisiontaking.Environment implements LLEnvironment

<dt>loadEnvDir = String(null)</dt>
<dd>If specified, loads the environment from directory</dd>

<dt>targetClass = int(-1)</dt>
<dd>If specified, use objects of that class as positive objects and the rest as negative objects.
Specifying targetClass implies environment will contain only 2 decision classes.
</dd>

### decisiontaking.CrossValidationExperiment extends decisiontaking.Experiment

The same as `decisiontaking.Experiment` except it also records precision, recall and accuracy on the test set.

<dt>testSetProp = float(0.1)</dt>
<dd>The proportion of objects used as testing set</dd>

Objects are split in `n=int(1/testSetProp)` groups labeled from 0 to n.

<dt>testSet = int(0)</dt>
<dd>Specifies which group is used as testSet.</dd>

To run for each test group, ExperimentalPlan can be used by passing argument `-DtestSet=1,2,...,n`

