# _Lazy lavender_ documentation

_Lazy lavender_ is a simulation tool for cultural knowledge evolution, i.e., running randomised experiments with agent adjusting their knowledge while attempting to communicate.

Here is some documentation for using _Lazy lavender_:

[List of parameters](parameters.md) which may be used (and standardised) for tuning the experiments.

Most of the [documentation](https://sake.re/docs/index.html) to properly design, perform and publish experiments are now available from the [Sake.re](https://sake.re) platform.

<!--
[Guidelines to design](design-exp.md) and [Guidelines to analyse experiments](analyze-exp.md).

Experiments are now provided with Docker files.
Hence we have [instructions to replay experiments with Docker](replay-docker.md).

There are additional [advices](reproduce.md) to reproduce or document experiments.

The intended [architecture](architecture.md) of the code is also to be documented.
-->

Here are the [old](https://bibexmo.inrialpes.fr/BibServ/BibrefServlet?format=xhtml&file=bibexmo.xml&abstrip=false&param=softwares&value=lazylav) and [more recent papers](https://bibexmo.inrialpes.fr/BibServ/BibrefServlet?format=xhtml&file=bibmoex.xml&abstrip=false&param=softwares&value=lazylav) in which the results are published.

