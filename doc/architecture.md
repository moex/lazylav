# _Lazy lavender_ code architecture

This page aims at (live-)documented the code architecture of _Lazy lavender_.
Nothing is stable and this can change as we are inventing the architecture as developing experiments.

## Basic process

The basic processing of _Lazy lavender_ is that a Monitor is launched with parameters.
This monitor will, for as many run which are requested, create an Experiment, initialize it and process it.
The Experiment will create an Environment, a ActionLoggers and Agents. 
The Environment will be initialised and, for as many games as requested agents will be asked to play the game.

Let see this in more detail

## Monitor

* `run()`: 
<dd>
read parameters;<br />
for as many runs as requested:<br />
&nbsp;&nbsp;&nbsp;&nbsp;create an Experiment; initialize it; and process it.
</dd>

## Experiment

* `init( Properties )`: 
<dd>considers the parameters applying to the Experiments; 
creates the environment; initialize it and create the network;<br />
creates a Logger; initiaze it<br />
creates the agents or the populations; initialize them;
</dd>

* `process()`: 
<dd>
for as many games as requested
&nbsp;&nbsp;&nbsp;&nbsp;picks up a pair of agents and an object and call `playGames( Agent, Object )`<br />
&nbsp;&nbsp;&nbsp;&nbsp;(interleaved)for as many time as requested: <br />
&nbsp;&nbsp;&nbsp;&nbsp;perform incidental operations and print their results.<br />
</dd>

* `report()`: 
<dd>
ask the Logger for a full report
</dd>

## Environment

* `init()`: 
<dd>
considers the parameters applying to the Environment
</dd>

* `initCompleteDisjointNetwork()`:
<dd>
creates the network of ontologies with the given characteristics
</dd>

## ActionLogger

## Population

* `init( Properties )`: 
<dd>
creates the agents; initialize them;
</dd>


* `synchronizeAlignments()`: // should be renamed synchronize
<dd>
synchronize knowledge among the agents of the population.
</dd>


## Agent

* `init( Properties )`: 
<dd>
considers the parameters applying to Agent; 
</dd>

* `playGame( Agent, Object )`:
<dd>
play the game with the other agent and the object
</dd>

* `repair()`:
<dd>
action performed when the experiment failed
</dd>

